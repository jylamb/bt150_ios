//
//  AppDelegate.swift
//  BT150
//
//  Created by KOBONGHWAN on 03/11/2018.
//  Copyright © 2018 bistos. All rights reserved.
//

import UIKit

struct GLConst {
    static let toOZ  = 0.033814
    static let toML  = 29.57353
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, SRResultDelegate {

    var window: UIWindow?
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
                
        TIOManager.sharedInstance()?.loadPeripherals()
        
        let dbHelper = DBHelper()
        
        dbHelper.createDatabase()

        PBCommon.loadVoiceRecMode()
        
        gPBSR.delegate = self
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        
        print("!!!!!!!!!!!!!!!! applicationWillTerminate !!!!!!!!!!!!!!!!")
        
        gPBSR.stopTranscribing()
        
        if let aPeripheral = PBCommon.gPeripheral {
            if aPeripheral.isConnected {
                if PBCommon.nOperationModeCur == PBCommon.OP_MODE_EXP {
                    PBCommon.saveExpressionTime()
                }
                aPeripheral.cancelConnection()
            }
        }        
    }

    
    //MARK: - SRResultDelegate
    func onSRResult(_ a_Result: String) {
        let aVCmdEnable =  UserDefaults.standard.string(forKey: "vcmd_enable") ?? "disable"
        let aShowResult =  UserDefaults.standard.string(forKey: "vcmd_showresult") ?? "hide"
        print("onSRResult: aVCmdEnable:\(aVCmdEnable) aShowResult:\(aShowResult) a_Result: \(a_Result)")
        
        if aVCmdEnable == "enable" {
            let aCmd = VCmdControl.findVoiceCommand(a_Result)
            let bRet = VCmdControl.handleVoiceCommand(aCmd)

            var aShowText = a_Result;
            if aCmd != VCmdControl.cmdNone {
                aShowText = aCmd.arrCmdPhrase[0]
            }
            if aShowResult == "show" {
                broadcastSRResult(bRet, Data: aShowText)
            }
        }
    }
    
    func broadcastSRResult(_ a_Result : Bool, Data a_Data : String) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MSG_SR_RESULT"),
                                        object: nil,
                                        userInfo: ["Result" : a_Result, "String" : a_Data])
    }

    func getVisibleViewController(_ rootViewController: UIViewController?) -> UIViewController? {
                
        var rootVC = rootViewController
        if rootVC == nil {
            rootVC = UIApplication.shared.keyWindow?.rootViewController
        }
        
        if rootVC?.presentedViewController == nil {
            return rootVC
        }
        
        if let presented = rootVC?.presentedViewController {
            if presented.isKind(of: UINavigationController.self) {
                let navigationController = presented as! UINavigationController
                return navigationController.viewControllers.last!
            }
            
            if presented.isKind(of: UITabBarController.self) {
                let tabBarController = presented as! UITabBarController
                return tabBarController.selectedViewController!
            }
            
            return getVisibleViewController(presented)
        }
        return nil
    }

    
}

