//
//  Menu02ViewController.swift
//  BT150
//
//  Created by KOBONGHWAN on 14/11/2018.
//  Copyright © 2018 bistos. All rights reserved.
//

import UIKit

class Menu02ViewController: UIViewController, PBTabDelegate, UIScrollViewDelegate, Menu02SubDelegate, ProgramEditDelegate, UIAlertViewDelegate {

    let nTabCnt : Int = 8
    
    var vcTopMenu : TopMenuViewController?
//    var nCurSelePage : Int = 0
    var vcProgramEdit = ProgramEditViewController()
    var arrTab = [PBTab]()
    var arrMenu02Sub = [Menu02SubViewController]()
    
    var vcSub01 = Menu02SubViewController()
    var vcSub02 = Menu02SubViewController()
    var vcSub03 = Menu02SubViewController()
    var vcSub04 = Menu02SubViewController()
    var vcSub05 = Menu02SubViewController()
    var vcSub06 = Menu02SubViewController()
    var vcSub07 = Menu02SubViewController()
    var vcSub08 = Menu02SubViewController()
    
    var timerInitActivityView : Timer?
    var nTimerInitActivityViewMax : Int = 30
    var bRecvFirst = false
    var bUseVCmd = false

    @IBOutlet weak var btnSingle: UIButton!
    @IBOutlet weak var btnDouble: UIButton!

    @IBOutlet weak var svSub: UIScrollView!
    @IBOutlet weak var vTab: UIView!
    
    @IBOutlet weak var btnSaveProgram: UIButton!
    @IBOutlet weak var btnDelAll: UIButton!
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "embedMenuSegue" {
            if let vc = segue.destination as? TopMenuViewController {
                vcTopMenu = vc
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let sizeView = self.view.frame.size
        
        makeSubTab()
        
        svSub.delegate = self
        svSub.bounces = false
        svSub.isScrollEnabled = true
        svSub.isPagingEnabled = true
        svSub.isUserInteractionEnabled = true
        svSub.backgroundColor = UIColor.clear
        svSub.contentSize =  CGSize(width: sizeView.width * CGFloat(nTabCnt), height: svSub.frame.size.height-20)
        
        makeSubView()
        
        vcProgramEdit.view.frame = CGRect(x: 0, y: 0, width: sizeView.width, height: sizeView.height)
        vcProgramEdit.delegate = self
        self.view.addSubview(vcProgramEdit.view)
        vcProgramEdit.view.isHidden = true

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
//        PBCommon.sendData(PBCommon.sCmdInqueryStatus)
        
        PBCommon.nActivityNo = PBCommon.ACT_MENU02
        PBCommon.nVoiceMode = PBCommon.VOICE_MODE_PROGRAM
        
        vcTopMenu?.ivOperation.image = UIImage(named: "menu_ic_2.png")
        vcTopMenu?.ivProgram.image = UIImage(named: "menu_ic_4_ov.png")
        vcTopMenu?.ivStatistic.image = UIImage(named: "menu_ic_5.png")

        NotificationCenter.default.addObserver(self, selector: #selector(onRecvData(_:)), name: Notification.Name("MSG_DATA_RECV"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onRecvVCmd(_:)), name: Notification.Name("MSG_VCMD_RECV"), object: nil)

        if !PBCommon.bMenu02VCmdReady {
            PBCommon.bMenu02VCmdReady = true
            bUseVCmd = true
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        bRecvFirst = true
        initSubViewLayout()
        selectTabMenu((PBCommon.nCurSelPrgTabIdx+1) * 100)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func onRecvData(_ a_Noti : NSNotification) {
        if let a_Data = a_Noti.userInfo?["recv_data"] as? String {
            parseRecvData(a_Data)
        }
    }
    
    func parseRecvData(_ a_Recv : String) {
        
        if (a_Recv.count == 16
            && a_Recv.substring(to: 1) == "M"
            && a_Recv.substring(with: 2..<3) == "P"
            && a_Recv.substring(with: 5..<6) == "C"
            && a_Recv.substring(with: 7..<8) == "R") {
            
            if a_Recv.count > 15 && a_Recv.substring(with: 13..<14) == "W" {
                let aOpType = a_Recv.substring(with: 14..<15)
                if aOpType == "0" {
                    PBCommon.nOperationType = PBCommon.OP_TYPE_SINGLE
                } else {
                    PBCommon.nOperationType = PBCommon.OP_TYPE_DOUBLE
                }
            }
            
            DispatchQueue.main.async {
                self.refreshCurrentOpType()
            }
        }
    }

    @objc func onRecvVCmd(_ a_Noti : NSNotification) {
        print("Menu02 onRecvVCmd-1")
        
        var aCmdCode = 0
        var aCmdType = VoiceCommand.CMD_TYPE_NORMAL
        var aStepVal = 0
        
        if let aRet = a_Noti.userInfo?["CmdCode"] as? Int {
            aCmdCode = aRet
        }
        if let aRet = a_Noti.userInfo?["CmdType"] as? Int {
            aCmdType = aRet
        }
        if let aRet = a_Noti.userInfo?["StepVal"] as? Int {
            aStepVal = aRet
        }
        
        print("Menu02 onReceive: aCmdCode:\(aCmdCode) aCmdType:\(aCmdType) aStepVal:\(aStepVal)")
        
        if aCmdType == VoiceCommand.CMD_TYPE_NORMAL {
            switch aCmdCode {
            case VoiceCommand.CMD_PLAY:
                if bUseVCmd {
                    onProgramPlay("")
                }
                break
            case VoiceCommand.CMD_STOP:
                if bUseVCmd {
                    onProgramStop("")
                }
                break
            case VoiceCommand.CMD_SKIP:
                if bUseVCmd {
                    onProgramSkip("")
                }
                break
            case VoiceCommand.CMD_NUMBER_1:
                doProgramIDN(1)
                break
            case VoiceCommand.CMD_NUMBER_2:
                doProgramIDN(2)
                break
            case VoiceCommand.CMD_NUMBER_3:
                doProgramIDN(3)
                break
            case VoiceCommand.CMD_NUMBER_4:
                doProgramIDN(4)
                break
            case VoiceCommand.CMD_NUMBER_5:
                doProgramIDN(5)
                break
            case VoiceCommand.CMD_NUMBER_6:
                doProgramIDN(6)
                break
            case VoiceCommand.CMD_NUMBER_7:
                doProgramIDN(7)
                break
            case VoiceCommand.CMD_NUMBER_8:
                doProgramIDN(8)
                break
            default:
                break
            }
            
        } else if aCmdType == VoiceCommand.CMD_TYPE_STEP {
            switch (aCmdCode) {
            case VoiceCommand.CMD_PROGRAMID_N:
                doProgramIDN(aStepVal)
                break
            default:
                break
            }
        }
        
    }
    
    func changeOperationType(_ a_OpType : Int) {
        var aChangableTabIdx = 4
//        if PBCommon.getLocaleLanguage() == "fr" {
//            aChangableTabIdx = 6
//        }

        if PBCommon.nCurSelPrgTabIdx >= aChangableTabIdx {
            if PBCommon.nCurSelPrgTabIdx == 4 {
                nPrgOpType5 = a_OpType
            } else if PBCommon.nCurSelPrgTabIdx == 5 {
                nPrgOpType6 = a_OpType
            } else if PBCommon.nCurSelPrgTabIdx == 6 {
                nPrgOpType7 = a_OpType
            } else if PBCommon.nCurSelPrgTabIdx == 7 {
                nPrgOpType8 = a_OpType
            }
            selectOperationType(a_OpType)
            PBCommon.sendSavePacket()

            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MSG_PRGLIST_RELOAD"), object: nil, userInfo: nil)
        }
    }
    
    func selectOperationType(_ a_Type : Int) {

        btnSingle.isSelected = false
        btnDouble.isSelected = false
        
        var aChangableTabIdx = 4
//        if PBCommon.getLocaleLanguage() == "fr" {
//            aChangableTabIdx = 6
//        }
        
        if a_Type == PBCommon.OP_TYPE_SINGLE {

            if PBCommon.nCurSelPrgTabIdx >= aChangableTabIdx {
                btnSingle.isSelected = true
                btnSingle.isEnabled = true
            } else {
                btnSingle.isSelected = false
                btnSingle.isEnabled = false
            }
            btnDouble.isSelected = false
    
        } else if a_Type == PBCommon.OP_TYPE_DOUBLE {
            btnSingle.isSelected = false
            if PBCommon.nCurSelPrgTabIdx >= aChangableTabIdx {
                btnDouble.isSelected = true
                btnDouble.isEnabled = true
            } else {
                btnDouble.isSelected = false
                btnDouble.isEnabled = false
            }

        } else {
            btnSingle.isSelected = false
            btnDouble.isSelected = false
            
            btnSingle.isEnabled = false
            btnDouble.isEnabled = false
        }
    }
    
    func getCurListCount() -> Int {
        let aData = arrProgramData[PBCommon.nCurSelPrgTabIdx].programDataList
        var aCnt = 0
        for i in 0 ..< aData.count {
            if aData[i].nMode != PBCommon.OP_MODE_NONE {
                aCnt += 1
            }
        }
        return aCnt
    }

    func initSubViewLayout() {
        for i in 0 ..< 8 {
            arrMenu02Sub[i].setTableViewLayout()
        }
    }
    
    func makeSubView() {
        
        arrMenu02Sub.append(vcSub01)
        arrMenu02Sub.append(vcSub02)
        arrMenu02Sub.append(vcSub03)
        arrMenu02Sub.append(vcSub04)
        arrMenu02Sub.append(vcSub05)
        arrMenu02Sub.append(vcSub06)
        arrMenu02Sub.append(vcSub07)
        arrMenu02Sub.append(vcSub08)
        
        let sizeView = self.view.frame.size
        
        for i in 0 ..< 8 {
            let aVC = arrMenu02Sub[i]
            aVC.view.frame = CGRect(x: sizeView.width * CGFloat(i), y: 0, width: sizeView.width, height: svSub.frame.size.height)
            aVC.delegate = self
            aVC.initMenu02Data((i + 1) * 100)
            svSub.addSubview(aVC.view)
        }
    }

    func makeSubTab() {
        let sizeView = self.view.frame.size
        
        let fTabW = sizeView.width / CGFloat(nTabCnt)
        let fTabH = vTab.frame.size.height
        
        for i in 0 ..< nTabCnt {
            let tabMenu = PBTab(frame: CGRect(x: fTabW * CGFloat(i), y: 0, width: fTabW, height: fTabH))
            tabMenu.delegate = self
            tabMenu.tag = (i + 1) * 100
            tabMenu.initPBTabColor(PBLib.RGB([0xe9, 0xe9, 0xe9]), andSelect: PBLib.RGB([0xe6, 0x62, 0x72]))
            tabMenu.initPBTabTitle(String(format: "%02d", i+1), andFont: PBLib.AppFont(16), andColor: UIColor.black)
            vTab.addSubview(tabMenu)
            arrTab.append(tabMenu)
        }
        
        for i in 0 ..< nTabCnt {
            if PBCommon.nCurSelPrgTabIdx == i {
                selectTabMenu(arrTab[i].tag)
            }
        }
        
        let vLine01 = UIView(frame: CGRect(x: 0, y: vTab.frame.size.height-1, width: vTab.frame.size.width, height: 1))
        vLine01.backgroundColor = PBLib.RGB([0xd8, 0xd8, 0xd8])
        vLine01.tag = 999
        vTab.addSubview(vLine01)
    }
    
    func refreshCurrentOpType() {
        if PBCommon.nCurSelPrgTabIdx == 0 {
            selectOperationType(PBCommon.OP_TYPE_DOUBLE)
            
        } else if PBCommon.nCurSelPrgTabIdx == 1 {
            selectOperationType(PBCommon.OP_TYPE_DOUBLE)
            
        } else if PBCommon.nCurSelPrgTabIdx == 2 {
            selectOperationType(PBCommon.OP_TYPE_SINGLE)
            
        } else if PBCommon.nCurSelPrgTabIdx == 3 {
            selectOperationType(PBCommon.OP_TYPE_SINGLE)
            
        } else if PBCommon.nCurSelPrgTabIdx == 4 {
            if nPrgOpType5 == PBCommon.OP_TYPE_NONE {
                selectOperationType(PBCommon.nOperationType)
            } else {
                selectOperationType(nPrgOpType5);
            }
            
        } else if PBCommon.nCurSelPrgTabIdx == 5 {
            if nPrgOpType6 == PBCommon.OP_TYPE_NONE {
                selectOperationType(PBCommon.nOperationType)
            } else {
                selectOperationType(nPrgOpType6)
            }
            
        } else if PBCommon.nCurSelPrgTabIdx == 6 {
            if nPrgOpType7 == PBCommon.OP_TYPE_NONE {
                selectOperationType(PBCommon.nOperationType)
            } else {
                selectOperationType(nPrgOpType7)
            }
            
        } else if PBCommon.nCurSelPrgTabIdx == 7 {
            if nPrgOpType8 == PBCommon.OP_TYPE_NONE {
                selectOperationType(PBCommon.nOperationType)
            } else {
                selectOperationType(nPrgOpType8)
            }
        }
    }
    
//    func refreshCurrentOpType() {
//        if PBCommon.nCurSelPrgTabIdx == 4 {
//            if nPrgOpType5 == PBCommon.OP_TYPE_NONE {
//                selectOperationType(PBCommon.nOperationType)
//            }
//
//        } else if PBCommon.nCurSelPrgTabIdx == 5 {
//            if nPrgOpType6 == PBCommon.OP_TYPE_NONE {
//                selectOperationType(PBCommon.nOperationType)
//            }
//
//        } else if PBCommon.nCurSelPrgTabIdx == 6 {
//            if nPrgOpType7 == PBCommon.OP_TYPE_NONE {
//                selectOperationType(PBCommon.nOperationType)
//            }
//
//        } else if PBCommon.nCurSelPrgTabIdx == 7 {
//            if nPrgOpType8 == PBCommon.OP_TYPE_NONE {
//                selectOperationType(PBCommon.nOperationType)
//            }
//        }
//    }
    
    func selectTabMenu(_ a_Tag : Int) {
        print("selectTabMenu:\(a_Tag)")
        PBCommon.nCurSelPrgTabIdx = selectTabMenuShow(a_Tag)
        PBCommon.sendData(String(format: "Y%d;", PBCommon.nCurSelPrgTabIdx))
        refreshCurrentOpType()
        moveMainViewScroll(PBCommon.nCurSelPrgTabIdx)
        refreshSaveDeleteAllButton()
        
        if PBCommon.nCurSelPrgTabIdx >= 0 && PBCommon.nCurSelPrgTabIdx < arrMenu02Sub.count {
            arrMenu02Sub[PBCommon.nCurSelPrgTabIdx].tvList.reloadData()
        }            
    }
    
    func refreshSaveDeleteAllButton() {
        var aProgramFixIdx = 4
//        if PBCommon.getLocaleLanguage() == "fr" {
//            aProgramFixIdx = 6
//        }

        let aCnt = getCurListCount()
        if (PBCommon.nCurSelPrgTabIdx < aProgramFixIdx || aCnt == 0) {
            btnSaveProgram.backgroundColor = GLColor.kDefLightGray
            btnDelAll.backgroundColor = GLColor.kDefLightGray
        } else {
            btnSaveProgram.backgroundColor = GLColor.kMainTheme
            btnDelAll.backgroundColor = GLColor.kMainTheme
        }
    }
    
    func selectTabMenuShow(_ a_Tag : Int) -> Int {
        var nPageIdx : Int = 0
        
        for i in 0 ..< nTabCnt {
            let tabMenu : PBTab = arrTab[i]
            if tabMenu.tag == a_Tag {
                tabMenu.select(true)
                nPageIdx = i
                
            } else {
                tabMenu.select(false)
            }
        }
//        print("selectTabMenuShow: nPageIdx: \(nPageIdx)")
//        arrMenu02Sub[nPageIdx].tvList?.reloadData()
        return nPageIdx
    }
    
    func moveMainViewScroll(_ a_Idx : Int) {
        var frame = svSub.frame
        frame.origin.x = frame.size.width * CGFloat(a_Idx)
        frame.origin.y = 0
        svSub.scrollRectToVisible(frame, animated: false)
    }
    
    //MARK: - PBTabDelegate
    func onTabClick(_ a_Idx: Int) {
        print("onTabClick: PBCommon.bProgramRunning: \(PBCommon.bProgramRunning)")
        if(PBCommon.bProgramRunning) {
            selectTabMenu((PBCommon.nCurSelPrgTabIdx+1) * 100)
        } else {
            selectTabMenu(a_Idx)
        }
    }
    
    //MARK: - UIScrollViewDelegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageWidth = svSub.frame.size.width
        let nIdx = Int( floor((svSub.contentOffset.x - pageWidth / CGFloat(svSub.subviews.count)) / pageWidth) + 1 )
        if nIdx != PBCommon.nCurSelPrgTabIdx {
            if(PBCommon.bProgramRunning) {
                print("scrollViewDidScroll PBCommon.bProgramRunning:\(PBCommon.bProgramRunning) PBCommon.nCurSelPrgTabIdx:\(PBCommon.nCurSelPrgTabIdx)")
//                _ = selectTabMenuShow( (PBCommon.nCurSelPrgTabIdx + 1) * 100)
                selectTabMenu((PBCommon.nCurSelPrgTabIdx+1) * 100)
                return ;
            }
            PBCommon.nCurSelPrgTabIdx = nIdx
            PBCommon.sendData(String(format: "Y%d;", PBCommon.nCurSelPrgTabIdx))
            refreshCurrentOpType()
            _ = selectTabMenuShow( (nIdx + 1) * 100)
            refreshSaveDeleteAllButton()
        }
    }

    func initProgramEditPop() {
        
        var aOpType = PBCommon.OP_TYPE_NONE
        if PBCommon.nCurSelPrgTabIdx == 0 {
            aOpType = PBCommon.OP_TYPE_SINGLE
        
        } else if PBCommon.nCurSelPrgTabIdx == 1 {
            aOpType = PBCommon.OP_TYPE_DOUBLE

        } else if PBCommon.nCurSelPrgTabIdx == 2 {
            aOpType = PBCommon.OP_TYPE_SINGLE

        } else if PBCommon.nCurSelPrgTabIdx == 3 {
            aOpType = PBCommon.OP_TYPE_DOUBLE

        } else if PBCommon.nCurSelPrgTabIdx == 4 {
            if nPrgOpType5 == PBCommon.OP_TYPE_NONE {
                aOpType = PBCommon.nOperationType
            } else {
                aOpType = nPrgOpType5
            }

        } else if PBCommon.nCurSelPrgTabIdx == 5 {
            if nPrgOpType6 == PBCommon.OP_TYPE_NONE {
                aOpType = PBCommon.nOperationType
            } else {
                aOpType = nPrgOpType6
            }

        } else if PBCommon.nCurSelPrgTabIdx == 6 {
            if nPrgOpType7 == PBCommon.OP_TYPE_NONE {
                aOpType = PBCommon.nOperationType
            } else {
                aOpType = nPrgOpType7
            }

        } else if PBCommon.nCurSelPrgTabIdx == 7 {
            if nPrgOpType8 == PBCommon.OP_TYPE_NONE {
                aOpType = PBCommon.nOperationType
            } else {
                aOpType = nPrgOpType8
            }
        }
            
        let aProgramIdx = Int(vcProgramEdit.nProgramIdx)
        let aProgramDataIdx = Int(vcProgramEdit.nProgramDataIdx)

        let aData = arrProgramData[aProgramIdx].programDataList[aProgramDataIdx]
        vcProgramEdit.initProgramEditPop(["code" : aData.nCode,
                                          "type" : aOpType,
                                          "mode" : aData.nMode,
                                          "pressure" : aData.nPressure,
                                          "cycle" : aData.nCycle,
                                          "time" : aData.nLen])
        PBLib.performTransition(true, view: vcProgramEdit.view)
        PBCommon.bProgramEditing = true
    }
    
    //MARK: - Menu02SubDelegate
    func onPopProgramEdit(_ a_ProgramIdx : Int, a_ProgramDataIdx : Int) {
        vcProgramEdit.nProgramIdx = Int32(a_ProgramIdx)
        vcProgramEdit.nProgramDataIdx = Int32(a_ProgramDataIdx)
        
        initProgramEditPop()
    }
    
    func onDelProgramData(_ a_ProgramIdx: Int, a_ProgramDataIdx: Int) {
        refreshSaveDeleteAllButton()
    }
    
    func onChangeTabIdx(_ a_Idx: Int) {
        print("onChangeTabIdx:\(a_Idx)")
        DispatchQueue.main.async {
            self.selectTabMenu((a_Idx + 1) * 100)
        }        
    }
    
    //MARK: ProgramEditDelegate
    func initProgramEditPop(_ a_ProgramIdx: Int32, programDataIdx a_ProgramDataIdx: Int32) {
        vcProgramEdit.nProgramIdx = Int32(a_ProgramIdx)
        vcProgramEdit.nProgramDataIdx = Int32(a_ProgramDataIdx)
        
        let aProgramIdx = Int(vcProgramEdit.nProgramIdx)
        let aProgramDataIdx = Int(vcProgramEdit.nProgramDataIdx)
        let aProgramData = arrProgramData[aProgramIdx].programDataList[aProgramDataIdx-1]
        if aProgramData.nMode == PBCommon.OP_MODE_NONE {
            vcProgramEdit.nProgramDataIdx -= 1
            if vcProgramEdit.nProgramDataIdx < 0 {
                vcProgramEdit.nProgramDataIdx = 0
            }
            PBLib.showAlert(NSLocalizedString("PreviousSeqFirst", comment: "Previous Seq First"))
            return
        }

        initProgramEditPop()
    }
    
    func onChangeOpType(_ a_OpType: Int32) {
        if a_OpType == PBCommon.OP_TYPE_SINGLE {
            changeOperationType(PBCommon.OP_TYPE_SINGLE)
        } else if a_OpType == PBCommon.OP_TYPE_DOUBLE {
            changeOperationType(PBCommon.OP_TYPE_DOUBLE)
        }
    }
    
    func onCloseProgramEdit(_ a_Param: [AnyHashable : Any]) {
        
        PBCommon.bProgramEditing = false
        
        let aTabIdx : Int = Int(vcProgramEdit.nProgramIdx)
        let aRowIdx : Int = Int(vcProgramEdit.nProgramDataIdx)
        
        print("onCloseProgramEdit: nProgramIdx:\(vcProgramEdit.nProgramIdx) nProgramDataIdx:\(vcProgramEdit.nProgramDataIdx)")
        print(a_Param)
                
        let aOpType = Int(vcProgramEdit.nType)
        
        var aTime : Int32 = vcProgramEdit.nLen
//        let aMin = aTime / 60
//        let aSec = aTime % 60
                
        if aTime == 0 {
            aTime = 1
        }
                
        var aTimeSum : Int32 = 0
        let aData = arrProgramData[PBCommon.nCurSelPrgTabIdx].programDataList
        for i in 0 ..< aData.count {
            if i != aRowIdx {
                aTimeSum += Int32(aData[i].nLen)
            }
        }
                
        if aTimeSum + aTime > 30*60 {
            aTime = 30*60 - aTimeSum
            Toast.show(message: NSLocalizedString("ProgramTimeLimitTotal", comment: "ProgramTime Limit Total"), baseView: self.view)
            if aTime == 0 {
                return
            }
        }
        
        if aOpType == PBCommon.OP_TYPE_SINGLE {
            changeOperationType(PBCommon.OP_TYPE_SINGLE)
        } else if aOpType == PBCommon.OP_TYPE_DOUBLE {
            changeOperationType(PBCommon.OP_TYPE_DOUBLE)
        }
        
        arrProgramData[aTabIdx].programDataList[aRowIdx].nMode = Int(vcProgramEdit.nMode)
        arrProgramData[aTabIdx].programDataList[aRowIdx].nPressure = Int(vcProgramEdit.nPressure)
        arrProgramData[aTabIdx].programDataList[aRowIdx].nCycle = Int(vcProgramEdit.nCycle)
        arrProgramData[aTabIdx].programDataList[aRowIdx].nLen = Int(aTime)
        
        arrMenu02Sub[aTabIdx].tvList.reloadData()
        refreshSaveDeleteAllButton()
    }
   
    @IBAction func onSingleMode(_ sender: Any) {
        changeOperationType(PBCommon.OP_TYPE_SINGLE)
    }
    
    @IBAction func onDoubleMode(_ sender: Any) {
        changeOperationType(PBCommon.OP_TYPE_DOUBLE)
    }

    
    @IBAction func onProgramSave(_ sender: Any) {
        
        var aProgramFixIdx = 4
//        if PBCommon.getLocaleLanguage() == "fr" {
//            aProgramFixIdx = 6
//        }

        if (PBCommon.nCurSelPrgTabIdx < aProgramFixIdx || getCurListCount() == 0) {
            return
        }
        if PBCommon.nCurSelPrgTabIdx > 3 {
            let aCnt = getCurListCount()
            if aCnt > 0 {
                let alert = UIAlertView(title: "", message: NSLocalizedString("WantSave", comment: "Want Save"), delegate: self, cancelButtonTitle: NSLocalizedString("NO", comment: "NO"), otherButtonTitles: NSLocalizedString("YES", comment: "YES"))
                alert.tag = 100
                alert.show()
            }
        }
    }
    
    @IBAction func onProgramEraseAll(_ sender: Any) {
        var aProgramFixIdx = 4
//        if PBCommon.getLocaleLanguage() == "fr" {
//            aProgramFixIdx = 6
//        }
        
        if (PBCommon.nCurSelPrgTabIdx < aProgramFixIdx || getCurListCount() == 0) {
            return
        }
        if PBCommon.nCurSelPrgTabIdx > 3 {
            let aCnt = getCurListCount()
            if aCnt > 0 {
                let alert = UIAlertView(title: "", message: NSLocalizedString("deleteAllSeq", comment: "delete All Seq"), delegate: self, cancelButtonTitle: NSLocalizedString("NO", comment: "NO"), otherButtonTitles: NSLocalizedString("YES", comment: "YES"))
                alert.tag = 200
                alert.show()
            }
        }
    }
    
    @IBAction func onProgramPlay(_ sender: Any) {
        PBCommon.sendData(PBCommon.cCmdProgramRun + String(format: "%d", PBCommon.nCurSelPrgTabIdx) + ";")
    }
    
    @IBAction func onProgramSkip(_ sender: Any) {
        PBCommon.sendData(PBCommon.sCmdProgramSkip)
    }
    
    @IBAction func onProgramStop(_ sender: Any) {
        PBCommon.sendData(PBCommon.sCmdProgramStop)
        
        PBCommon.sendData(PBCommon.sCmdInqueryStatus)
    }

    //MARK: - UIAlertViewDelegate
    func alertView(_ alertView: UIAlertView, didDismissWithButtonIndex buttonIndex: Int) {
        if alertView.tag == 100 {
            if buttonIndex == 1 {
                saveProgramData()
            }
        } else if alertView.tag == 200 {
            if buttonIndex == 1 {
                clearProgramSequece()
                refreshSaveDeleteAllButton()
            }
        }
    }
    
    func clearProgramSequece() {
        let aData = arrProgramData[PBCommon.nCurSelPrgTabIdx].programDataList
        for i in 0 ..< aData.count {
            aData[i].initProgramData()
            arrMenu02Sub[i].tvList.reloadData()
        }
        
        switch (PBCommon.nCurSelPrgTabIdx) {
        case 4:
            nPrgReqCnt5 = 0
            nPrgOpType5 = PBCommon.OP_TYPE_DOUBLE
            break
        case 5:
            nPrgReqCnt6 = 0
            nPrgOpType6 = PBCommon.OP_TYPE_DOUBLE
            break
        case 6:
            nPrgReqCnt7 = 0
            nPrgOpType7 = PBCommon.OP_TYPE_NONE
            break
        case 7:
            nPrgReqCnt8 = 0
            nPrgOpType8 = PBCommon.OP_TYPE_NONE
            break
        default:
            break
        }
        
        for idx in 0 ..< 8 {
            UserDefaults.standard.removeObject(forKey: "programdata_\(PBCommon.nCurSelPrgTabIdx)_\(idx)")
        }
        UserDefaults.standard.synchronize()
        
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MSG_PRGLIST_RELOAD"), object: nil, userInfo: nil)
        
        PBCommon.sendSavePacket()
        
        refreshCurrentOpType()
    }
    
    func saveProgramData() {
        let aData = arrProgramData[PBCommon.nCurSelPrgTabIdx].programDataList
        for i in 0 ..< aData.count {
            if i == aData.count-1 {
//                if PBCommon.getLocaleLanguage() != "fr"
//                || !(PBCommon.nCurSelPrgTabIdx == 4 || PBCommon.nCurSelPrgTabIdx == 5) {
//                    aData[i].nLen = PBCommon.nProgramLastTime
//                }
                setProgramData(i)
                break
                
            } else if i < aData.count-1 && aData[i+1].nMode == PBCommon.OP_MODE_NONE {
//                if PBCommon.getLocaleLanguage() != "fr"
//                || !(PBCommon.nCurSelPrgTabIdx == 4 || PBCommon.nCurSelPrgTabIdx == 5) {
//                    aData[i].nLen = PBCommon.nProgramLastTime
//                }
                setProgramData(i)
                break
                
            } else {
                setProgramData(i)
            }
        }
        
        PBCommon.sendSavePacket()
        
        refreshCurrentOpType()
                
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MSG_PRGLIST_RELOAD"), object: nil, userInfo: nil)
    }
    
    func setProgramData(_ a_Idx : Int) {
    
        let aData = arrProgramData[PBCommon.nCurSelPrgTabIdx].programDataList[a_Idx]
        print("setProgramData: nCode:\(aData.nCode) nMode:\(aData.nMode) nPressure:\(aData.nPressure) nCycle:\(aData.nCycle) nLen:\(aData.nLen)");
        
        let aCmd = String(format: "I%dS%dM%dP%02dC%dT%04d;",
                          PBCommon.nCurSelPrgTabIdx,
                          a_Idx,
                          aData.nMode,
                          aData.nPressure,
                          aData.nCycle,
                          aData.nLen)
    
        print("setProgramData:" + aCmd)
        PBCommon.sendData(aCmd)
        
        UserDefaults.standard.set(aCmd, forKey: "programdata_\(PBCommon.nCurSelPrgTabIdx)_\(a_Idx)")
//        UserDefaults.standard.synchronize()

        //DB 헬퍼 할당.
//        DBHelper dbHelper = new DBHelper(this);
//        dbHelper.setProgramData(a_Data.nCode, a_Data.nMode, a_Data.nPressure, a_Data.nCycle, a_Data.nLen);
//        dbHelper.close();
    }

//    func sendSavePacket() {
//        var aCmd = "A"
//        for i in 0 ..< arrProgramData.count {
//            let aSeqData = arrProgramData[i].programDataList
//            var aCnt = 0
//            for j in 0 ..< aSeqData.count {
//                if aSeqData[j].nMode == PBCommon.OP_MODE_NONE {
//                    break
//                }
//                aCnt += 1
//            }
//
//            if i == 4 {
//                nPrgReqCnt5 = aCnt
//            } else if i == 5 {
//                nPrgReqCnt6 = aCnt
//            } else if i == 6 {
//                nPrgReqCnt7 = aCnt
//            } else if i == 7 {
//                nPrgReqCnt8 = aCnt
//            }
////            aCmd += String(format: "%d", aCnt)
//        }
//
//        aCmd += String(format: "%d", nPrgReqCnt5)
//        aCmd += String(format: "%d", nPrgReqCnt6)
//        aCmd += String(format: "%d", nPrgReqCnt7)
//        aCmd += String(format: "%d", nPrgReqCnt8)
//
//        if nPrgOpType5 == PBCommon.OP_TYPE_NONE {
//            aCmd += String(format: "%d", PBCommon.nOperationType)
//        } else {
//            aCmd += String(format: "%d", nPrgOpType5)
//        }
//
//        if nPrgOpType6 == PBCommon.OP_TYPE_NONE {
//            aCmd += String(format: "%d", PBCommon.nOperationType)
//        } else {
//            aCmd += String(format: "%d", nPrgOpType6)
//        }
//
//        if nPrgOpType7 == PBCommon.OP_TYPE_NONE {
//            aCmd += String(format: "%d", PBCommon.nOperationType)
//        } else {
//            aCmd += String(format: "%d", nPrgOpType7)
//        }
//
//        if nPrgOpType8 == PBCommon.OP_TYPE_NONE {
//            aCmd += String(format: "%d", PBCommon.nOperationType)
//        } else {
//            aCmd += String(format: "%d", nPrgOpType8)
//        }
//
//        print(String(format: "sendSavePacket nPrgReqCnt: %d %d %d %d", nPrgReqCnt5, nPrgReqCnt6, nPrgReqCnt7, nPrgReqCnt8))
//        print(String(format: "sendSavePacket nPrgOpType: %d %d %d %d", nPrgOpType5, nPrgOpType6, nPrgOpType7, nPrgOpType8))
//
//        print("sendSavePacket:" + aCmd + ";")
//        PBCommon.sendData(aCmd + ";")
//
//        UserDefaults.standard.set(aCmd, forKey: "program_cnt_optype")
//    }
    
    func stopInitActivityViewTimer() {
        nTimerInitActivityViewMax = 30
    
        if timerInitActivityView != nil {
            timerInitActivityView?.invalidate()
            timerInitActivityView = nil
        }
    }
    
    func doProgramIDN(_ a_ID : Int) {
        stopInitActivityViewTimer()
        
        timerInitActivityView = Timer.scheduledTimer(timeInterval: 0.5,
                                                target: self,
                                                selector: #selector(timerTaskInitDataReceived),
                                                userInfo: ["TabIdx" : a_ID],
                                                repeats: true)

    }
    
    @objc func timerTaskInitDataReceived(_ timer : Timer) {
     
        let userInfo = timer.userInfo as! Dictionary <String, AnyObject>
        
        if let aTabIdx = userInfo["TabIdx"] as? Int {
            if bRecvFirst {
                stopInitActivityViewTimer()
                
                selectTabMenu(aTabIdx * 100)
            }
            
            if nTimerInitActivityViewMax < 0 {
                stopInitActivityViewTimer()
            }
            
            nTimerInitActivityViewMax -= 1
        }
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
