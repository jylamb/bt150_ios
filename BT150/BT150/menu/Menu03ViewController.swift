//
//  Menu03ViewController.swift
//  BT150
//
//  Created by KOBONGHWAN on 14/11/2018.
//  Copyright © 2018 bistos. All rights reserved.
//

import UIKit

class Menu03ViewController: UIViewController, PBTabDelegate, UIScrollViewDelegate, Menu03SubDelegate, UIAlertViewDelegate, StatisticsEditDelegate {

    let nTabCnt : Int = 3
    
    var vcTopMenu : TopMenuViewController?
    var nCurSelePage : Int = 0
    var vcStatisticsEdit = StatisticsEditViewController()
    var arrTab = [PBTab]()
    var arrMenu03Sub = [Menu03SubViewController]()
    var lMilkDateOrg : Int64 = 0
    
    var vcSub01 = Menu03SubViewController()
    var vcSub02 = Menu03SubViewController()
    var vcSub03 = Menu03SubViewController()

    @IBOutlet weak var vTab: UIView!
    @IBOutlet weak var svSub: UIScrollView!
    @IBOutlet weak var btnStatisticsAdd: UIButton!
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "embedMenuSegue" {
            if let vc = segue.destination as? TopMenuViewController {
                vcTopMenu = vc
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        let sizeView = self.view.frame.size
        
        makeSubTab()
        
        svSub.delegate = self
        svSub.bounces = false
        svSub.isScrollEnabled = true
        svSub.isPagingEnabled = true
        svSub.isUserInteractionEnabled = true
        svSub.backgroundColor = UIColor.clear
        svSub.contentSize =  CGSize(width: sizeView.width * CGFloat(nTabCnt), height: svSub.frame.size.height)
        
        makeSubView()
        
        vcStatisticsEdit.view.frame = CGRect(x: 0, y: 0, width: sizeView.width, height: sizeView.height)
        vcStatisticsEdit.delegate = self
        self.view.addSubview(vcStatisticsEdit.view)
        vcStatisticsEdit.view.isHidden = true

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        PBCommon.nActivityNo = PBCommon.ACT_MENU03
        PBCommon.nVoiceMode = PBCommon.VOICE_MODE_OPERATION
        
        vcTopMenu?.ivOperation.image = UIImage(named: "menu_ic_2.png")
        vcTopMenu?.ivProgram.image = UIImage(named: "menu_ic_4.png")
        vcTopMenu?.ivStatistic.image = UIImage(named: "menu_ic_5_ov.png")
        
        if !PBCommon.bMenu03VCmdReady {
            PBCommon.bMenu03VCmdReady = true
        }
        NotificationCenter.default.addObserver(self, selector: #selector(onSRRsult(_:)), name: Notification.Name("MSG_SR_RESULT"), object: nil)        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func onSRRsult(_ a_Noti : NSNotification) {
//        print("Menu01 onSRRsult-1")
        if let aData = a_Noti.userInfo?["String"] as? String {
            if let aResult = a_Noti.userInfo?["Result"] as? Bool {
                if aResult {
                    Toast.show(message: aData, baseView: self.view)
                } else {
                    Toast.show(message: "Recognizing Fail:" + aData, baseView: self.view)
                }
            }
        }
    }
    
    func makeSubView() {
        
        arrMenu03Sub.append(vcSub01)
        arrMenu03Sub.append(vcSub02)
        arrMenu03Sub.append(vcSub03)

        let sizeView = self.view.frame.size

        for i in 0 ..< 3 {
            let aVC = arrMenu03Sub[i]
            aVC.view.frame = CGRect(x: sizeView.width * CGFloat(i), y: 0, width: sizeView.width, height: svSub.frame.size.height)
            aVC.delegate = self
            aVC.initMenu03Data((i + 1) * 100)
            svSub.addSubview(aVC.view)
        }
    }

    func makeSubTab() {
        let sizeView = self.view.frame.size
        
        let fTabW = sizeView.width / CGFloat(nTabCnt)
        let fTabH = vTab.frame.size.height
        
        for i in 0 ..< nTabCnt {
            let tabMenu = PBTab(frame: CGRect(x: fTabW * CGFloat(i), y: 0, width: fTabW, height: fTabH))
            tabMenu.delegate = self
            tabMenu.tag = (i + 1) * 100
            tabMenu.initPBTabColor(PBLib.RGB([0xe9, 0xe9, 0xe9]), andSelect: PBLib.RGB([0xe6, 0x62, 0x72]))
            switch i {
            case 0:
                tabMenu.initPBTabTitle(NSLocalizedString("Day", comment: "Day"), andFont: PBLib.AppFont(16), andColor: UIColor.black)
                break
            case 1:
                tabMenu.initPBTabTitle(NSLocalizedString("Week", comment: "Week"), andFont: PBLib.AppFont(16), andColor: UIColor.black)
                break
            case 2:
                tabMenu.initPBTabTitle(NSLocalizedString("Month", comment: "Month"), andFont: PBLib.AppFont(16), andColor: UIColor.black)
                break
            default:
                break
            }
            vTab.addSubview(tabMenu)
            arrTab.append(tabMenu)
        }
        
        for i in 0 ..< nTabCnt {
            if nCurSelePage == i {
                selectTabMenu(arrTab[i].tag)
            }
        }
        
        let vLine01 = UIView(frame: CGRect(x: 0, y: vTab.frame.size.height-1, width: vTab.frame.size.width, height: 1))
        vLine01.backgroundColor = PBLib.RGB([0xd8, 0xd8, 0xd8])
        vLine01.tag = 999
        vTab.addSubview(vLine01)
    }

    func selectTabMenu(_ a_Tag : Int) {
        nCurSelePage = selectTabMenuShow(a_Tag)
        
        moveWebviewScroll(nCurSelePage)
        
        if nCurSelePage == 0 {
            _ = vcSub01.readDBData()
            vcSub01.tvList.reloadData()
        
        } else if nCurSelePage == 1 {
            _ = vcSub02.readDBData()
            vcSub02.tvList.reloadData()
        
        } else if nCurSelePage == 2 {
            _ = vcSub03.readDBData()
            vcSub03.tvList.reloadData()
        }
    }

    func selectTabMenuShow(_ a_Tag : Int) -> Int {
        var nPageIdx : Int = 0
        
        for i in 0 ..< nTabCnt {
            let tabMenu : PBTab = arrTab[i]
            if tabMenu.tag == a_Tag {
                tabMenu.select(true)
                nPageIdx = i
                
            } else {
                tabMenu.select(false)
            }
        }
        
        if nPageIdx == 0 {
            btnStatisticsAdd.isHidden = false
        } else {
            btnStatisticsAdd.isHidden = true
        }
//        print("selectTabMenuShow: nPageIdx: \(nPageIdx)")
//        arrMenu02Sub[nPageIdx].tvList?.reloadData()
        
        return nPageIdx
    }
    
    func moveWebviewScroll(_ a_Idx : Int) {
        var frame = svSub.frame
        frame.origin.x = frame.size.width * CGFloat(a_Idx)
        frame.origin.y = 0
        svSub.scrollRectToVisible(frame, animated: true)
    }
    
    //MARK: - PBTabDelegate
    func onTabClick(_ a_Idx: Int) {
        selectTabMenu(a_Idx)
    }
    
    //MARK: - UIScrollViewDelegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageWidth = svSub.frame.size.width
        let nIdx = Int( floor((svSub.contentOffset.x - pageWidth / CGFloat(svSub.subviews.count)) / pageWidth) + 1 )
        if nIdx != nCurSelePage {
            nCurSelePage = nIdx
            _ = selectTabMenuShow( (nIdx + 1) * 100)
        }
    }
    
    //MARK: - Menu03SubDelegate
    func onPopStatisticsEdit(_ a_Idx : Int) {
        let aData = vcSub01.arrData[a_Idx]
        vcStatisticsEdit.nIdx = Int32(a_Idx)
        vcStatisticsEdit.initStatisticsEditPop(["date" : aData.lDate,
                                                "left" : aData.nAmtL,
                                                "right" : aData.nAmtR,
                                                "time" : aData.nTime])
        PBLib.performTransition(true, view: vcStatisticsEdit.view)
        lMilkDateOrg = aData.lDate
    }
    
    //MARK: - StatisticsEditDelegate
    func onCloseStatisticsEdit(_ a_Param: [AnyHashable : Any]) {
        let aIdx : Int = Int(vcStatisticsEdit.nIdx)
        
        print("vcStatisticsEdit.nAmtL:\(vcStatisticsEdit.nAmtL) vcStatisticsEdit.nAmtR:\(vcStatisticsEdit.nAmtR)")
        
        if vcStatisticsEdit.nIdx < 0 {
            let aMilkData = PBCommon.MilkData(Date: Int64(vcStatisticsEdit.lDate), AmtL: Int(vcStatisticsEdit.nAmtL), AmtR: Int(vcStatisticsEdit.nAmtR), Time: Int64(vcStatisticsEdit.nTime))
            vcSub01.arrData.append(aMilkData)
            
            saveStatisticItem(Int64(vcStatisticsEdit.lDate), Left: Int(vcStatisticsEdit.nAmtL), Right: Int(vcStatisticsEdit.nAmtR), Time: Int64(vcStatisticsEdit.nTime))
            
        } else {
            vcSub01.arrData[aIdx].lDate = Int64(vcStatisticsEdit.lDate)
            vcSub01.arrData[aIdx].nAmtL = Int(vcStatisticsEdit.nAmtL)
            vcSub01.arrData[aIdx].nAmtR = Int(vcStatisticsEdit.nAmtR)
            vcSub01.arrData[aIdx].nTime = Int64(vcStatisticsEdit.nTime)
            
            saveStatisticItem(Int64(vcStatisticsEdit.lDate), Left: Int(vcStatisticsEdit.nAmtL), Right: Int(vcStatisticsEdit.nAmtR), Time: Int64(vcStatisticsEdit.nTime))
        }
        vcSub01.tvList.reloadData()
    }
    
    @IBAction func onPopStatisticAdd(_ sender: Any) {
        vcStatisticsEdit.nIdx = -1
        vcStatisticsEdit.initStatisticsEditPop(["date" : Date().getTimeMillis(),
                                                "left" : 0,
                                                "right" : 0,
                                                "time" : 0])
        PBLib.performTransition(true, view: vcStatisticsEdit.view)
        lMilkDateOrg = Date().getTimeMillis()
    }
    
    func saveStatisticItem(_ a_Date : Int64, Left a_Left : Int, Right a_Right : Int, Time a_Duration : Int64) {
        
        print("saveStatisticItem: a_Left:\(a_Left) a_Right:\(a_Right)")
        
        let  dbHelper = DBHelper()
    
        if (lMilkDateOrg != a_Date) {
            dbHelper.deleteMilkData(Int(lMilkDateOrg))
        }
        print("saveStatisticItem:\(Date(timeIntervalSince1970: TimeInterval(a_Date/1000))) \(a_Date)")

//        let aUnit =  UserDefaults.standard.string(forKey: "unit") ?? "ml"
//        if aUnit == "ml" {
//            dbHelper.setMilkData(Int(a_Date), left: Int32(a_Left), right: Int32(a_Right), duration: Int(a_Duration))
//        } else {
//            dbHelper.setMilkData(Int(a_Date), left: Int32(ceil(Double(a_Left))), right: Int32(ceil(Double(a_Right)*GLConst.toML)), duration: Int(a_Duration))
//        }
        
        dbHelper.setMilkData(Int(a_Date), left: Int32(a_Left), right: Int32(a_Right), duration: Int(a_Duration))
        _ = vcSub01.readDBData()
        vcSub01.tvList.reloadData()
    }
    
    //MARK: - UIAlertViewDelegate
    func alertView(_ alertView: UIAlertView, didDismissWithButtonIndex buttonIndex: Int) {
        
//        if alertView.tag == 100 {
//            if buttonIndex == 1 {
//                saveProgramData()
//            }
//        } else if alertView.tag == 200 {
//            clearProgramSequece()
//            refreshSaveDeleteAllButton()
//        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
