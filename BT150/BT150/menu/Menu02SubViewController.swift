//
//  Menu02SubViewController.swift
//  BT150
//
//  Created by KOBONGHWAN on 19/12/2018.
//  Copyright © 2018 bistos. All rights reserved.
//

import UIKit

protocol Menu02SubDelegate {
    func onPopProgramEdit(_ a_ProgramIdx : Int, a_ProgramDataIdx : Int)
    func onDelProgramData(_ a_ProgramIdx : Int, a_ProgramDataIdx : Int)
    func onChangeTabIdx(_ a_Idx : Int)
}


class Menu02SubViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate {

    struct tablelist {
        static let kTableViewCellHTop : CGFloat = 40
        static let kTableViewCellH : CGFloat = 50
    }

    var delegate : Menu02SubDelegate?
    var nViewID : Int = 0
    var tvList = UITableView()
    var nDelIdx = 0
    var nCurSelIdx : Int = -1

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(onRecvData(_:)), name: Notification.Name("MSG_DATA_RECV"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onSRRsult(_:)), name: Notification.Name("MSG_SR_RESULT"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onRecvVCmd(_:)), name: Notification.Name("MSG_VCMD_RECV"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshList(_:)), name: Notification.Name("MSG_PRGLIST_RELOAD"), object: nil)
                
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if PBCommon.bProgramRunning {
            parseRecvData(PBCommon.gRecvDataProgram)
        }
    }
    
    var nProgramPopMode = PBCommon.OP_MODE_NONE
    var nProgramPopPressureExp = 0
    var nProgramPopPressureMsg = 0
    var nProgramPopCycleExp = 0
    var nProgramPopCycleMsg = 0

    
    @objc func onRecvVCmd(_ a_Noti : NSNotification) {
//        print("Menu02Sub onRecvVCmd-1")
        
        var aCmdCode = 0
        var aCmdType = VoiceCommand.CMD_TYPE_NORMAL
        var aStepVal = 0
        
        if let aRet = a_Noti.userInfo?["CmdCode"] as? Int {
            aCmdCode = aRet
        }
        if let aRet = a_Noti.userInfo?["CmdType"] as? Int {
            aCmdType = aRet
        }
        if let aRet = a_Noti.userInfo?["StepVal"] as? Int {
            aStepVal = aRet
        }
        
//        print("onReceive: aCmdCode:\(aCmdCode) aCmdType:\(aCmdType) aStepVal:\(aStepVal)")
        
        switch aCmdCode {
//        case VoiceCommand.CMD_MODE_CHANGE:
//            if PBCommon.isProgramEditing() {
//                if nProgramPopMode == PBCommon.OP_MODE_EXP {
//                    nProgramPopMode = PBCommon.OP_MODE_MSG
//                } else {
//                    nProgramPopMode = PBCommon.OP_MODE_EXP
//                }
//                refreshModeTap();
//            }
//            break;
//        case CMD_PRESSURE_UP:
//            if (isProgramEditing()) {
//                doPressureUp();
//            }
//            break;
//        case CMD_PRESSURE_DOWN:
//            if (isProgramEditing()) {
//                doPressureDown();
//            }
//            break;
//        case CMD_CYCLE_UP:
//            if (isProgramEditing()) {
//                doCycleUp();
//            }
//            break;
//        case CMD_CYCLE_DOWN:
//            if (isProgramEditing()) {
//                doCycleDown();
//            }
//            break;

        default:
            break;
        }
        
    }
    
    @objc func onSRRsult(_ a_Noti : NSNotification) {
//        print("Menu01 onSRRsult-1")
        if let aData = a_Noti.userInfo?["String"] as? String {
            if let aResult = a_Noti.userInfo?["Result"] as? Bool {
                if aResult {
                    Toast.show(message: aData, baseView: self.view)
                } else {
                    Toast.show(message: "Recognizing Fail:" + aData, baseView: self.view)
                }
            }
        }
    }

    @objc func onRecvData(_ a_Noti : NSNotification) {        
        if let a_Data = a_Noti.userInfo?["recv_data"] as? String {
            parseRecvData(a_Data)
        }
    }
    
    @objc func refreshList(_ a_Noti : NSNotification) {
        reloadTable()
    }
    
    func reloadTable() {
        DispatchQueue.main.async {
            self.tvList.reloadData()
            let aTabIdx = (self.tvList.tag / 100) - 1
            if self.nCurSelIdx >= 0 && self.nCurSelIdx < arrProgramData[aTabIdx].programDataList.count {
                self.tvList.scrollToRow(at: IndexPath(row: self.nCurSelIdx, section: 0), at: .top, animated: true)
            }
        }
    }

    func parseRecvData(_ a_Recv : String) {
        
        if (a_Recv.count == 16
            && a_Recv.substring(to: 1) == "M"
            && a_Recv.substring(with: 2..<3) == "P"
            && a_Recv.substring(with: 5..<6) == "C"
            && a_Recv.substring(with: 7..<8) == "R"
            && a_Recv.substring(with: 9..<10) == "I"
            && a_Recv.substring(with: 11..<12) == "S") {


            let aRun = a_Recv.substring(with: 8..<9)
            let aNo = Int(a_Recv.substring(with: 10..<11))
            let aSeq = Int(a_Recv.substring(with: 12..<13))
            
            let aSelIdx = PBCommon.nCurSelPrgTabIdx
            let aTabIdx = (tvList.tag / 100) - 1
            
//            if aRun == "2" {
                if aSelIdx != aNo {
                    delegate?.onChangeTabIdx(aNo ?? 0)
                }
                
                if aNo == aTabIdx {
                    nCurSelIdx = aSeq ?? 0
                }
            
                if aRun == "2" {
                    PBCommon.bProgramRunning = true
                    
                } else {
                    PBCommon.bProgramRunning = false
                    nCurSelIdx = -1
                }
            
                reloadTable()
            
//            } else {
//                nCurSelIdx = -1
//                tvList.reloadData()
//            }
        }
    }
    
    func initListHeader() {
        
        let vBase = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: tablelist.kTableViewCellHTop))
        vBase.backgroundColor = PBLib.RGB([0xe9, 0xe9, 0xe9])
        self.view.addSubview(vBase)

        let sizeBase = vBase.frame.size
        let fTextW : CGFloat = sizeBase.width / 6

        for i in 0 ..< 5 {
            let lblColName01 = UILabel()
            lblColName01.backgroundColor = UIColor.clear
            lblColName01.font = PBLib.AppFont(14)
//            lblColName01.adjustsFontSizeToFitWidth = true
            lblColName01.textColor = PBLib.RGB([51,51,51])
            lblColName01.textAlignment = NSTextAlignment.center
            vBase.addSubview(lblColName01)
            
            lblColName01.frame = CGRect(x: fTextW * CGFloat(i), y: 0, width: fTextW, height: tablelist.kTableViewCellHTop)
            
            switch i {
            case 0:
                lblColName01.text = NSLocalizedString("Seq", comment: "Seq")
                break;
            case 1:
                lblColName01.text = NSLocalizedString("Mode", comment: "Mode")
                break;
            case 2:
                lblColName01.text = NSLocalizedString("Pressure", comment: "Pressure")
                break;
            case 3:
                lblColName01.text = NSLocalizedString("Cycle", comment: "Cycle")
                lblColName01.frame = CGRect(x: (fTextW+5) * CGFloat(i), y: 0, width: fTextW+10, height: tablelist.kTableViewCellHTop)
                break;
            case 4:
                lblColName01.text = NSLocalizedString("Time", comment: "Time")
                lblColName01.frame = CGRect(x: (fTextW+5) * CGFloat(i), y: 0, width: fTextW, height: tablelist.kTableViewCellHTop)
                break;
            default:
                break;
            }
        }
    }
    
    func initMenu02Data(_ a_ID : Int) {
        nViewID = a_ID
        
        initListHeader()

        let sizeView = self.view.frame.size
        
//        print("initMenu02Data w:\(sizeView.width) h:\(sizeView.height)")
        
        tvList.frame = CGRect(x: 0, y: tablelist.kTableViewCellHTop, width: sizeView.width, height: sizeView.height-tablelist.kTableViewCellHTop)
        tvList.bounces = false
        tvList.separatorStyle = UITableViewCell.SeparatorStyle.none
        tvList.separatorColor = UIColor.orange
        tvList.allowsSelection = true
        tvList.isScrollEnabled = true
        tvList.backgroundColor = UIColor.clear
        tvList.delegate = self
        tvList.dataSource = self
        tvList.tag = a_ID
        tvList.register(UITableViewCell.self, forCellReuseIdentifier: "mycell")
        self.view.addSubview(tvList)
        
//        setTableViewLayout()
    }
    
    func setTableViewLayout() {
        let sizeView = self.view.frame.size
//        print("setTableViewLayout w:\(sizeView.width) h:\(sizeView.height)")
        tvList.frame = CGRect(x: 0, y: tablelist.kTableViewCellHTop, width: sizeView.width, height: sizeView.height-tablelist.kTableViewCellHTop)

//        if #available(iOS 11.0, *) {
//            let safeArea = self.view.safeAreaLayoutGuide
//            tvList.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor).isActive = true
//            tvList.topAnchor.constraint(equalTo: safeArea.topAnchor).isActive = true
//            tvList.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor).isActive = true
//            tvList.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor).isActive = true
//        } else {
//            tvList.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
//            tvList.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
//            tvList.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
//            tvList.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
//        }
    }
    
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let aTabIdx = (tableView.tag / 100) - 1
        return arrProgramData[aTabIdx].programDataList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tablelist.kTableViewCellH
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "mycell", for: indexPath)

        cell.backgroundColor = UIColor.white
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        for vSub in cell.contentView.subviews {
            if vSub.tag >= 100 {
                vSub.removeFromSuperview()
            }
        }
        
        let sizeView = self.view.frame.size
        
        let vBase = UIView(frame: CGRect(x: 0, y: 0, width: sizeView.width, height: tablelist.kTableViewCellH))
        vBase.backgroundColor = UIColor.white
        vBase.tag = 999
        cell.contentView.addSubview(vBase)
        
        if nCurSelIdx == indexPath.row {
            vBase.backgroundColor = PBLib.RGB([0xf4, 0xfa, 0x97])
        } else {
            vBase.backgroundColor = UIColor.white
        }
        
        let sizeBase = vBase.frame.size
        let fTextW : CGFloat = sizeBase.width / 6.2
//        print("index:\(indexPath.row) tableView tag: \(tableView.tag) sizeBase width: \(sizeBase.width) height:\(sizeBase.height) fTextW:\(fTextW)")
        
        let aTabIdx = (tableView.tag / 100) - 1
        let aProgramData = arrProgramData[aTabIdx].programDataList[indexPath.row]
//        print("tableview:\(tableView.tag) aTabIdx:\(aTabIdx) indexPath.row:\(indexPath.row) code:\(aProgramData.nCode) len:\(aProgramData.nLen)")

        let imgvNo = UIImageView(image: UIImage(named: String(format: "program_seq_%d.png", indexPath.row+1)))
        imgvNo.frame = CGRect(x: (fTextW-23)/2 - 10, y: (tablelist.kTableViewCellH-23)/2, width: 23, height: 23)
        imgvNo.tag = 999
        cell.contentView.addSubview(imgvNo)

        let imgvSD = UIImageView(image: UIImage(named: "program_ic_p.png"))
        imgvSD.frame = CGRect(x: fTextW + (fTextW-67/2)/2 - 30, y: (tablelist.kTableViewCellH-34)/2, width: 67/2, height: 34)
        imgvSD.tag = 999
        vBase.addSubview(imgvSD)
        
        if aTabIdx == 0 {
            imgvSD.image = UIImage(named: "program_ic_double.png")
        } else if aTabIdx == 1 {
            imgvSD.image = UIImage(named: "program_ic_double.png")
        } else if aTabIdx == 2 {
            imgvSD.image = UIImage(named: "program_ic_single.png")
        } else if aTabIdx == 3 {
            imgvSD.image = UIImage(named: "program_ic_single.png")
        } else if aTabIdx == 4 {
//            if PBCommon.getLocaleLanguage() == "fr" {
//                imgvSD.image = UIImage(named: "program_ic_single.png")
//            } else {
                if nPrgOpType5 == PBCommon.OP_TYPE_SINGLE {
                    imgvSD.image = UIImage(named: "program_ic_single.png")
                } else {
                    imgvSD.image = UIImage(named: "program_ic_double.png")
                }
//            }
        } else if aTabIdx == 5 {
//            if PBCommon.getLocaleLanguage() == "fr" {
//                imgvSD.image = UIImage(named: "program_ic_double.png")
//            } else {
                if nPrgOpType6 == PBCommon.OP_TYPE_SINGLE {
                    imgvSD.image = UIImage(named: "program_ic_single.png")
                } else {
                    imgvSD.image = UIImage(named: "program_ic_double.png")
                }
//            }
        } else if aTabIdx == 6 {
            if nPrgOpType7 == PBCommon.OP_TYPE_SINGLE {
                imgvSD.image = UIImage(named: "program_ic_single.png")
            } else {
                imgvSD.image = UIImage(named: "program_ic_double.png")
            }

        } else if aTabIdx == 7 {
            if nPrgOpType8 == PBCommon.OP_TYPE_SINGLE {
                imgvSD.image = UIImage(named: "program_ic_single.png")
            } else {
                imgvSD.image = UIImage(named: "program_ic_double.png")
            }
        }

        let imgvMode = UIImageView(image: UIImage(named: "program_ic_p.png"))
        imgvMode.frame = CGRect(x: fTextW + (fTextW-67/2)/2 + 15, y: (tablelist.kTableViewCellH-34)/2, width: 67/2, height: 34)
        imgvMode.tag = 999
        vBase.addSubview(imgvMode)
        if aProgramData.nMode ==  PBCommon.OP_MODE_MSG {
            imgvMode.image = UIImage(named: "program_ic_m.png")
            imgvMode.frame = CGRect(x: fTextW + (fTextW-67/2)/2 + 15, y: (tablelist.kTableViewCellH-33)/2, width: 67/2, height: 33)
        }

        let lblPressure = UILabel(frame: CGRect(x: fTextW*2, y: 0, width: fTextW, height: tablelist.kTableViewCellH))
        lblPressure.backgroundColor = UIColor.clear
        lblPressure.font = PBLib.AppFont(16)
        lblPressure.textColor = PBLib.RGB([51,51,51])
        lblPressure.text = String(aProgramData.nPressure+1)
        lblPressure.textAlignment = NSTextAlignment.center
        lblPressure.tag = 999
        vBase.addSubview(lblPressure)

        let lblCycle = UILabel(frame: CGRect(x: (fTextW+5)*3, y: 0, width: fTextW, height: tablelist.kTableViewCellH))
        lblCycle.backgroundColor = UIColor.clear
        lblCycle.font = PBLib.AppFont(16)
        lblCycle.textColor = PBLib.RGB([51,51,51])
        lblCycle.text = String(aProgramData.nCycle+1)
        lblCycle.textAlignment = NSTextAlignment.center
        lblCycle.tag = 999
        vBase.addSubview(lblCycle)

        let aMin = Int(aProgramData.nLen / 60)
        let aSec = Int(aProgramData.nLen % 60)

        let lblTime = UILabel(frame: CGRect(x: (fTextW+5)*4, y: 0, width: fTextW, height: tablelist.kTableViewCellH))
        lblTime.backgroundColor = UIColor.clear
        lblTime.font = PBLib.AppFont(16)
        lblTime.textColor = PBLib.RGB([51,51,51])
        lblTime.text = String(format: "%02d:%02d", aMin, aSec)
        lblTime.textAlignment = NSTextAlignment.center
        lblTime.tag = 999
        vBase.addSubview(lblTime)
        
        var aProgramFixIdx = 4
//        if PBCommon.getLocaleLanguage() == "fr" {
//            aProgramFixIdx = 6
//        }
        
//        print("aTabIdx:\(aTabIdx) aProgramFixIdx:\(aProgramFixIdx)")
        if aTabIdx >= aProgramFixIdx {
            let aDelX = lblTime.frame.origin.x + lblTime.frame.size.width;
            let btnDel = UIButton(frame: CGRect(x: aDelX, y: (sizeBase.height-32)/2, width: 32, height: 32))
            btnDel.setImage(UIImage(named: "program_btn_del.png"), for: UIControl.State())
            btnDel.addTarget(self, action: #selector(self.onDel(_:)), for: UIControl.Event.touchUpInside)
            btnDel.tag = (indexPath as NSIndexPath).row + 100
            vBase.addSubview(btnDel)
        }
        
        if aProgramData.nMode == PBCommon.OP_MODE_NONE {
            vBase.isHidden = true
        
            let lblAddInfo = UILabel(frame: CGRect(x: fTextW/2, y: 0, width: sizeBase.width-fTextW, height: tablelist.kTableViewCellH))
            lblAddInfo.backgroundColor = UIColor.clear
            lblAddInfo.font = PBLib.AppFont(16)
            lblAddInfo.textColor = PBLib.RGB([0xbb, 0xc0, 0xc6])
            lblAddInfo.text = NSLocalizedString("addContents", comment: "add Contents")
            lblAddInfo.textAlignment = NSTextAlignment.center
            lblAddInfo.tag = 999
            cell.contentView.addSubview(lblAddInfo)
            
            let imgvArr = UIImageView(image: UIImage(named: "program_btn_next.png"))
//            imgvArr.frame = CGRect(x: sizeBase.width-40, y: (tablelist.kTableViewCellH-23)/2, width: 23, height: 23)
            imgvArr.frame = CGRect(x: sizeBase.width-fTextW, y: (tablelist.kTableViewCellH-23)/2, width: 23, height: 23)
            imgvArr.tag = 999
            cell.contentView.addSubview(imgvArr)
            
//            if PBCommon.getLocaleLanguage() == "fr" && aTabIdx < aProgramFixIdx {
            if aTabIdx < aProgramFixIdx {
                lblAddInfo.isHidden = true
                imgvArr.isHidden = true
            }

        } else {
            vBase.isHidden = false
        }

        
        let vLine01 = UIView(frame: CGRect(x: 0, y: sizeBase.height - 1, width: sizeBase.width, height: 1))
        vLine01.backgroundColor = GLColor.kThemeLine
        vLine01.tag = 999
        cell.contentView.addSubview(vLine01)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let aTabIdx = (tableView.tag / 100) - 1
        
        var aProgramFixIdx = 4
//        if PBCommon.getLocaleLanguage() == "fr" {
//            aProgramFixIdx = 6
//        }
        
        if aTabIdx >= aProgramFixIdx {
            if(indexPath.row > 0) {
                let aProgramData = arrProgramData[aTabIdx].programDataList[indexPath.row-1]
                if aProgramData.nMode == PBCommon.OP_MODE_NONE {
                    PBLib.showAlert(NSLocalizedString("PreviousSeqFirst", comment: "Previous Seq First"))
                    return
                }
            }
            delegate?.onPopProgramEdit(aTabIdx, a_ProgramDataIdx: indexPath.row)
        }
    }
    
    @objc func onDel(_ a_Btn : UIButton) {
        nDelIdx = a_Btn.tag - 100
        let alert = UIAlertView(title: "", message: NSLocalizedString("deleteSequence", comment: "delete Sequence"), delegate: self, cancelButtonTitle: NSLocalizedString("NO", comment: "NO"), otherButtonTitles: NSLocalizedString("YES", comment: "YES"))
        alert.tag = 100
        alert.show()
    }
    
    func alertView(_ alertView: UIAlertView, didDismissWithButtonIndex buttonIndex: Int) {
        
        if alertView.tag == 100 {
            if buttonIndex == 1 {
                let aTabIdx = (tvList.tag / 100) - 1
                for i in nDelIdx ..< arrProgramData[aTabIdx].programDataList.count-1 {
                    arrProgramData[aTabIdx].programDataList[i].nCode = arrProgramData[aTabIdx].programDataList[i+1].nCode
                    arrProgramData[aTabIdx].programDataList[i].nMode = arrProgramData[aTabIdx].programDataList[i+1].nMode
                    arrProgramData[aTabIdx].programDataList[i].nPressure = arrProgramData[aTabIdx].programDataList[i+1].nPressure
                    arrProgramData[aTabIdx].programDataList[i].nCycle = arrProgramData[aTabIdx].programDataList[i+1].nCycle
                    arrProgramData[aTabIdx].programDataList[i].nLen = arrProgramData[aTabIdx].programDataList[i+1].nLen
                }
                arrProgramData[aTabIdx].programDataList[arrProgramData[aTabIdx].programDataList.count-1].initProgramData()
                reloadTable()
                delegate?.onDelProgramData(aTabIdx, a_ProgramDataIdx: nDelIdx)
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
