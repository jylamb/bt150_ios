//
//  Menu03SubViewController.swift
//  BT150
//
//  Created by KOBONGHWAN on 24/12/2018.
//  Copyright © 2018 bistos. All rights reserved.
//

import UIKit

protocol Menu03SubDelegate {
    func onPopStatisticsEdit(_ a_Idx : Int)
}

class Menu03SubViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate {

    struct tablelist {
        static let kTableViewCellHTop : CGFloat = 40
        static let kTableViewCellH : CGFloat = 70
    }
    
    var delegate : Menu03SubDelegate?
    var nTabIdx : Int = 0
    var tvList = UITableView()
    var nDelIdx = 0
    var dateCurSel = Date()
    let lblDate = UILabel()
    let lblNoData = UILabel()
    var arrData = [PBCommon.MilkData]()
    var nMaxAmount = 0
    var sUnit = "ml"

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func initMenu03Data(_ a_ID : Int) {
        nTabIdx = (a_ID / 100) - 1
        
        initListHeader()
        
        let sizeView = self.view.frame.size
        
        tvList.frame = CGRect(x: 0, y: tablelist.kTableViewCellHTop, width: sizeView.width, height: sizeView.height-tablelist.kTableViewCellHTop)
        tvList.bounces = false
        tvList.separatorStyle = UITableViewCell.SeparatorStyle.none
        tvList.separatorColor = UIColor.orange
        tvList.allowsSelection = true
        tvList.isScrollEnabled = true
        tvList.backgroundColor = UIColor.clear
        tvList.delegate = self
        tvList.dataSource = self
        tvList.tag = a_ID
        tvList.register(UITableViewCell.self, forCellReuseIdentifier: "mycell")
        self.view.addSubview(tvList)
     
        
        lblNoData.frame = tvList.frame
        lblNoData.backgroundColor = UIColor.clear
        lblNoData.font = PBLib.BoldFont(28)
        lblNoData.adjustsFontSizeToFitWidth = true
        lblNoData.textColor = PBLib.RGB([51,51,51])
        lblNoData.textAlignment = NSTextAlignment.center
        lblNoData.text = NSLocalizedString("NoData", comment: "No Data")
        self.view.addSubview(lblNoData)

        
        dateCurSel = PBLib.dateWithOutTime(Date())
        
        print("00 nTabIdx: \(nTabIdx) dateCurSel: \(dateCurSel)")
        
        if (nTabIdx == 1) {
            dateCurSel = Date().startOfMonth()
        } else if(nTabIdx == 2) {
            dateCurSel = Date().startOfYear()

        } else {
            let dtAdd = dateCurSel.addDay(-1)
            let aDayOfWeek = PBLib.dayOfWeek(dtAdd)
            var dtColDay = dtAdd.addingTimeInterval(-60*60*24*Double(aDayOfWeek-1))
            print("dtColDay: \(dtColDay) aDayOfWeek:\(aDayOfWeek)")
        }
        print("01 nTabIdx: \(nTabIdx) dateCurSel: \(dateCurSel)")
        
        refreshDateTitle()
        
        initData()
        
        sUnit =  UserDefaults.standard.string(forKey: "unit") ?? "ml"
        print("############### sUnit: \(sUnit)")
    }
    
    func initData() {
    
//        initMilkAmtData();
//
//        initSeqTime();
//
//        initSearchMonth();
    
        doRefreshData()
    }

    func refreshDateTitle() {
        let dfDate = DateFormatter()
        if nTabIdx == 0 {
            dfDate.dateFormat = "yyyy.MM.dd"
            lblDate.text = dfDate.string(from: dateCurSel)
            
        } else if nTabIdx == 1 {
            dfDate.dateFormat = "yyyy.MM"
            lblDate.text = dfDate.string(from: dateCurSel)

        } else if nTabIdx == 2 {
            dfDate.dateFormat = "yyyy"
            lblDate.text = dfDate.string(from: dateCurSel)
        }
    }

    func initListHeader() {
        
        let vBase = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: tablelist.kTableViewCellHTop))
        vBase.backgroundColor = PBLib.RGB([0xe9, 0xe9, 0xe9])
        self.view.addSubview(vBase)
        
        let sizeBase = vBase.frame.size
        
        lblDate.frame = CGRect(x: 0, y: 0, width: sizeBase.width, height: tablelist.kTableViewCellHTop)
        lblDate.backgroundColor = UIColor.clear
        lblDate.font = PBLib.AppFont(16)
        lblDate.adjustsFontSizeToFitWidth = true
        lblDate.textColor = PBLib.RGB([51,51,51])
        lblDate.textAlignment = NSTextAlignment.center
        lblDate.text = "0000.00.00"
        vBase.addSubview(lblDate)
        
        let btnPrev = UIButton(frame: CGRect(x: sizeBase.width/3-32, y: (sizeBase.height-32)/2, width: 32, height: 32))
        btnPrev.setImage(UIImage(named: "popup_btn_pre.png"), for: UIControl.State())
        btnPrev.addTarget(self, action: #selector(self.onPrev(_:)), for: UIControl.Event.touchUpInside)
        vBase.addSubview(btnPrev)

        let btnNext = UIButton(frame: CGRect(x: sizeBase.width*2/3, y: (sizeBase.height-32)/2, width: 32, height: 32))
        btnNext.setImage(UIImage(named: "popup_btn_next.png"), for: UIControl.State())
        btnNext.addTarget(self, action: #selector(self.onNext(_:)), for: UIControl.Event.touchUpInside)
        vBase.addSubview(btnNext)

    }
    
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tablelist.kTableViewCellH
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "mycell", for: indexPath)
        
        cell.backgroundColor = UIColor.white
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        for vSub in cell.contentView.subviews {
            if vSub.tag >= 100 {
                vSub.removeFromSuperview()
            }
        }
        
        let sizeView = cell.contentView.frame.size
        
//        let aTabIdx = (tableView.tag / 100) - 1
        let aMilkData = arrData[indexPath.row]
        print("lDate:\(aMilkData.lDate)")
        print("nAmtL:\(aMilkData.nAmtL)")
        print("nAmtR:\(aMilkData.nAmtR)")
        print("nTime:\(aMilkData.nTime)")
        
        let vBase = UIView(frame: CGRect(x: 0, y: 0, width: sizeView.width, height: tablelist.kTableViewCellH))
        vBase.backgroundColor = UIColor.white
        vBase.tag = 999
        cell.contentView.addSubview(vBase)
        
        let sizeBase = vBase.frame.size
        let fTextW : CGFloat = sizeBase.width / 6
        let fBarH : CGFloat = sizeBase.height / 7
        
        let lblTimeH = UILabel(frame: CGRect(x: 0, y: 0, width: fTextW, height: sizeBase.height/2))
        lblTimeH.backgroundColor = UIColor.clear
        lblTimeH.font = PBLib.AppFont(16)
        lblTimeH.textColor = PBLib.RGB([51,51,51])
        lblTimeH.textAlignment = NSTextAlignment.center
        lblTimeH.tag = 999
        vBase.addSubview(lblTimeH)
        
        
        let lblTimeL = UILabel(frame: CGRect(x: 0, y: sizeBase.height/2, width: fTextW, height: sizeBase.height/2))
        lblTimeL.backgroundColor = UIColor.clear
        lblTimeL.font = PBLib.AppFont(16)
        lblTimeL.textColor = PBLib.RGB([51,51,51])
        lblTimeL.textAlignment = NSTextAlignment.center
        lblTimeL.tag = 999
        vBase.addSubview(lblTimeL)

        let dfDate = DateFormatter()

        if nTabIdx == 0 {
            lblTimeH.text = NSLocalizedString("Time", comment: "Time")
            dfDate.dateFormat = "HH:mm"
            lblTimeL.text = dfDate.string(from: Date(timeIntervalSince1970: TimeInterval(aMilkData.lDate/1000)))
        
        } else if nTabIdx == 1 {
            lblTimeH.text = NSLocalizedString("Day", comment: "Day")
            dfDate.dateFormat = "dd"
            let aDayS = Date(timeIntervalSince1970: TimeInterval(aMilkData.lDate/1000))
            let aDayE = Date(timeIntervalSince1970: TimeInterval(aMilkData.lDate/1000)).addDay(6)
            lblTimeL.text = "\(dfDate.string(from: aDayS)) ~ \(dfDate.string(from: aDayE))"

        } else if nTabIdx == 2 {
            lblTimeH.text = NSLocalizedString("Month", comment: "Month")
            dfDate.dateFormat = "MM"
            lblTimeL.text = dfDate.string(from: Date(timeIntervalSince1970: TimeInterval(aMilkData.lDate/1000)))
        }
        
        let imgvLeft = UIImageView(image: UIImage(named: "statistics_ic_left.png"))
        imgvLeft.frame = CGRect(x: fTextW, y: (sizeBase.height/2-23)/2, width: 23, height: 23)
        imgvLeft.tag = 999
        cell.contentView.addSubview(imgvLeft)

        let imgvRight = UIImageView(image: UIImage(named: "statistics_ic_right.png"))
        imgvRight.frame = CGRect(x: fTextW, y: sizeBase.height/2 + (sizeBase.height/2-23)/2, width: 23, height: 23)
        imgvRight.tag = 999
        cell.contentView.addSubview(imgvRight)
        
        let lblAmtLeft = UILabel(frame: CGRect(x: fTextW+30, y: 2, width: fTextW, height: fBarH))
        lblAmtLeft.backgroundColor = UIColor.clear
        lblAmtLeft.font = PBLib.AppFont(10)
        lblAmtLeft.textColor = PBLib.RGB([0x04, 0xb5, 0xbb])
        if sUnit == "ml" {
            lblAmtLeft.text = "\(aMilkData.nAmtL)\(sUnit)"
        } else {
            lblAmtLeft.text = String(format: "%.2f\(sUnit)", Double(aMilkData.nAmtL)*GLConst.toOZ)
        }
        lblAmtLeft.tag = 999
        vBase.addSubview(lblAmtLeft)
        
        let barViewLeft = PBBarView()
        barViewLeft.frame = CGRect(x: fTextW+25, y: (sizeBase.height/2-fBarH)/2, width: sizeBase.width*2/3-20, height: fBarH)
        barViewLeft.backgroundColor = UIColor.clear
        barViewLeft.tag = 999
        cell.contentView.addSubview(barViewLeft)
        barViewLeft.fPercent = Float(aMilkData.nAmtL) / Float(nMaxAmount)
        barViewLeft.clBar = PBLib.RGB([0x04, 0xb5, 0xbb])
        barViewLeft.setNeedsDisplay()

        let lblAmtRight = UILabel(frame: CGRect(x: fTextW+30, y: 2+sizeBase.height/2, width: fTextW, height: fBarH))
        lblAmtRight.backgroundColor = UIColor.clear
        lblAmtRight.font = PBLib.AppFont(10)
        lblAmtRight.textColor = PBLib.RGB([0x04, 0xb5, 0xbb])
        if sUnit == "ml" {
            lblAmtRight.text = "\(aMilkData.nAmtR)\((sUnit))"
        } else {
            lblAmtRight.text = String(format: "%.2f\(sUnit)", Double(aMilkData.nAmtR)*GLConst.toOZ)
            
        }
        lblAmtRight.tag = 999
        vBase.addSubview(lblAmtRight)

        let barViewRight = PBBarView()
        barViewRight.frame = CGRect(x: fTextW+25, y: sizeBase.height/2+(sizeBase.height/2-fBarH)/2, width: sizeBase.width*2/3-20, height: fBarH)
        barViewRight.backgroundColor = UIColor.clear
        barViewRight.tag = 999
        cell.contentView.addSubview(barViewRight)
        barViewRight.fPercent = Float(aMilkData.nAmtR) / Float(nMaxAmount)
        barViewRight.clBar = PBLib.RGB([0x9f, 0xbb, 0x04])
        barViewRight.setNeedsDisplay()

        
        let aMin = Int(aMilkData.nTime / 60)
        let aSec = Int(aMilkData.nTime % 60)

        let lblTimeMilk = UILabel(frame: CGRect(x: sizeBase.width-fTextW, y: 0, width: fTextW, height: sizeBase.height/2))
        lblTimeMilk.backgroundColor = UIColor.clear
        lblTimeMilk.font = PBLib.AppFont(16)
        lblTimeMilk.textColor = PBLib.RGB([51,51,51])
        lblTimeMilk.text = String(format: "%02d:%02d", aMin, aSec)
        lblTimeMilk.textAlignment = NSTextAlignment.center
        lblTimeMilk.tag = 999
        vBase.addSubview(lblTimeMilk)
        
        if nTabIdx == 0 {
            let btnDel = UIButton(frame: CGRect(x: sizeBase.width-40, y: sizeBase.height/2+(sizeBase.height/2-32)/2-5, width: 32, height: 32))
            btnDel.setImage(UIImage(named: "program_btn_del.png"), for: UIControl.State())
            btnDel.addTarget(self, action: #selector(self.onDel(_:)), for: UIControl.Event.touchUpInside)
            btnDel.tag = (indexPath as NSIndexPath).row + 100
            vBase.addSubview(btnDel)
        }
        
        let vLine01 = UIView(frame: CGRect(x: 0, y: sizeBase.height - 1, width: sizeBase.width, height: 1))
        vLine01.backgroundColor = GLColor.kThemeLine
        vLine01.tag = 999
        cell.contentView.addSubview(vLine01)
    
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if nTabIdx == 0 {
            delegate?.onPopStatisticsEdit(indexPath.row)
        }        
    }
    
    @objc func onDel(_ a_Btn : UIButton) {
        nDelIdx = a_Btn.tag - 100
        let alert = UIAlertView(title: "",
                                message: NSLocalizedString("wantToDelete", comment: "want To Delete"),
                                delegate: self,
                                cancelButtonTitle: NSLocalizedString("NO", comment: "NO"),
                                otherButtonTitles: NSLocalizedString("YES", comment: "YES"))
        alert.tag = 100
        alert.show()
    }
    
    func alertView(_ alertView: UIAlertView, didDismissWithButtonIndex buttonIndex: Int) {
        if alertView.tag == 100 {
            if buttonIndex == 1 {
                let dbHelper = DBHelper()
                dbHelper.deleteMilkData(Int(arrData[nDelIdx].lDate))
                _ = readDBData()
                tvList.reloadData()
            }
        }
    }
    
    @objc func onPrev(_ a_Button : UIButton) {
        moveReferenceDate(-1)
    }

    @objc func onNext(_ a_Button : UIButton) {
        moveReferenceDate(1)
    }
    
    func moveReferenceDate(_ a_Dir : Int) {

        if a_Dir < 0 {
            if nTabIdx == 0 {
                dateCurSel = dateCurSel.addDay(-1)
            } else if nTabIdx == 1 {
                dateCurSel = dateCurSel.addMonth(-1)
            } else if nTabIdx == 2 {
                dateCurSel = dateCurSel.addYear(-1)
            }
        } else {
            if nTabIdx == 0 {
                dateCurSel = dateCurSel.addDay(1)
            } else if nTabIdx == 1 {
                dateCurSel = dateCurSel.addMonth(1)
            } else if nTabIdx == 2 {
                dateCurSel = dateCurSel.addYear(1)
            }
        }
    
        refreshDateTitle();
    
        doRefreshData();
    }
    
    func doRefreshData() {
        print("doRefreshData-1")
        if (readDBData() <= 0) {
//            initSampleTestData();
        }
        tvList.reloadData()
    }
    
    func readDBData() -> Int {
    
        arrData.removeAll()

        let dfDate = DateFormatter()
        dfDate.locale = Locale(identifier: "ko_KR")
        dfDate.timeZone = TimeZone(identifier: "GMT")!
        dfDate.dateFormat = "yyyy-MM-dd"
        
        let dfTime = DateFormatter()
        dfTime.locale = Locale(identifier: "ko_KR")
        dfTime.timeZone = TimeZone(identifier: "GMT")!
        dfTime.dateFormat = "yyyy-MM-dd HH:mm:ss"
    
        var aFrom = ""
        var aTo = ""
        var aFromTime : Int64 = 0
        var aToTime : Int64 = 0
    
        let dbHelper = DBHelper()

        aFrom = dfDate.string(from: dateCurSel) + " 00:00:00"
        aFromTime = PBLib.convertStringToDate(format: "yyyy-MM-dd HH:mm:ss", Date: aFrom).getTimeMillis()

        if nTabIdx == 0 {
            //Day
            aTo = dfDate.string(from: dateCurSel) + " 23:59:59"
            
            aToTime = PBLib.convertStringToDate(format: "yyyy-MM-dd HH:mm:ss", Date: aTo).getTimeMillis()
            
            print("aFrom:\(aFrom) aFromTime:\(aFromTime) aTo:\(aTo) aToTime:\(aToTime)")
            
            for aData in dbHelper.getMilkData(from: aFromTime, to: aToTime) {
                if let aRet = aData as? [String : Any] {
                    let aDate = aRet["date"] as! Int64
                    let aLeft = aRet["left"] as! Int
                    let aRight = aRet["right"] as! Int
                    let aTime = aRet["time"] as! Int64
                    print("readdbdata date: \(Date(timeIntervalSince1970: TimeInterval(aDate/1000))) aDate:\(aDate)")
                    let aMilkData = PBCommon.MilkData(Date: aDate, AmtL: aLeft, AmtR: aRight, Time: aTime)
                    arrData.append(aMilkData)
                }
            }
    
        } else {
            //Week & Month
            if nTabIdx == 1 {
                aToTime = dateCurSel.addMonth(1).getTimeMillis() - 1
            } else if nTabIdx == 2 {
                aToTime = dateCurSel.addYear(1).getTimeMillis() - 1
            }
            
//            print("aFromTime:\(Date(timeIntervalSince1970: TimeInterval(aFromTime/1000))) aToTime:\(Date(timeIntervalSince1970: TimeInterval(aToTime/1000)))")
            for aData in dbHelper.getMilkData(from: aFromTime, to: aToTime) {
                if let aRet = aData as? [String : Any] {
                    let aDate = aRet["date"] as! Int64
                    let aLeft = aRet["left"] as! Int
                    let aRight = aRet["right"] as! Int
                    let aTime = aRet["time"] as! Int64
                    let aMilkData = PBCommon.MilkData(Date: aDate, AmtL: aLeft, AmtR: aRight, Time: aTime)
                    addWeekMonthData(aMilkData)
                }
            }
        }
        
        for i in 0 ..< arrData.count {
            if nMaxAmount < arrData[i].nAmtL {
                nMaxAmount = arrData[i].nAmtL
            }
            if nMaxAmount < arrData[i].nAmtR {
                nMaxAmount = arrData[i].nAmtR
            }
        }
        
        print("readDBData Cnt:\(arrData.count) nMaxAmount:\(nMaxAmount)")


        if arrData.count > 0 {
            tvList.isHidden = false
            lblNoData.isHidden = true
        } else {
            tvList.isHidden = true
            lblNoData.isHidden = false
        }
        
//        printReadDBData()
        
        return arrData.count
    }

    func getFirstDayOfWeek(_ a_Date : Int64) -> Date {
        let dtDay = Date(timeIntervalSince1970: TimeInterval(a_Date / 1000))
        let aDayOfWeek = PBLib.dayOfWeek(dtDay as Date)
        let dtFirstDayOfWeek = dtDay.addingTimeInterval(-60*60*24*Double(aDayOfWeek-1))
//        print("dtFirstDayOfWeek: \(dtFirstDayOfWeek) aDayOfWeek:\(aDayOfWeek)")
        return dtFirstDayOfWeek
    }

    func addWeekMonthData(_ a_MilkData : PBCommon.MilkData) {
    
        var aDateAdd = 0
        let dfDate = DateFormatter()

        if nTabIdx == 1 {
            //Week
            dfDate.dateFormat = "yyyyMMdd"
            let aDate = dfDate.string(from: getFirstDayOfWeek(a_MilkData.lDate))
            aDateAdd = Int(aDate)!
    
        } else if nTabIdx == 2 {
            //Month
            dfDate.dateFormat = "yyyyMM"
            let aDate = dfDate.string(from: Date(timeIntervalSince1970: TimeInterval(a_MilkData.lDate / 1000)))
            aDateAdd = Int(aDate)!
        }
//        print("addWeekMonthData: aDateAdd:\(aDateAdd) nTabIdx:\(nTabIdx) arrData.count:\(arrData.count)")
    
        var aFound = -1
        for i in 0 ..< arrData.count {
            let milkData = arrData[i]
            var aDate = dfDate.string(from: Date(timeIntervalSince1970: TimeInterval(milkData.lDate / 1000)))
            if nTabIdx == 1 {
                aDate = dfDate.string(from: getFirstDayOfWeek(a_MilkData.lDate))
                print("aDate:\(aDate)")
            }
            if Int(aDate)! == aDateAdd {
                aFound = i
                break
            }
        }
    
        if aFound < 0 {
            let aDate = getFirstDayOfWeek(a_MilkData.lDate).getTimeMillis()
            arrData.append(PBCommon.MilkData(Date: aDate, AmtL: a_MilkData.nAmtL, AmtR: a_MilkData.nAmtR, Time: a_MilkData.nTime))
            aFound = arrData.count - 1
        
        } else {
            let milkdate = arrData[aFound]
            milkdate.nAmtL += a_MilkData.nAmtL
            milkdate.nAmtR += a_MilkData.nAmtR
            milkdate.nTime += a_MilkData.nTime
        }
    }
    
    func printReadDBData() {
        
        let dfDate = DateFormatter()
        dfDate.dateFormat = "yyyy.MM.dd HH:mm:ss"
        
        for aData in arrData {
            let aDate = dfDate.string(from: Date(timeIntervalSince1970: TimeInterval(aData.lDate/1000)))
            print("printReadDBData date:\(aDate) left:\(aData.nAmtL) right:\(aData.nAmtR) time:\(aData.nTime)")
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
