//
//  Menu01ViewController.swift
//  BT150
//
//  Created by KOBONGHWAN on 14/11/2018.
//  Copyright © 2018 bistos. All rights reserved.
//

import UIKit
import Charts

class Menu01ViewController: UIViewController, ChartViewDelegate {

    var vcTopMenu : TopMenuViewController?
    var bRecvFirst : Bool = false
    
    var mTimerElapsed : Timer?

    @IBOutlet weak var chartviewTimer: PieChartView!
    @IBOutlet weak var chartviewPressure: PieChartView!
    @IBOutlet weak var chartviewCycle: PieChartView!
    
    @IBOutlet weak var imgvBattery: UIImageView!
    @IBOutlet weak var btnExpression: UIButton!
    @IBOutlet weak var btnMassage: UIButton!
    @IBOutlet weak var btnSingle: UIButton!
    @IBOutlet weak var btnDouble: UIButton!

    @IBOutlet weak var txtPressureMin: UILabel!
    @IBOutlet weak var txtPressureMax: UILabel!
    
    @IBOutlet weak var txtCycleMin: UILabel!
    @IBOutlet weak var txtCycleMax: UILabel!
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "embedMenuSegue" {
            if let vc = segue.destination as? TopMenuViewController {
                vcTopMenu = vc
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("Menu01 viewDidLoad-1")
        
        initPressureChartView()
        
        initCycleChartView()
        
        initTimerChartView()
        
    }
    
    @objc func onRecvVCmd(_ a_Noti : NSNotification) {
        print("Menu01 onRecvVCmd-1")
        
        var aCmdCode = 0
        var aCmdType = VoiceCommand.CMD_TYPE_NORMAL
        var aStepVal = 0
        
        if let aRet = a_Noti.userInfo?["CmdCode"] as? Int {
            aCmdCode = aRet
        }
        if let aRet = a_Noti.userInfo?["CmdType"] as? Int {
            aCmdType = aRet
        }
        if let aRet = a_Noti.userInfo?["StepVal"] as? Int {
            aStepVal = aRet
        }
        
//        print("onReceive: aCmdCode:\(aCmdCode) aCmdType:\(aCmdType) aStepVal:\(aStepVal)")
        
        if aCmdType == VoiceCommand.CMD_TYPE_NORMAL {
            switch aCmdCode {
            case VoiceCommand.CMD_POWER:
                PBCommon.powerDown()
                break
            case VoiceCommand.CMD_LAMP:
                PBCommon.toggleLamp()
                break
            case VoiceCommand.CMD_MODE_CHANGE:
                PBCommon.sendData(PBCommon.sCmdModeChange)
                break
//                    case CMD_MODE_EXPRESS:
//                    case CMD_EXPRESS:
//                        selectOperation(PHCommon.OP_MODE_EXP);
//                        break;
//                    case CMD_MODE_MASSAGE:
//                    case CMD_MASSAGE:
//                        selectOperation(PHCommon.OP_MODE_MSG);
//                        break;
            case VoiceCommand.CMD_PRESSURE_UP:
                doPressureUp("")
                break
            case VoiceCommand.CMD_PRESSURE_DOWN:
                doPressureDown("")
                break
            case VoiceCommand.CMD_CYCLE_UP:
                doCycleUp("")
                break
            case VoiceCommand.CMD_CYCLE_DOWN:
                doCycleDown("")
                break
            default:
                break
            }
            
        } else if aCmdType == VoiceCommand.CMD_TYPE_STEP {
//                switch (aCmdCode) {
//                    case CMD_PRESSURE_N:
//                        doPressureN(aStepVal);
//                        break;
//                    case CMD_CYCLE_N:
//                        doCycleN(aStepVal);
//                        break;
//                    default:
//                        break;
//                }
        }
    }
    
    @objc func onSRRsult(_ a_Noti : NSNotification) {
        print("Menu01 onSRRsult-1")
        if let aData = a_Noti.userInfo?["String"] as? String {
            if let aResult = a_Noti.userInfo?["Result"] as? Bool {
                if aResult {
                    Toast.show(message: aData, baseView: self.view)
                } else {
                    Toast.show(message: "Recognizing Fail:" + aData, baseView: self.view)
                }
            }
        }
    }
    
    @objc func onRecvData(_ a_Noti : NSNotification) {
        
        if let aBattery = a_Noti.userInfo?["recv_battery"] as? String {
            parseRecvBattery(aBattery)
        
        } else if let a_Data = a_Noti.userInfo?["recv_data"] as? String {
            parseRecvData(a_Data)
        }
    }
    
    func parseRecvData(_ a_Recv : String) {
        
//        print("Menu01 >>>>>>>>>>> parseRecvData: \(a_Recv)")
        
        if (a_Recv.count == 16
            && a_Recv.substring(to: 1) == "M"
            && a_Recv.substring(with: 2..<3) == "P"
            && a_Recv.substring(with: 5..<6) == "C"
            && a_Recv.substring(with: 7..<8) == "R") {

            bRecvFirst = true;
            
            PBCommon.nOperationMode = Int(a_Recv.substring(with: 1..<2)) ?? PBCommon.OP_MODE_NONE
            
            if a_Recv.count > 15 && a_Recv.substring(with: 13..<14) == "W" {
                let aOpType = a_Recv.substring(with: 14..<15)
                if aOpType == "0" {
                    PBCommon.nOperationType = PBCommon.OP_TYPE_SINGLE
                } else {
                    PBCommon.nOperationType = PBCommon.OP_TYPE_DOUBLE
                }
            }
            
            if (PBCommon.nOperationMode == PBCommon.OP_MODE_EXP) {
                PBCommon.nPressureExp = Int(a_Recv.substring(with: 3..<5)) ?? 0
                PBCommon.nCycleExp = Int(a_Recv.substring(with: 6..<7)) ?? 0
                
            } else if (PBCommon.nOperationMode == PBCommon.OP_MODE_MSG) {
                PBCommon.nPressureMsg = Int(a_Recv.substring(with: 3..<5)) ?? 0
                PBCommon.nCycleMsg = Int(a_Recv.substring(with: 6..<7)) ?? 0
            }
            
            DispatchQueue.main.async {
                self.selectOperation(PBCommon.nOperationMode)
                self.selectOperationType(PBCommon.nOperationType)
            }
        }
    }
    
    func parseRecvBattery(_ a_Battery : String) {
     
//        print("Menu01 >>>>>>>>>>> parseRecvBattery: \(a_Battery)")
        
        PBCommon.gRecvBattery = a_Battery
        
        DispatchQueue.main.async {
            if a_Battery == "0" {
                self.imgvBattery.image = UIImage(named: "operation_b_0.png")
                
            } else if a_Battery == "1" {
                self.imgvBattery.image = UIImage(named: "operation_b_1.png")
                
            } else if a_Battery == "2" {
                self.imgvBattery.image = UIImage(named: "operation_b_2.png")
                
            } else if a_Battery == "3" {
                self.imgvBattery.image = UIImage(named: "operation_b_3.png")
                
            } else if a_Battery == "4" {
                self.imgvBattery.image = UIImage(named: "operation_b_4.png")
                
            } else if a_Battery == "5" {
                self.imgvBattery.image = UIImage(named: "operation_b_5.png")
            }
        }
    }

    func selectOperationType(_ a_Type : Int) {
        selectOperationType(a_Type, Disp: true)
    }
    
    func selectOperationType(_ a_Type : Int, Disp a_Disp : Bool) {
        
        if a_Type != PBCommon.nOperationType {
            if !a_Disp && !PBCommon.bProgramRunning {
                PBCommon.sendData("W;")
            }
        }
    
        if a_Type == PBCommon.OP_TYPE_SINGLE {
            btnSingle.isSelected = true
            btnDouble.isSelected = false
            PBCommon.nOperationType = PBCommon.OP_TYPE_SINGLE
    
        } else {
            btnSingle.isSelected = false
            btnDouble.isSelected = true
            PBCommon.nOperationType = PBCommon.OP_TYPE_DOUBLE
        }
    
    }
    
    func selectOperation(_ a_Operation : Int) {
        if a_Operation == PBCommon.OP_MODE_EXP {
            if PBCommon.nOperationMode == PBCommon.OP_MODE_MSG {
                PBCommon.sendData(PBCommon.sCmdModeChange)
            }

            btnExpression.isSelected = true
            btnMassage.isSelected = false
            
            PBCommon.nOperationMode = PBCommon.OP_MODE_EXP
            refreshOperationData()
            
        } else if a_Operation == PBCommon.OP_MODE_MSG {
            if PBCommon.nOperationMode == PBCommon.OP_MODE_EXP {
                PBCommon.sendData(PBCommon.sCmdModeChange)
            }
            
            btnExpression.isSelected = false
            btnMassage.isSelected = true
            
            PBCommon.nOperationMode = PBCommon.OP_MODE_MSG
            refreshOperationData()
        }

    }

    func refreshOperationData() {
//        print("########## refreshOperationData ###########")
        refreshChart(chartviewPressure, Type: PBCommon.OP_PRESSURE)
        refreshChart(chartviewCycle, Type: PBCommon.OP_CYCLE)
        refreshChart(chartviewTimer, Type: PBCommon.OP_TIMER)
    }
    
    func refreshChart(_ a_Chart : PieChartView, Type a_Type : Int) {
        var aChartValue : Int = 0
        var aValueData : Float = 0
        var aValueMax : Int = 0
        
        if PBCommon.nOperationMode == PBCommon.OP_MODE_MSG {
            reCalcPressureValue()
            
            txtPressureMin.text = String(format: "%d", PBCommon.MSG_PRESSURE_MIN + 1)
            txtPressureMax.text = String(format: "%d", PBCommon.arrPressureLimit[PBCommon.nCycleMsg] + 1)
            
            txtCycleMin.text = String(format: "%d", PBCommon.MSG_CYCLE_MIN + 1)
            txtCycleMax.text = String(format: "%d", PBCommon.MSG_CYCLE_MAX + 1)
            
        } else if (PBCommon.nOperationMode == PBCommon.OP_MODE_EXP) {
            txtPressureMin.text = String(format: "%d", PBCommon.EXP_PRESSURE_MIN + 1)
            txtPressureMax.text = String(format: "%d", PBCommon.EXP_PRESSURE_MAX + 1)
            
            txtCycleMin.text = String(format: "%d", PBCommon.EXP_CYCLE_MIN + 1)
            txtCycleMax.text = String(format: "%d", PBCommon.EXP_CYCLE_MAX + 1)
        }
        
        if a_Type == PBCommon.OP_PRESSURE {
            if PBCommon.nOperationMode == PBCommon.OP_MODE_EXP {
                aChartValue = PBCommon.nPressureExp + 1
                aValueData =  Float(aChartValue) / Float(PBCommon.EXP_PRESSURE_MAX + 1)
                aValueMax = PBCommon.EXP_PRESSURE_MAX + 1
                
            } else if PBCommon.nOperationMode == PBCommon.OP_MODE_MSG {
                aChartValue = PBCommon.nPressureMsg + 1
                aValueData = Float(aChartValue) / Float(PBCommon.arrPressureLimit[PBCommon.nCycleMsg] + 1)
                aValueMax = PBCommon.arrPressureLimit[PBCommon.nCycleMsg] + 1
            }
            
        } else if (a_Type == PBCommon.OP_CYCLE) {
            if PBCommon.nOperationMode == PBCommon.OP_MODE_EXP {
                aChartValue = PBCommon.nCycleExp + 1
                aValueData =  Float(aChartValue) / Float(PBCommon.EXP_CYCLE_MAX + 1)
                aValueMax = PBCommon.EXP_CYCLE_MAX + 1
                
            } else if PBCommon.nOperationMode == PBCommon.OP_MODE_MSG {
                aChartValue = PBCommon.nCycleMsg + 1
                aValueData = Float(aChartValue) / Float(PBCommon.MSG_CYCLE_MAX + 1)
                aValueMax = PBCommon.MSG_CYCLE_MAX + 1
            }
            
        } else if (a_Type == PBCommon.OP_TIMER) {
            aChartValue = PBCommon.nElapsedTime
            aValueData = Float(aChartValue) / Float(PBCommon.ELAPSED_TIME_MAX)
            aValueMax = PBCommon.ELAPSED_TIME_MAX / 60
        }
        
        if a_Type == PBCommon.OP_CYCLE {
            if PBCommon.nOperationMode == PBCommon.OP_MODE_EXP {
                a_Chart.centerAttributedText = generateCenterSpannableText(String(format: "%d", PBCommon.nCycleExp + 1))
            
            } else if PBCommon.nOperationMode == PBCommon.OP_MODE_MSG {
                a_Chart.centerAttributedText = generateCenterSpannableText(String(format: "%d", PBCommon.nCycleMsg + 1))
            }
            
        } else if a_Type == PBCommon.OP_PRESSURE {
            a_Chart.centerAttributedText = generateCenterSpannableText(String(format: "%d", aChartValue))
            
        } else if a_Type == PBCommon.OP_TIMER {
            let aMin : Int = aChartValue / 60
            let aSec : Int = aChartValue % 60
            a_Chart.centerAttributedText = generateCenterSpannableText(String(format: "%02d:%02d", aMin, aSec), FontSize: 20)
            
        } else {
            a_Chart.centerAttributedText = generateCenterSpannableText(String(format: "%d", aChartValue + 1), FontSize: 20)
        }
        
        setChartData(aValueData, SliceCnt: aValueMax, Chart: a_Chart)
    }

    func generateCenterSpannableText(_ a_Text : String) -> NSAttributedString {
        return generateCenterSpannableText(a_Text, FontSize: 28)
    }

    func generateCenterSpannableText(_ a_Text : String, FontSize a_Size : Int) -> NSAttributedString {
        let centerText = NSMutableAttributedString(string: a_Text)
        centerText.addAttributes([.font : PBLib.BoldFont(CGFloat(a_Size)), .foregroundColor : PBLib.RGB([0xff, 0xa5, 0x00])], range: NSRange(location: 0, length: centerText.length))
        return centerText
    }
    
    func setChartData(_ a_Value : Float, SliceCnt a_SliceCnt : Int, Chart a_Chart : PieChartView) {
        if a_Chart.isEqual(chartviewTimer) {
            setDataCountTimer(a_SliceCnt, Value: a_Value)
            
        } else if a_Chart.isEqual(chartviewCycle) {
            setDataCountCycle(a_SliceCnt, Value: a_Value)
            
        } else if a_Chart.isEqual(chartviewPressure) {
            setDataCountPressure(a_SliceCnt, Value: a_Value)
        }
    }
    
    func reCalcPressureValue() {
        if PBCommon.nCycleMsg >= 0 && PBCommon.nCycleMsg < PBCommon.arrPressureLimit.count {
            let aPressure = PBCommon.arrPressureLimit[PBCommon.nCycleMsg]
            if PBCommon.nPressureMsg > aPressure {
                PBCommon.nPressureMsg = aPressure
            }
        }
    }

    func initPressureChartView() {

        self.setupPressureChart(pieChartView: chartviewPressure)
        
        chartviewPressure.delegate = self
        chartviewPressure.holeRadiusPercent = 0.58
        chartviewPressure.rotationEnabled = false
        chartviewPressure.highlightPerTapEnabled = true
        chartviewPressure.maxAngle = 180 // Half chart
        chartviewPressure.rotationAngle = 180 // Rotate to make the half on the upper side
        chartviewPressure.centerTextOffset = CGPoint(x: 0, y: -10)
        chartviewPressure.animate(xAxisDuration: 1.4, easingOption: .easeOutBack)
        
        self.setDataCountPressure(16)
    }
    
    func initCycleChartView() {
        
        self.setupCycleChart(pieChartView: chartviewCycle)
        
        chartviewCycle.delegate = self
        chartviewCycle.holeRadiusPercent = 0.58
        chartviewCycle.rotationEnabled = false
        chartviewCycle.highlightPerTapEnabled = true
        chartviewCycle.maxAngle = 180 // Half chart
        chartviewCycle.rotationAngle = 180 // Rotate to make the half on the upper side
        chartviewCycle.centerTextOffset = CGPoint(x: 0, y: -10)
        chartviewCycle.animate(xAxisDuration: 1.4, easingOption: .easeOutBack)
        
        self.setDataCountCycle(6)
    }

    
    func initTimerChartView() {

        self.setupTimerChart(pieChartView: chartviewTimer)
        chartviewTimer.delegate = self
        chartviewTimer.legend.enabled = false
        chartviewTimer.animate(xAxisDuration: 1.4, easingOption: .easeOutBack)
        
        chartviewTimer.centerAttributedText = generateCenterSpannableText(String(format: "%02d:%02d", 0, 0), FontSize: 20)
        
        self.setDataCountTimer(30)
    }
        
    override func viewWillAppear(_ animated: Bool) {
        
        PBCommon.nActivityNo = PBCommon.ACT_MENU01
        PBCommon.nVoiceMode = PBCommon.VOICE_MODE_OPERATION
        
        vcTopMenu?.ivOperation.image = UIImage(named: "menu_ic_2_ov.png")
        vcTopMenu?.ivProgram.image = UIImage(named: "menu_ic_4.png")
        vcTopMenu?.ivStatistic.image = UIImage(named: "menu_ic_5.png")
             
        NotificationCenter.default.addObserver(self, selector: #selector(onRecvData(_:)), name: Notification.Name("MSG_DATA_RECV"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onSRRsult(_:)), name: Notification.Name("MSG_SR_RESULT"), object: nil)
        if !PBCommon.bMenu01VCmdReady {
            PBCommon.bMenu01VCmdReady = true
            NotificationCenter.default.addObserver(self, selector: #selector(onRecvVCmd(_:)), name: Notification.Name("MSG_VCMD_RECV"), object: nil)
        }

        stopElapsedTimer()
        
        startElapsedTimer()
        
        PBCommon.sendData(PBCommon.sCmdInqueryStatus)
//        print("Menu01 viewWillAppear")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
    }
    
    override func viewDidAppear(_ animated: Bool) {
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
        stopElapsedTimer()
    }

    func stopElapsedTimer() {
        mTimerElapsed?.invalidate()
        mTimerElapsed = nil
    }
    
    func startElapsedTimer() {
        
        stopElapsedTimer()
        
        mTimerElapsed = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(taskElapsed), userInfo: nil, repeats: true)
    }
    
    @objc func taskElapsed() {
        
        if let aPeripheral = PBCommon.gPeripheral {
            if aPeripheral.isConnected && PBCommon.bDeviceWakeUp {
                PBCommon.nElapsedTime += 1
                if PBCommon.nElapsedTime > PBCommon.ELAPSED_TIME_MAX {
                   PBCommon.nElapsedTime = PBCommon.ELAPSED_TIME_MAX
                }
            
                DispatchQueue.main.async {
                    self.refreshChart(self.chartviewTimer, Type: PBCommon.OP_TIMER)
                }
            }
        }
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func setupPressureChart(pieChartView chartView: PieChartView) {
        
        chartView.usePercentValuesEnabled = true
        chartView.drawSlicesUnderHoleEnabled = false
        chartView.holeRadiusPercent = 0.58
        chartView.transparentCircleRadiusPercent = 0.61
        chartView.chartDescription?.enabled = false
        chartView.backgroundColor = PBLib.RGBA([0,0,0,0])
        
        chartView.setExtraOffsets(left: 5, top: 10, right: 5, bottom: 5)
        
        chartView.drawCenterTextEnabled = true
        
        let paragraphStyle = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        paragraphStyle.lineBreakMode = .byTruncatingTail
        paragraphStyle.alignment = .center
        
        
        let centerText = NSMutableAttributedString(string: "1")
        centerText.addAttributes([.font : PBLib.BoldFont(28), .foregroundColor : PBLib.RGB([0xff, 0xa5, 0x00])], range: NSRange(location: 0, length: centerText.length))
        chartView.centerAttributedText = centerText
        
        chartView.drawHoleEnabled = true
        chartView.rotationAngle = 0
        chartView.rotationEnabled = true
        chartView.highlightPerTapEnabled = true
        
        let l = chartView.legend
        l.enabled = false
    }

    func setupCycleChart(pieChartView chartView: PieChartView) {
        
        chartView.usePercentValuesEnabled = true
        chartView.drawSlicesUnderHoleEnabled = false
        chartView.holeRadiusPercent = 0.58
        chartView.transparentCircleRadiusPercent = 0.61
        chartView.chartDescription?.enabled = false
        chartView.backgroundColor = PBLib.RGBA([0,0,0,0])
        
        chartView.setExtraOffsets(left: 5, top: 10, right: 5, bottom: 5)
        
        chartView.drawCenterTextEnabled = true
        
        let paragraphStyle = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        paragraphStyle.lineBreakMode = .byTruncatingTail
        paragraphStyle.alignment = .center
        
        
        let centerText = NSMutableAttributedString(string: "1")
        centerText.addAttributes([.font : PBLib.BoldFont(28), .foregroundColor : PBLib.RGB([0xff, 0xa5, 0x00])], range: NSRange(location: 0, length: centerText.length))
        chartView.centerAttributedText = centerText
        
        chartView.drawHoleEnabled = true
        chartView.rotationAngle = 0
        chartView.rotationEnabled = true
        chartView.highlightPerTapEnabled = true
        
        let l = chartView.legend
        l.enabled = false
    }
    
    func setupTimerChart(pieChartView chartView: PieChartView) {
        
        chartView.usePercentValuesEnabled = true
        chartView.chartDescription?.enabled = false
        chartView.drawHoleEnabled = true
        chartView.holeColor = UIColor.white
        chartView.transparentCircleColor = UIColor.white
        chartView.holeRadiusPercent = 0.58
        chartView.transparentCircleRadiusPercent = 0.61
        chartView.drawCenterTextEnabled = true
        chartView.rotationEnabled = false
        chartView.highlightPerTapEnabled = false
        
        
        chartView.drawSlicesUnderHoleEnabled = false
        chartView.backgroundColor = UIColor.white
        chartView.setExtraOffsets(left: 5, top: 10, right: 5, bottom: 5)
        chartView.rotationAngle = 0
        
        let l = chartView.legend
        l.enabled = false
        
        let paragraphStyle = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        paragraphStyle.lineBreakMode = .byTruncatingTail
        paragraphStyle.alignment = .center
        
        let centerText = NSMutableAttributedString(string: "00:00")
        centerText.addAttributes([.font : PBLib.BoldFont(28), .foregroundColor : PBLib.RGB([0xff, 0xa5, 0x00])], range: NSRange(location: 0, length: centerText.length))
        chartView.centerAttributedText = centerText
    }
    
    func updateChartData() {
//        if self.shouldHideData {
//            chartView.data = nil
//            return
//        }
//        self.setDataCount(Int(sliderX.value), range: UInt32(sliderY.value))
    }
    
    func setDataCountPressure(_ count: Int) {
        setDataCountPressure(count, Value: 0)
    }
    
    func setDataCountPressure(_ count: Int, Value a_Value : Float) {
        
        var arrColors = [NSUIColor](repeating: PBLib.RGB([0xb2, 0xac, 0xa1]), count:   count)
        
        let entries = (0..<count).map { (i) -> PieChartDataEntry in
            return PieChartDataEntry(value: Double(1), label: "")
        }
        
        let set = PieChartDataSet(values: entries, label: NSLocalizedString("Pressure", comment: "Pressure"))
        set.sliceSpace = 1
        
        let aValueIdx = Int((Float(count) * a_Value))
        for i in 0 ..< arrColors.count {
            if i < aValueIdx {
                arrColors[i] = PBLib.RGB([0xff, 0xa5, 0x00])
            } else {
                arrColors[i] = PBLib.RGB([0xb2, 0xac, 0xa1])
            }
        }
        set.colors = arrColors
        
        let data = PieChartData(dataSet: set)
        data.setValueTextColor(PBLib.RGBA([0xb2, 0xac, 0xa1, 0]))
        
        chartviewPressure.data = data
        
        chartviewPressure.setNeedsDisplay()
    }

    func setDataCountCycle(_ count: Int) {
        setDataCountCycle(count, Value: 0)
    }
    
    func setDataCountCycle(_ count: Int, Value a_Value : Float) {
        
        var arrColors = [NSUIColor](repeating: PBLib.RGB([0xb2, 0xac, 0xa1]), count:   count)
        
        let entries = (0..<count).map { (i) -> PieChartDataEntry in
            return PieChartDataEntry(value: Double(1), label: "")
        }
        
        let set = PieChartDataSet(values: entries, label: "Cycle")
        set.sliceSpace = 1
        
        let aValueIdx = Int((Float(count) * a_Value))
//        print("setDataCountCycle    ************** Value: \(a_Value) Max:\(count) aValueIdx:\(aValueIdx)")

        for i in 0 ..< arrColors.count {
            if i < aValueIdx {
                arrColors[i] = PBLib.RGB([0xff, 0xa5, 0x00])
            } else {
                arrColors[i] = PBLib.RGB([0xb2, 0xac, 0xa1])
            }
        }
        set.colors = arrColors

        let data = PieChartData(dataSet: set)
        data.setValueTextColor(PBLib.RGBA([0xb2, 0xac, 0xa1, 0]))
        
        chartviewCycle.data = data
        
        chartviewCycle.setNeedsDisplay()
    }
    
    func setDataCountTimer(_ count: Int) {
        setDataCountTimer(count, Value: 0)
    }
    
    func setDataCountTimer(_ count: Int, Value a_Value : Float) {
        
        var arrColors = [NSUIColor](repeating: PBLib.RGB([0xb2, 0xac, 0xa1]), count:   count)
        
        let entries = (0..<count).map { (i) -> PieChartDataEntry in
            // IMPORTANT: In a PieChart, no values (Entry) should have the same xIndex (even if from different DataSets), since no values can be drawn above each other.
            return PieChartDataEntry(value: Double(1), label: "")
        }
        
        let set = PieChartDataSet(values: entries, label: "")
        set.drawIconsEnabled = false
        set.sliceSpace = 1
        
        let aValueIdx = Int((Float(count) * a_Value))
//        print("setDataCountTimer    ************** Value: \(a_Value) Max:\(count) aValueIdx:\(aValueIdx)")

        for i in 0 ..< arrColors.count {
            if i < aValueIdx {
                arrColors[i] = PBLib.RGB([0xff, 0xa5, 0x00])
            } else {
                arrColors[i] = PBLib.RGB([0xb2, 0xac, 0xa1])
            }
        }
        set.colors = arrColors
        
        let data = PieChartData(dataSet: set)
        data.setValueTextColor(PBLib.RGBA([0xb2, 0xac, 0xa1, 0]))
        
        chartviewTimer.data = data
        chartviewTimer.highlightValues(nil)
    }
    
    // MARK: - ChartViewDelegate
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        NSLog("chartValueSelected");
    }
    
    func chartValueNothingSelected(_ chartView: ChartViewBase) {
        NSLog("chartValueNothingSelected");
    }
    
    func chartScaled(_ chartView: ChartViewBase, scaleX: CGFloat, scaleY: CGFloat) {
        
    }
    
    func chartTranslated(_ chartView: ChartViewBase, dX: CGFloat, dY: CGFloat) {
        
    }

    @IBAction func doPressureDown(_ sender: Any) {
        
        PBCommon.sendData(PBCommon.sCmdPressureDown)
        
        if PBCommon.nOperationMode == PBCommon.OP_MODE_EXP {
            PBCommon.nPressureExp -= 1
        } else if PBCommon.nOperationMode == PBCommon.OP_MODE_MSG {
            PBCommon.nPressureMsg -= 1
        }
        
        if PBCommon.nPressureExp < PBCommon.EXP_PRESSURE_MIN {
            PBCommon.nPressureExp = PBCommon.EXP_PRESSURE_MIN
        }
        
        if PBCommon.nPressureMsg < PBCommon.MSG_PRESSURE_MIN {
            PBCommon.nPressureMsg = PBCommon.MSG_PRESSURE_MIN
        }
        refreshChart(chartviewPressure, Type: PBCommon.OP_PRESSURE)
    }
    
    @IBAction func doPressureUp(_ sender: Any) {
        
        PBCommon.sendData(PBCommon.sCmdPressureUp)
        
        if PBCommon.nOperationMode == PBCommon.OP_MODE_EXP {
            PBCommon.nPressureExp += 1
        } else if PBCommon.nOperationMode == PBCommon.OP_MODE_MSG {
            PBCommon.nPressureMsg += 1
        }
        
        if PBCommon.nPressureExp > PBCommon.EXP_PRESSURE_MAX {
            PBCommon.nPressureExp = PBCommon.EXP_PRESSURE_MAX
        }
        
        if PBCommon.nPressureMsg > PBCommon.arrPressureLimit[PBCommon.nCycleMsg] {
            PBCommon.nPressureMsg = PBCommon.arrPressureLimit[PBCommon.nCycleMsg]
        }
        refreshChart(chartviewPressure, Type: PBCommon.OP_PRESSURE)
    }
    
    @IBAction func doCycleDown(_ sender: Any) {
    
        PBCommon.sendData(PBCommon.sCmdCycleDown)
        
        if PBCommon.nOperationMode == PBCommon.OP_MODE_EXP {
            PBCommon.nCycleExp -= 1
        } else if PBCommon.nOperationMode == PBCommon.OP_MODE_MSG {
            PBCommon.nCycleMsg -= 1
        }
        
        if PBCommon.nCycleExp < PBCommon.EXP_CYCLE_MIN {
            PBCommon.nCycleExp = PBCommon.EXP_CYCLE_MIN
        }
        
        if PBCommon.nCycleMsg < PBCommon.MSG_CYCLE_MIN {
            PBCommon.nCycleMsg = PBCommon.MSG_CYCLE_MIN
        }
        
        refreshOperationData()
    }
    
    @IBAction func doCycleUp(_ sender: Any) {
        
        PBCommon.sendData(PBCommon.sCmdCycleUp)
        
        if PBCommon.nOperationMode == PBCommon.OP_MODE_EXP {
            PBCommon.nCycleExp += 1
        } else if PBCommon.nOperationMode == PBCommon.OP_MODE_MSG {
            PBCommon.nCycleMsg += 1
        }
        
        if PBCommon.nCycleExp > PBCommon.EXP_CYCLE_MAX {
            PBCommon.nCycleExp = PBCommon.EXP_CYCLE_MAX
        }
        
        if PBCommon.nCycleMsg > PBCommon.MSG_CYCLE_MAX {
            PBCommon.nCycleMsg = PBCommon.MSG_CYCLE_MAX
        }
        
        refreshOperationData()
    }
    
    @IBAction func onExpression(_ sender: Any) {
        selectOperation(PBCommon.OP_MODE_EXP)
    }
    
    @IBAction func onMassage(_ sender: Any) {
        selectOperation(PBCommon.OP_MODE_MSG)
    }
    
    @IBAction func onPower(_ sender: Any) {
        PBCommon.powerDown()
    }
    
    @IBAction func onLamp(_ sender: Any) {
        PBCommon.toggleLamp()
    }
    
    @IBAction func onSingle(_ sender: Any) {
        selectOperationType(PBCommon.OP_TYPE_SINGLE, Disp: false)
    }

    @IBAction func onDouble(_ sender: Any) {
        selectOperationType(PBCommon.OP_TYPE_DOUBLE, Disp: false)
    }

        
}
