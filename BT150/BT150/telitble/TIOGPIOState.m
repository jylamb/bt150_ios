//
//  TIOGPIOState.m
//  TerminalIO
//
//  Created by Telit
//  Copyright (c) 2013,2014 Telit Wireless Solutions GmbH, Germany
//

#import "TIOGPIOState.h"


@interface TIOGPIOState ()

//@property (nonatomic) UInt32 value;

@end


@implementation TIOGPIOState


#pragma  mark - Initialization

- (TIOGPIOState *)initWithValue:(UInt32)value
{
	self = [super init];
	if (self)
	{
		self.value = value;
	}
	return self;
}


#pragma  mark - Properties

- (NSData *)data
{
	UInt32 value = self.value;
	return [NSData dataWithBytes: &value length: 4];
}


- (NSString *)description
{
	return [NSString stringWithFormat:@"0x%x", (unsigned int) self.value];
}


#pragma mark - Public methods

+ (TIOGPIOState *)gpioState
{
	return [[TIOGPIOState alloc] initWithValue:0];
}


+ (TIOGPIOState *)gpioStateWithData:(NSData *)data
{
	UInt32 value = 0;
	NSInteger length = data.length <= 4 ? data.length : 4;
	Byte *bytes = (Byte *) data.bytes;
	for (int n = 0; n < length; n++)
	{
		value |= (bytes[n] << n * 8);
	}
	return [[TIOGPIOState alloc] initWithValue:value];
}


+ (TIOGPIOState *)gpioStateWithValue:(UInt32)value
{
	return [[TIOGPIOState alloc] initWithValue:value];
}


- (void)setGPIOAtIndex:(NSInteger)index toHigh:(BOOL)high
{
	if (index < 0 || index > 31)
		return;
	
	self.value = high ? (self.value | (1 << index)) : (self.value & ~(1 << index));
}


- (BOOL)gpioIsHighAtIndex:(NSInteger)index
{
	if (index < 0 || index > 31)
		return NO;
	
    return ((self.value & (1 << index)) != 0);
}


@end
