//
//  TIOGPIOState.h
//  TerminalIO
//
//  Created by Telit
//  Copyright (c) 2013,2014 Telit Wireless Solutions GmbH, Germany
//

#import <Foundation/Foundation.h>


/**
 *  TIOGPIOState instances represent a local or remote TerminalIO state of 32 (virtual) GPIOs represented by the single bits of a UInt32 value.
 *  TIOPeripheral instances supporting GPIO may send or receive TIOGPIOState instances when connected.
 */
@interface TIOGPIOState : NSObject

///@{
/// @name Instantiation

/**
*  Creates a new TIOGPIOState instance with all GPIOs set to LOW.
*  @return An empty TIOGPIOState instance.
*/
+ (TIOGPIOState *)gpioState;

/**
 *  Creates a new TIOGPIOState instance from the specified data.
 *  @param data An NSData instance (4 bytes) to create the TIOGPIOState instance from. Byte order is least significant byte first = "Little Endian".
 *  @return The requested TIOGPIOState instance.
 */
+ (TIOGPIOState *)gpioStateWithData:(NSData *)data;

/**
 *  Creates a new TIOGPIOState instance from the specified value.
 *  @param value A UInt32 value to create the TIOGPIOState instance from.
 *  @return The requested TIOGPIOState instance.
 */
+ (TIOGPIOState *)gpioStateWithValue:(UInt32)value;

///@}


///@{
/// @name Retrieving Properties Information

/**
 *  The data containing the 32 bits of GPIO state information. Byte order is least significant byte first = "Little Endian".
 */
@property (nonatomic, readonly) NSData *data;

/**
 *  The UInt32 value representing the 32 bits of GPIO state information.
 */
@property (nonatomic) UInt32 value;

/**
 *  Retrieves information about a single GPIO state at the specified index.
 *  @param index The index to retrieve the GPIO state information for. Valid indices range from 0 to 31. For invalid indices, NO will be returned.
 *  @return YES if the GPIO at the specified index is set to HIGH, NO otherwise.
 */
- (BOOL)gpioIsHighAtIndex:(NSInteger)index;

///@}

///@{
/// @name Setting GPIO States

/**
 *  Sets a single GPIOs state to HIGH or LOW.
 *  @param index The index at which to set the GPIO state. Valid indices range from 0 to 31. For invalid indices, no operation is performed.
 *  @param high  Specify YES in order to set the GPIO to HIGH, NO otherwise.
 */
- (void)setGPIOAtIndex:(NSInteger)index toHigh:(BOOL)high;

///@}

@end
