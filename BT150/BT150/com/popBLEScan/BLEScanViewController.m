//
//  BLEScanViewController.m
//  BT150
//
//  Created by KOBONGHWAN on 04/12/2018.
//  Copyright © 2018 bistos. All rights reserved.
//

#import "BLEScanViewController.h"
#import "MBProgressHUD.h"
#import "PBLibOC.h"

NSTimeInterval const SCAN_DURATION = 8.0;  // seconds

@interface BLEScanViewController ()
{
    MBProgressHUD *hud;
    UITableView *tvList;
    
    CGSize sizeView;
    CGFloat fCellH;
//    CGFloat fLineH;
    NSTimer *timerFindBebe;
    NSString *sUUID;
}
@end

@implementation BLEScanViewController

@synthesize delegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = RGBA(0, 0, 0, 0.6);
    sizeView = self.view.frame.size;
    
    sUUID = @"";

    [[TIOManager sharedInstance] removeAllPeripherals];
}

-(void)initControls
{
    fCellH = kNavBarH;
//    fLineH = 25;
    
    CGFloat fTableH = 400;
    CGFloat fTableY = (sizeView.height - fTableH) / 2;
    
    if (tvList == nil) {
        
        tvList = [[UITableView alloc] initWithFrame:CGRectMake(10, fTableY, sizeView.width-20, fTableH) style:UITableViewStylePlain];
        tvList.bounces = NO;
        tvList.separatorStyle = UITableViewCellSeparatorStyleNone;
        tvList.scrollEnabled = YES;
        tvList.allowsSelection = YES;
        tvList.delegate = self;
        tvList.dataSource = self;
        tvList.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:tvList];
        
        hud = [[MBProgressHUD alloc] initWithView:self.view];
        hud.opacity = 0.6;
        hud.labelText = NSLocalizedString(@"DeviceSearching", @"Device Searching....");
        [hud addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onClose)]];
        [self.view addSubview:hud];
    }
    
    sUUID = @"";

    NSString *aUUID = [[NSUserDefaults standardUserDefaults] objectForKey:@"device_addr"];
    if (aUUID != nil && aUUID.length > 0) {
        sUUID = aUUID;
    }
//    NSLog(@"sUUID:%@", sUUID);
    
    [PBLibOC doAfterTime:2 doBlock:^{
        [self refreshList];
    }];    
    
    [self findHibebe];
}

-(void)findHibebe
{
    [[TIOManager sharedInstance] removeAllPeripherals];

    [hud show:YES];
    
    timerFindBebe = [NSTimer scheduledTimerWithTimeInterval: SCAN_DURATION target: self selector: @selector(scanTimerElapsed:) userInfo: nil repeats: NO];
    [[TIOManager sharedInstance] stopScan];
    [PBLibOC doAfterTime:0.5 doBlock:^{
        [[TIOManager sharedInstance] startScan];
    }];
}

-(void)refreshList
{
    [tvList reloadData];
    
    if ([TIOManager sharedInstance].peripherals.count > 0) {
        [hud hide:YES];
        
        [timerFindBebe invalidate];
        timerFindBebe = nil;
        [[TIOManager sharedInstance] stopScan];
    }
}

- (void)scanTimerElapsed:(NSTimer *)timer
{
    [self findHibebe];
}

#pragma mark -
#pragma mark UITableViewDelegate, UITableViewDataSource
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return fCellH + 10;
    }
    return fCellH;
}

-(NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section
{
    NSInteger numberOfPeripherals = [TIOManager sharedInstance].peripherals.count;
    return numberOfPeripherals + 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = kDefWhite;
    
    for(UIView *subView in cell.contentView.subviews) {
        if (subView.tag >= 100) {
            [subView removeFromSuperview];
        }
    }
    
    CGSize sizeCell = sizeView;
    
    if (indexPath.row == 0) {
        cell.backgroundColor = RGB(0xe6, 0x62, 0x72);
        
        UILabel *lblPopTitle = [PBLibOC buildFlatColorLabel:[UIColor clearColor] TitleColor:kTextBlack];
        lblPopTitle.frame = CGRectMake(10, 0, sizeCell.width-20, fCellH+10);
        lblPopTitle.font = AppFont(16);
        lblPopTitle.textColor = kDefWhite;
        lblPopTitle.tag = 999;
        lblPopTitle.text = NSLocalizedString(@"DeviceSearch", @"Device Search");
        [cell.contentView addSubview:lblPopTitle];
        
        UIButton *btnClose = [UIButton buttonWithType:UIButtonTypeCustom];
        btnClose.frame = CGRectMake(sizeCell.width-60, (fCellH+10-40)/2, 40, 40);
        [btnClose setImage:[UIImage imageNamed:@"ic_close_white_24dp.png"] forState:UIControlStateNormal];
        [btnClose addTarget:self action:@selector(onClose) forControlEvents:UIControlEventTouchUpInside];
        btnClose.tag = 999;
        [cell.contentView addSubview:btnClose];
        
//        UIView *vLine01 = [[UIView alloc] initWithFrame:CGRectMake(10, fCellH-5, sizeCell.width-40, 1)];
//        vLine01.backgroundColor = kMenuLineGray;
//        vLine01.tag = 999;
//        [cell.contentView addSubview:vLine01];
        
    } else {        
        TIOPeripheral *aPeripheral = [[TIOManager sharedInstance].peripherals objectAtIndex:indexPath.row-1];
        NSLog(@"aPeripheral:%@", aPeripheral.advertisement.displayDescription);

        UILabel *lblDevName = [PBLibOC buildFlatColorLabel:[UIColor clearColor] TitleColor:kTextGray];
        lblDevName.frame = CGRectMake(10, 0, sizeCell.width-40, fCellH/2);
        lblDevName.font = AppFont(18);
        lblDevName.tag = 999;
        lblDevName.numberOfLines = 2;
        [cell.contentView addSubview:lblDevName];
//        if (aPeripheral.advertisement != nil) {
//            lblDevName.text = [NSString stringWithFormat:@"%@", aPeripheral.advertisement.displayDescription];
//        } else {
//            lblDevName.text = [NSString stringWithFormat:@"%@", aPeripheral.name];
//        }
        lblDevName.text = [NSString stringWithFormat:@"%@", aPeripheral.name];
        
        NSString *aDevName = [PBLibOC filterNSNull:aPeripheral.name];
        if (aDevName.length > 10) {
            NSString *aPrefix = [aDevName substringWithRange:NSMakeRange(1, 2)];
            NSString *aSerial = @"";
            if ([[aPrefix lowercaseString] isEqualToString:@"hb"]) {
                aSerial = [aDevName substringWithRange:NSMakeRange(7, 4)];
            } else if ([[aPrefix lowercaseString] isEqualToString:@"hi"]) {
                NSString *aHex = [aDevName substringWithRange:NSMakeRange(9, 2)];
                unsigned result = 0;
                NSScanner *scanner = [NSScanner scannerWithString:aHex];
                [scanner scanHexInt:&result];
                aSerial = [NSString stringWithFormat:@"%04d", result];
            }
            lblDevName.text = [NSString stringWithFormat:@"Hi Bebe Breast Pump – %@", aSerial];
        }
        
        UILabel *lblDevUUID = [PBLibOC buildFlatColorLabel:[UIColor clearColor] TitleColor:kTextGray];
        lblDevUUID.frame = CGRectMake(10, fCellH/2, sizeCell.width-40, fCellH/2);
        lblDevUUID.font = AppFont(14);
        lblDevUUID.tag = 999;
        lblDevUUID.numberOfLines = 2;
        [cell.contentView addSubview:lblDevUUID];
        lblDevUUID.text = [NSString stringWithFormat:@"%@", aPeripheral.identifier];

        if ([sUUID isEqualToString:lblDevUUID.text]) {
            TIOPeripheral *aPeripheral = [[TIOManager sharedInstance].peripherals objectAtIndex:indexPath.row-1];
            [delegate onPeripheralCellPressed:aPeripheral];
            
            [self onClose];
        }
    }
        
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row > 0) {
        TIOPeripheral *aPeripheral = [[TIOManager sharedInstance].peripherals objectAtIndex:indexPath.row-1];
        [delegate onPeripheralCellPressed:aPeripheral];
        
        [self onClose];
    }
    
}

-(void)onClose
{
    [hud hide:YES];
    
    [timerFindBebe invalidate];
    timerFindBebe = nil;
    [[TIOManager sharedInstance] stopScan];
    [PBLibOC performTransition:NO andView:self.view];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
