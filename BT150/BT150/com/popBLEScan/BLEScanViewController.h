//
//  BLEScanViewController.h
//  BT150
//
//  Created by KOBONGHWAN on 04/12/2018.
//  Copyright © 2018 bistos. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TerminalIO.h"

NS_ASSUME_NONNULL_BEGIN

@protocol BLEScanViewDelegate <NSObject>
    -(void)onPeripheralCellPressed:(TIOPeripheral*)a_Peripheral;
@end

@interface BLEScanViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    id<BLEScanViewDelegate> delegate;
}

@property (strong, nonatomic) id<BLEScanViewDelegate> delegate;

-(void)initControls;
-(void)refreshList;

@end

NS_ASSUME_NONNULL_END
