//
//  PBSR.swift
//  BT150
//
//  Created by KOBONGHWAN on 08/02/2019.
//  Copyright © 2019 bistos. All rights reserved.
//

import UIKit
import Speech
import AVFoundation

public let VOICE_REC_LANG_KOREAN = "ko-KR"
public let VOICE_REC_LANG_ENGLISH = "en-US"
public let VOICE_REC_LANG_FRENCH = "fr-FR"

protocol SRResultDelegate {
    func onSRResult(_ a_Result : String)
}

public class PBSR: NSObject {

    var delegate : SRResultDelegate?
    var speechRecognizer : SFSpeechRecognizer?
    
    var speechRecognitionRequest : SFSpeechAudioBufferRecognitionRequest?
    var speechRecognitionTask : SFSpeechRecognitionTask?
    let audioEngine = AVAudioEngine()
    var capture: AVCaptureSession?
    
    var lastSRStr : String = ""
    var lastSRItemCnt : Int = 0
//    var bStart = false
    var bUseTranscribing = false
    var bStartSession = false
    var bRestartDone = false

    func logToMain(_ a_Log : String) {
        print("logToMain:\(a_Log)")
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MSG_LOGTO_MAIN"),
                                        object: nil,
                                        userInfo: ["Type" : "Log", "String" : a_Log])
    }
    
    func startTransribing() {
        logToMain("startTransribing!!!!!")
        
        self.bUseTranscribing = true

//        if bStart {
//            logToMain("startTransribing is already started.")
//            return
//        }
//        bStart = true
        
        if PBCommon.sUseVCmd == "enable" {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MSG_SR_RESULT"),
                                            object: nil,
                                            userInfo: ["Result" : true, "String" : "\(NSLocalizedString("StartVCmd", comment: "Start VCmd"))!"])
        }
        
        do {
            if bStartSession {
                logToMain("################## startSession aleary called. do not call again!!!!!!!!!!!!!!!!!!!!!!!!")
                restartSPRSesson()
            } else {
                try self.startSession()
            }
        } catch {
            logToMain("startTransribing error: \(error)")
        }
    }

    func pauseTranscribing() {
        self.bUseTranscribing = false
    }


    func stopTranscribing() {
        logToMain("stopTranscribing!!!!!")
        
        if PBCommon.sUseVCmd == "enable" {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MSG_SR_RESULT"),
                                            object: nil,
                                            userInfo: ["Result" : true, "String" : "Stop voice command mode!"])
        }

//        stopSFRecording()
        endRecognizer()
    }
    
    func stopSFRecording() {
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MSG_STT_TASK"),
                                        object: nil,
                                        userInfo: ["Action" : "Stop",])
        
        logToMain("stopSFRecording")
        
        audioEngine.stop()
        
        speechRecognitionRequest?.endAudio()
        speechRecognitionTask?.cancel()
        
        self.speechRecognitionTask = nil
        self.speechRecognitionRequest = nil
        let inputNode = audioEngine.inputNode
        inputNode.removeTap(onBus: 0)
    }
    
    func initSpeechRecognition(_ a_Lang : String) -> Bool {
        
        logToMain("initSpeechRecognition:\(a_Lang)")
        
        speechRecognizer = SFSpeechRecognizer(locale:Locale(identifier: a_Lang))!
        if speechRecognizer == nil {
            return false
        }
        speechRecognizer?.delegate = self
        return true
    }
    
    func authorizeSR(_ a_Lang : String) {
        SFSpeechRecognizer.requestAuthorization { authStatus in
            
            OperationQueue.main.addOperation {
                switch authStatus {
                case .authorized:
                    let aInit = self.initSpeechRecognition(a_Lang)
                    if !aInit {
                        PBLib.showAlert(NSLocalizedString("PSPRauthorized", comment: "Speech recognition initializing failed."))
                    }
                    break
                case .denied:
                    PBLib.showAlert(NSLocalizedString("PSPRdenied", comment: "Speech recognition access denied by user"))
                    break
                    
                case .restricted:
                    PBLib.showAlert(NSLocalizedString("PSPRrestricted", comment: "Speech recognition restricted on device"))
                    break
                    
                case .notDetermined:
                    PBLib.showAlert(NSLocalizedString("PSPRnotDetermined", comment: "Speech recognition not authorized"))
                    break
                }
            }
        }
    }
    
    func restartSPRSesson() {
        
        bRestartDone = false
        
        logToMain("restartSPRSesson ####################:")
        
        self.stopSFRecording()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            do {
                try self.startSession()
            } catch {
                self.logToMain("restartSPRSesson error: \(error)")
            }
        }
    }
    
    func startSession () throws {
        
        bStartSession = true
        
        logToMain("################## startSession ####################:")
        
        lastSRItemCnt = 0
        
        if let recognitionTask = speechRecognitionTask {
            logToMain("recognitionTask already exist, cancel previous recognitionTask.")
//            recognitionTask.cancel()
//            self.speechRecognitionTask = nil
        }
        
        do {
            let audioSession = AVAudioSession.sharedInstance()
//        try audioSession.setCategory(AVAudioSession.Category.record)
            try audioSession.setCategory(.record, mode: .default)
            try audioSession.setActive(true)

            speechRecognitionRequest = SFSpeechAudioBufferRecognitionRequest()

            guard let recognitionRequest = speechRecognitionRequest else {
            fatalError("SFSpeechAudioBufferRecognitionRequest object creation failed")
            }
            recognitionRequest.shouldReportPartialResults = true

            let inputNode = audioEngine.inputNode
            let recordingFormat = inputNode.outputFormat(forBus: 0)
            inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer: AVAudioPCMBuffer, when: AVAudioTime) in
            self.speechRecognitionRequest?.append(buffer)
            }
            audioEngine.prepare()
            try audioEngine.start()

            doSpeechRecTask(recognitionRequest)
//        speechRecognizer?.recognitionTask(with: recognitionRequest, delegate: self)

//        audioEngine.prepare()
//        try audioEngine.start()

//            //calls that might throw an NSException
//            try ObjC.catchException {
//            }
            
        } catch {
            print("An error ocurred: \(error)")
        }
        
        

    }
    
    func doSpeechRecTask(_ recognitionRequest : SFSpeechAudioBufferRecognitionRequest) {
        
        logToMain("doSpeechRecTask start!!!!!")
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MSG_STT_TASK"),
                                        object: nil,
                                        userInfo: ["Action" : "Start",])
        
        speechRecognitionTask = speechRecognizer?.recognitionTask(with: recognitionRequest) { result, error in
            var finished = false
            if let result = result {
                finished = result.isFinal
                let aResStr = result.bestTranscription.formattedString.components(separatedBy: " ")
//                for i  in 0 ..< aResStr.count {
//                    logToMain("\(i): \(aResStr[i])")
//                }
                self.logToMain("recognitionTask >>>> \(result.bestTranscription.formattedString)")
                
                if self.lastSRStr != aResStr[aResStr.count-1] || self.lastSRItemCnt < aResStr.count {
                    self.lastSRStr = aResStr[aResStr.count-1]
                    self.lastSRItemCnt = aResStr.count
                    if self.bUseTranscribing {
                        self.delegate?.onSRResult(aResStr[aResStr.count-1])
                    }
                }
            }
            
            if error != nil {
                self.logToMain("recognitionTask error:\(String(describing: error))")
                if self.bRestartDone {
                    self.restartSPRSesson()
                }
                
            } else {
                if finished {
                    self.logToMain("recognitionTask finished:\(finished)")
                    if self.bRestartDone {
                        self.restartSPRSesson()
                    }
                } else {
                    self.bRestartDone = true
                }
            }            
//            logToMain("self.bRestartDone:\(self.bRestartDone)")
        }
        
    }
    
    
    func startCapture() {
        
        capture = AVCaptureSession()
        
        guard let audioDev = AVCaptureDevice.default(for: .audio) else {
            logToMain("Could not get capture device.")
            return
        }
        
        guard let audioIn = try? AVCaptureDeviceInput(device: audioDev) else {
            logToMain("Could not create input device.")
            return
        }
        
        guard true == capture?.canAddInput(audioIn) else {
            logToMain("Couls not add input device")
            return
        }
        
        capture?.addInput(audioIn)
        
        let audioOut = AVCaptureAudioDataOutput()
        audioOut.setSampleBufferDelegate(self, queue: DispatchQueue.main)
        
        guard true == capture?.canAddOutput(audioOut) else {
            logToMain("Could not add audio output")
            return
        }
        
        capture?.addOutput(audioOut)
        audioOut.connection(with: .audio)
        capture?.startRunning()
    }
    
    func endCapture() {
        if true == capture?.isRunning {
            capture?.stopRunning()
        }
    }
    
    func startRecognizer() {
        
        logToMain("startRecognizer-1")

        speechRecognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        guard let recognitionRequest = speechRecognitionRequest else {
            fatalError("SFSpeechAudioBufferRecognitionRequest object creation failed")
        }
        recognitionRequest.shouldReportPartialResults = true

        speechRecognizer?.recognitionTask(with: self.speechRecognitionRequest!, delegate: self)
    }
    
    func endRecognizer() {
        logToMain("endRecognizer-1")
        endCapture()
        speechRecognitionRequest?.endAudio()
    }

    
}

extension PBSR : AVCaptureAudioDataOutputSampleBufferDelegate {
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputSampleBuffer sampleBuffer: CMSampleBuffer!, from connection: AVCaptureConnection!) {
        speechRecognitionRequest?.appendAudioSampleBuffer(sampleBuffer)
    }
}

extension PBSR : SFSpeechRecognitionTaskDelegate {
    
    func speechRecognitionDidDetectSpeech(_ task: SFSpeechRecognitionTask) {
        logToMain("speechRecognitionDidDetectSpeech)")
    }
    
    public func speechRecognitionTask(_ task: SFSpeechRecognitionTask, didHypothesizeTranscription transcription: SFTranscription) {
        logToMain("speechRecognitionTask didHypothesizeTranscription:)\(transcription.formattedString)")
    }
    
    public func speechRecognitionTaskFinishedReadingAudio(_ task: SFSpeechRecognitionTask) {
        logToMain("speechRecognitionTaskFinishedReadingAudio")
    }
    
    public func speechRecognitionTaskWasCancelled(_ task: SFSpeechRecognitionTask) {
        logToMain("speechRecognitionTaskWasCancelled")
    }
    
    func speechRecognitionTask(_ task: SFSpeechRecognitionTask, didFinishRecognition recognitionResult: SFSpeechRecognitionResult) {
//        console.text = console.text + "\n" + recognitionResult.bestTranscription.formattedString
//        logToMain("speechRecognitionTask didFinishRecognition:\(recognitionResult.bestTranscription.formattedString)")
        
        let aResStr = recognitionResult.bestTranscription.formattedString.components(separatedBy: " ")
        logToMain("speechRecognitionTask didFinishRecognition >>>> \(recognitionResult.bestTranscription.formattedString)")
        
        if self.lastSRStr != aResStr[aResStr.count-1] || self.lastSRItemCnt < aResStr.count {
            self.lastSRStr = aResStr[aResStr.count-1]
            self.lastSRItemCnt = aResStr.count
            if self.bUseTranscribing {
                self.delegate?.onSRResult(aResStr[aResStr.count-1])
            }
        }

    }
    
    public func speechRecognitionTask(_ task: SFSpeechRecognitionTask, didFinishSuccessfully successfully: Bool) {
        logToMain("speechRecognitionTask didFinishSuccessfully:\(successfully)")
        
        self.stopSFRecording()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//            self.startRecognizer()
//            try! self.startSession()
        }
        
    }
}

extension PBSR : SFSpeechRecognizerDelegate {
    public func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        logToMain("speechRecognizer availabilityDidChange:\(available)")
    }
}
