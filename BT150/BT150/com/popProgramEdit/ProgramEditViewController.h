//
//  ProgramEditViewController.h
//  BT150
//
//  Created by KOBONGHWAN on 20/12/2018.
//  Copyright © 2018 bistos. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol ProgramEditDelegate <NSObject>

-(void)onCloseProgramEdit:(NSDictionary*)a_Param;
-(void)onChangeOpType:(int)a_OpType;
-(void)initProgramEditPop:(int)a_ProgramIdx ProgramDataIdx:(int)a_ProgramDataIdx;

@end

@interface ProgramEditViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate>
{
    id <ProgramEditDelegate> delegate;
    int nType;
    int nMode;
    int nPressure;
    int nCycle;
    int nLen;
    int nProgramIdx;
    int nProgramDataIdx;
}

@property(strong, nonatomic) id <ProgramEditDelegate> delegate;
@property(readwrite, nonatomic) int nType;
@property(readwrite, nonatomic) int nMode;
@property(readwrite, nonatomic) int nPressure;
@property(readwrite, nonatomic) int nCycle;
@property(readwrite, nonatomic) int nLen;
@property(readwrite, nonatomic) int nProgramIdx;
@property(readwrite, nonatomic) int nProgramDataIdx;

-(void)initProgramEditPop:(NSDictionary*)a_Param;

@end

NS_ASSUME_NONNULL_END
