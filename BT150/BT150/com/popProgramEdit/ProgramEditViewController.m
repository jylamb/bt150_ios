//
//  ProgramEditViewController.m
//  BT150
//
//  Created by KOBONGHWAN on 20/12/2018.
//  Copyright © 2018 bistos. All rights reserved.
//

#import "ProgramEditViewController.h"
#import "PBLibOC.h"

#define kTableCellPicker    120

#define kPickerMinTag       501
#define kPickerSecTag       502

#define kModeExpression     1
#define kModeMassage        0

#define kTypeSingle         0
#define kTypeDouble         1

@interface ProgramEditViewController ()
{
    UITableView *tvList;
    UILabel *lblPressure;
    UILabel *lblCycle;
    UIPickerView *pvOptionMin;
    UIPickerView *pvOptionSec;
    UIButton *btnNext;
    UIButton *btnPrev;
    
    CGSize sizeView;
    CGFloat fCellH;
    NSMutableArray *arrMin;
    NSMutableArray *arrSec;
}
@end



@implementation ProgramEditViewController

@synthesize nProgramIdx, nProgramDataIdx;
@synthesize nType, nMode, nPressure, nCycle, nLen;
@synthesize delegate;

-(void)initProgramEditPop:(NSDictionary*)a_Param
{
    NSLog(@"initProgramEditPop:%@", a_Param);
    
    nMode = [[a_Param objectForKey:@"mode"] intValue];
    if (nMode == 999) {
        nMode = kModeExpression;
    }
    nType = [[a_Param objectForKey:@"type"] intValue];
    nPressure = [[a_Param objectForKey:@"pressure"] intValue];
    nCycle = [[a_Param objectForKey:@"cycle"] intValue];
    nLen = [[a_Param objectForKey:@"time"] intValue];
    
    if (nLen == 0) {
        nLen = 15;
    }
    
    [self initControls];
    
    [self refreshDataNavi];
}

-(void)refreshDataNavi
{
    if (nProgramDataIdx == 0) {
        btnPrev.hidden = YES;
        btnNext.hidden = NO;
    } else if (nProgramDataIdx == 7) {
        btnPrev.hidden = NO;
        btnNext.hidden = YES;
    } else {
        btnPrev.hidden = NO;
        btnNext.hidden = NO;
    }
        
    [tvList reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = RGBA(0, 0, 0, 0.6);
    sizeView = self.view.frame.size;
}


-(void)initControls
{
    fCellH = kNavBarH;
    
    CGFloat fTableH = (fCellH + 10) * 4 + 115 + fCellH / 2 + kTableCellPicker;
    CGFloat fTableY = (sizeView.height - fTableH) / 2;
    
    if (tvList == nil) {
        
        tvList = [[UITableView alloc] initWithFrame:CGRectMake(10, fTableY, sizeView.width-20, fTableH) style:UITableViewStylePlain];
        tvList.bounces = NO;
        tvList.separatorStyle = UITableViewCellSeparatorStyleNone;
        tvList.scrollEnabled = YES;
        tvList.allowsSelection = NO;
        tvList.delegate = self;
        tvList.dataSource = self;
        tvList.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:tvList];

        
        btnPrev = [PBLibOC buildFlatColorButton:[UIColor clearColor]];
        btnPrev.frame = CGRectMake(2, (self.view.frame.size.height-32)/2+15, 32, 32);
        [btnPrev setImage:[UIImage imageNamed:@"popup_btn_pre.png"] forState:UIControlStateNormal];
        [btnPrev addTarget:self action:@selector(onPrev) forControlEvents:UIControlEventTouchUpInside];
        btnPrev.tag = 999;
        [self.view addSubview:btnPrev];
        
        btnNext = [PBLibOC buildFlatColorButton:[UIColor clearColor]];
        btnNext.frame = CGRectMake(self.view.frame.size.width-32-2, (self.view.frame.size.height-32)/2+15, 32, 32);
        [btnNext setImage:[UIImage imageNamed:@"popup_btn_next.png"] forState:UIControlStateNormal];
        [btnNext addTarget:self action:@selector(onNext) forControlEvents:UIControlEventTouchUpInside];
        btnNext.tag = 999;
        [self.view addSubview:btnNext];


        arrMin = [[NSMutableArray alloc] init];
        arrSec = [[NSMutableArray alloc] init];
        
        for (int i = 0; i < 31; i++) {
            [arrMin addObject:@{@"code" : [NSString stringWithFormat:@"%d", i], @"codename" : [NSString stringWithFormat:@"%02d", i]}];
        }
        
        for (int i = 0; i < 60; i++) {
            [arrSec addObject:@{@"code" : [NSString stringWithFormat:@"%d", i], @"codename" : [NSString stringWithFormat:@"%02d", i]}];
        }
    }
    
    [tvList reloadData];
    
}

#pragma mark -
#pragma mark UITableViewDelegate, UITableViewDataSource
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0 || indexPath.row == 7) {
        return fCellH + 10;
    } else if (indexPath.row == 1 || indexPath.row == 2) {
        return 115 / 2;
    } else if (indexPath.row == 3 || indexPath.row == 4) {
        return fCellH + 10;
    } else if (indexPath.row == 5) {
        return fCellH / 2;
    } else if (indexPath.row == 6) {
        return kTableCellPicker;
    }
    return fCellH;
}

-(NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section
{
    return 8;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = kDefWhite;
    
    for(UIView *subView in cell.contentView.subviews) {
        if (subView.tag >= 100) {
            [subView removeFromSuperview];
        }
    }
    
    CGSize sizeCell = tvList.frame.size;
    
    UILabel *lblTitle = [PBLibOC buildFlatColorLabel:[UIColor clearColor] TitleColor:kTextGray];
    lblTitle.frame = CGRectMake(0, 0, sizeCell.width/3, fCellH);
    lblTitle.font = AppFont(16);
    lblTitle.textAlignment = NSTextAlignmentRight;
    lblTitle.tag = 999;
    [cell.contentView addSubview:lblTitle];

    if (indexPath.row == 0) {
        cell.backgroundColor = kDefThemeColor;
        lblTitle.hidden = YES;

        UILabel *lblPopTitle = [PBLibOC buildFlatColorLabel:[UIColor clearColor] TitleColor:kTextBlack];
        lblPopTitle.frame = CGRectMake(10, 0, sizeCell.width-20, fCellH+10);
        lblPopTitle.font = AppFont(18);
        lblPopTitle.textColor = kDefWhite;
        lblPopTitle.tag = 999;
        lblPopTitle.text = [NSString stringWithFormat:@"%@ %d", NSLocalizedString(@"Sequence", @"Sequence"), nProgramDataIdx+1];
        [cell.contentView addSubview:lblPopTitle];
        
        UIButton *btnClose = [UIButton buttonWithType:UIButtonTypeCustom];
        btnClose.frame = CGRectMake(sizeCell.width-60, (fCellH+10-40)/2, 40, 40);
        [btnClose setImage:[UIImage imageNamed:@"ic_close_white_24dp.png"] forState:UIControlStateNormal];
        [btnClose addTarget:self action:@selector(onClose) forControlEvents:UIControlEventTouchUpInside];
        btnClose.tag = 999;
        [cell.contentView addSubview:btnClose];
    
    } else if (indexPath.row == 1) {
        cell.backgroundColor = RGB(0xe9, 0xe9, 0xe9);
        lblTitle.hidden = YES;

        UIButton *btnSingle = [PBLibOC buildFlatColorButton:[UIColor clearColor]];
        btnSingle.frame = CGRectMake(0, 0, sizeCell.width/2, 115/2);
        [btnSingle setImage:[UIImage imageNamed:@"menu_ic_single.png"] forState:UIControlStateNormal];
        [btnSingle setImage:[UIImage imageNamed:@"menu_ic_single.png"] forState:UIControlStateNormal|UIControlStateHighlighted];
        [btnSingle setImage:[UIImage imageNamed:@"menu_ic_single_ov.png"] forState:UIControlStateSelected];
        [btnSingle setImage:[UIImage imageNamed:@"menu_ic_single_ov.png"] forState:UIControlStateSelected|UIControlStateHighlighted];
        [btnSingle setImage:[UIImage imageNamed:@"menu_ic_single_ov_disable.png"] forState:UIControlStateDisabled];
        [btnSingle addTarget:self action:@selector(onSingle) forControlEvents:UIControlEventTouchUpInside];
        btnSingle.tag = 999;
        [cell.contentView addSubview:btnSingle];
        
        UIButton *btnDouble = [PBLibOC buildFlatColorButton:[UIColor clearColor]];
        btnDouble.frame = CGRectMake(sizeCell.width/2, 0, sizeCell.width/2, 115/2);
        [btnDouble setImage:[UIImage imageNamed:@"menu_ic_double.png"] forState:UIControlStateNormal];
        [btnDouble setImage:[UIImage imageNamed:@"menu_ic_double.png"] forState:UIControlStateNormal|UIControlStateHighlighted];
        [btnDouble setImage:[UIImage imageNamed:@"menu_ic_double_ov.png"] forState:UIControlStateSelected];
        [btnDouble setImage:[UIImage imageNamed:@"menu_ic_double_ov.png"] forState:UIControlStateSelected|UIControlStateHighlighted];
        [btnDouble setImage:[UIImage imageNamed:@"menu_ic_double_ov_disable.png"] forState:UIControlStateDisabled];
        [btnDouble addTarget:self action:@selector(onDouble) forControlEvents:UIControlEventTouchUpInside];
        btnDouble.tag = 999;
        [cell.contentView addSubview:btnDouble];
        
        int aChangableTabIdx = 4;
//        NSString *aLang = [PBLibOC getLocaleLanguage];
//        if ([aLang isEqualToString:@"fr"]) {
//            aChangableTabIdx = 6;
//        }
        
        btnSingle.enabled = YES;
        btnDouble.enabled = YES;
        
        btnSingle.selected = NO;
        btnDouble.selected = NO;
        
        if (nType == kTypeSingle) {
            if (nProgramIdx >= aChangableTabIdx) {
                btnSingle.selected = YES;
            } else {
                btnSingle.enabled = NO;
            }
        } else {
            if (nProgramIdx >= aChangableTabIdx) {
                btnDouble.selected = YES;
            } else {
                btnDouble.enabled = NO;
            }
        }
                
    } else if (indexPath.row == 2) {
        cell.backgroundColor = RGB(0xe9, 0xe9, 0xe9);
        lblTitle.hidden = YES;

        UIButton *btnExpression = [PBLibOC buildFlatColorButton:[UIColor clearColor]];
        btnExpression.frame = CGRectMake(0, 0, sizeCell.width/2, 115/2);
        [btnExpression setImage:[UIImage imageNamed:@"menu_ic_expr.png"] forState:UIControlStateNormal];
        [btnExpression setImage:[UIImage imageNamed:@"menu_ic_expr.png"] forState:UIControlStateNormal|UIControlStateHighlighted];
        [btnExpression setImage:[UIImage imageNamed:@"menu_ic_expr_ov.png"] forState:UIControlStateSelected];
        [btnExpression setImage:[UIImage imageNamed:@"menu_ic_expr_ov.png"] forState:UIControlStateSelected|UIControlStateHighlighted];
        [btnExpression addTarget:self action:@selector(onExpression) forControlEvents:UIControlEventTouchUpInside];
        btnExpression.tag = 999;
        [cell.contentView addSubview:btnExpression];
        
        UIButton *btnMassage = [PBLibOC buildFlatColorButton:[UIColor clearColor]];
        btnMassage.frame = CGRectMake(sizeCell.width/2, 0, sizeCell.width/2, 115/2);
        [btnMassage setImage:[UIImage imageNamed:@"menu_ic_mass.png"] forState:UIControlStateNormal];
        [btnMassage setImage:[UIImage imageNamed:@"menu_ic_mass.png"] forState:UIControlStateNormal|UIControlStateHighlighted];
        [btnMassage setImage:[UIImage imageNamed:@"menu_ic_mass_ov.png"] forState:UIControlStateSelected];
        [btnMassage setImage:[UIImage imageNamed:@"menu_ic_mass_ov.png"] forState:UIControlStateSelected|UIControlStateHighlighted];
        [btnMassage addTarget:self action:@selector(onMassage) forControlEvents:UIControlEventTouchUpInside];
        btnMassage.tag = 999;
        [cell.contentView addSubview:btnMassage];
        
        if (nMode == kModeMassage) {
            //Massage
            btnExpression.selected = NO;
            btnMassage.selected = YES;
        } else if (nMode == kModeExpression) {
            //Expression
            btnExpression.selected = YES;
            btnMassage.selected = NO;
        }
        
    } else if (indexPath.row == 3) {
        lblTitle.text = NSLocalizedString(@"Pressure", @"Pressure");
        lblTitle.font = BoldFont(16);
        lblTitle.frame = CGRectMake(0, 5, sizeCell.width/4, fCellH/2);
        
        lblPressure = [PBLibOC buildFlatColorLabel:[UIColor clearColor] TitleColor:kDefThemeColor];
        lblPressure.frame = CGRectMake(0, 5 + fCellH/2, sizeCell.width/4, fCellH/2);
        lblPressure.font = BoldFont(16);
        lblPressure.textAlignment = NSTextAlignmentRight;
        lblPressure.tag = 999;
        [cell.contentView addSubview:lblPressure];
        lblPressure.text = [NSString stringWithFormat:@"%d", nPressure + 1];

        UIButton *btnPressureDown = [PBLibOC buildFlatColorButton:[UIColor clearColor]];
        btnPressureDown.frame = CGRectMake(lblPressure.frame.origin.x + lblPressure.frame.size.width + 5, (fCellH+10-32)/2, 32, 32);
        [btnPressureDown setImage:[UIImage imageNamed:@"popup_btn_m.png"] forState:UIControlStateNormal];
        [btnPressureDown addTarget:self action:@selector(onPressureDown) forControlEvents:UIControlEventTouchUpInside];
        btnPressureDown.tag = 999;
        [cell.contentView addSubview:btnPressureDown];
        
        CGFloat fSliderX = btnPressureDown.frame.origin.x+btnPressureDown.frame.size.width+5;
        UISlider *sliderPressure = [[UISlider alloc] initWithFrame:CGRectMake(fSliderX, (fCellH+10-32)/2, sizeCell.width-fSliderX-65, 32)];
        [sliderPressure addTarget:self action:@selector(onPressureValue:) forControlEvents:UIControlEventValueChanged];
        sliderPressure.thumbTintColor = kDefThemeColor;
        sliderPressure.minimumTrackTintColor = kDefThemeColor;
        sliderPressure.tag = 999;
        [cell.contentView addSubview:sliderPressure];
        
        if (nMode == kModeExpression) {
            sliderPressure.minimumValue = EXP_PRESSURE_MIN;
            sliderPressure.maximumValue = EXP_PRESSURE_MAX;
        } else {
            sliderPressure.minimumValue = MSG_PRESSURE_MIN;
            sliderPressure.maximumValue = MSG_PRESSURE_MAX;
        }
        sliderPressure.value = nPressure;

        UIButton *btnPressureUp = [PBLibOC buildFlatColorButton:[UIColor clearColor]];
        btnPressureUp.frame = CGRectMake(sizeCell.width-60, (fCellH+10-32)/2, 32, 32);
        [btnPressureUp setImage:[UIImage imageNamed:@"popup_btn_p.png"] forState:UIControlStateNormal];
        [btnPressureUp addTarget:self action:@selector(onPressureUp) forControlEvents:UIControlEventTouchUpInside];
        btnPressureUp.tag = 999;
        [cell.contentView addSubview:btnPressureUp];
        
    } else if (indexPath.row == 4) {
        lblTitle.text = NSLocalizedString(@"Cycle", @"Cycle");
        lblTitle.font = BoldFont(16);
        lblTitle.frame = CGRectMake(0, 5, sizeCell.width/4, fCellH/2);
        
        lblCycle = [PBLibOC buildFlatColorLabel:[UIColor clearColor] TitleColor:kDefThemeColor];
        lblCycle.frame = CGRectMake(0, 5 + fCellH/2, sizeCell.width/4, fCellH/2);
        lblCycle.font = BoldFont(16);
        lblCycle.textAlignment = NSTextAlignmentRight;
        lblCycle.tag = 999;
        [cell.contentView addSubview:lblCycle];
        lblCycle.text = [NSString stringWithFormat:@"%d", nCycle + 1];
        
        UIButton *btnCycleDown = [PBLibOC buildFlatColorButton:[UIColor clearColor]];
        btnCycleDown.frame = CGRectMake(lblCycle.frame.origin.x + lblCycle.frame.size.width + 5, (fCellH+10-32)/2, 32, 32);
        [btnCycleDown setImage:[UIImage imageNamed:@"popup_btn_m.png"] forState:UIControlStateNormal];
        [btnCycleDown addTarget:self action:@selector(onCycleDown) forControlEvents:UIControlEventTouchUpInside];
        btnCycleDown.tag = 999;
        [cell.contentView addSubview:btnCycleDown];
        
        CGFloat fSliderX = btnCycleDown.frame.origin.x+btnCycleDown.frame.size.width+5;
        UISlider *sliderCycle = [[UISlider alloc] initWithFrame:CGRectMake(fSliderX, (fCellH+10-32)/2, sizeCell.width-fSliderX-65, 32)];
        [sliderCycle addTarget:self action:@selector(onCycleValue:) forControlEvents:UIControlEventValueChanged];
        sliderCycle.thumbTintColor = kDefThemeColor;
        sliderCycle.minimumTrackTintColor = kDefThemeColor;
        sliderCycle.tag = 999;
        [cell.contentView addSubview:sliderCycle];
        
        if (nMode == kModeExpression) {
            sliderCycle.minimumValue = EXP_CYCLE_MIN;
            sliderCycle.maximumValue = EXP_CYCLE_MAX;
        } else {
            sliderCycle.minimumValue = MSG_CYCLE_MIN;
            sliderCycle.maximumValue = MSG_CYCLE_MAX;
        }
        sliderCycle.value = nCycle;
        
        UIButton *btnCycleUp = [PBLibOC buildFlatColorButton:[UIColor clearColor]];
        btnCycleUp.frame = CGRectMake(sizeCell.width-60, (fCellH+10-32)/2, 32, 32);
        [btnCycleUp setImage:[UIImage imageNamed:@"popup_btn_p.png"] forState:UIControlStateNormal];
        [btnCycleUp addTarget:self action:@selector(onCycleUp) forControlEvents:UIControlEventTouchUpInside];
        btnCycleUp.tag = 999;
        [cell.contentView addSubview:btnCycleUp];
        
        UIView *vLine01 = [[UIView alloc] initWithFrame:CGRectMake(0, fCellH+10-1, sizeCell.width, 1)];
        vLine01.backgroundColor = kMenuLineGray;
        vLine01.tag = 999;
        [cell.contentView addSubview:vLine01];

    } else if (indexPath.row == 5) {
        
        UILabel *lblMin = [PBLibOC buildFlatColorLabel:[UIColor clearColor] TitleColor:kTextGray];
        lblMin.frame = CGRectMake(sizeCell.width/3, 0, sizeCell.width/3, fCellH/2);
        lblMin.font = AppFont(16);
        lblMin.textAlignment = NSTextAlignmentCenter;
        lblMin.tag = 999;
        lblMin.text = @"min";
        [cell.contentView addSubview:lblMin];

        UILabel *lblSec = [PBLibOC buildFlatColorLabel:[UIColor clearColor] TitleColor:kTextGray];
        lblSec.frame = CGRectMake(sizeCell.width*2/3, 0, sizeCell.width/3, fCellH/2);
        lblSec.font = AppFont(16);
        lblSec.textAlignment = NSTextAlignmentCenter;
        lblSec.tag = 999;
        lblSec.text = @"sec";
        [cell.contentView addSubview:lblSec];

        
    } else if (indexPath.row == 6) {
        lblTitle.text = NSLocalizedString(@"ElapsedTimeNewLine", @"Elapsed Time NewLine");
        lblTitle.numberOfLines = 2;
        lblTitle.frame = CGRectMake(0, 0, sizeCell.width/3, kTableCellPicker);

        pvOptionMin = [[UIPickerView alloc] initWithFrame:CGRectMake(sizeCell.width/3, 0, sizeCell.width/3, kTableCellPicker)];
        pvOptionMin.backgroundColor = kDefWhite;
        pvOptionMin.dataSource = self;
        pvOptionMin.delegate = self;
        pvOptionMin.showsSelectionIndicator = YES;
        pvOptionMin.translatesAutoresizingMaskIntoConstraints = NO;
        pvOptionMin.tag = kPickerMinTag;
        [cell.contentView addSubview:pvOptionMin];

        pvOptionSec = [[UIPickerView alloc] initWithFrame:CGRectMake(sizeCell.width*2/3, 0, sizeCell.width/3, kTableCellPicker)];
        pvOptionSec.backgroundColor = kDefWhite;
        pvOptionSec.dataSource = self;
        pvOptionSec.delegate = self;
        pvOptionSec.showsSelectionIndicator = YES;
        pvOptionSec.translatesAutoresizingMaskIntoConstraints = NO;
        pvOptionSec.tag = kPickerSecTag;
        [cell.contentView addSubview:pvOptionSec];
        
        int aMin = nLen / 60;
        int aSec = nLen % 60;
        
        [pvOptionMin selectRow:aMin inComponent:0 animated:NO];
        [pvOptionSec selectRow:aSec inComponent:0 animated:NO];

    } else if (indexPath.row == 7) {
        
        UIButton *btnCancel = [PBLibOC buildFlatColorButton:kCancelGray BorderColor:kCancelGray Radius:0];
        btnCancel.frame = CGRectMake(0, 0, sizeCell.width/2, fCellH+10);
        btnCancel.titleLabel.font = AppFont(18);
        [btnCancel setTitleColor:kDefThemeColor forState:UIControlStateNormal];
        [btnCancel setTitle:NSLocalizedString(@"NO", @"NO") forState:UIControlStateNormal];
        [btnCancel addTarget:self action:@selector(onClose) forControlEvents:UIControlEventTouchUpInside];
        btnCancel.tag = 999;
        [cell.contentView addSubview:btnCancel];
        
        UIButton *btnConfirm = [PBLibOC buildFlatColorButton:kDefThemeColor BorderColor:kDefThemeColor Radius:0];
        btnConfirm.frame = CGRectMake(sizeCell.width/2, 0, sizeCell.width/2, fCellH+10);
        btnConfirm.titleLabel.font = AppFont(18);
        [btnConfirm setTitleColor:kDefWhite forState:UIControlStateNormal];
        [btnConfirm setTitle:NSLocalizedString(@"YES", @"YES") forState:UIControlStateNormal];
        [btnConfirm addTarget:self action:@selector(onConfirm) forControlEvents:UIControlEventTouchUpInside];
        btnConfirm.tag = 999;
        [cell.contentView addSubview:btnConfirm];
        
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [PBLibOC performTransition:NO andView:self.view];
}

-(void)refreshPressure
{
    if (nMode == kModeExpression) {
        if (nPressure < EXP_PRESSURE_MIN) {
            nPressure = EXP_PRESSURE_MIN;
        }
        if (nPressure > EXP_PRESSURE_MAX) {
            nPressure = EXP_PRESSURE_MAX;
        }
    } else {
        if (nPressure < MSG_PRESSURE_MIN) {
            nPressure = MSG_PRESSURE_MIN;
        }
        if (nPressure > MSG_PRESSURE_MAX) {
            nPressure = MSG_PRESSURE_MAX;
        }
    }
    [tvList reloadData];
}

-(void)refreshCycle
{
    if (nMode == kModeExpression) {
        if (nCycle < EXP_CYCLE_MIN) {
            nCycle = EXP_CYCLE_MIN;
        }
        if (nCycle > EXP_CYCLE_MAX) {
            nCycle = EXP_CYCLE_MAX;
        }
    } else {
        if (nCycle < MSG_CYCLE_MIN) {
            nCycle = MSG_CYCLE_MIN;
        }
        if (nCycle > MSG_CYCLE_MAX) {
            nCycle = MSG_CYCLE_MAX;
        }
    }
    [tvList reloadData];
}

-(void)onSingle
{
    nType = kTypeSingle;
    [tvList reloadData];
    [delegate onChangeOpType:nType];
}

-(void)onDouble
{
    nType = kTypeDouble;
    [tvList reloadData];
    [delegate onChangeOpType:nType];
}

-(void)onExpression
{
    nMode = kModeExpression;
    [self refreshPressure];
    [self refreshCycle];
    [tvList reloadData];
}

-(void)onMassage
{
    nMode = kModeMassage;
    [self refreshPressure];
    [self refreshCycle];
    [tvList reloadData];
}

-(void)onPressureDown
{
    nPressure--;
    [self refreshPressure];
}

-(void)onPressureUp
{
    nPressure++;
    [self refreshPressure];
}

-(void)onPressureValue:(UISlider*)a_Slider
{
    nPressure = a_Slider.value;
    lblPressure.text = [NSString stringWithFormat:@"%d", nPressure + 1];
//    [tvList reloadData];
}

-(void)onCycleDown
{
    nCycle--;
    [self refreshCycle];
}

-(void)onCycleUp
{
    nCycle++;
    [self refreshCycle];
}

-(void)onCycleValue:(UISlider*)a_Slider
{
    nCycle = a_Slider.value;
    lblCycle.text = [NSString stringWithFormat:@"%d", nCycle + 1];
//    [tvList reloadData];
}

-(void)onPrev
{
    nProgramDataIdx--;
    if (nProgramDataIdx < 0) {
        nProgramDataIdx = 0;
    }
//    [self refreshDataNavi];
    [delegate initProgramEditPop:nProgramIdx ProgramDataIdx:nProgramDataIdx];
}

-(void)onNext
{
    nProgramDataIdx++;
    if (nProgramDataIdx > 7) {
        nProgramDataIdx = 7;
    }
//    [self refreshDataNavi];
    [delegate initProgramEditPop:nProgramIdx ProgramDataIdx:nProgramDataIdx];
}

-(void)onConfirm
{
    [PBLibOC performTransition:NO andView:self.view];
    [delegate onCloseProgramEdit:@{@"mode" : [NSNumber numberWithInt:nMode],
                                   @"type" : [NSNumber numberWithInt:nType],
                                   @"pressure" : [NSNumber numberWithInt:nPressure],
                                   @"cycle" : [NSNumber numberWithInt:nCycle],
                                   @"time" : [NSNumber numberWithInt:nLen]}];
}

-(void)onClose
{
    [PBLibOC performTransition:NO andView:self.view];
}

#pragma mark -
#pragma mark UIPickerView DataSource
// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (pickerView.tag == kPickerMinTag) {
        return [arrMin count];
    }
    return [arrSec count];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component;
{
    return kNavBarH/2;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *pickerLabel = (UILabel *)view;
    int nWL = 260;

    if (pickerLabel == nil) {
        pickerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, nWL, kNavBarH/2)];
        [pickerLabel setTextAlignment:NSTextAlignmentCenter];
        [pickerLabel setBackgroundColor:[UIColor clearColor]];
        [pickerLabel setFont:AppFont(16)];
    }
    
    long aRow = [pickerView selectedRowInComponent:component];
    if (row == aRow) {
        pickerLabel.textColor = kDefThemeColor;
    }

    NSString *aTitleName = @"";
    if (pickerView.tag == kPickerMinTag) {
        aTitleName = [PBLibOC filterNSNull:[[arrMin objectAtIndex:row] objectForKey:@"codename"]];
    } else {
        aTitleName = [PBLibOC filterNSNull:[[arrSec objectAtIndex:row] objectForKey:@"codename"]];
    }

    pickerLabel.text = aTitleName;
    return pickerLabel;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    [pvOptionMin reloadAllComponents];
    [pvOptionSec reloadAllComponents];
    
    long aMin = [pvOptionMin selectedRowInComponent:0];
    long aSec = [pvOptionSec selectedRowInComponent:0];
    
    if (aMin == 30) {
        aSec = 0;
        [pvOptionSec selectRow:aSec inComponent:0 animated:NO];
    }
    nLen = aMin*60 + aSec;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
