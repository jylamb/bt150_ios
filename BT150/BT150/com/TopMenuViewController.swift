//
//  TopMenuViewController.swift
//  BT150
//
//  Created by KOBONGHWAN on 15/11/2018.
//  Copyright © 2018 bistos. All rights reserved.
//

import UIKit

class TopMenuViewController: UIViewController {

    @IBOutlet weak var ivOperation: UIImageView!
    @IBOutlet weak var ivProgram: UIImageView!    
    @IBOutlet weak var ivStatistic: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(onBack(_:)), name: Notification.Name("UNWIND_TO_MAIN"), object: nil)
    }        
    
    @IBAction func onBack(_ sender: Any) {
        performSegue(withIdentifier: "UnwindToMain", sender: self)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
