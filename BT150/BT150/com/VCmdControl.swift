//
//  VCmdControl.swift
//  BT150
//
//  Created by KOBONGHWAN on 02/01/2019.
//  Copyright © 2019 bistos. All rights reserved.
//

import UIKit

class VCmdControl: NSObject {

//    static var CMD_MODE_CHANGE = ""
    
    static var arrCmd = [VoiceCommand]()
    static let cmdNone = VoiceCommand(VoiceCommand.CMD_NONE, Phrase: "")
    
    static var timerSendMessage : Timer?
    static var nTimerTaskSendMessageMax : Int = 30

    
    class func initVoiceCommand() {
        arrCmd.removeAll()
        
        arrCmd.append(VoiceCommand(VoiceCommand.CMD_MODE_CHANGE, Phrase: VCmdPhrase.CMD_MODE_CHANGE))        

        arrCmd.append(VoiceCommand(VoiceCommand.CMD_PRESSURE_UP, Phrase: VCmdPhrase.CMD_PRESSURE_UP))
        arrCmd.append(VoiceCommand(VoiceCommand.CMD_PRESSURE_DOWN, Phrase: VCmdPhrase.CMD_PRESSURE_DOWN))

        arrCmd.append(VoiceCommand(VoiceCommand.CMD_CYCLE_UP, Phrase: VCmdPhrase.CMD_CYCLE_UP))
        arrCmd.append(VoiceCommand(VoiceCommand.CMD_CYCLE_DOWN, Phrase: VCmdPhrase.CMD_CYCLE_DOWN))
        
        arrCmd.append(VoiceCommand(VoiceCommand.CMD_PROGRAMID_N, Phrase: VCmdPhrase.CMD_PROGRAMID_N, Type: VoiceCommand.CMD_TYPE_STEP))
        
        arrCmd.append(VoiceCommand(VoiceCommand.CMD_PLAY, Phrase: VCmdPhrase.CMD_PLAY))
        arrCmd.append(VoiceCommand(VoiceCommand.CMD_STOP, Phrase: VCmdPhrase.CMD_STOP))
        arrCmd.append(VoiceCommand(VoiceCommand.CMD_SKIP, Phrase: VCmdPhrase.CMD_SKIP))
        
        arrCmd.append(VoiceCommand(VoiceCommand.CMD_POWER, Phrase: VCmdPhrase.CMD_POWER))
        arrCmd.append(VoiceCommand(VoiceCommand.CMD_LAMP, Phrase: VCmdPhrase.CMD_LAMP))
        
        arrCmd.append(VoiceCommand(VoiceCommand.CMD_NUMBER_1, Phrase: VCmdPhrase.CMD_NUMBER_1))
        arrCmd.append(VoiceCommand(VoiceCommand.CMD_NUMBER_2, Phrase: VCmdPhrase.CMD_NUMBER_2))
        arrCmd.append(VoiceCommand(VoiceCommand.CMD_NUMBER_3, Phrase: VCmdPhrase.CMD_NUMBER_3))
        arrCmd.append(VoiceCommand(VoiceCommand.CMD_NUMBER_4, Phrase: VCmdPhrase.CMD_NUMBER_4))
        arrCmd.append(VoiceCommand(VoiceCommand.CMD_NUMBER_5, Phrase: VCmdPhrase.CMD_NUMBER_5))
        arrCmd.append(VoiceCommand(VoiceCommand.CMD_NUMBER_6, Phrase: VCmdPhrase.CMD_NUMBER_6))
        arrCmd.append(VoiceCommand(VoiceCommand.CMD_NUMBER_7, Phrase: VCmdPhrase.CMD_NUMBER_7))
        arrCmd.append(VoiceCommand(VoiceCommand.CMD_NUMBER_8, Phrase: VCmdPhrase.CMD_NUMBER_8))
    }
    
    class func initAddingPhrase(_ a_Language : String) {
    
        if a_Language == VOICE_REC_LANG_KOREAN {
            addPhrase(VoiceCommand.CMD_PRESSURE_UP, Phrase: "늦게");

            addPhrase(VoiceCommand.CMD_PRESSURE_DOWN, Phrase: "맞게");
            addPhrase(VoiceCommand.CMD_PRESSURE_DOWN, Phrase: "낱개");
            addPhrase(VoiceCommand.CMD_PRESSURE_DOWN, Phrase: "맛게");
            addPhrase(VoiceCommand.CMD_PRESSURE_DOWN, Phrase: "업계");

            addPhrase(VoiceCommand.CMD_CYCLE_UP, Phrase: "다르게");

            addPhrase(VoiceCommand.CMD_PROGRAMID_N, Phrase: "프로그램 id");
            addPhrase(VoiceCommand.CMD_PROGRAMID_N, Phrase: "변호");

            addPhrase(VoiceCommand.CMD_NUMBER_1, Phrase: "일본");
            addPhrase(VoiceCommand.CMD_NUMBER_1, Phrase: "일번");

            addPhrase(VoiceCommand.CMD_NUMBER_2, Phrase: "이번");
            addPhrase(VoiceCommand.CMD_NUMBER_3, Phrase: "삼번");
            addPhrase(VoiceCommand.CMD_NUMBER_4, Phrase: "사번");
            addPhrase(VoiceCommand.CMD_NUMBER_5, Phrase: "오번");
            addPhrase(VoiceCommand.CMD_NUMBER_6, Phrase: "육번");
            addPhrase(VoiceCommand.CMD_NUMBER_7, Phrase: "칠번");
            addPhrase(VoiceCommand.CMD_NUMBER_8, Phrase: "팔번");

            addPhrase(VoiceCommand.CMD_NUMBER_4, Phrase: "변호사");
        
        } else if a_Language == VOICE_REC_LANG_FRENCH {
            
        } else if a_Language == VOICE_REC_LANG_ENGLISH {
         
            addPhrase(VoiceCommand.CMD_MODE_CHANGE, Phrase: "changing");
            addPhrase(VoiceCommand.CMD_MODE_CHANGE, Phrase: "change it");
            addPhrase(VoiceCommand.CMD_MODE_CHANGE, Phrase: "changey");

            addPhrase(VoiceCommand.CMD_PRESSURE_UP, Phrase: "Hi");

            //lower, hello, no, law, lou, blue, lowe, lowes,
            addPhrase(VoiceCommand.CMD_PRESSURE_DOWN, Phrase: "lower");
            addPhrase(VoiceCommand.CMD_PRESSURE_DOWN, Phrase: "hello");
            addPhrase(VoiceCommand.CMD_PRESSURE_DOWN, Phrase: "no");
            addPhrase(VoiceCommand.CMD_PRESSURE_DOWN, Phrase: "law");
            addPhrase(VoiceCommand.CMD_PRESSURE_DOWN, Phrase: "lou");
            addPhrase(VoiceCommand.CMD_PRESSURE_DOWN, Phrase: "blue");
            addPhrase(VoiceCommand.CMD_PRESSURE_DOWN, Phrase: "lowe");
            addPhrase(VoiceCommand.CMD_PRESSURE_DOWN, Phrase: "lowes");
            addPhrase(VoiceCommand.CMD_PRESSURE_DOWN, Phrase: "lo");
            
            addPhrase(VoiceCommand.CMD_CYCLE_UP, Phrase: "fest");
            addPhrase(VoiceCommand.CMD_CYCLE_UP, Phrase: "best");
            addPhrase(VoiceCommand.CMD_CYCLE_UP, Phrase: "first");
            addPhrase(VoiceCommand.CMD_CYCLE_UP, Phrase: "past");
            addPhrase(VoiceCommand.CMD_CYCLE_UP, Phrase: "pest");
            addPhrase(VoiceCommand.CMD_CYCLE_UP, Phrase: "speedy");

            addPhrase(VoiceCommand.CMD_CYCLE_DOWN, Phrase: "snow");
            addPhrase(VoiceCommand.CMD_CYCLE_DOWN, Phrase: "below");
            
            addPhrase(VoiceCommand.CMD_LAMP, Phrase: "lymph");
            addPhrase(VoiceCommand.CMD_LAMP, Phrase: "lemfo");
            addPhrase(VoiceCommand.CMD_LAMP, Phrase: "limp");
            addPhrase(VoiceCommand.CMD_LAMP, Phrase: "light");
            addPhrase(VoiceCommand.CMD_LAMP, Phrase: "right");
            addPhrase(VoiceCommand.CMD_LAMP, Phrase: "length");
            addPhrase(VoiceCommand.CMD_LAMP, Phrase: "planet");
            
            addPhrase(VoiceCommand.CMD_POWER, Phrase: "poem");
            addPhrase(VoiceCommand.CMD_POWER, Phrase: "piper");
//            addPhrase(VoiceCommand.CMD_POWER, Phrase: "are");            
        }
        
    }
    
    class func printPhrase() {
        for aCmd in arrCmd {
            aCmd.printPhrase()
        }
    }

    class func addPhrase(_ a_CmdID : Int, Phrase a_Phrase : String) {
        for aCmd in arrCmd {
            if (aCmd.nCmdCode == a_CmdID) {
                aCmd.addPhrase(a_Phrase)
                break
            }
        }
    }
    
    class func checkCommandString(_ a_Search : String, Cmd a_Cmd : VoiceCommand) -> Bool {
        let aChunk : String = a_Search.replacingOccurrences(of: " ", with: "")
        if a_Cmd.checkCommand(aChunk) {
//            print("checkCommandString: aChunk:"  + aChunk)
            return true
        }
        return false
    }

    static var sLastVCmdTrim = ""
    
    class func findVoiceCommand(_ a_Search : String) -> VoiceCommand {
    
        var aTrim = a_Search.trimmingCharacters(in: .whitespacesAndNewlines).lowercased()
//        print("aTrim:" + aTrim)
        
        for aCmd in arrCmd {
            
            if PBCommon.curSpeechLanguage == VOICE_REC_LANG_ENGLISH {
                
                let aStepNo = checkNumericValue(aTrim)
                if aStepNo > 0 {
                    if sLastVCmdTrim == "number" {
                        aTrim = "\(sLastVCmdTrim) \(aTrim)"
                    }
                }
            }
            
            if checkCommandString(aTrim, Cmd: aCmd) {
                if aCmd.nCmdType == VoiceCommand.CMD_TYPE_NORMAL {
                    return aCmd
                } else {
                    //CMD_TYPE_STEP
                    var aStepVal : String = ""
                    if PBCommon.curSpeechLanguage == VOICE_REC_LANG_KOREAN {
                        aStepVal = aTrim.substring(with: aTrim.count-1 ..< aTrim.count)
                    } else {
                        let aWord = aTrim.components(separatedBy: " ")
                        aStepVal = aWord[aWord.count-1]
                    }
//                    print("aStepVal:" + aStepVal)
                    
                    let aStepNo = checkNumericValue(aStepVal)
//                    print("aStepVal:\(aStepVal) aStepNo:\(aStepNo)")
                    if aStepNo > 0 {
                        aCmd.nStepVal = aStepNo
                        return aCmd
                    }
                }
            }
        }

        sLastVCmdTrim = aTrim
        print("sLastVCmdTrim: \(sLastVCmdTrim)")

        return cmdNone
    }
    
    class func checkNumericValue(_ a_Value : String) -> Int {
        var aRetVal : Int = -1
        
        if a_Value.isNumber {
            aRetVal = Int(a_Value)!
            
        } else {            
            var aValue = a_Value.trimmingCharacters(in: .whitespacesAndNewlines).lowercased()
            
            if aValue == VCmdPhrase.SPEECH_NUMBER_ONE {
                aRetVal = 1
            
            } else if aValue == VCmdPhrase.SPEECH_NUMBER_TWO
                || aValue == VCmdPhrase.SPEECH_NUMBER_TWO1
                || aValue == VCmdPhrase.SPEECH_NUMBER_TWO2
                || aValue == VCmdPhrase.SPEECH_NUMBER_TWO3
                || aValue == VCmdPhrase.SPEECH_NUMBER_TWO4
                || aValue == VCmdPhrase.SPEECH_NUMBER_TWO5
            {
                aRetVal = 2
            
            } else if aValue == VCmdPhrase.SPEECH_NUMBER_THREE {
                aRetVal = 3
            
            } else if aValue == VCmdPhrase.SPEECH_NUMBER_Four
                || aValue == VCmdPhrase.SPEECH_NUMBER_Four1
                || aValue == VCmdPhrase.SPEECH_NUMBER_Four2
                || aValue == VCmdPhrase.SPEECH_NUMBER_Four3
                || aValue == VCmdPhrase.SPEECH_NUMBER_Four4
                || aValue == VCmdPhrase.SPEECH_NUMBER_Four5
                || aValue == VCmdPhrase.SPEECH_NUMBER_Four6
                || aValue == VCmdPhrase.SPEECH_NUMBER_Four7
                || aValue == VCmdPhrase.SPEECH_NUMBER_Four8
                || aValue == VCmdPhrase.SPEECH_NUMBER_Four9
            {
                aRetVal = 4

            } else if aValue == VCmdPhrase.SPEECH_NUMBER_Five
                || aValue == VCmdPhrase.SPEECH_NUMBER_Five1
                || aValue == VCmdPhrase.SPEECH_NUMBER_Five2
            {
                aRetVal = 5

            } else if aValue == VCmdPhrase.SPEECH_NUMBER_SIX {
                aRetVal = 6
            
            } else if aValue == VCmdPhrase.SPEECH_NUMBER_SEVEN {
                aRetVal = 7
            
            } else if aValue == VCmdPhrase.SPEECH_NUMBER_EIGHT {
                aRetVal = 8

            } else if aValue == VCmdPhrase.SPEECH_NUMBER_NINE {
                aRetVal = 9

            } else if aValue == VCmdPhrase.SPEECH_NUMBER_TEN {
                aRetVal = 10
            }
            
        }

        
        return aRetVal
    }
    
    class func broadcastVoiceCommand(_ a_Cmd : VoiceCommand) {
        
        print("broadcastVoiceCommand: nCmdCode:\(a_Cmd.nCmdCode) nCmdType:\(a_Cmd.nCmdType)")
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MSG_VCMD_RECV"),
                                        object: nil,
                                        userInfo: ["CmdCode" : a_Cmd.nCmdCode, "CmdType" : a_Cmd.nCmdType, "StepVal" : a_Cmd.nStepVal])
    }

    class func getCommandName(_ a_Cmd : Int) -> String {
        
        var aCommandName = ""
        
        switch a_Cmd {
            case VoiceCommand.CMD_MODE_CHANGE:
                aCommandName = VCmdPhrase.CMD_MODE_CHANGE
                break
            case VoiceCommand.CMD_PRESSURE_UP:
                aCommandName = VCmdPhrase.CMD_PRESSURE_UP
                break
            case VoiceCommand.CMD_PRESSURE_DOWN:
                aCommandName = VCmdPhrase.CMD_PRESSURE_DOWN
                break
            case VoiceCommand.CMD_CYCLE_UP:
                aCommandName = VCmdPhrase.CMD_CYCLE_UP
                break
            case VoiceCommand.CMD_CYCLE_DOWN:
                aCommandName = VCmdPhrase.CMD_CYCLE_DOWN
                break
            case VoiceCommand.CMD_PLAY:
                aCommandName = VCmdPhrase.CMD_PLAY
                break
            case VoiceCommand.CMD_STOP:
                aCommandName = VCmdPhrase.CMD_STOP
                break
            case VoiceCommand.CMD_SKIP:
                aCommandName = VCmdPhrase.CMD_SKIP
                break
            default:
                aCommandName = ""
                break
        }
        
        return aCommandName
    }
    
    class func handleVoiceCommand(_ a_Cmd : VoiceCommand) -> Bool {
    
        var aRet = true
//        let aCmd = findVoiceCommand(a_Phrase)
        print("handleVoiceCommand:" + getCommandName(a_Cmd.nCmdCode))
        if a_Cmd != cmdNone {
            if a_Cmd.nCmdType == VoiceCommand.CMD_TYPE_NORMAL {
                switch a_Cmd.nCmdCode {
                    case VoiceCommand.CMD_POWER, VoiceCommand.CMD_LAMP:
                        if PBCommon.nActivityNo != PBCommon.ACT_MENU01 {
                            PBCommon.createOperationActivity()
                        }
                        sendMessageWhenActivityIsStarted(PBCommon.ACT_MENU01, Cmd: a_Cmd)
                        break
                    case VoiceCommand.CMD_MODE_CHANGE:
                        if PBCommon.nVoiceMode == PBCommon.VOICE_MODE_PROGRAM_EDIT {
                            broadcastVoiceCommand(a_Cmd)
    
                        } else {
                            if PBCommon.nActivityNo != PBCommon.ACT_MENU01 {
                                PBCommon.createOperationActivity()
                            }
                            sendMessageWhenActivityIsStarted(PBCommon.ACT_MENU01, Cmd: a_Cmd)
                        }
                        break
                    case VoiceCommand.CMD_PRESSURE_UP:
                        if PBCommon.nVoiceMode == PBCommon.VOICE_MODE_PROGRAM_EDIT {
                            broadcastVoiceCommand(a_Cmd)
    
                        } else {
                            if PBCommon.nActivityNo != PBCommon.ACT_MENU01 {
                                PBCommon.createOperationActivity()
                            }
                            sendMessageWhenActivityIsStarted(PBCommon.ACT_MENU01, Cmd: a_Cmd)
                        }
                        break
                    case VoiceCommand.CMD_PRESSURE_DOWN:
                        if PBCommon.nVoiceMode == PBCommon.VOICE_MODE_PROGRAM_EDIT {
                            broadcastVoiceCommand(a_Cmd)
    
                        } else {
                            if PBCommon.nActivityNo != PBCommon.ACT_MENU01 {
                                PBCommon.createOperationActivity()
                            }
                            sendMessageWhenActivityIsStarted(PBCommon.ACT_MENU01, Cmd: a_Cmd)
                        }
                        break
                    case VoiceCommand.CMD_CYCLE_UP:
                        if PBCommon.nVoiceMode == PBCommon.VOICE_MODE_PROGRAM_EDIT {
                            broadcastVoiceCommand(a_Cmd)
        
                        } else {
                            if PBCommon.nActivityNo != PBCommon.ACT_MENU01 {
                                PBCommon.createOperationActivity()
                            }
                            sendMessageWhenActivityIsStarted(PBCommon.ACT_MENU01, Cmd: a_Cmd)
                        }
                        break
                    case VoiceCommand.CMD_CYCLE_DOWN:
                        if PBCommon.nVoiceMode == PBCommon.VOICE_MODE_PROGRAM_EDIT {
                            broadcastVoiceCommand(a_Cmd)
    
                        } else {
                            if PBCommon.nActivityNo != PBCommon.ACT_MENU01 {
                                PBCommon.createOperationActivity()
                            }
                            sendMessageWhenActivityIsStarted(PBCommon.ACT_MENU01, Cmd: a_Cmd)
                        }
                        break
                    case VoiceCommand.CMD_PLAY:
                        if PBCommon.nVoiceMode == PBCommon.VOICE_MODE_PROGRAM_EDIT {
    
                        } else {
                            if PBCommon.nActivityNo != PBCommon.ACT_MENU02 {
                                PBCommon.createProgramActivity()
                            }
                            sendMessageWhenActivityIsStarted(PBCommon.ACT_MENU02, Cmd: a_Cmd)
                        }
                        break
                    case VoiceCommand.CMD_STOP:
                        if PBCommon.nVoiceMode == PBCommon.VOICE_MODE_PROGRAM_EDIT {
    
                        } else {
                            if PBCommon.nActivityNo != PBCommon.ACT_MENU02 {
                                PBCommon.createProgramActivity()
                            }
                            sendMessageWhenActivityIsStarted(PBCommon.ACT_MENU02, Cmd: a_Cmd)
                        }
                        break

                    case VoiceCommand.CMD_SKIP:
                        if PBCommon.nVoiceMode == PBCommon.VOICE_MODE_PROGRAM_EDIT {
                        
                        } else {
                            if PBCommon.nActivityNo != PBCommon.ACT_MENU02 {
                                PBCommon.createProgramActivity()
                            }
                            sendMessageWhenActivityIsStarted(PBCommon.ACT_MENU02, Cmd: a_Cmd)
                        }
                        break

                    case VoiceCommand.CMD_NUMBER_1, VoiceCommand.CMD_NUMBER_2, VoiceCommand.CMD_NUMBER_3, VoiceCommand.CMD_NUMBER_4, VoiceCommand.CMD_NUMBER_5, VoiceCommand.CMD_NUMBER_6, VoiceCommand.CMD_NUMBER_7, VoiceCommand.CMD_NUMBER_8:
                        if PBCommon.nVoiceMode != PBCommon.VOICE_MODE_PROGRAM_EDIT {
                            if PBCommon.nActivityNo != PBCommon.ACT_MENU02 {
                                PBCommon.createProgramActivity()
                            }
                            sendMessageWhenActivityIsStarted(PBCommon.ACT_MENU02, Cmd: a_Cmd)
                        }
                        break
                    default:
                        aRet = false
                        break
                }
    
            } else if a_Cmd.nCmdType == VoiceCommand.CMD_TYPE_STEP {
    
                switch a_Cmd.nCmdCode {
                case VoiceCommand.CMD_PROGRAMID_N:
                    if PBCommon.nVoiceMode != PBCommon.VOICE_MODE_PROGRAM_EDIT {
                        if PBCommon.nActivityNo != PBCommon.ACT_MENU02 {
                            PBCommon.createProgramActivity()
                        }
                        sendMessageWhenActivityIsStarted(PBCommon.ACT_MENU02, Cmd: a_Cmd)
                    }
                    break
                default:
                    aRet = false
                    break
                }
    
            } else {
                aRet = false
            }
    
        } else {
            aRet = false
        }
    
        return aRet
    }
    
    class func stopSendMessageTimer() {
        nTimerTaskSendMessageMax = 30
        
        if timerSendMessage != nil {
            timerSendMessage?.invalidate()
            timerSendMessage = nil
        }
    }

    class func sendMessageWhenActivityIsStarted(_ a_ActID : Int, Cmd a_Cmd : VoiceCommand ) {
        
        stopSendMessageTimer()
        
        timerSendMessage = Timer.scheduledTimer(timeInterval: 0.1,
                                                target: self,
                                                selector: #selector(timerTaskSendMessage),
                                                userInfo: ["ActID" : a_ActID, "Cmd" : a_Cmd],
                                                repeats: true)
    }
    
    @objc class func timerTaskSendMessage(_ timer : Timer) {
        
        let userInfo = timer.userInfo as! Dictionary <String, AnyObject>
        
        if let aActID = userInfo["ActID"] as? Int {
            if let aCmd = userInfo["Cmd"] as? VoiceCommand {
                print("timerTaskSendMessage-1 aActID:\(aActID) aCmd:\(aCmd)")
             
                if PBCommon.nActivityNo == aActID {
                    stopSendMessageTimer();
                    
                    broadcastVoiceCommand(aCmd)
                }
            }
        }
        
        if nTimerTaskSendMessageMax < 0 {
            stopSendMessageTimer()
        }
        
        nTimerTaskSendMessageMax -= 1
    }
    
}
