//
//  SettingsViewController.m
//  BT150
//
//  Created by KOBONGHWAN on 04/12/2018.
//  Copyright © 2018 bistos. All rights reserved.
//

#import "SettingsViewController.h"
#import "PBLibOC.h"

#define kTableCellPicker    100

#define kPickerMinTag       501
#define kPickerSecTag       502
#define kPickerLanTag       601


@interface SettingsViewController ()
{
    NSString *sUnit;
    NSString *sDeviceName;
    NSString *sDeviceUUID;
    NSString *sOnline;
    NSString *sShowResult;
    NSString *sVCmdEnable;
//    NSString *sLanguage;

    UITableView *tvList;
    UIPickerView *pvOptionLanguage;
    UIPickerView *pvOptionMin;
    UIPickerView *pvOptionSec;

    CGSize sizeView;
    CGFloat fCellH;
    NSMutableArray *arrLanguage;
    int nCurSelLangIdx;
    
    NSMutableArray *arrMin;
    NSMutableArray *arrSec;
    int nLen;

}
@end

@implementation SettingsViewController

@synthesize delegate;
//@synthesize sUnit;
//@synthesize sDevice;
//@synthesize sOnline;
//@synthesize sShowResult;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = RGBA(0, 0, 0, 0.6);
    sizeView = self.view.frame.size;
    nCurSelLangIdx = 0;
}

-(void)initControls
{
    fCellH = kNavBarH;
    
//    CGFloat fTableH = (fCellH + 10) * 2 + fCellH * 5 + kTableCellPicker*2 + fCellH / 2;
    CGFloat fTableH = (fCellH + 10) * 2 + fCellH * 6 + kTableCellPicker + fCellH / 2;
    if ([[PBLibOC getLocaleLanguage] isEqualToString:@"fr"]) {
//        fTableH = (fCellH + 10) * 2 + fCellH * 3 + kTableCellPicker;
        fTableH = (fCellH + 10) * 2 + fCellH * 4;
    }
    CGFloat fTableY = (sizeView.height - fTableH) / 2;
    
    if (tvList == nil) {
        
        tvList = [[UITableView alloc] initWithFrame:CGRectMake(10, fTableY, sizeView.width-20, fTableH) style:UITableViewStylePlain];
        tvList.bounces = NO;
        tvList.separatorStyle = UITableViewCellSeparatorStyleNone;
        tvList.scrollEnabled = YES;
        tvList.allowsSelection = NO;
        tvList.delegate = self;
        tvList.dataSource = self;
        tvList.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:tvList];
        
        arrLanguage =[[NSMutableArray alloc] init];
    }
    
    [arrLanguage removeAllObjects];
    [arrLanguage addObject:[[NSDictionary alloc] initWithObjectsAndKeys:
                         @"ko-KR", @"code",
                         NSLocalizedString(@"LanguageKorean", @"Korean"), @"codename",
                         nil]];
    [arrLanguage addObject:[[NSDictionary alloc] initWithObjectsAndKeys:
                            @"en-US", @"code",
                            NSLocalizedString(@"LanguageEnglish", @"English"), @"codename",
                            nil]];
//    [arrLanguage addObject:[[NSDictionary alloc] initWithObjectsAndKeys:
//                            @"en-US", @"code",
//                            NSLocalizedString(@"LanguageFrench", @"French"), @"codename",
//                            nil]];

    
//    sDevice = @"No devices registered.";
    sDeviceName = @"";
    sDeviceUUID = @"";
    sUnit = @"ml";
    sShowResult = @"show";
    sVCmdEnable = @"enable";
    
    NSString *aDevAddr = [[NSUserDefaults standardUserDefaults] objectForKey:@"device_addr"];
    NSString *aDevName = [[NSUserDefaults standardUserDefaults] objectForKey:@"device_name"];
    NSString *aUnit = [[NSUserDefaults standardUserDefaults] objectForKey:@"unit"];
    NSString *aLanguage = [[NSUserDefaults standardUserDefaults] objectForKey:@"vcmd_language"];
    NSString *aShowResult = [[NSUserDefaults standardUserDefaults] objectForKey:@"vcmd_showresult"];
    NSString *aVCmdEnable = [[NSUserDefaults standardUserDefaults] objectForKey:@"vcmd_enable"];
    NSString *aProgramLastTime = [[NSUserDefaults standardUserDefaults] objectForKey:@"program_last_time"];
    
    
    if(aDevAddr != nil && aDevAddr.length > 0) {
        sDeviceUUID = aDevAddr;
    }
    if(aDevName != nil && aDevName.length > 0) {
        sDeviceName = aDevName;
    }
    if(aUnit != nil && aUnit.length > 0) {
        sUnit = aUnit;
    }
    if(aLanguage != nil && aLanguage.length > 0) {
        if ([aLanguage isEqualToString:@"ko-KR"]) {
            nCurSelLangIdx = 0;
        } else {
            nCurSelLangIdx = 1;
        }
    }
    if(aShowResult != nil && aShowResult.length > 0) {
        sShowResult = aShowResult;
    }
    if(aVCmdEnable != nil && aVCmdEnable.length > 0) {
        sVCmdEnable = aVCmdEnable;
    }
    
    arrMin = [[NSMutableArray alloc] init];
    arrSec = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < 100; i++) {
        [arrMin addObject:@{@"code" : [NSString stringWithFormat:@"%d", i], @"codename" : [NSString stringWithFormat:@"%02d", i]}];
    }
    
    for (int i = 0; i < 60; i++) {
        [arrSec addObject:@{@"code" : [NSString stringWithFormat:@"%d", i], @"codename" : [NSString stringWithFormat:@"%02d", i]}];
    }
        
    nLen = [PBLibOC convertToInt:aProgramLastTime];

    [tvList reloadData];
    
}

#pragma mark -
#pragma mark UITableViewDelegate, UITableViewDataSource
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int aLastIdx = 10;
    if ([[PBLibOC getLocaleLanguage] isEqualToString:@"fr"]) {
        aLastIdx = 6;
    }
    
    if (indexPath.row == 0 || indexPath.row == aLastIdx) {
        return fCellH + 10;
    } else if (indexPath.row == 3) {
//        return kTableCellPicker;
        return 0;
    } else if (indexPath.row == 6) {
        return fCellH / 2;
    } else if (indexPath.row == 7) {
        return kTableCellPicker;
    }
    return fCellH;
}

-(NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section
{
    if ([[PBLibOC getLocaleLanguage] isEqualToString:@"fr"]) {
        return 7;
    }
    return 11;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = kDefWhite;
    
    for(UIView *subView in cell.contentView.subviews) {
        if (subView.tag >= 100) {
            [subView removeFromSuperview];
        }
    }
    
    CGSize sizeCell = tvList.frame.size;

    UILabel *lblTitle = [PBLibOC buildFlatColorLabel:[UIColor clearColor] TitleColor:kTextGray];
    lblTitle.frame = CGRectMake(0, 0, sizeCell.width/3, fCellH);
    lblTitle.font = AppFont(16);
    lblTitle.textAlignment = NSTextAlignmentRight;
    lblTitle.tag = 999;
    [cell.contentView addSubview:lblTitle];
    lblTitle.text = @"";
    
    if (indexPath.row == 0) {
        lblTitle.hidden = YES;
        cell.backgroundColor = kDefThemeColor;
        
        UILabel *lblPopTitle = [PBLibOC buildFlatColorLabel:[UIColor clearColor] TitleColor:kTextBlack];
        lblPopTitle.frame = CGRectMake(10, 0, sizeCell.width-20, fCellH+10);
        lblPopTitle.font = AppFont(18);
        lblPopTitle.textColor = kDefWhite;
        lblPopTitle.tag = 999;
        lblPopTitle.text = NSLocalizedString(@"Settings", @"Settings");
        [cell.contentView addSubview:lblPopTitle];
        
        UIButton *btnClose = [UIButton buttonWithType:UIButtonTypeCustom];
        btnClose.frame = CGRectMake(sizeCell.width-60, (fCellH+10-40)/2, 40, 40);
        [btnClose setImage:[UIImage imageNamed:@"ic_close_white_24dp.png"] forState:UIControlStateNormal];
        [btnClose addTarget:self action:@selector(onClose) forControlEvents:UIControlEventTouchUpInside];
        btnClose.tag = 999;
        [cell.contentView addSubview:btnClose];
        
    } else if (indexPath.row == 1) {
        lblTitle.text = NSLocalizedString(@"Unit", @"Unit");
        
        CGFloat fCheckW = 30;
        CGFloat fCheckTextW = 100;

        UIButton *chkML = [UIButton buttonWithType:UIButtonTypeCustom];
        chkML.frame = CGRectMake(sizeCell.width/3 + 40, (fCellH-fCheckW)/2, fCheckW, fCheckW);
        [chkML setImage:[UIImage imageNamed:@"check_n.png"] forState:UIControlStateNormal];
        [chkML setImage:[UIImage imageNamed:@"check_n.png"] forState:UIControlStateNormal|UIControlStateHighlighted];
        [chkML setImage:[UIImage imageNamed:@"check_o.png"] forState:UIControlStateSelected];
        [chkML setImage:[UIImage imageNamed:@"check_o.png"] forState:UIControlStateSelected|UIControlStateHighlighted];
        [chkML addTarget:self action:@selector(onML) forControlEvents:UIControlEventTouchUpInside];
        chkML.tag = 999;
        [cell.contentView addSubview:chkML];
        
        UIButton *btnML = [PBLibOC buildFlatColorButton:[UIColor clearColor]];
        btnML.frame = CGRectMake(chkML.frame.origin.x+fCheckW, (fCellH-20)/2, fCheckTextW, 20);
        btnML.titleLabel.font = AppFont(14);
        btnML.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [btnML setTitleColor:kTextBlack forState:UIControlStateNormal];
        [btnML setTitle:@" ml" forState:UIControlStateNormal];
        [btnML addTarget:self action:@selector(onML) forControlEvents:UIControlEventTouchUpInside];
        btnML.tag = 999;
        [cell.contentView addSubview:btnML];
        
        UIButton *chkOZ = [UIButton buttonWithType:UIButtonTypeCustom];
        chkOZ.frame = CGRectMake(sizeCell.width*2/3 + 10, (fCellH-fCheckW)/2, fCheckW, fCheckW);
        [chkOZ setImage:[UIImage imageNamed:@"check_n.png"] forState:UIControlStateNormal];
        [chkOZ setImage:[UIImage imageNamed:@"check_n.png"] forState:UIControlStateNormal|UIControlStateHighlighted];
        [chkOZ setImage:[UIImage imageNamed:@"check_o.png"] forState:UIControlStateSelected];
        [chkOZ setImage:[UIImage imageNamed:@"check_o.png"] forState:UIControlStateSelected|UIControlStateHighlighted];
        [chkOZ addTarget:self action:@selector(onOZ) forControlEvents:UIControlEventTouchUpInside];
        chkOZ.tag = 999;
        [cell.contentView addSubview:chkOZ];
        
        UIButton *btnOZ = [PBLibOC buildFlatColorButton:[UIColor clearColor]];
        btnOZ.frame = CGRectMake(chkOZ.frame.origin.x+fCheckW, (fCellH-20)/2, fCheckTextW, 20);
        btnOZ.titleLabel.font = AppFont(14);
        btnOZ.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [btnOZ setTitleColor:kTextBlack forState:UIControlStateNormal];
        [btnOZ setTitle:@" oz" forState:UIControlStateNormal];
        [btnOZ addTarget:self action:@selector(onOZ) forControlEvents:UIControlEventTouchUpInside];
        btnOZ.tag = 999;
        [cell.contentView addSubview:btnOZ];
        
        if ([sUnit isEqualToString:@"ml"]) {
            chkML.selected = YES;
            chkOZ.selected = NO;
        } else {
            chkML.selected = NO;
            chkOZ.selected = YES;
        }
        
        UIView *vLine01 = [[UIView alloc] initWithFrame:CGRectMake(0, fCellH-1, sizeCell.width, 1)];
        vLine01.backgroundColor = kMenuLineGray;
        vLine01.tag = 999;
        [cell.contentView addSubview:vLine01];
        
    } else if (indexPath.row == 2) {
        lblTitle.text = NSLocalizedString(@"ConnectedDevice", @"Connected Device");
        lblTitle.numberOfLines = 2;
        
        UILabel *lblDeviceName = [PBLibOC buildFlatColorLabel:[UIColor clearColor] TitleColor:kTextGray];
        lblDeviceName.frame = CGRectMake(sizeCell.width/3 + 5, 0, sizeCell.width*2/3-50, fCellH);
        lblDeviceName.font = AppFont(16);
        lblDeviceName.textAlignment = NSTextAlignmentLeft;
        lblDeviceName.numberOfLines = 2;
        lblDeviceName.tag = 999;
        [cell.contentView addSubview:lblDeviceName];
        lblDeviceName.text = sDeviceName;
        
//        UILabel *lblDeviceUUID = [PBLibOC buildFlatColorLabel:[UIColor clearColor] TitleColor:kTextGray];
//        lblDeviceUUID.frame = CGRectMake(sizeCell.width/3 + 10, fCellH/2, sizeCell.width*2/3-50, fCellH/2);
//        lblDeviceUUID.font = AppFont(12);
//        lblDeviceUUID.adjustsFontSizeToFitWidth = YES;
//        lblDeviceUUID.textAlignment = NSTextAlignmentCenter;
//        lblDeviceUUID.tag = 999;
//        [cell.contentView addSubview:lblDeviceUUID];
//        lblDeviceUUID.text = sDeviceUUID;
//        if (lblDeviceUUID.text.length == 0) {
//            lblDeviceName.text = @"No devices registered.";
//            lblDeviceName.frame = CGRectMake(sizeCell.width/3 + 5, 0, sizeCell.width*2/3-50, fCellH);
//        }
        
        UIButton *btnDelDevice = [UIButton buttonWithType:UIButtonTypeCustom];
        btnDelDevice.frame = CGRectMake(sizeCell.width-40, (fCellH-40)/2, 40, 40);
        [btnDelDevice setImage:[UIImage imageNamed:@"program_btn_del.png"] forState:UIControlStateNormal];
        [btnDelDevice addTarget:self action:@selector(onDelDevice) forControlEvents:UIControlEventTouchUpInside];
        btnDelDevice.tag = 999;
        [cell.contentView addSubview:btnDelDevice];
        
        UIView *vLine01 = [[UIView alloc] initWithFrame:CGRectMake(0, fCellH-1, sizeCell.width, 1)];
        vLine01.backgroundColor = kMenuLineGray;
        vLine01.tag = 999;
        [cell.contentView addSubview:vLine01];

    } else if (indexPath.row == 3) {
//        lblTitle.text = NSLocalizedString(@"ProgramLastTime", @"Program Last Time");
//        lblTitle.numberOfLines = 2;
//        lblTitle.frame = CGRectMake(0, 0, sizeCell.width/3, kTableCellPicker);
//
//        pvOptionMin = [[UIPickerView alloc] initWithFrame:CGRectMake(sizeCell.width/3, 0, sizeCell.width/3, kTableCellPicker)];
//        pvOptionMin.backgroundColor = kDefWhite;
//        pvOptionMin.dataSource = self;
//        pvOptionMin.delegate = self;
//        pvOptionMin.showsSelectionIndicator = YES;
//        pvOptionMin.translatesAutoresizingMaskIntoConstraints = NO;
//        pvOptionMin.tag = kPickerMinTag;
//        [cell.contentView addSubview:pvOptionMin];
//
//        pvOptionSec = [[UIPickerView alloc] initWithFrame:CGRectMake(sizeCell.width*2/3, 0, sizeCell.width/3, kTableCellPicker)];
//        pvOptionSec.backgroundColor = kDefWhite;
//        pvOptionSec.dataSource = self;
//        pvOptionSec.delegate = self;
//        pvOptionSec.showsSelectionIndicator = YES;
//        pvOptionSec.translatesAutoresizingMaskIntoConstraints = NO;
//        pvOptionSec.tag = kPickerSecTag;
//        [cell.contentView addSubview:pvOptionSec];
//
//        int aMin = nLen / 60;
//        int aSec = nLen % 60;
//
//        [pvOptionMin selectRow:aMin inComponent:0 animated:NO];
//        [pvOptionSec selectRow:aSec inComponent:0 animated:NO];
//
//        UIView *vLine01 = [[UIView alloc] initWithFrame:CGRectMake(0, kTableCellPicker-1, sizeCell.width, 1)];
//        vLine01.backgroundColor = kMenuLineGray;
//        vLine01.tag = 999;
//        [cell.contentView addSubview:vLine01];
        
    } else if (indexPath.row == 4) {
        lblTitle.text = NSLocalizedString(@"AppVersion", @"App Version");
        lblTitle.numberOfLines = 2;

        UILabel *lblAppVer = [PBLibOC buildFlatColorLabel:[UIColor clearColor] TitleColor:kTextGray];
        lblAppVer.frame = CGRectMake(sizeCell.width/3 + 10, 0, sizeCell.width*2/3-50, fCellH);
        lblAppVer.font = AppFont(16);
        lblAppVer.textAlignment = NSTextAlignmentCenter;
        lblAppVer.tag = 999;
        [cell.contentView addSubview:lblAppVer];
        lblAppVer.text = [PBLibOC getAppInfo:@"version"];

        UIView *vLine01 = [[UIView alloc] initWithFrame:CGRectMake(0, fCellH-1, sizeCell.width, 1)];
        vLine01.backgroundColor = kMenuLineGray;
        vLine01.tag = 999;
        [cell.contentView addSubview:vLine01];
    
    } else if (indexPath.row == 5) {
        //FW Upgrade
        lblTitle.text = NSLocalizedString(@"Upgrade", @"Upgrade");
        lblTitle.numberOfLines = 2;
        
        UIButton *btnUpgrade = [PBLibOC buildFlatColorButton:kDefThemeColor BorderColor:kDefThemeColor Radius:0];
        btnUpgrade.frame = CGRectMake(sizeCell.width/3 + 10, 5, sizeCell.width*2/3-20, fCellH-10);
        btnUpgrade.titleLabel.font = AppFont(18);
        [btnUpgrade setTitleColor:kDefWhite forState:UIControlStateNormal];
        [btnUpgrade setTitle:NSLocalizedString(@"Upgrade", @"Upgrade") forState:UIControlStateNormal];
        [btnUpgrade addTarget:self action:@selector(onUpgrade) forControlEvents:UIControlEventTouchUpInside];
        btnUpgrade.tag = 999;
        [cell.contentView addSubview:btnUpgrade];


    } else if (indexPath.row == 6) {
        
        if ([[PBLibOC getLocaleLanguage] isEqualToString:@"fr"]) {
            lblTitle.hidden = YES;
            
            UIButton *btnCancel = [PBLibOC buildFlatColorButton:kCancelGray BorderColor:kCancelGray Radius:0];
            btnCancel.frame = CGRectMake(0, 0, sizeCell.width/2, fCellH+10);
            btnCancel.titleLabel.font = AppFont(18);
            [btnCancel setTitleColor:kDefThemeColor forState:UIControlStateNormal];
            [btnCancel setTitle:NSLocalizedString(@"NO", @"NO") forState:UIControlStateNormal];
            [btnCancel addTarget:self action:@selector(onClose) forControlEvents:UIControlEventTouchUpInside];
            btnCancel.tag = 999;
            [cell.contentView addSubview:btnCancel];

            UIButton *btnConfirm = [PBLibOC buildFlatColorButton:kDefThemeColor BorderColor:kDefThemeColor Radius:0];
            btnConfirm.frame = CGRectMake(sizeCell.width/2, 0, sizeCell.width/2, fCellH+10);
            btnConfirm.titleLabel.font = AppFont(18);
            [btnConfirm setTitleColor:kDefWhite forState:UIControlStateNormal];
            [btnConfirm setTitle:NSLocalizedString(@"YES", @"YES") forState:UIControlStateNormal];
            [btnConfirm addTarget:self action:@selector(onConfirm) forControlEvents:UIControlEventTouchUpInside];
            btnConfirm.tag = 999;
            [cell.contentView addSubview:btnConfirm];

        } else {
            lblTitle.text = @"";
            lblTitle.text = NSLocalizedString(@"VoiceCommand", @"Voice Command");
            cell.backgroundColor = menulinegraylight;
            lblTitle.frame = CGRectMake(10, 0, sizeCell.width-20, fCellH/2);
            lblTitle.textAlignment = NSTextAlignmentLeft;
        }

    } else if (indexPath.row == 7) {
        lblTitle.text = NSLocalizedString(@"Language", @"Language");
        lblTitle.frame = CGRectMake(0, 0, sizeCell.width/3, kTableCellPicker);
        
        
        pvOptionLanguage = [[UIPickerView alloc] initWithFrame:CGRectMake(sizeCell.width/3 + 10, 0, sizeCell.width*2/3-10, kTableCellPicker)];
        pvOptionLanguage.backgroundColor = kDefWhite;
        pvOptionLanguage.dataSource = self;
        pvOptionLanguage.delegate = self;
        pvOptionLanguage.showsSelectionIndicator = YES;
        pvOptionLanguage.translatesAutoresizingMaskIntoConstraints = NO;
        pvOptionLanguage.tag = kPickerLanTag;
        [cell.contentView addSubview:pvOptionLanguage];

        UIView *vLine01 = [[UIView alloc] initWithFrame:CGRectMake(0, kTableCellPicker-1, sizeCell.width, 1)];
        vLine01.backgroundColor = kMenuLineGray;
        vLine01.tag = 999;
        [cell.contentView addSubview:vLine01];
        
        [pvOptionLanguage selectRow:nCurSelLangIdx inComponent:0 animated:NO];

    } else if (indexPath.row == 8) {
        lblTitle.text = NSLocalizedString(@"ShowResult", @"Show Result");
        lblTitle.numberOfLines = 2;
        
        CGFloat fCheckW = 30;
        CGFloat fCheckTextW = 100;
        
        UIButton *chkShow = [UIButton buttonWithType:UIButtonTypeCustom];
        chkShow.frame = CGRectMake(sizeCell.width/3 + 40, (fCellH-fCheckW)/2, fCheckW, fCheckW);
        [chkShow setImage:[UIImage imageNamed:@"check_n.png"] forState:UIControlStateNormal];
        [chkShow setImage:[UIImage imageNamed:@"check_n.png"] forState:UIControlStateNormal|UIControlStateHighlighted];
        [chkShow setImage:[UIImage imageNamed:@"check_o.png"] forState:UIControlStateSelected];
        [chkShow setImage:[UIImage imageNamed:@"check_o.png"] forState:UIControlStateSelected|UIControlStateHighlighted];
        [chkShow addTarget:self action:@selector(onShowResult) forControlEvents:UIControlEventTouchUpInside];
        chkShow.tag = 999;
        [cell.contentView addSubview:chkShow];
        
        UIButton *btnShow = [PBLibOC buildFlatColorButton:[UIColor clearColor]];
        btnShow.frame = CGRectMake(chkShow.frame.origin.x+fCheckW, (fCellH-20)/2, fCheckTextW, 20);
        btnShow.titleLabel.font = AppFont(14);
        btnShow.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [btnShow setTitleColor:kTextBlack forState:UIControlStateNormal];
        [btnShow setTitle:[NSString stringWithFormat:@" %@", NSLocalizedString(@"Show", @"Show")] forState:UIControlStateNormal];
        [btnShow addTarget:self action:@selector(onShowResult) forControlEvents:UIControlEventTouchUpInside];
        btnShow.tag = 999;
        [cell.contentView addSubview:btnShow];
        
        UIButton *chkHide = [UIButton buttonWithType:UIButtonTypeCustom];
        chkHide.frame = CGRectMake(sizeCell.width*2/3 + 10, (fCellH-fCheckW)/2, fCheckW, fCheckW);
        [chkHide setImage:[UIImage imageNamed:@"check_n.png"] forState:UIControlStateNormal];
        [chkHide setImage:[UIImage imageNamed:@"check_n.png"] forState:UIControlStateNormal|UIControlStateHighlighted];
        [chkHide setImage:[UIImage imageNamed:@"check_o.png"] forState:UIControlStateSelected];
        [chkHide setImage:[UIImage imageNamed:@"check_o.png"] forState:UIControlStateSelected|UIControlStateHighlighted];
        [chkHide addTarget:self action:@selector(onHideResult) forControlEvents:UIControlEventTouchUpInside];
        chkHide.tag = 999;
        [cell.contentView addSubview:chkHide];
        
        UIButton *btnHide = [PBLibOC buildFlatColorButton:[UIColor clearColor]];
        btnHide.frame = CGRectMake(chkHide.frame.origin.x+fCheckW, (fCellH-20)/2, fCheckTextW, 20);
        btnHide.titleLabel.font = AppFont(14);
        btnHide.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [btnHide setTitleColor:kTextBlack forState:UIControlStateNormal];
        [btnHide setTitle:[NSString stringWithFormat:@" %@", NSLocalizedString(@"Hide", @"Hide")] forState:UIControlStateNormal];
        [btnHide addTarget:self action:@selector(onHideResult) forControlEvents:UIControlEventTouchUpInside];
        btnHide.tag = 999;
        [cell.contentView addSubview:btnHide];
        
        if ([sShowResult isEqualToString:@"show"]) {
            chkShow.selected = YES;
            chkHide.selected = NO;
        } else {
            chkShow.selected = NO;
            chkHide.selected = YES;
        }
        
        UIView *vLine01 = [[UIView alloc] initWithFrame:CGRectMake(0, fCellH-1, sizeCell.width, 1)];
        vLine01.backgroundColor = kMenuLineGray;
        vLine01.tag = 999;
        [cell.contentView addSubview:vLine01];
        
    } else if (indexPath.row == 9) {
        lblTitle.text = NSLocalizedString(@"Enable", @"Enable");
        
        CGFloat fCheckW = 30;
        CGFloat fCheckTextW = 100;
        
        UIButton *chkEnable = [UIButton buttonWithType:UIButtonTypeCustom];
        chkEnable.frame = CGRectMake(sizeCell.width/3 + 40, (fCellH-fCheckW)/2, fCheckW, fCheckW);
        [chkEnable setImage:[UIImage imageNamed:@"check_n.png"] forState:UIControlStateNormal];
        [chkEnable setImage:[UIImage imageNamed:@"check_n.png"] forState:UIControlStateNormal|UIControlStateHighlighted];
        [chkEnable setImage:[UIImage imageNamed:@"check_o.png"] forState:UIControlStateSelected];
        [chkEnable setImage:[UIImage imageNamed:@"check_o.png"] forState:UIControlStateSelected|UIControlStateHighlighted];
        [chkEnable addTarget:self action:@selector(onVCmdEnable) forControlEvents:UIControlEventTouchUpInside];
        chkEnable.tag = 999;
        [cell.contentView addSubview:chkEnable];
        
        UIButton *btnEnable = [PBLibOC buildFlatColorButton:[UIColor clearColor]];
        btnEnable.frame = CGRectMake(chkEnable.frame.origin.x+fCheckW, (fCellH-20)/2, fCheckTextW, 20);
        btnEnable.titleLabel.font = AppFont(14);
        btnEnable.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [btnEnable setTitleColor:kTextBlack forState:UIControlStateNormal];
        [btnEnable setTitle:[NSString stringWithFormat:@" %@", NSLocalizedString(@"Enable", @"Enable")] forState:UIControlStateNormal];
        [btnEnable addTarget:self action:@selector(onShowResult) forControlEvents:UIControlEventTouchUpInside];
        btnEnable.tag = 999;
        [cell.contentView addSubview:btnEnable];
        
        UIButton *chkDisable = [UIButton buttonWithType:UIButtonTypeCustom];
        chkDisable.frame = CGRectMake(sizeCell.width*2/3 + 10, (fCellH-fCheckW)/2, fCheckW, fCheckW);
        [chkDisable setImage:[UIImage imageNamed:@"check_n.png"] forState:UIControlStateNormal];
        [chkDisable setImage:[UIImage imageNamed:@"check_n.png"] forState:UIControlStateNormal|UIControlStateHighlighted];
        [chkDisable setImage:[UIImage imageNamed:@"check_o.png"] forState:UIControlStateSelected];
        [chkDisable setImage:[UIImage imageNamed:@"check_o.png"] forState:UIControlStateSelected|UIControlStateHighlighted];
        [chkDisable addTarget:self action:@selector(onVCmdDisable) forControlEvents:UIControlEventTouchUpInside];
        chkDisable.tag = 999;
        [cell.contentView addSubview:chkDisable];
        
        UIButton *btnDisable = [PBLibOC buildFlatColorButton:[UIColor clearColor]];
        btnDisable.frame = CGRectMake(chkDisable.frame.origin.x+fCheckW, (fCellH-20)/2, fCheckTextW, 20);
        btnDisable.titleLabel.font = AppFont(14);
        btnDisable.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [btnDisable setTitleColor:kTextBlack forState:UIControlStateNormal];
        [btnDisable setTitle:[NSString stringWithFormat:@" %@", NSLocalizedString(@"Disable", @"Disable")] forState:UIControlStateNormal];
        [btnDisable addTarget:self action:@selector(onVCmdDisable) forControlEvents:UIControlEventTouchUpInside];
        btnDisable.tag = 999;
        [cell.contentView addSubview:btnDisable];
        
        if ([sVCmdEnable isEqualToString:@"enable"]) {
            chkEnable.selected = YES;
            chkDisable.selected = NO;
        } else {
            chkEnable.selected = NO;
            chkDisable.selected = YES;
        }
        
        UIView *vLine01 = [[UIView alloc] initWithFrame:CGRectMake(0, fCellH-1, sizeCell.width, 1)];
        vLine01.backgroundColor = kMenuLineGray;
        vLine01.tag = 999;
        [cell.contentView addSubview:vLine01];

    } else if (indexPath.row == 10) {
        lblTitle.hidden = YES;
        
        UIButton *btnCancel = [PBLibOC buildFlatColorButton:kCancelGray BorderColor:kCancelGray Radius:0];
        btnCancel.frame = CGRectMake(0, 0, sizeCell.width/2, fCellH+10);
        btnCancel.titleLabel.font = AppFont(18);
        [btnCancel setTitleColor:kDefThemeColor forState:UIControlStateNormal];
        [btnCancel setTitle:NSLocalizedString(@"NO", @"NO") forState:UIControlStateNormal];
        [btnCancel addTarget:self action:@selector(onClose) forControlEvents:UIControlEventTouchUpInside];
        btnCancel.tag = 999;
        [cell.contentView addSubview:btnCancel];

        UIButton *btnConfirm = [PBLibOC buildFlatColorButton:kDefThemeColor BorderColor:kDefThemeColor Radius:0];
        btnConfirm.frame = CGRectMake(sizeCell.width/2, 0, sizeCell.width/2, fCellH+10);
        btnConfirm.titleLabel.font = AppFont(18);
        [btnConfirm setTitleColor:kDefWhite forState:UIControlStateNormal];
        [btnConfirm setTitle:NSLocalizedString(@"YES", @"YES") forState:UIControlStateNormal];
        [btnConfirm addTarget:self action:@selector(onConfirm) forControlEvents:UIControlEventTouchUpInside];
        btnConfirm.tag = 999;
        [cell.contentView addSubview:btnConfirm];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [PBLibOC performTransition:NO andView:self.view];
}

-(void)onML
{
    sUnit = @"ml";
    [tvList reloadData];
}

-(void)onOZ
{
    sUnit = @"oz";
    [tvList reloadData];
}

-(void)onDelDevice
{
    if (sDeviceUUID.length > 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:NSLocalizedString(@"wantToDelete", @"want To Delete")
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"NO", @"NO")
                                              otherButtonTitles:NSLocalizedString(@"YES", @"YES"), nil];
        alert.tag = 100;
        [alert show];
    }
}

-(void)onOnline
{
    sOnline = @"online";
    [tvList reloadData];
}

-(void)onOffline
{
    sOnline = @"offline";
    [tvList reloadData];
}

-(void)onShowResult
{
    sShowResult = @"show";
    [tvList reloadData];
}

-(void)onHideResult
{
    sShowResult = @"hide";
    [tvList reloadData];
}

-(void)onVCmdEnable
{
    sVCmdEnable = @"enable";
    [tvList reloadData];
}

-(void)onVCmdDisable
{
    sVCmdEnable = @"disable";
    [tvList reloadData];
}


-(void)onConfirm
{
    [[NSUserDefaults standardUserDefaults] setObject:sUnit forKey:@"unit"];
    
    NSString *aLanguage = [[arrLanguage objectAtIndex:nCurSelLangIdx] objectForKey:@"code"];
    [[NSUserDefaults standardUserDefaults] setObject:aLanguage forKey:@"vcmd_language"];
    
    [[NSUserDefaults standardUserDefaults] setObject:sShowResult forKey:@"vcmd_showresult"];
    
    [[NSUserDefaults standardUserDefaults] setObject:sVCmdEnable forKey:@"vcmd_enable"];
            
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%d", nLen] forKey:@"program_last_time"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [delegate onSettingsClosed:@{@"unit" : sUnit, @"vcmd_language" : aLanguage, @"vcmd_showresult" : sShowResult}];
    
    
    //PBCommon.curSpeechLanguage
        
    [PBLibOC performTransition:NO andView:self.view];
    
}

-(void)onUpgrade
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"FWUpgrade", @"FW Upgrade")
                                                    message:NSLocalizedString(@"FWUpConfirm", @"FW Upgrade Confirm")
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"NO", @"NO")
                                          otherButtonTitles:NSLocalizedString(@"YES", @"YES"), nil];
    alert.tag = 200;
    [alert show];

}

-(void) changeVoiceCommadLanguage:(NSString*)a_Language
{
    
}

-(void)onClose
{
    [PBLibOC performTransition:NO andView:self.view];
}

#pragma mark -
#pragma mark UIPickerView DataSource
// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (pickerView.tag == kPickerMinTag) {
        return [arrMin count];
    } else if (pickerView.tag == kPickerSecTag) {
        return [arrSec count];
    }
    return [arrLanguage count];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component;
{
    return kNavBarH/2;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *pickerLabel = (UILabel *)view;
    int nWL = 260;
    
    if (pickerLabel == nil) {
        pickerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, nWL, kNavBarHeight/2)];
        [pickerLabel setTextAlignment:NSTextAlignmentCenter];
        [pickerLabel setBackgroundColor:[UIColor clearColor]];
        [pickerLabel setFont:AppFont(16)];
    }
    
    long aRow = [pickerView selectedRowInComponent:component];
    if (row == aRow) {
        pickerLabel.textColor = kDefThemeColor;
    }

    NSString *aTitleName = @"";
    if (pickerView.tag == kPickerMinTag) {
        aTitleName = [PBLibOC filterNSNull:[[arrMin objectAtIndex:row] objectForKey:@"codename"]];
    } else if (pickerView.tag == kPickerSecTag) {
        aTitleName = [PBLibOC filterNSNull:[[arrSec objectAtIndex:row] objectForKey:@"codename"]];
    } else {
        aTitleName = [PBLibOC filterNSNull:[[arrLanguage objectAtIndex:row] objectForKey:@"codename"]];
    }
    pickerLabel.text = aTitleName;
    return pickerLabel;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (pickerView.tag == kPickerLanTag) {
        [pvOptionLanguage reloadAllComponents];
        nCurSelLangIdx = row;
        
    } else {
        [pvOptionMin reloadAllComponents];
        [pvOptionSec reloadAllComponents];
        
        long aMin = [pvOptionMin selectedRowInComponent:0];
        long aSec = [pvOptionSec selectedRowInComponent:0];
        nLen = aMin*60 + aSec;
    }
}


#pragma mark - UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 100) {
        if (buttonIndex == 1) {
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"device_name"];
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"device_addr"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            sDeviceName = @"";
            sDeviceUUID = @"";
            [tvList reloadData];
        }
        
    } else if (alertView.tag == 200) {
        if (buttonIndex == 1) {
            [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"bFWUpMode"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self onConfirm];
        }
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
