//
//  SettingsViewController.h
//  BT150
//
//  Created by KOBONGHWAN on 04/12/2018.
//  Copyright © 2018 bistos. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol SettingsViewDelegate <NSObject>
-(void)onSettingsClosed:(NSDictionary*)a_Param;
@end

@interface SettingsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UIAlertViewDelegate>
{
    id <SettingsViewDelegate> delegate;
}

@property (strong, nonatomic) id <SettingsViewDelegate> delegate;
//{
//    NSString *sUnit;
//    NSString *sDevice;
//    NSString *sOnline;
//    NSString *sShowResult;
//}
//
//@property (strong, nonatomic) NSString *sUnit;
//@property (strong, nonatomic) NSString *sDevice;
//@property (strong, nonatomic) NSString *sOnline;
//@property (strong, nonatomic) NSString *sShowResult;

-(void)initControls;

@end

NS_ASSUME_NONNULL_END
