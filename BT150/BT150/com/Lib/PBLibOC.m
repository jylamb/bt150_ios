//
//  PBLibOC.m
//  kbc_cha_ios
//
//  Created by lamb on 2014. 11. 13..
//  Copyright (c) 2014년 pointbank. All rights reserved.
//

#import "PBLibOC.h"
//#import "SSKeychain.h"

@implementation PBLibOC

-(id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

//+(NSString*)UUID
//{
//    NSString *uuid = nil;
//    
//    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//    uuid = [userDefault objectForKey:@"UUID"];
//    
//    if (uuid == nil) {
//        uuid = [self saveUUID];
//    }
//#if TARGET_IPHONE_SIMULATOR
//    return @"E7A7F4A2-377E-4030-B5A8-A892B298DC81";
//#endif
//    return uuid;
//}
//
//+(NSString*)saveUUID
//{
//    NSString *uuid = nil;
//    NSError *error = nil;
//    
//    // 키체인
//    uuid = [SSKeychain passwordForService:SERVICE
//                                  account:ACCOUNT
//                                    error:&error];
//    // 기본저장
//    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//    
//    if (uuid == nil || ![uuid isEqualToString:[userDefault objectForKey:@"UUID"]]) {
//// IOS 5 부터 uniqueIdentifier 사용불가능부분 보완
////        if([UIDevice instancesRespondToSelector:@selector(uniqueIdentifier)]){
////            uuid = [[UIDevice currentDevice] uniqueIdentifier];
////            NSLog(@"UIDevice:%@", uuid);
////        }
//        
//        if (uuid == nil) {
//            CFUUIDRef uuidss = CFUUIDCreate(kCFAllocatorDefault);
//            uuid = (__bridge NSString *)CFUUIDCreateString(kCFAllocatorDefault, uuidss);
////            NSLog(@"CFUUIDRef:%@", uuid);
//        }
//        
//        [SSKeychain setPassword:uuid
//                     forService:SERVICE
//                        account:ACCOUNT
//                          error:&error];
//        [userDefault setObject:[NSString stringWithFormat:@"%@", uuid] forKey:@"UUID"];
//    }
//    
//    return uuid;
//}

+(BOOL)validateEmail:(NSString*)candidate
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}

+(UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+(NSDate *)dateWithOutTime:(NSDate *)datDate
{
    if( datDate == nil ) {
        datDate = [NSDate date];
    }
    NSDateComponents* comps = [[NSCalendar currentCalendar] components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:datDate];
    return [[NSCalendar currentCalendar] dateFromComponents:comps];
}

+(NSDate *)get1stDayOfYear:(NSDate *)datDate
{
    NSDateFormatter *dfDate = [[NSDateFormatter alloc] init];
    [dfDate setDateFormat:@"yyyy-MM-dd"];
    
    NSDateFormatter *dfStart = [[NSDateFormatter alloc] init];
    [dfStart setDateFormat:@"yyyy"];
    
    return [dfDate dateFromString:[NSString stringWithFormat:@"%@-01-01", [dfStart stringFromDate:[NSDate date]]]];
}

+(NSDate *)get1stDayOfMonth:(NSDate *)datDate
{
    NSDateFormatter *dfDate = [[NSDateFormatter alloc] init];
    [dfDate setDateFormat:@"yyyy-MM-dd"];
    
    NSDateFormatter *dfStart = [[NSDateFormatter alloc] init];
    [dfStart setDateFormat:@"yyyy-MM"];
    
    return [dfDate dateFromString:[NSString stringWithFormat:@"%@-01", [dfStart stringFromDate:[NSDate date]]]];
}

+(NSDate *)getMonthDiff:(int)a_Month FromDate:(NSDate*)a_FromDate
{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setMonth:1*a_Month];
    
    NSDate *dtAgo = [gregorian dateByAddingComponents:offsetComponents toDate:a_FromDate options:0];
    return dtAgo;
}

+(BOOL)isNSNull:(id)a_In
{
    if (a_In != nil && a_In != (id)[NSNull null]) {
        return NO;
    }
    return YES;
}

+(id)objectNSNull:(id)a_In
{
    if ([self isNSNull:a_In]) {
        return  @"";
    }
    NSString *aIn = [NSString stringWithFormat:@"%@", a_In];
    if ([aIn isEqualToString:@"NULL"]) {
        return @"";
    }
    if ([aIn isEqualToString:@"<null>"]) {
        return @"";
    }
    return a_In;
}

+(NSString*)filterNSNull:(NSString*)a_In
{
    if ([self isNSNull:a_In]) {
        return  @"";
    }
    if ([a_In isEqualToString:@"NULL"]) {
        return @"";
    }
    if ([a_In isEqualToString:@"<null>"]) {
        return @"";
    }
    return a_In;
}

+(int)convertToInt:(id)a_Int
{
    return [[self objectNSNull:a_Int] intValue];
}

+(NSString*)putComma:(long)num
{
    NSNumber *number = [NSNumber numberWithLong:num];
    
    NSNumberFormatter *frmtr = [[NSNumberFormatter alloc] init];
    [frmtr setGroupingSize:3];
    [frmtr setGroupingSeparator:@","];
    [frmtr setUsesGroupingSeparator:YES];
    NSString *commaString = [frmtr stringFromNumber:number];
    
    return commaString;
}

+(NSString*)putCommaString:(NSString*)a_Value
{
    NSString *szRetR = @"";
    NSString *commaString = @"";
    
    int nCnt = 1;
    for (long i = a_Value.length-1; i >= 0; i--) {
        szRetR = [NSString stringWithFormat:@"%@%@", szRetR, [a_Value substringWithRange:NSMakeRange(i, 1)]];
        if (nCnt % 3 == 0 && nCnt < a_Value.length) {
            szRetR = [NSString stringWithFormat:@"%@%@", szRetR, @","];
        }
        nCnt++;
    }
    
    for (long i = szRetR.length-1; i >= 0; i--) {
        commaString = [NSString stringWithFormat:@"%@%@", commaString, [szRetR substringWithRange:NSMakeRange(i, 1)]];
    }
    
    return commaString;
}

+(UIColor *)colorFromHexString:(NSString *)hexString
{
    if (hexString.length > 6) {
        unsigned rgbValue = 0;
        NSScanner *scanner = [NSScanner scannerWithString:hexString];
        [scanner setScanLocation:1]; // bypass '#' character
        [scanner scanHexInt:&rgbValue];
        return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
    }
    return [UIColor clearColor];
}

+(UIView*)buildFlatColorView:(UIColor*)a_Base Line:(UIColor*)a_Line Radius:(float)a_R Thick:(float)a_Thick
{
    UIView *vFlat = [[UIView alloc] init];
    vFlat.backgroundColor = a_Base;
    vFlat.layer.backgroundColor = [a_Base CGColor];
    vFlat.layer.borderColor = [a_Line CGColor];
    vFlat.layer.borderWidth = a_Thick;
    vFlat.layer.cornerRadius = a_R;
    vFlat.layer.masksToBounds = YES;
    return vFlat;
}

+(UILabel*)buildFlatColorLabel:(UIColor*)a_Base
{
    return [self buildFlatColorLabel:a_Base TitleColor:[UIColor whiteColor]];
}

+(UILabel*)buildFlatColorLabel:(UIColor*)a_Base TitleColor:(UIColor*)a_Title
{
    UILabel *lblFlat = [[UILabel alloc] init];
    lblFlat.backgroundColor = a_Base;
    lblFlat.layer.backgroundColor = [a_Base CGColor];
    lblFlat.layer.borderColor = [a_Base CGColor];
    lblFlat.layer.borderWidth = 1;
    lblFlat.layer.cornerRadius = 1;
//    lblFlat.layer.masksToBounds = YES;
    lblFlat.textColor = a_Title;
    return lblFlat;
}

+(UIButton*)buildFlatColorButton:(UIColor*)a_Base
{
    return [self buildFlatColorButton:a_Base BorderColor:a_Base];
}

+(UIButton*)buildFlatColorButton:(UIColor*)a_Base BorderColor:(UIColor*)a_Border
{
    return [self buildFlatColorButton:a_Base BorderColor:a_Border Radius:1];
}

+(UIButton*)buildFlatColorButton:(UIColor*)a_Base BorderColor:(UIColor*)a_Border Radius:(float)a_R
{
    return [self buildFlatColorButton:a_Base BorderColor:a_Border Radius:a_R Thick:1];
}

+(UIButton*)buildFlatColorButton:(UIColor*)a_Base BorderColor:(UIColor*)a_Border Radius:(float)a_R Thick:(float)a_Thick
{
    UIButton *btnFlat = [UIButton buttonWithType:UIButtonTypeCustom];
    btnFlat.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    btnFlat.backgroundColor = a_Base;
//    btnFlat.layer.backgroundColor = [a_Base CGColor];
    btnFlat.layer.borderColor = [a_Border CGColor];
    btnFlat.layer.borderWidth = a_Thick;
    btnFlat.layer.cornerRadius = a_R;
//    btnFlat.layer.masksToBounds = YES;
    [btnFlat setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    return btnFlat;
}

//+(PBTextField*)buildFlatColorEdit:(UIColor*)a_Text
//{
//    return [self buildFlatColorEdit:a_Text BackColor:[UIColor whiteColor] BorderColor:kMenuLineGrayLight Radius:0 Thick:1];
//}

+(PBTextField*)buildFlatColorEdit:(UIColor*)a_Text BackColor:(UIColor*)a_Back BorderColor:(UIColor*)a_Border Radius:(float)a_R Thick:(float)a_Thick
{
    PBTextField *txtFlat = [[PBTextField alloc] init];
    txtFlat.textColor = a_Text;
    txtFlat.backgroundColor = a_Back;
    txtFlat.layer.backgroundColor = [a_Back CGColor];
    txtFlat.layer.borderColor = [a_Border CGColor];
    txtFlat.layer.borderWidth = a_Thick;
    txtFlat.layer.cornerRadius = a_R;
    txtFlat.layer.masksToBounds = YES;
    return txtFlat;
}

+(void)performTransition:(BOOL)a_Show andView:(UIView*)a_View
{
    [self performTransition:a_Show andView:a_View andType:kCATransitionFade subType:kCATransitionFromBottom];
}

+(void)performTransition:(BOOL)a_Show andView:(UIView*)a_View andType:(NSString*)a_Type subType:(NSString*)a_SubType
{
    [self performTransition:a_Show andView:a_View andType:a_Type subType:a_SubType Time:0.4];
}

+(void)performTransition:(BOOL)a_Show andView:(UIView*)a_View andType:(NSString*)a_Type subType:(NSString*)a_SubType Time:(double)a_Time
{
    CATransition *transition = [CATransition animation];
    transition.duration = a_Time;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = a_Type;
    transition.subtype = a_SubType;
    
    [a_View.layer addAnimation:transition forKey:nil];
    
    // Here we hide view1, and show view2, which will cause Core Animation to animate view1 away and view2 in.
    a_View.hidden = !a_Show;
}

+(void)showAlertMsg:(NSString*)a_Msg
{
    [[[UIAlertView alloc] initWithTitle:@""
                                message:a_Msg
                               delegate:nil
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil, nil] show];
}

+(void)AlertMsgMainThread:(NSString*)a_Msg
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self showAlertMsg:a_Msg];
    });
//    [self performSelectorOnMainThread:@selector(showAlertMsg:) withObject:a_Msg waitUntilDone:YES];
}

+(void)setNavBackTitleReset:(UINavigationItem*)a_NavItem
{
    [self setNavBackTitleReset:a_NavItem Title:@"Back"];
}

+(void)setNavBackTitleReset:(UINavigationItem*)a_NavItem Title:(NSString*)a_Title
{
    a_NavItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:a_Title style:UIBarButtonItemStylePlain target:nil action:nil];
}

+(void)drawColorLine:(UIView*)a_View Color:(UIColor*)a_Color From:(CGPoint)a_From To:(CGPoint)a_To
{
    [self drawColorLine:a_View Color:a_Color Thick:1 From:a_From To:a_To];
}

+(void)drawColorLine:(UIView*)a_View Color:(UIColor*)a_Color Thick:(CGFloat)a_Thick From:(CGPoint)a_From To:(CGPoint)a_To
{
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:a_From];
    [path addLineToPoint:a_To];
    
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    shapeLayer.path = [path CGPath];
    shapeLayer.strokeColor = [a_Color CGColor];
    shapeLayer.lineWidth = a_Thick;
    shapeLayer.fillColor = [a_Color CGColor];
    
    [a_View.layer addSublayer:shapeLayer];

}

+(UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

+(NSString*)makeDateString:(NSString*)a_Date
{
    return [self makeDateString:a_Date Deli:@"-"];
}

+(NSString*)makeDateString:(NSString*)a_Date Deli:(NSString*)a_Deli
{
    NSString *aRet = @"";
    if (![self isNSNull:a_Date]) {
        if (a_Date.length == 6) {
            aRet = [NSString stringWithFormat:@"%@%@%@",
                    [a_Date substringToIndex:4],
                    a_Deli,
                    [a_Date substringWithRange:NSMakeRange(4, 2)]];
            
        } else if (a_Date.length == 8) {
            aRet = [NSString stringWithFormat:@"%@%@%@%@%@",
                    [a_Date substringToIndex:4],
                    a_Deli,
                    [a_Date substringWithRange:NSMakeRange(4, 2)],
                    a_Deli,
                    [a_Date substringWithRange:NSMakeRange(6, 2)]];
        } else {
            aRet = a_Date;
        }
    }
    
    return aRet;
}

+(BOOL)isPointInRect:(CGPoint)a_Point Rect:(CGRect)a_Rect
{
    //    NSLog(@"x:%.0f = %.0f %.0f", a_Point.x, a_Rect.origin.x, a_Rect.origin.x + a_Rect.size.width);
    //    NSLog(@"y:%.0f = %.0f %.0f", a_Point.y, a_Rect.origin.y, a_Rect.origin.y + a_Rect.size.height);
    
    if (a_Point.x >= a_Rect.origin.x && a_Point.x <= (a_Rect.origin.x + a_Rect.size.width)
        && a_Point.y >= a_Rect.origin.y && a_Point.y <= (a_Rect.origin.y + a_Rect.size.height)) {
        return YES;
    }
    
    return NO;
}

+(BOOL)containsString:(NSString*)a_Source Find:(NSString*)a_Find
{
    NSRange rangeFound = [a_Source rangeOfString:a_Find];
    if (rangeFound.location != NSNotFound && rangeFound.length > 0) {
        return YES;
    }    
    return NO;
}

+(NSMutableURLRequest*)getTokenRequest:(NSString*)a_Url Dictionary:(NSDictionary*)a_Param
{
    return [self getTokenRequest:a_Url Dictionary:a_Param UseGate:YES];
}

+(NSMutableURLRequest*)getTokenRequest:(NSString*)a_Url Dictionary:(NSDictionary*)a_Param UseGate:(BOOL)a_UseGate
{
    NSString *post = @"";
    
    int nCnt = 0;
    for(id key in a_Param) {
        if (nCnt == 0) {
            post = [NSString stringWithFormat:@"%@%@=%@", post, key, [a_Param objectForKey:key]];
        } else {
            post = [NSString stringWithFormat:@"%@&%@=%@", post, key, [a_Param objectForKey:key]];
        }
        nCnt++;
        
    }
    return [self getTokenRequest:a_Url Param:post UseGate:a_UseGate];
}

+(NSMutableURLRequest*)getTokenRequest:(NSString*)a_Url Param:(NSString*)a_Param
{
    return [self getTokenRequest:a_Url Param:a_Param UseGate:YES];
}

+(NSMutableURLRequest*)getTokenRequest:(NSString*)a_Url Param:(NSString*)a_Param UseGate:(BOOL)a_UseGate
{
//    NSLog(@"getTokenRequest: url: %@", a_Url);
//    NSLog(@"getTokenRequest: param: %@", a_Param);
    NSString *aUrl = a_Url;
    if (a_Param.length > 0) {
        aUrl = [NSString stringWithFormat:@"%@?%@", a_Url, a_Param];
    }
    
    NSMutableURLRequest *request = [self getTokenRequest:aUrl UseGate:a_UseGate];
    
    return request;
}

//+(NSMutableURLRequest*)getTokenRequest:(NSString*)a_Url
//{
//    return [self getTokenRequest:a_Url UseGate:YES];
//}
//
//+(NSMutableURLRequest*)getTokenRequest:(NSString*)a_Url UseGate:(BOOL)a_UseGate
//{
//    //    NSLog(@"getTokenRequest: url: %@", a_Url);
//    NSMutableURLRequest *request;
//
//    if (a_UseGate) {
//        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:sUrlGatePage]
//                                          cachePolicy:NSURLRequestReloadIgnoringCacheData
//                                      timeoutInterval:30.0];
//        [request setValue:a_Url forHTTPHeaderField:@"targetUrl"];
//
//    } else {
//        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:a_Url]
//                                          cachePolicy:NSURLRequestReloadIgnoringCacheData
//                                      timeoutInterval:30.0];
//    }
//
//    [request setValue:[[GlobalHead sharedSingleton] gRestToken] forHTTPHeaderField:@"token"];
//    [request setValue:@"true" forHTTPHeaderField:@"X-AJAX"];
//    [request setHTTPMethod:@"GET"];
//
//    return request;
//}

+(void)setImageView:(UIImageView*)a_ImageView Anim:(CGFloat)a_Time
{
    a_ImageView.alpha = 0.0;
    [UIView animateWithDuration:a_Time animations:^{
        a_ImageView.alpha = 1.0;
    }];
}

+(NSString*)convertUrlString:(NSString*)a_Url
{
    CFStringRef aRet = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                                 (__bridge CFStringRef)a_Url,
                                                                 NULL,
                                                                 CFSTR(":/?#[]@!$&'()*+,;="),
                                                                 kCFStringEncodingUTF8);
    return (__bridge NSString *)aRet;
}

+(void)doAfterTime:(double)a_Delay doBlock:(void(^)(void))doBlock
{
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, a_Delay * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        doBlock();
    });
}

+(BOOL)isAppInstalled:(NSString*)a_AppScheme
{
    NSURL *ourURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@://", a_AppScheme]];
    if (![[UIApplication sharedApplication] canOpenURL:ourURL]) {
        return NO;
    }
    return YES;
}

+(NSString*)getAppInfo:(NSString*)a_Type
{
    NSDictionary *infoDictionary = [[NSBundle mainBundle]infoDictionary];

    if ([a_Type isEqualToString:@"version"]) {
        return infoDictionary[@"CFBundleShortVersionString"];
        
    } else if ([a_Type isEqualToString:@"build"]) {
        return infoDictionary[(NSString*)kCFBundleVersionKey];
    
    } else if ([a_Type isEqualToString:@"bundlename"]) {
        return infoDictionary[(NSString *)kCFBundleNameKey];
    }
    
    return @"";
}

+(NSString*)getLocaleLanguage
{
    NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
    NSDictionary *languageDic = [NSLocale componentsFromLocaleIdentifier:language];
    NSString *languageCode = [languageDic objectForKey:@"kCFLocaleLanguageCodeKey"];
    return languageCode;
}

@end
