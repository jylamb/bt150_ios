//
//  DBHelper.h
//  BT150
//
//  Created by KOBONGHWAN on 23/12/2018.
//  Copyright © 2018 bistos. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface DBHelper : NSObject

-(void)createDatabase;
-(void)insertMilkData:(long)a_Date Left:(int)a_Left Right:(int)a_Right Duration:(long)a_Duration;
-(void)updateMilkData:(long)a_Date Left:(int)a_Left Right:(int)a_Right Duration:(long)a_Duration;
-(void)setMilkData:(long)a_Date Left:(int)a_Left Right:(int)a_Right Duration:(long)a_Duration;
-(void)deleteMilkData:(long)a_Date;
-(NSArray*) getMilkData;
-(NSArray*) getMilkDataFrom:(int64_t)a_From To:(int64_t)a_To;

@end


NS_ASSUME_NONNULL_END
