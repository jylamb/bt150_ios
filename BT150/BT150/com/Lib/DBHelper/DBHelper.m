//
//  DBHelper.m
//  BT150
//
//  Created by KOBONGHWAN on 23/12/2018.
//  Copyright © 2018 bistos. All rights reserved.
//

#import "DBHelper.h"
#import <sqlite3.h>

@implementation DBHelper
{
    sqlite3 *database;
    NSString *sTableMilk;
}

-(id)init
{
    self = [super init];
    if (self) {
        sTableMilk = @"BT150_MILK";
    }
    return self;
}

- (NSString*)databasePath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    return [documentDirectory stringByAppendingPathComponent:@"/BT150.sqlite3"];
}

-(void)createDatabase
{
    //sqllite3 메서드(쿼리 사용) utf8
    //유니코드
    //기본적으로 쿼리문이나 경로는 문자열형이기 떄문에.. 바꾼다...
    if (sqlite3_open([[self databasePath] UTF8String], &database) != SQLITE_OK) {
        sqlite3_close(database);
    }
    
    NSString *aQuery = [NSString stringWithFormat:@"Create table if not exists %@ (_id INTEGER PRIMARY KEY AUTOINCREMENT, reg_date REAL, left_amt INTEGER, right_amt INTEGER, duration REAL);", sTableMilk];
    
    if (sqlite3_exec(database, [aQuery UTF8String], NULL, NULL, nil) != SQLITE_OK) {
        sqlite3_close(database);
    }
    sqlite3_close(database);
}

-(BOOL)openDB
{
    if (sqlite3_open([[self databasePath] UTF8String], &database) != SQLITE_OK) {
        sqlite3_close(database);
        return false;
    }
    return true;
}

-(void)closeDB
{
    sqlite3_close(database);
}

-(NSArray*)readDBData:(NSString*)a_Sql ColCnt:(int)a_ColCnt
{
    NSMutableArray *arrRet = [[NSMutableArray alloc] init];
    sqlite3_stmt *stmt;
    if (sqlite3_prepare_v2(database, [a_Sql UTF8String], -1, &stmt, nil) == SQLITE_OK) {
        int rowNum = 0;
        while (sqlite3_step(stmt) == SQLITE_ROW) {
            char *aCol01 = (char *)sqlite3_column_text(stmt, 0);
            char *aCol02 = (char *)sqlite3_column_text(stmt, 1);
            char *aCol03 = (char *)sqlite3_column_text(stmt, 2);
            char *aCol04 = (char *)sqlite3_column_text(stmt, 3);
            char *aCol05 = (char *)sqlite3_column_text(stmt, 4);
            NSInteger aCol06 = sqlite3_column_int(stmt, 5);
            char *aCol07 = (char *)sqlite3_column_text(stmt, 6);
            char *aCol08 = (char *)sqlite3_column_text(stmt, 7);
            char *aCol09 = (char *)sqlite3_column_text(stmt, 8);
            char *aCol10 = (char *)sqlite3_column_text(stmt, 9);
            char *aCol11 = (char *)sqlite3_column_text(stmt, 10);
        }
        sqlite3_finalize(stmt);
    }
    sqlite3_close(database);

    return arrRet;
}

-(void)execQuery:(NSString*)a_Query
{
    [self openDB];
    
    sqlite3_stmt *stmt;
    if (sqlite3_prepare_v2(database, [a_Query UTF8String], -1, &stmt, nil) != SQLITE_OK) {
        sqlite3_close(database);
    }
    //쿼리문 실행을 함
    if (sqlite3_step(stmt) != SQLITE_DONE) {
        sqlite3_close(database);
    }
    sqlite3_finalize(stmt);
    sqlite3_close(database);
}

//////////////////////////////////////////////////////////////////////////////
//   Milk Data
//
-(void)insertMilkData:(long)a_Date Left:(int)a_Left Right:(int)a_Right Duration:(long)a_Duration
{
    // 읽고 쓰기가 가능하게 DB 열기
    NSString *aQuery = [NSString stringWithFormat:@"INSERT INTO %@ VALUES(null, %ld, %d, %d, %ld);",
                        sTableMilk, a_Date, a_Left, a_Right, a_Duration];
    NSLog(@"aQuery:%@", aQuery);
    [self execQuery:aQuery];
}

-(void)updateMilkData:(long)a_Date Left:(int)a_Left Right:(int)a_Right Duration:(long)a_Duration
{
    // 입력한 항목과 일치하는 행의 가격 정보 수정
    NSString *aQuery = [NSString stringWithFormat:@"UPDATE %@ SET left_amt = %d, right_amt = %d, duration = %ld WHERE reg_date = %ld;",
                        sTableMilk, a_Left, a_Right, a_Duration, a_Date];
    NSLog(@"aQuery:%@", aQuery);
    [self execQuery:aQuery];
}

-(void)setMilkData:(long)a_Date Left:(int)a_Left Right:(int)a_Right Duration:(long)a_Duration
{
    // DB에 있는 데이터를 쉽게 처리하기 위해 Cursor를 사용하여 테이블에 있는 모든 데이터 출력
    NSString *aQuery = [NSString stringWithFormat:@"SELECT count(reg_date) FROM  %@ where reg_date = %ld;",
                        sTableMilk, a_Date];
    NSLog(@"aQuery:%@", aQuery);

    int aCnt = 0;
    
    [self openDB];
    
    sqlite3_stmt *stmt ;        //SQL 데이터 초기화.
    if (sqlite3_prepare_v2(database, [aQuery UTF8String], -1, &stmt, NULL) == SQLITE_OK) {
        while (sqlite3_step(stmt) == SQLITE_ROW) {
            aCnt = sqlite3_column_int(stmt, 0);
        }
    }
    sqlite3_finalize(stmt);
    sqlite3_close(database);

    if (aCnt > 0) {
        [self updateMilkData:a_Date Left:a_Left Right:a_Right Duration:a_Duration];
    } else {
        [self insertMilkData:a_Date Left:a_Left Right:a_Right Duration:a_Duration];
    }
}

-(void)deleteMilkData:(long)a_Date
{
    // 입력한 항목과 일치하는 행 삭제
//    String aQuery = "DELETE FROM " + sTableMilk + " WHERE reg_date = " + a_Date + ";";
    NSString *aQuery = [NSString stringWithFormat:@"DELETE FROM %@ WHERE reg_date = %ld;", sTableMilk, a_Date];
    NSLog(@"aQuery:%@", aQuery);
    [self execQuery:aQuery];
}

-(NSArray*) getMilkData
{
    NSMutableArray *arrData = [[NSMutableArray alloc] init];
    NSString *aQuery = [NSString stringWithFormat:@"SELECT * FROM %@", sTableMilk];
    NSLog(@"aQuery:%@", aQuery);
    
    [self openDB];
    
    sqlite3_stmt *stmt ;        //SQL 데이터 초기화.
    if (sqlite3_prepare_v2(database, [aQuery UTF8String], -1, &stmt, NULL) == SQLITE_OK) {
        while (sqlite3_step(stmt) == SQLITE_ROW) {
            long aCol01 = sqlite3_column_int(stmt, 1);
            int aCol02 = sqlite3_column_int(stmt, 2);
            int aCol03 = sqlite3_column_int(stmt, 3);
            int aCol04 = sqlite3_column_int(stmt, 4);
            [arrData addObject:@{@"date" : [NSNumber numberWithLong:aCol01],
                                 @"left" : [NSNumber numberWithLong:aCol02],
                                 @"right" : [NSNumber numberWithLong:aCol03],
                                 @"time" : [NSNumber numberWithLong:aCol04]}];
            
        }
    }
    sqlite3_finalize(stmt);
    sqlite3_close(database);
    
    return arrData;
}

-(NSArray*) getMilkDataFrom:(int64_t)a_From To:(int64_t)a_To
{
    NSMutableArray *arrData = [[NSMutableArray alloc] init];
    NSString *aQuery = [NSString stringWithFormat:@"SELECT * FROM %@ where reg_date between %lld and %lld", sTableMilk, a_From, a_To];
    NSLog(@"aQuery:%@", aQuery);
    
    [self openDB];
    sqlite3_stmt *stmt ;        //SQL 데이터 초기화.
    if (sqlite3_prepare_v2(database, [aQuery UTF8String], -1, &stmt, NULL) == SQLITE_OK) {
        while (sqlite3_step(stmt) == SQLITE_ROW) {
            long aCol01 = sqlite3_column_int64(stmt, 1);
            int aCol02 = sqlite3_column_int(stmt, 2);
            int aCol03 = sqlite3_column_int(stmt, 3);
            long aCol04 = sqlite3_column_int64(stmt, 4);
            [arrData addObject:@{@"date" : [NSNumber numberWithLong:aCol01],
                                 @"left" : [NSNumber numberWithLong:aCol02],
                                 @"right" : [NSNumber numberWithLong:aCol03],
                                 @"time" : [NSNumber numberWithLong:aCol04]}];
        }
    }
    sqlite3_finalize(stmt);
    sqlite3_close(database);
    
//    for (int i = 0; i < 10; i++) {
//        long aDate = [[NSDate date] timeIntervalSince1970] * 1000;
//        [arrData addObject:@{@"date" : [NSNumber numberWithLong:aDate],
//                             @"left" : [NSNumber numberWithLong:i*20],
//                             @"right" : [NSNumber numberWithLong:i*30],
//                             @"time" : [NSNumber numberWithLong:i*40]}];
//    }

    
    return arrData;
}


@end
