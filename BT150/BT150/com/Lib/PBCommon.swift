//
//  PBCommon.swift
//  BT150
//
//  Created by KOBONGHWAN on 01/12/2018.
//  Copyright © 2018 bistos. All rights reserved.
//

import UIKit

public struct GLColor {
    static let kMainTheme = PBLib.RGB([0xe6, 0x62, 0x72])
    static let kThemeLine = PBLib.RGB([0xd8, 0xd8, 0xd8])
    static let kThemeDark = PBLib.RGB([0x35, 0x39, 0x42])
    static let kDefBackgroundGray = PBLib.RGB([0xee, 0xee, 0xee])
    static let kBottomBG = PBLib.RGB([0x4b, 0x4f, 0x5b])
    static let kDefBlack = PBLib.RGB([0, 0, 0])
    static let kDefWhite = PBLib.RGB([0xff, 0xff, 0xff])
    static let kDefDarkGray = PBLib.RGB([80, 80, 80])
    static let kDefLightGray = PBLib.RGB([0xE0, 0xE0, 0xE0])
}


public var arrProgramData = [PBCommon.Program]()
public let kProgramDataCnt = 8
public var gPBSR = PBSR()

public var nPrgReqCnt5 = 0
public var nPrgReqCnt6 = 0
public var nPrgReqCnt7 = 0
public var nPrgReqCnt8 = 0

public var nPrgOpType5 = PBCommon.OP_TYPE_DOUBLE
public var nPrgOpType6 = PBCommon.OP_TYPE_DOUBLE
public var nPrgOpType7 = PBCommon.OP_TYPE_NONE
public var nPrgOpType8 = PBCommon.OP_TYPE_NONE


public class PBCommon: NSObject {
    
    public static let FW_BLOCK_SIZE : UInt8 = 0x80

    public static let ACT_MAIN : Int      = 100
    public static let ACT_MENU01 : Int    = 200
    public static let ACT_MENU02 : Int    = 300
    public static let ACT_MENU03 : Int    = 400
        
    public static let VOICE_MODE_OPERATION : Int      = 100
    public static let VOICE_MODE_PROGRAM : Int        = 200
    public static let VOICE_MODE_PROGRAM_EDIT : Int   = 300

    public static let DataChunkQueMax : Int = 128
    public static let RecvDataQueMax : Int = 1024*100
    
    public static let OP_MODE_NONE : Int = 999
    public static let OP_MODE_MSG : Int = 0
    public static let OP_MODE_EXP : Int = 1
    
    public static let OP_TYPE_NONE = 9
    public static let OP_TYPE_SINGLE = 0
    public static let OP_TYPE_DOUBLE = 1

    public static let OP_PRESSURE : Int = 300
    public static let OP_CYCLE : Int = 400
    public static let OP_TIMER : Int    = 500

    public static let EXP_PRESSURE_MIN : Int    = 0
    public static let EXP_PRESSURE_MAX : Int    = 15
    
    public static let MSG_PRESSURE_MIN : Int    = 0
//    public static let MSG_PRESSURE_MAX : Int    = 7
    
    public static let EXP_CYCLE_MIN : Int    = 0
    public static let EXP_CYCLE_MAX : Int    = 5

    public static let MSG_CYCLE_MIN : Int    = 0
    public static let MSG_CYCLE_MAX : Int    = 2
    
    public static let ELAPSED_TIME_MAX : Int = 60*30
    
    public static let arrCycleExp = [36, 40, 44, 48, 52, 56, 60]
    public static let arrCycleMsg = [36, 40, 44, 48, 52, 56, 60]

//    public static let arrPressureLimit = [14, 12, 10, 9, 8, 6]
    public static let arrPressureLimit = [15, 15, 15, 15, 15, 15]
    
    public static let sCmdModeChange : String = "M;"
    public static let sCmdPressureUp : String = "PU;"
    public static let sCmdPressureDown : String = "PD;"
    public static let sCmdCycleUp : String = "CU;"
    public static let sCmdCycleDown : String = "CD;"
    
    public static let cCmdProgramRun : String = "R"
    public static let sCmdProgramStop : String = "E;"
    public static let sCmdProgramSkip : String = "S;"
    
    public static let sCmdElapsedTime : String = "T;"
    public static let sCmdInqueryStatus : String = "Q;"

    public static let sCmdPowerDown : String = "G;"
    public static let sCmdToggleLamp : String = "L;"
    public static let sCmdInqueryBattery : String = "B;"
    
    public static let sCmdFWReadySend_Idle : UInt8 = 0x7f
    public static let sCmdFWReadySend_Ready : UInt8 = 0x7e
    public static let sCmdFWReadySend_Z : UInt8 = 0x5a //'Z'
    public static let sCmdFWReadySend_X : UInt8 = 0x58 //'X'
    public static let sCmdFWReadySend_V : UInt8 = 0x56 //'V'
    public static let sCmdFWReadySend_U : UInt8 = 0x55 //'U'


    public static let sCmdFWReadyRecv_R : UInt8 = 0x52 //'R'
    public static let sCmdFWReadyRecv_E : UInt8 = 0x45 //'E'
    public static let sCmdFWReadyRecv_D : UInt8 = 0x44 // 'D'
    public static let sCmdFWReadyRecv_Y : UInt8 = 0x59 // 'Y'
    public static let sCmdFWReadyRecv_CR : UInt8 = 0x0D;
    public static let sCmdFWReadyRecv_ERR : UInt8 = 0x3f

    public static let sCmdFWEraseDelay : UInt8 = 0x7d
    public static let sCmdFWErase : UInt8 = 0x65 // 'e'
    public static let sCmdFWErase0 : UInt8 = 0x7d
    public static let sCmdFWErase1 : UInt8 = 0x7c
    public static let sCmdFWErase10 : UInt8 = 0x7b
    public static let sCmdFWAddress : UInt8 = 0x41 // 'A'
    public static let sCmdFWBinary : UInt8 = 0x42 // 'B'
    public static let sCmdFWFlash : UInt8 = 0x46 // 'F'
    public static let sCmdFWEnd : UInt8 = 0x7e
    
//    public static var arrProgramData = [Program](repeating: Program(), count: 8)
    public static var dataChunkQue : DataChunkQue = DataChunkQue()
    public static var recvDataQue : RecvDataQue = RecvDataQue()
    
    public static var sUserName = "user name"
    public static var dtBirthday = Date(timeIntervalSinceNow: -31557600*20)
    public static var sGender = ""
    public static var sUserPhoto = UIImage()

    public static var gRecvData : String = ""
    public static var gRecvDataProgram : String = ""
    public static var gRecvBattery : String = ""
    public static var nElapsedTime : Int = 0
    public static var nExpressionTimeS : Int = 0
    public static var nOperationModeCur : Int = PBCommon.OP_MODE_NONE
    public static var bProgramDataLoaded : Bool = false
    public static var bRecvProgramCnt : Bool = false
    public static var bProcessingProgramData : Bool = false
    public static var bDeviceWakeUp : Bool = false
    public static var bProgramRunning : Bool = false
    
    public static var bMainVCmdReady : Bool = false
    public static var bMenu01VCmdReady : Bool = false
    public static var bMenu02VCmdReady : Bool = false
    public static var bMenu03VCmdReady : Bool = false
    public static var bMenu02SubVCmdReady : Bool = false
    public static var bMenu03SubVCmdReady : Bool = false

    public static var nCurSelPrgTabIdx : Int = 0

    public static var nOperationMode : Int = PBCommon.OP_MODE_NONE
    public static var nOperationType : Int = PBCommon.OP_TYPE_DOUBLE
    public static var nPressureExp : Int = 0
    public static var nCycleExp : Int = 0
    public static var nPressureMsg : Int = 0
    public static var nCycleMsg : Int = 0
    
    public static var gPeripheral : TIOPeripheral?
    public static var mTimerElapsed : Timer?
    public static var mTimerRecv : Timer?
    public static var bBLEAvailable : Bool = false
    
    public static var curSpeechLanguage = VOICE_REC_LANG_KOREAN
    public static var nVoiceMode = VOICE_MODE_OPERATION
    public static var nActivityNo : Int = ACT_MAIN
    public static var sUseVCmd = "disable"
    
    public static var bProgramEditing = false
    public static var nProgramLastTime = 0
    
    public class Program {
//        public var programDataList = [ProgramData](repeating: ProgramData(), count: 8)
        public var programDataList = [ProgramData]()
        
        init() {
            initProgram()
        }
        
        public func initProgram() {
            for _ in 0 ..< 8 {
                programDataList.append(ProgramData())
            }
        }
    }
    
    public class ProgramData {
        public var nCode : Int = 0
        public var nMode : Int = PBCommon.OP_MODE_NONE
        public var nPressure : Int = 0
        public var nCycle : Int = 0
        public var nLen : Int = 0
        public var bDownloaded : Bool = false
        
        init() {
            initProgramData()
        }
        
        public func initProgramData() {
            nCode = 0
            nMode = PBCommon.OP_MODE_NONE
            nPressure = 0
            nCycle = 0
            nLen = 0
            bDownloaded = false
        }
        
        public func getCycleValue() -> Int {
            if (nMode == PBCommon.OP_MODE_EXP
                && nCycle >= 0 && nCycle < PBCommon.arrCycleExp.count ) {
                return arrCycleExp[nCycle]
                
            } else if (nMode == PBCommon.OP_MODE_MSG
                && nCycle >= 0 && nCycle < PBCommon.arrCycleMsg.count ) {
                return arrCycleMsg[nCycle]
            }
            return 0
        }
    }
    
    public class MilkData {
        
        var lDate : Int64 = 0
        var nAmtL : Int = 0
        var nAmtR : Int = 0
        var nTime : Int64 = 0
        
        init() {
            initMilkData()
        }
        
        init(Date a_Date : String, Amt a_Amt : Int, Time a_Time : Int64) {
            initMilkData(Date: a_Date, AmtL: a_Amt, AmtR: a_Amt, Time: a_Time)
        }
        
        init(Date a_Date : Int64, AmtL a_AmtL : Int, AmtR a_AmtR : Int, Time a_Time : Int64) {
            lDate = a_Date
            nAmtL = a_AmtL
            nAmtR = a_AmtR
            nTime = a_Time
        }
        
        func initMilkData(Date a_Date : String, AmtL a_AmtL : Int, AmtR a_AmtR : Int, Time a_Time : Int64) {
            let dfDate = DateFormatter()
            dfDate.dateFormat = "yyyyMMddHHmm"
            dfDate.locale = Locale(identifier: "ko_KR")
            dfDate.timeZone = TimeZone(identifier: "GMT")!
            if let aDate = dfDate.date(from: a_Date) {
                lDate = aDate.getTimeMillis()
                nAmtL = a_AmtL
                nAmtR = a_AmtR
                nTime = a_Time
            }
        }
        
        func initMilkData() {
            lDate = PBLib.getCurrentMillis()
            nAmtL = 0
            nAmtR = 0
            nTime = 0
        }
    }
    
    public class DataChunkQue {
        public var QMAX : Int = PBCommon.DataChunkQueMax
        public var nFront : Int = 0
        public var nRear : Int = 0
        private var dataChunkList = [String](repeating: "", count: PBCommon.DataChunkQueMax)
        public var bUsed : Bool = false
        
        init() {
            bUsed = false
            InitQueue()
        }
        
        func InitQueue() {
            nFront = 0;
            nRear = 0;
        }
        
        func Put(_ data : String) -> String {
            //queue is full
            if (nRear + 1) % QMAX == nFront {
                return ""
            }
            dataChunkList[nRear] = data
            nRear += 1
            nRear = nRear % QMAX
            return data
        }

        func Get() -> String {
            //queue is empty
            if nFront == nRear {
                return ""
            }
            let data : String = dataChunkList[nFront]
            nFront += 1
            nFront = nFront % QMAX
            return data
        }
        
        func isEmpty() -> Bool {
            return (nFront == nRear) ? true : false
        }
    }
    
    public class RecvDataQue {
        public var QMAX : Int = PBCommon.RecvDataQueMax
        public var nFront : Int = 0
        public var nRear : Int = 0
        private var recvDataList = [String](repeating: "", count: PBCommon.RecvDataQueMax)
        public var bUsed : Bool = false
        
        init() {
            bUsed = false
            InitQueue()
        }
        
        func InitQueue() {
            nFront = 0;
            nRear = 0;
        }
        
        func Put(_ data : String) -> String {
            //queue is full
            if (nRear + 1) % QMAX == nFront {
                return ""
            }
            recvDataList[nRear] = data
            nRear += 1
            nRear = nRear % QMAX
            return data
        }
        
        func Get() -> String {
            //queue is empty
            if nFront == nRear {
                return ""
            }
            let data : String = recvDataList[nFront]
            nFront += 1
            nFront = nFront % QMAX
            return data
        }
        
        func isEmpty() -> Bool {
            return (nFront == nRear) ? true : false
        }
    }
    
    class func stopElapsedTimer() {
        mTimerElapsed?.invalidate()
        mTimerElapsed = nil
    }
    
    class func doSendiingDataThread() {
        stopElapsedTimer()
        
        mTimerElapsed = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(taskElapsed), userInfo: nil, repeats: true)
    }
    
    @objc class func taskElapsed() {
        if let aPeripheral = gPeripheral {
            if aPeripheral.isConnected {
                
                let aData = dataChunkQue.Get()
                if aData.count > 0 {
                    sendDataBLE(aData)
                }
            }
        }
    }
    
    class func stopRecvimer() {
        mTimerRecv?.invalidate()
        mTimerRecv = nil
    }
    
    class func doRecvParsingThread() {
        stopRecvimer()
        
        mTimerRecv = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(taskRecvData), userInfo: nil, repeats: true)
    }
    
    @objc class func taskRecvData() {
        let aRecv = recvDataQue.Get()
        if aRecv.count > 0 {
            let aPatckets = aRecv.split(separator: ";")
            for aData in aPatckets {
                PBCommon.parseRecvData(String(aData+";"))
            }
        }
    }
    
    public class func parseRecvData(_ a_Recv : String) {
        if a_Recv != PBCommon.gRecvData || (a_Recv.count == 16 && a_Recv.substring(to: 1) == "M") {
            PBCommon.gRecvData = a_Recv

            if a_Recv.count == 6
                && a_Recv.substring(to: 1) == "T" {
                PBCommon.nElapsedTime = Int(a_Recv.substring(with: 1..<5)) ?? 0
//                print("################################# PBCommon.nElapsedTime:\(PBCommon.nElapsedTime)")
                
            } else if a_Recv.count == 17
                && a_Recv.substring(to: 1) == "I"
                && a_Recv.substring(with: 2..<3) == "S"
                && a_Recv.substring(with: 4..<5) == "M"
                && a_Recv.substring(with: 6..<7) == "P"
                && a_Recv.substring(with: 9..<10) == "C"
                && a_Recv.substring(with: 11..<12) == "T" {
                parseProgramData(a_Recv)
                
            } else if a_Recv.count == 3
                && a_Recv.substring(to: 1) == "V" {
                let aBattery = a_Recv.substring(with: 1..<2)
                //send battery status
                broadcastData("recv_battery", Data: aBattery)
                return
                
            } else if a_Recv.count == 10
                && a_Recv.substring(to: 1) == "A" {

                //Asssswwww;
                nPrgReqCnt5 = Int(a_Recv.substring(with: 1..<2)) ?? 0
                nPrgReqCnt6 = Int(a_Recv.substring(with: 2..<3)) ?? 0
                nPrgReqCnt7 = Int(a_Recv.substring(with: 3..<4)) ?? 0
                nPrgReqCnt8 = Int(a_Recv.substring(with: 4..<5)) ?? 0
                
                if nPrgReqCnt5 > 0 {
                    nPrgOpType5 = Int(a_Recv.substring(with: 5..<6)) ?? 0
                }
                if nPrgReqCnt6 > 0 {
                    nPrgOpType6 = Int(a_Recv.substring(with: 6..<7)) ?? 0
                }
                if nPrgReqCnt7 > 0 {
                    nPrgOpType7 = Int(a_Recv.substring(with: 7..<8)) ?? 0
                }
                if nPrgReqCnt8 > 0 {
                    nPrgOpType8 = Int(a_Recv.substring(with: 8..<9)) ?? 0
                }
                
                print("onDataReceived: a_Recv: \(a_Recv)")
                print("onDataReceived: nPrgOpType5:\(nPrgOpType5) nPrgOpType6:\(nPrgOpType6) nPrgOpType7:\(nPrgOpType7) nPrgOpType8:\(nPrgOpType8)")
                print("onDataReceived: nPrgReqCnt5:\(nPrgReqCnt5) nPrgReqCnt6:\(nPrgReqCnt6) nPrgReqCnt7:\(nPrgReqCnt7) nPrgReqCnt8:\(nPrgReqCnt8)")
                
                bRecvProgramCnt = true
                
                goBackToMain()
                
                DispatchQueue.main.asyncAfter(deadline: .now()+1.0) {
                    print("req_prgdata sent =====!!!!")
                    broadcastData("req_prgdata", Data: a_Recv)
                }
                return
                
            } else if a_Recv.count == 16
                && a_Recv.substring(to: 1) == "M"
                && a_Recv.substring(with: 2..<3) == "P"
                && a_Recv.substring(with: 5..<6) == "C"
                && a_Recv.substring(with: 7..<8) == "R" {
                
                PBCommon.gRecvDataProgram = a_Recv
                
                let aRun = a_Recv.substring(with: 8..<9)
                if aRun == "X" || aRun == "x" {
                    PBCommon.bDeviceWakeUp = false
                } else {
                    PBCommon.bDeviceWakeUp = true
                }
                
                if aRun == "2" {
                    PBCommon.bProgramRunning = true
                } else {
                    PBCommon.bProgramRunning = false
                }
                
//                print("createOperationActivity-1: a_Recv:\(a_Recv) running:\(PBCommon.bProgramRunning)")
                print("nOperationType:\(nOperationType)")
                if !PBCommon.bProgramRunning && isOperatingConditionChanged(a_Recv) {
//                    print("createOperationActivity-2")
                    PBCommon.createOperationActivity()
                }
                
                let aOperationMode = Int(a_Recv.substring(with: 1..<2))
                if aOperationMode == PBCommon.OP_MODE_EXP && PBCommon.nOperationModeCur != PBCommon.OP_MODE_EXP {
                    PBCommon.nExpressionTimeS = PBCommon.nElapsedTime
                    
                } else if (aOperationMode != PBCommon.OP_MODE_EXP && PBCommon.nOperationModeCur == PBCommon.OP_MODE_EXP) {
                    saveExpressionTime()
                }
                
                PBCommon.nOperationModeCur = aOperationMode ?? PBCommon.OP_MODE_NONE
             
                PBCommon.nOperationMode = aOperationMode ?? PBCommon.OP_MODE_NONE
                
                if a_Recv.count > 15 && a_Recv.substring(with: 13..<14) == "W" {
                    let aOpType = a_Recv.substring(with: 14..<15)
                    if aOpType == "0" {
                        PBCommon.nOperationType = PBCommon.OP_TYPE_SINGLE
                    } else {
                        PBCommon.nOperationType = PBCommon.OP_TYPE_DOUBLE
                    }
                }
                
            }
            broadcastData("recv_data", Data: a_Recv)
            
        }
    }
    
    class func isOperatingConditionChanged(_ a_Recv : String) -> Bool {
    
        let aOperationMode = Int(a_Recv.substring(with: 1..<2))
//        if aOperationMode != aOperationMode {
//            return true
//        }
    
        if a_Recv.count > 15 && a_Recv.substring(with: 13..<14) == "W" {
            let aOpType = Int(a_Recv.substring(with: 14..<15))
            if aOpType != nOperationType {
                return true
            }
        }
    
        if aOperationMode == PBCommon.OP_MODE_EXP {
            let aPressureExp = Int(a_Recv.substring(with: 3..<5))
//            let aCycleExp = Int(a_Recv.substring(with: 6..<7))
            if aPressureExp != nPressureExp {
    
            }
    
        } else if aOperationMode == PBCommon.OP_MODE_MSG {
//            let aPressureMsg = Int(a_Recv.substring(with: 3..<5))
//            let aCycleMsg = Int(a_Recv.substring(with: 6..<7))
        }
    
        return  false
    }
    
    class func saveExpressionTime() {
        let aElapsed = PBCommon.nElapsedTime - PBCommon.nExpressionTimeS
        print("saveExpressionTime: aElapsed:\( aElapsed)");
        if (aElapsed > 0) {
            let aDate = Date()
            let aLogTime = aDate.getTimeMillis() -  Int64(aElapsed) * 1000
            
            let dfDate = DateFormatter()
            dfDate.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let aDateString = dfDate.string(from: Date(timeIntervalSince1970: TimeInterval(aLogTime/1000)))
            
            print("saveExpressionTime: aElapsed:\(aElapsed) Time:\(aDateString)");
            
            let dbHelper = DBHelper()
            dbHelper.setMilkData(Int(aLogTime), left: 0, right: 0, duration: aElapsed)
            
//            let aDate : Date = new Date(from: <#T##Decoder#>)
//            let aDate : Date = aDate.

//            SimpleDateFormat aSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            LOGe(TAG, "saveExpressionTime: aElapsed:" + aElapsed + " Time:" + aSdf.format(aLogTime));
//
//            DBHelper dbHelper = new DBHelper(this);
//            dbHelper.setMilkData(aLogTime, 0, 0, aElapsed);
        }
    }

    
    class func broadcastData(_ a_Key : String, Data a_Data : String) {
//        print("broadcastData...................... Key:\(a_Key) Data:\(a_Data)")
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MSG_DATA_RECV"), object: nil, userInfo: [a_Key : a_Data])
    }
    
//    class func loadDefaultProgramData_20190424() {
//
//        for _ in 0 ..< 8 {
//            arrProgramData.append(Program())
//        }
//
//        //P01 - 한쪽 유축시 신생아 프로그램 Table
//        parseProgramData("I0S0M0P00C0T0010E0;")
//        parseProgramData("I0S1M0P00C1T0020E0;")
//        parseProgramData("I0S2M0P00C2T0030E0;")
//        parseProgramData("I0S3M0P01C2T0030E0;")
//        parseProgramData("I0S4M1P02C5T0003E0;")
//        parseProgramData("I0S5M1P03C5T0004E0;")
//        parseProgramData("I0S6M1P04C5T0005E0;")
//        parseProgramData("I0S7M1P05C5T1800E0;")
//        //P02 - 한쪽 유축시 유아 프로그램 Table
//        parseProgramData("I1S0M0P00C0T0010E0;")
//        parseProgramData("I1S1M0P00C1T0020E0;")
//        parseProgramData("I1S2M0P00C2T0030E0;")
//        parseProgramData("I1S3M0P01C2T0030E0;")
//        parseProgramData("I1S4M1P03C5T0003E0;")
//        parseProgramData("I1S5M1P04C5T0005E0;")
//        parseProgramData("I1S6M1P05C5T0005E0;")
//        parseProgramData("I1S7M1P06C5T1800E0;")
//        //P03 - 양쪽 유축시 신생아 프로그램 Table
//        parseProgramData("I2S0M0P00C0T0010E0;")
//        parseProgramData("I2S1M0P00C1T0020E0;")
//        parseProgramData("I2S2M0P00C2T0020E0;")
//        parseProgramData("I2S3M0P01C2T0030E0;")
//        parseProgramData("I2S4M1P02C5T0003E0;")
//        parseProgramData("I2S5M1P03C5T0005E0;")
//        parseProgramData("I2S6M1P04C5T0005E0;")
//        parseProgramData("I2S7M1P05C5T1800E0;")
//        //P04 - 양쪽 유축시 유아 프로그램 Table
//        parseProgramData("I3S0M0P00C0T0010E0;")
//        parseProgramData("I3S1M0P00C1T0020E0;")
//        parseProgramData("I3S2M0P00C2T0030E0;")
//        parseProgramData("I3S3M0P01C2T0030E0;")
//        parseProgramData("I3S4M1P03C5T0003E0;")
//        parseProgramData("I3S5M1P04C5T0005E0;")
//        parseProgramData("I3S6M1P05C5T0005E0;")
//        parseProgramData("I3S7M1P06C5T1800E0;")
//
//        if getLocaleLanguage() == "fr" {
//            //P05 - for France
//            parseProgramData("I4S0M0P01C2T0135E0;")
//            parseProgramData("I4S1M1P03C0T0005E0;")
//            parseProgramData("I4S2M1P05C0T1060E0;")
//
//            //P06 - for France
//            parseProgramData("I5S0M0P01C2T0240E0;")
//            parseProgramData("I5S1M0P01C1T0180E0;")
//            parseProgramData("I5S2M0P02C0T0120E0;")
//            parseProgramData("I5S3M0P01C2T0240E0;")
//            parseProgramData("I5S4M0P01C1T0180E0;")
//            parseProgramData("I5S5M0P02C0T0120E0;")
//            parseProgramData("I5S6M0P01C2T0720E0;")
//        }
//    }
    
    class func loadDefaultProgramData_20210325() {
        
        for _ in 0 ..< 8 {
            arrProgramData.append(Program())
        }
        
        //P01 - P05
        parseProgramData("I0S0M0P01C2T0135E0;");
        parseProgramData("I0S1M1P03C0T0005E0;");
        parseProgramData("I0S2M1P05C0T1060E0;");
        //P02 - P06
        parseProgramData("I1S0M0P01C2T0240E0;");
        parseProgramData("I1S1M0P01C1T0180E0;");
        parseProgramData("I1S2M0P02C0T0120E0;");
        parseProgramData("I1S3M0P01C2T0240E0;");
        parseProgramData("I1S4M0P01C1T0180E0;");
        parseProgramData("I1S5M0P02C0T0120E0;");
        parseProgramData("I1S6M0P01C2T0120E0;");
        //P03 - P05
        parseProgramData("I2S0M0P01C2T0135E0;");
        parseProgramData("I2S1M1P03C0T0005E0;");
        parseProgramData("I2S2M1P05C0T1060E0;");
        //P04 - P06
        parseProgramData("I3S0M0P01C2T0240E0;");
        parseProgramData("I3S1M0P01C1T0180E0;");
        parseProgramData("I3S2M0P02C0T0120E0;");
        parseProgramData("I3S3M0P01C2T0240E0;");
        parseProgramData("I3S4M0P01C1T0180E0;");
        parseProgramData("I3S5M0P02C0T0120E0;");
        parseProgramData("I3S6M0P01C2T0120E0;");

        //P05 - for France
        parseProgramData("I4S0M0P01C2T0135E0;");
        parseProgramData("I4S1M1P03C0T0005E0;");
        parseProgramData("I4S2M1P05C0T1060E0;");

        //P06 - for France
        parseProgramData("I5S0M0P01C2T0240E0;");
        parseProgramData("I5S1M0P01C1T0180E0;");
        parseProgramData("I5S2M0P02C0T0120E0;");
        parseProgramData("I5S3M0P01C2T0240E0;");
        parseProgramData("I5S4M0P01C1T0180E0;");
        parseProgramData("I5S5M0P02C0T0120E0;");
        parseProgramData("I5S6M0P01C2T0120E0;");
    }
    
    class func parseProgramData(_ a_Data : String) {
        print("parseProgramData: \(a_Data)")
        
        let aIdx : Int = Int(a_Data.substring(with: 1..<2)) ?? 0
        let aSeq : Int = Int(a_Data.substring(with: 3..<4)) ?? 0
        
        let aData = arrProgramData[aIdx]
        let programData = aData.programDataList[aSeq]
        
        programData.nCode = aSeq
        programData.nMode = Int(a_Data.substring(with: 5..<6)) ?? 0
        programData.nPressure = Int(a_Data.substring(with: 7..<9)) ?? 0
        programData.nCycle = Int(a_Data.substring(with: 10..<11)) ?? 0
        programData.nLen = Int(a_Data.substring(with: 12..<16)) ?? 0
        programData.bDownloaded = true
        
        if programData.nLen == 0 {
            programData.initProgramData()
        }
        
//        print("\(aIdx) \(aSeq) mode:\(programData.nMode) pressure:\(programData.nPressure) cycle:\(programData.nCycle) len:\(programData.nLen) ")
    }
    
    class func sendData(_ a_Data : String) {
        _ = dataChunkQue.Put(a_Data)
    }
    
    class func sendDataBLE(_ a_Data : String) {
//        print("sendDataBLE : \(a_Data) >>>>>>>>>>")
        let aData = a_Data.data(using: String.Encoding.windowsCP1252)
        PBCommon.gPeripheral?.writeUARTData(aData)
    }
 
    static var mTimerDisconnect : Timer?
    
    class func powerDown() {
        print("send power down!!!!!!!!!")

        goBackToMain()

        sendData(sCmdPowerDown)
                
//        mTimerDisconnect?.invalidate()
//        mTimerDisconnect = nil
//        mTimerDisconnect = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(taskSendDisconnect), userInfo: nil, repeats: false)
    }
    
//    @objc class func taskSendDisconnect() {
//        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MSG_CMD_DISCONNECT"), object: nil, userInfo: nil)
//    }
    
    class func  toggleLamp() {
        print("send toggle Lamp!!!!!!!!!")
        sendData(sCmdToggleLamp)
    }
    
    class func appDelegate() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }

    class func goBackToMain() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UNWIND_TO_MAIN"), object: nil, userInfo: nil)
    }
    
    class func createOperationActivity() {
        let vc = appDelegate().getVisibleViewController(UIApplication.shared.keyWindow?.rootViewController)

        if (vc?.isKind(of: MainViewController.self))! {
            vc?.performSegue(withIdentifier: "SEG_MAIN_MENU01", sender: nil)
        
        } else if (vc?.isKind(of: Menu02ViewController.self))! {
            let vcMenu = vc as! Menu02ViewController
            vcMenu.vcTopMenu?.performSegue(withIdentifier: "SB_SEGUE_MENU01", sender: nil)

        } else if (vc?.isKind(of: Menu03ViewController.self))! {
            let vcMenu = vc as! Menu03ViewController
            vcMenu.vcTopMenu?.performSegue(withIdentifier: "SB_SEGUE_MENU01", sender: nil)
        }
    }

    class func createProgramActivity() {
        let vc = appDelegate().getVisibleViewController(UIApplication.shared.keyWindow?.rootViewController)
        
        if (vc?.isKind(of: MainViewController.self))! {
            vc?.performSegue(withIdentifier: "SEG_MAIN_MENU02", sender: nil)
            
        } else if (vc?.isKind(of: Menu01ViewController.self))! {
            let vcMenu = vc as! Menu01ViewController
            vcMenu.vcTopMenu?.performSegue(withIdentifier: "SB_SEGUE_MENU02", sender: nil)
            
        } else if (vc?.isKind(of: Menu03ViewController.self))! {
            let vcMenu = vc as! Menu03ViewController
            vcMenu.vcTopMenu?.performSegue(withIdentifier: "SB_SEGUE_MENU02", sender: nil)
        }
    }

    class func createStatisticActivity() {
        let vc = appDelegate().getVisibleViewController(UIApplication.shared.keyWindow?.rootViewController)
        
        if (vc?.isKind(of: MainViewController.self))! {
            vc?.performSegue(withIdentifier: "SEG_MAIN_MENU03", sender: nil)
            
        } else if (vc?.isKind(of: Menu02ViewController.self))! {
            let vcMenu = vc as! Menu02ViewController
            vcMenu.vcTopMenu?.performSegue(withIdentifier: "SB_SEGUE_MENU03", sender: nil)
            
        } else if (vc?.isKind(of: Menu03ViewController.self))! {
            let vcMenu = vc as! Menu03ViewController
            vcMenu.vcTopMenu?.performSegue(withIdentifier: "SB_SEGUE_MENU03", sender: nil)
        }
    }
    
    class func isProgramEditing() -> Bool {
        return bProgramEditing
    }
    
    class func loadVoiceRecMode() {
        if let aLanguage = UserDefaults.standard.object(forKey: "vcmd_language") as? String {
            if aLanguage == VOICE_REC_LANG_KOREAN {
                PBCommon.curSpeechLanguage = VOICE_REC_LANG_KOREAN
            
            } else if aLanguage == VOICE_REC_LANG_FRENCH {
                PBCommon.curSpeechLanguage = VOICE_REC_LANG_FRENCH
                
            } else {
                PBCommon.curSpeechLanguage = VOICE_REC_LANG_ENGLISH
            }
        }
        print("loadVoiceRecMode : PBCommon.curSpeechLanguage:\(PBCommon.curSpeechLanguage)")
        gPBSR.authorizeSR(PBCommon.curSpeechLanguage)
    }
    

    class func sendProgramDataAll() {

//        PBCommon.sendData("A00000000;")
        
        sendSavePacket()
        
        if (getProgramListCount(4) > 0) {
            saveProgramData(4)
        }
        
        if (getProgramListCount(5) > 0) {
            saveProgramData(5)
        }
        
        if (getProgramListCount(6) > 0) {
            saveProgramData(6)
        }
        
        if (getProgramListCount(7) > 0) {
            saveProgramData(7)
        }        
    }

    class func getProgramListCount(_ a_TabIdx : Int) -> Int {
        let aData = arrProgramData[a_TabIdx].programDataList
        var aCnt = 0
        for i in 0 ..< aData.count {
            if aData[i].nMode != PBCommon.OP_MODE_NONE {
                aCnt += 1
            }
        }
        return aCnt
    }

    class func saveProgramData(_ a_TabIdx : Int) {
        let aData = arrProgramData[a_TabIdx].programDataList
        for i in 0 ..< aData.count {
            if i == aData.count-1 {
//                if PBCommon.getLocaleLanguage() != "fr"
//                || !(a_TabIdx == 4 || a_TabIdx == 5) {
//                    aData[i].nLen = PBCommon.nProgramLastTime
//                }
                sendProgramData(i, TabIdx: a_TabIdx)
                break
               
            } else if i < aData.count-1 && aData[i+1].nMode == PBCommon.OP_MODE_NONE {
//                if PBCommon.getLocaleLanguage() != "fr"
//                || !(a_TabIdx == 4 || a_TabIdx == 5) {
//                    aData[i].nLen = PBCommon.nProgramLastTime
//                }
                sendProgramData(i, TabIdx: a_TabIdx)
                break
               
            } else {
                sendProgramData(i, TabIdx: a_TabIdx)
            }
        }

        sendSavePacket()
       
//       refreshCurrentOpType()
              
//        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MSG_PRGLIST_RELOAD"), object: nil, userInfo: nil)
    }
   
    class func sendProgramData(_ a_Idx : Int, TabIdx a_TabIdx : Int) {

       let aData = arrProgramData[a_TabIdx].programDataList[a_Idx]
       print("sendProgramData: nCode:\(aData.nCode) nMode:\(aData.nMode) nPressure:\(aData.nPressure) nCycle:\(aData.nCycle) nLen:\(aData.nLen)");
       
       let aCmd = String(format: "I%dS%dM%dP%02dC%dT%04d;",
                         a_TabIdx,
                         a_Idx,
                         aData.nMode,
                         aData.nPressure,
                         aData.nCycle,
                         aData.nLen)

       print("sendProgramData:" + aCmd)
       PBCommon.sendData(aCmd)
       
//       UserDefaults.standard.set(aCmd, forKey: "programdata_\(a_TabIdx)")
    }

    class func sendSavePacket() {
        var aCmd = "A"
        for i in 0 ..< arrProgramData.count {
            let aSeqData = arrProgramData[i].programDataList
            var aCnt = 0
            for j in 0 ..< aSeqData.count {
                if aSeqData[j].nMode == PBCommon.OP_MODE_NONE {
                    break
                }
                aCnt += 1
            }
            
            if i == 4 {
                nPrgReqCnt5 = aCnt
            } else if i == 5 {
                nPrgReqCnt6 = aCnt
            } else if i == 6 {
                nPrgReqCnt7 = aCnt
            } else if i == 7 {
                nPrgReqCnt8 = aCnt
            }
//            aCmd += String(format: "%d", aCnt)
        }
        
        aCmd += String(format: "%d", nPrgReqCnt5)
        aCmd += String(format: "%d", nPrgReqCnt6)
        aCmd += String(format: "%d", nPrgReqCnt7)
        aCmd += String(format: "%d", nPrgReqCnt8)
        
        if nPrgOpType5 == PBCommon.OP_TYPE_NONE {
            aCmd += String(format: "%d", PBCommon.nOperationType)
        } else {
            aCmd += String(format: "%d", nPrgOpType5)
        }
        
        if nPrgOpType6 == PBCommon.OP_TYPE_NONE {
            aCmd += String(format: "%d", PBCommon.nOperationType)
        } else {
            aCmd += String(format: "%d", nPrgOpType6)
        }
        
        if nPrgOpType7 == PBCommon.OP_TYPE_NONE {
            aCmd += String(format: "%d", PBCommon.nOperationType)
        } else {
            aCmd += String(format: "%d", nPrgOpType7)
        }
        
        if nPrgOpType8 == PBCommon.OP_TYPE_NONE {
            aCmd += String(format: "%d", PBCommon.nOperationType)
        } else {
            aCmd += String(format: "%d", nPrgOpType8)
        }

        print(String(format: "sendSavePacket nPrgReqCnt: %d %d %d %d", nPrgReqCnt5, nPrgReqCnt6, nPrgReqCnt7, nPrgReqCnt8))
        print(String(format: "sendSavePacket nPrgOpType: %d %d %d %d", nPrgOpType5, nPrgOpType6, nPrgOpType7, nPrgOpType8))

        print("sendSavePacket:" + aCmd + ";")
        PBCommon.sendData(aCmd + ";")
        
        UserDefaults.standard.set(aCmd, forKey: "program_cnt_optype")
        UserDefaults.standard.synchronize()
    }
    
    class func getLocaleLanguage() -> String {
        var aRet = ""
        if let languageCode = (Locale.current as NSLocale).object(forKey: .languageCode) as? String {
            aRet = languageCode
        }
        return aRet
    }
    
}
