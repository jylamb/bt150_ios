//
//  ObjC.h
//  BT150
//
//  Created by PHILOLAMB on 2020/07/07.
//  Copyright © 2020 bistos. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ObjC : NSObject

+ (BOOL)catchException:(void(^)(void))tryBlock error:(__autoreleasing NSError **)error;

@end

NS_ASSUME_NONNULL_END
