//
//  PBLib.swift
//  cmc
//
//  Created by lamb on 2015. 5. 6..
//  Copyright (c) 2015년 CMC. All rights reserved.
//

import UIKit

public extension UIViewController {
    
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: 150, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
}

public extension String {
    func index(from: Int) -> Index {
        return self.index(startIndex, offsetBy: from)
    }
    
    func substring(from: Int) -> String {
        let fromIndex = index(from: from)
        return String(self[fromIndex...])
    }
    
    func substring(to: Int) -> String {
        let toIndex = index(from: to)
        return String(self[..<toIndex])
    }
    
    func substring(with r: Range<Int>) -> String {
        let startIndex = index(from: r.lowerBound)
        let endIndex = index(from: r.upperBound)
        return String(self[startIndex..<endIndex])
    }
    
    func contains(find: String) -> Bool {
        return self.range(of: find) != nil
    }
    
    func containsIgnoringCase(find: String) -> Bool{
        return self.range(of: find, options: .caseInsensitive) != nil
    }
    
    var isNumber: Bool {
        return !isEmpty && rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
    }
}

public extension Date {
    func startOfYear() -> Date {
        var gregorian = Calendar(identifier: Calendar.Identifier.gregorian)
//        gregorian.locale = Locale(identifier: "ko_KR")
        gregorian.timeZone = TimeZone(identifier: "GMT")!
        return gregorian.date(from: gregorian.dateComponents([.year], from: gregorian.startOfDay(for: self)))!
    }

    func startOfMonth() -> Date {
        var gregorian = Calendar(identifier: Calendar.Identifier.gregorian)
//        gregorian.locale = Locale(identifier: "ko_KR")
        gregorian.timeZone = TimeZone(identifier: "GMT")!
        return gregorian.date(from: gregorian.dateComponents([.year, .month], from: gregorian.startOfDay(for: self)))!
    }
    
    func endOfMonth() -> Date {
        var gregorian = Calendar(identifier: Calendar.Identifier.gregorian)
//        gregorian.locale = Locale(identifier: "ko_KR")
        gregorian.timeZone = TimeZone(identifier: "GMT")!
        return gregorian.date(byAdding: DateComponents(month: 1, day: -1), to: self.startOfMonth())!
    }
    
    func addDay(_ a_Day : Int) -> Date {
        var gregorian = Calendar(identifier: Calendar.Identifier.gregorian)
//        gregorian.locale = Locale(identifier: "ko_KR")
        gregorian.timeZone = TimeZone(identifier: "GMT")!
        return gregorian.date(byAdding: DateComponents(day: a_Day), to: self)!
    }

    func addMonth(_ a_Month : Int) -> Date {
        var gregorian = Calendar(identifier: Calendar.Identifier.gregorian)
//        gregorian.locale = Locale(identifier: "ko_KR")
        gregorian.timeZone = TimeZone(identifier: "GMT")!
        return gregorian.date(byAdding: DateComponents(month: a_Month), to: self)!
    }

    func addYear(_ a_Year : Int) -> Date {
        var gregorian = Calendar(identifier: Calendar.Identifier.gregorian)
//        gregorian.locale = Locale(identifier: "ko_KR")
        gregorian.timeZone = TimeZone(identifier: "GMT")!
        return gregorian.date(byAdding: DateComponents(year: a_Year), to: self)!
    }
    
    func getTimeMillis() -> Int64 {
        return Int64(self.timeIntervalSince1970 * 1000)
    }

}

public extension DispatchQueue {
    
    private static var _onceTracker = [String]()
    
    /**
     Executes a block of code, associated with a unique token, only once.  The code is thread safe and will
     only execute the code once even in the presence of multithreaded calls.
     
     - parameter token: A unique reverse DNS style name such as com.vectorform.<name> or a GUID
     - parameter block: Block to execute once
     */
    public class func once(token: String, block:()->Void) {
        objc_sync_enter(self); defer { objc_sync_exit(self) }
        
        if _onceTracker.contains(token) {
            return
        }
        
        _onceTracker.append(token)
        block()
    }
}

struct DownImage {
    var bDown = false
    var imgdata = UIImage()
}

class PBLib: NSObject {
    
    static var bVMode = false
    static var sUUIDKey = "lessonbook"
    
//    class func UUID () -> String? {
//        var uuid = UserDefaults.standard.object(forKey: "uuid") as? String
//        if(uuid == nil) {
//            uuid = saveUUID()
//        }
//        if bVMode {
//            return "38D9EE68-0F9C-4C5B-9C1C-4EE99488FAD0"
//            //            return "lessonbook_uuid_001"
//        }
//        return uuid
//    }
//
//    class func saveUUID() -> String {
//        var uuid = SSKeychain.password(forService: sUUIDKey, account: "uuid")
//        if uuid == nil || uuid != UserDefaults.standard.object(forKey: "uuid") as? String {
//            if uuid ==  nil {
//                let uuidss : CFUUID = CFUUIDCreate(kCFAllocatorDefault)
//                uuid = CFUUIDCreateString(kCFAllocatorDefault, uuidss) as String
//                print("uuid: \(uuid)")
//            }
//            SSKeychain.setPassword(uuid, forService: sUUIDKey, account: "uuid")
//            UserDefaults.standard.set(uuid, forKey: "uuid")
//        }
//        return uuid!
//    }
    
    class func performTransition (_ show : Bool, view : UIView) {
        performTransition(show, view : view, type: CATransitionType.fade.rawValue, subtype: CATransitionSubtype.fromBottom.rawValue)
    }
    
    class func performTransition (_ show : Bool, view : UIView, type : String, subtype : String) {
        performTransition(show, view : view, type: type, subtype: subtype, time : 0.4)
    }
    
    class func performTransition (_ show : Bool, view : UIView, type : String, subtype : String, time : Double) {
        let transition = CATransition()
        transition.duration = 0.4
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType(rawValue: type)
        transition.subtype = CATransitionSubtype(rawValue: subtype)
        view.layer.add(transition, forKey: nil)
        view.isHidden = !show
    }

    class func RGBHex (_ rgbhex : Int) -> UIColor {
        return UIColor(red: CGFloat((rgbhex >> 16) & 0xFF), green: CGFloat((rgbhex >> 8) & 0xFF), blue: CGFloat(rgbhex & 0xFF), alpha: 1.0)
    }

    class func RGB (_ rgb : [CGFloat]) -> UIColor {
        if rgb.count == 3 {
            return UIColor(red: rgb[0]/255.0, green: rgb[1]/255.0, blue: rgb[2]/255.0, alpha: 1.0)
        }
        return UIColor.clear
    }
    
    class func RGBA (_ rgba : [CGFloat]) -> UIColor {
        if rgba.count == 4 {
            return UIColor(red: rgba[0]/255.0, green: rgba[1]/255.0, blue: rgba[2]/255.0, alpha: rgba[3])
        }
        return UIColor.clear
    }
    
    class func AppFont( _ font : CGFloat) -> UIFont {
        return UIFont.systemFont(ofSize: font)
    }
    
    class func BoldFont( _ font : CGFloat) -> UIFont {
        return UIFont.boldSystemFont(ofSize: font)
    }
    
    class func putCommaInt (_ num : Int) -> String {
        return putComma(NSNumber(value: num as Int))
    }
    
    class func putCommaDouble (_ num : Double) -> String {
        return putComma(NSNumber(value: num as Double))
    }
    
    class func putComma (_ num : NSNumber) -> String {
        let nf = NumberFormatter()
        nf.groupingSize = 3
        nf.groupingSeparator = ","
        nf.usesGroupingSeparator = true
        if let szRet = nf.string(from: num) {
            return szRet
        }
        return "0"
    }
    
    class func dayOfWeek(_ date : Date) -> Int {
        var gregorian = Calendar(identifier: Calendar.Identifier.gregorian)
//        gregorian.locale = Locale(identifier: "ko_KR")
        gregorian.timeZone = TimeZone(identifier: "GMT")!
        
        let comps = (gregorian as NSCalendar).components(.weekday, from: date)
        return comps.weekday!
    }
    
    class func dateWithOutTime(_ date : Date) -> Date {
        
        var gregorian = Calendar(identifier: Calendar.Identifier.gregorian)
//        gregorian.locale = Locale(identifier: "ko_KR")
        gregorian.timeZone = TimeZone(identifier: "GMT")!
        
        let comps = (gregorian as NSCalendar).components([NSCalendar.Unit.year, NSCalendar.Unit.month, NSCalendar.Unit.day], from: date)
        return gregorian.date(from: comps)!
    }
    
    class func convertStringToDate(format a_Format : String, Date a_Date : String) -> Date {
        let dfDate = DateFormatter()
        dfDate.dateFormat = a_Format
//        dfDate.locale = Locale(identifier: "ko_KR")
        dfDate.timeZone = TimeZone(identifier: "GMT")!
        return convertStringToDate(format: dfDate, Date: a_Date)
    }
    
    class func convertStringToDate(format a_Format : DateFormatter, Date a_Date : String) -> Date {
        if let aDate = a_Format.date(from: a_Date) {
            return aDate
        }
        return Date()
    }
    
    class func filterNil (_ a_In : AnyObject?) -> String? {
        if let szRet = a_In as? String {
            return szRet
        }
        return ""
    }
    
    class  func showAlert (_ msg : String) {
        let alert = UIAlertView(title: "", message: msg, delegate: nil, cancelButtonTitle: NSLocalizedString("OK", comment: "OK"))
        alert.show()
    }
    
    class func saveConfigFile(_ filepath : String, text : String) {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let fileURL = documentsURL.appendingPathComponent(filepath)
        let path = fileURL.path
        
        let error : NSErrorPointer? = nil
        let bRet: Bool
        do {
            try text.write(toFile: path, atomically: false, encoding: String.Encoding.utf8)
            bRet = true
        } catch let error1 as NSError {
            error??.pointee = error1
            bRet = false
        }
        if !bRet {
            print("saveConfigFile error:\(error)")
        }
        
    }
    
    class func loadConfigFile(_ filepath : String) -> String {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let fileURL = documentsURL.appendingPathComponent(filepath)
        let path = fileURL.path
        
        let error : NSErrorPointer? = nil
        do {
            let szRet = try String(contentsOfFile: path, encoding: String.Encoding.utf8)
            return szRet
        } catch let error1 as NSError {
            error??.pointee = error1
        }
        print("loadConfigFile error:\(error)")
        
        return ""
    }
    
    class func imageToScaledSize(_ a_Image : UIImage, a_NewSize : CGSize) -> UIImage {
        UIGraphicsBeginImageContext(a_NewSize)
        a_Image.draw(in: CGRect(x: 0, y: 0, width: a_NewSize.width, height: a_NewSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    class func resizeImageWithMax(_ a_Image : UIImage, a_Max : CGFloat) -> UIImage {
        var fRatio : CGFloat = 1
        var fW : CGFloat = 0
        var fH : CGFloat = 0
        
        if a_Image.size.width > a_Image.size.height {
            if a_Image.size.width > a_Max {
                fRatio = a_Image.size.width / a_Max
            }
            
        } else {
            if a_Image.size.height > a_Max {
                fRatio = a_Image.size.height / a_Max
            }
        }
        fW = a_Image.size.width / fRatio
        fH = a_Image.size.height / fRatio
        
        return imageToScaledSize(a_Image, a_NewSize: CGSize(width: fW, height: fH))
    }
    
    class func convAnyToStr(_ a_In : AnyObject?) -> String {
        
        var sRet = ""
        
        if a_In == nil {
            sRet = ""
            
        } else {
            if let aRet = a_In as? String {
                sRet = aRet
            } else if let aRet = a_In as? Int {
                sRet = "\(aRet)"
            }
        }
        return sRet
    }
    
    class func urlEncode(_ a_Str : String) -> String {
        return String(CFURLCreateStringByAddingPercentEscapes(
            nil,
            a_Str as CFString!,
            nil,
            "!*'();:@&=+$,/?%#[]" as CFString!,
            CFStringBuiltInEncodings.UTF8.rawValue))
    }
    
    class func getTextSize(_ a_Str : String, a_Font : UIFont, a_Constraint : CGSize) -> CGSize {
        let str = a_Str as NSString
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineBreakMode = NSLineBreakMode.byCharWrapping
        let attr = [NSAttributedString.Key.font:a_Font, NSAttributedString.Key.paragraphStyle:paragraphStyle]
        let rect = str.boundingRect(with: a_Constraint, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes:attr, context:nil)
        return rect.size
    }
    
//    class func geoCodeUsingAddress(_ a_Addr : String) -> CLLocationCoordinate2D {
//        var latitude : Double = 0
//        var longitude : Double = 0
//
//        let esc_addr : String = a_Addr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
//        let szUrl = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=\(esc_addr)"
//        let sendUrl = URL(string: szUrl)!
//        print("geoCodeUsingAddress:\(szUrl)")
//        let myRequest = NSMutableURLRequest(url: sendUrl)
//        //        let error : NSErrorPointer = nil
//        let response: AutoreleasingUnsafeMutablePointer<URLResponse?>? = nil
//        do {
//            let dataRet = try NSURLConnection.sendSynchronousRequest(myRequest as URLRequest, returning: response)
//            //            println("response=\(response)")
//            if let jsonRes = try JSONSerialization.jsonObject(with: dataRet, options: JSONSerialization.ReadingOptions.mutableContainers) as? Dictionary <String, AnyObject> {
//                //                println("jsonRes=\(jsonRes)")
//                if let aStat = jsonRes["status"] as? String {
//                    if aStat == "OK" {
//                        if let aRes = jsonRes["results"] as? Array <AnyObject> {
//                            if let aGeo = aRes[0]["geometry"] as? Dictionary <String, AnyObject> {
//                                if let aLoc = aGeo["location"] as? Dictionary <String, Double> {
//                                    latitude = aLoc["lat"]!
//                                    longitude = aLoc["lng"]!
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        } catch {
//            print(error)
//        }
//
//        print("geoCodeUsingAddress: lat:\(latitude) long:\(longitude)")
//        let center = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
//        return center
//    }
    
    class func getImageData(_ a_Url : String) -> UIImage? {
        var imgDown : UIImage?
        if a_Url.count > 7 {
            if let url = URL(string: a_Url) {
                if let data = try? Data(contentsOf: url){
                    imgDown = UIImage(data: data)
                }
            }
        }
        return imgDown
    }
    
    class func getCurrentMillis() ->Int64 {
        return Int64(Date().timeIntervalSince1970 * 1000)
    }
    
    
}

class Toast {
    static func show(message: String, baseView: UIView) {
        let toastContainer = UIView(frame: CGRect())
        toastContainer.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastContainer.alpha = 0.0
        toastContainer.layer.cornerRadius = 25;
        toastContainer.clipsToBounds  =  true
        
        let toastLabel = UILabel(frame: CGRect())
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font.withSize(12.0)
        toastLabel.text = message
        toastLabel.clipsToBounds  =  true
        toastLabel.numberOfLines = 0
        
        toastContainer.addSubview(toastLabel)
        baseView.addSubview(toastContainer)
        
        toastLabel.translatesAutoresizingMaskIntoConstraints = false
        toastContainer.translatesAutoresizingMaskIntoConstraints = false
        
        let a1 = NSLayoutConstraint(item: toastLabel, attribute: .leading, relatedBy: .equal, toItem: toastContainer, attribute: .leading, multiplier: 1, constant: 15)
        let a2 = NSLayoutConstraint(item: toastLabel, attribute: .trailing, relatedBy: .equal, toItem: toastContainer, attribute: .trailing, multiplier: 1, constant: -15)
        let a3 = NSLayoutConstraint(item: toastLabel, attribute: .bottom, relatedBy: .equal, toItem: toastContainer, attribute: .bottom, multiplier: 1, constant: -15)
        let a4 = NSLayoutConstraint(item: toastLabel, attribute: .top, relatedBy: .equal, toItem: toastContainer, attribute: .top, multiplier: 1, constant: 15)
        toastContainer.addConstraints([a1, a2, a3, a4])
        
        let c1 = NSLayoutConstraint(item: toastContainer, attribute: .leading, relatedBy: .equal, toItem: baseView, attribute: .leading, multiplier: 1, constant: 65)
        let c2 = NSLayoutConstraint(item: toastContainer, attribute: .trailing, relatedBy: .equal, toItem: baseView, attribute: .trailing, multiplier: 1, constant: -65)
        let c3 = NSLayoutConstraint(item: toastContainer, attribute: .bottom, relatedBy: .equal, toItem: baseView, attribute: .bottom, multiplier: 1, constant: -75)
        baseView.addConstraints([c1, c2, c3])
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseIn, animations: {
            toastContainer.alpha = 1.0
        }, completion: { _ in
            UIView.animate(withDuration: 0.5, delay: 1.5, options: .curveEaseOut, animations: {
                toastContainer.alpha = 0.0
            }, completion: {_ in
                toastContainer.removeFromSuperview()
            })
        })
    }
}
