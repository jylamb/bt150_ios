//
//  PBBarView.h
//  sagapp2
//
//  Created by Yang JungYun on 13. 6. 12..
//  Copyright (c) 2013년 sag. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PBBarView : UIView
{
    float fPercent;
    UIColor *clBar;
}

@property (nonatomic, readwrite) float fPercent;
@property (nonatomic, strong) UIColor *clBar;

@end
