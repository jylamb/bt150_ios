//
//  PBLibOC.h
//  kbc_cha_ios
//
//  Created by lamb on 2014. 11. 13..
//  Copyright (c) 2014년 pointbank. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PBTextField.h"
#import <QuartzCore/QuartzCore.h>
//#import "Define.h"
//#import "GlobalHead.h"

#define kFlatButtonLeft     100
#define kFlatButtonRight    200
#define kFlatButtonCenter   300

#define AppFont(pt) [UIFont systemFontOfSize:pt]
#define BoldFont(pt) [UIFont boldSystemFontOfSize:pt]

#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
#define RGBA(r, g, b, a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]
#define RGBTintColor() [UIColor colorWithRed:kGrayTintRed/255.0 green:kGrayTintGreen/255.0 blue:kGrayTintBlue/255.0 alpha:1]

#define kDefThemeColor      RGB(0xe6, 0x62, 0x72)
#define kDefBlack           RGB(66, 61, 57)
#define kDefWhite           RGB(0xff, 0xff, 0xff)
#define kDefDarkGray        RGB(80, 80, 80)
#define kDefLightGray       RGB(157, 157, 157)
//#define kDefBackgroundGray  RGB(240, 240, 240)
#define kDefBackgroundGray  RGB(0xf2, 0xef, 0xe8)
#define kDefGreen           RGB(104, 159, 56)
#define kDefRed             RGB(0xf3, 0, 0)
#define kDefBlue            RGB(0, 0, 0xc4)
#define kDefLightRed        RGB(231, 49, 62)
#define kDefLightBlue       RGB(38, 144, 232)
#define kMainMenuBG         RGB(0xff, 0xc0, 0x00)
#define kMainMenuItemBG     RGB(0xff, 0xae, 0x00)
#define kMainMenuItemBGOn   RGB(0xf2, 0xa5, 0x00)
#define kMenuBackgroudGray  RGB(0xfa, 0xfa, 0xfa)
#define kMenuLineGray       RGB(0xe7, 0xe2, 0xda)
#define kMenuLineGrayDark   RGB(0xb2, 0xac, 0xa1)
#define kMenuLineGrayLight  RGB(0xe0, 0xe0, 0xe0)
#define kCancelGray         RGB(0xe9, 0xe9, 0xe9)
#define kConfirmGray        RGB(0x72, 0x69, 0x5c)

#define kTextBlack          RGB(0x1b, 0x1b, 0x1b)
//#define kTextGray           RGB(0x86, 0x85, 0x81)
#define kTextGray           RGB(88, 88, 88)
#define kTextYellow         RGB(0xff, 0xae, 0x00)

#define menulinegraylight   RGB(0xe0, 0xe0, 0xe0)

#define kNavBarHeight           44
#define kNavBarH                kNavBarHeight


#define EXP_PRESSURE_MIN        0
#define EXP_PRESSURE_MAX        15

#define MSG_PRESSURE_MIN        0
#define MSG_PRESSURE_MAX        15

#define EXP_CYCLE_MIN           0
#define EXP_CYCLE_MAX           5

#define MSG_CYCLE_MIN           0
#define MSG_CYCLE_MAX           2


@interface PBLibOC : NSObject

+(NSString*)UUID;
+(BOOL)validateEmail:(NSString*)candidate;
+(UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize;
+(NSDate *)dateWithOutTime:(NSDate *)datDate;
+(NSDate *)get1stDayOfYear:(NSDate *)datDate;
+(NSDate *)get1stDayOfMonth:(NSDate *)datDate;
+(NSDate *)getMonthDiff:(int)a_Month FromDate:(NSDate*)a_FromDate;
+(BOOL)isNSNull:(id)a_In;
+(NSString*)filterNSNull:(NSString*)a_In;
+(id)objectNSNull:(id)a_In;
+(int)convertToInt:(id)a_Int;
+(NSString*)putComma:(long)num;
+(NSString*)putCommaString:(NSString*)a_Value;
+(UIColor *)colorFromHexString:(NSString *)hexString;
+(UIView*)buildFlatColorView:(UIColor*)a_Base Line:(UIColor*)a_Line Radius:(float)a_R Thick:(float)a_Thick;
+(UILabel*)buildFlatColorLabel:(UIColor*)a_Base;
+(UILabel*)buildFlatColorLabel:(UIColor*)a_Base TitleColor:(UIColor*)a_Title;
+(UIButton*)buildFlatColorButton:(UIColor*)a_Base;
+(UIButton*)buildFlatColorButton:(UIColor*)a_Base BorderColor:(UIColor*)a_Border;
+(UIButton*)buildFlatColorButton:(UIColor*)a_Base BorderColor:(UIColor*)a_Border Radius:(float)a_R;
+(UIButton*)buildFlatColorButton:(UIColor*)a_Base BorderColor:(UIColor*)a_Border Radius:(float)a_R Thick:(float)a_Thick;
//+(UIButton*)buildFlatColorButton:(UIColor*)a_Base Icon:(UIImage*)a_Icon;
+(PBTextField*)buildFlatColorEdit:(UIColor*)a_Text;
+(PBTextField*)buildFlatColorEdit:(UIColor*)a_Text BackColor:(UIColor*)a_Back BorderColor:(UIColor*)a_Border Radius:(float)a_R Thick:(float)a_Thick;
+(void)performTransition:(BOOL)a_Show andView:(UIView*)a_View;
+(void)performTransition:(BOOL)a_Show andView:(UIView*)a_View andType:(NSString*)a_Type subType:(NSString*)a_SubType;
+(void)performTransition:(BOOL)a_Show andView:(UIView*)a_View andType:(NSString*)a_Type subType:(NSString*)a_SubType Time:(double)a_Time;
+(void)showAlertMsg:(NSString*)a_Msg;
+(void)AlertMsgMainThread:(NSString*)a_Msg;
+(void)setNavBackTitleReset:(UINavigationItem*)a_NavItem;
+(void)setNavBackTitleReset:(UINavigationItem*)a_NavItem Title:(NSString*)a_Title;
+(void)drawColorLine:(UIView*)a_View Color:(UIColor*)a_Color From:(CGPoint)a_From To:(CGPoint)a_To;
+(void)drawColorLine:(UIView*)a_View Color:(UIColor*)a_Color Thick:(CGFloat)a_Thick From:(CGPoint)a_From To:(CGPoint)a_To;
+(UIImage *)imageWithColor:(UIColor *)color;
+(NSString*)makeDateString:(NSString*)a_Date;
+(NSString*)makeDateString:(NSString*)a_Date Deli:(NSString*)a_Deli;
+(BOOL)isPointInRect:(CGPoint)a_Point Rect:(CGRect)a_Rect;
+(BOOL)containsString:(NSString*)a_Source Find:(NSString*)a_Find;
+(NSMutableURLRequest*)getTokenRequest:(NSString*)a_Url Dictionary:(NSDictionary*)a_Param;
+(NSMutableURLRequest*)getTokenRequest:(NSString*)a_Url Dictionary:(NSDictionary*)a_Param UseGate:(BOOL)a_UseGate;
+(NSMutableURLRequest*)getTokenRequest:(NSString*)a_Url Param:(NSString*)a_Param;
+(NSMutableURLRequest*)getTokenRequest:(NSString*)a_Url Param:(NSString*)a_Param UseGate:(BOOL)a_UseGate;
+(NSMutableURLRequest*)getTokenRequest:(NSString*)a_Url;
+(NSMutableURLRequest*)getTokenRequest:(NSString*)a_Url UseGate:(BOOL)a_UseGate;
+(void)setImageView:(UIImageView*)a_ImageView Anim:(CGFloat)a_Time;
+(NSString*)convertUrlString:(NSString*)a_Url;
+(void)doAfterTime:(double)a_Delay doBlock:(void(^)(void))doBlock;
+(BOOL)isAppInstalled:(NSString*)a_AppScheme;
+(NSString*)getAppInfo:(NSString*)a_Type;
+(NSString*)getLocaleLanguage;
@end
