//
//  PBOCPicker.m
//  okedongmu
//
//  Created by lamb on 2015. 3. 5..
//  Copyright (c) 2015년 pointbank. All rights reserved.
//

#import "PBOCPicker.h"
#import "PBLibOC.h"

@implementation PBOCPicker

@synthesize delegate;
@synthesize nTag;

-(id)init
{
    self = [super init];
    if (self) {
        [self initData];
    }
    return self;
}

-(void)initData
{
    nTag = 0;
    fontText = AppFont(14);
    alignText = NSTextAlignmentCenter;
}

-(void)initPBOCPicker:(UIView*)a_View Data:(NSArray*)a_Data
{
    [self initPBOCPicker:a_View Font:AppFont(18) TextAlign:NSTextAlignmentCenter Data:a_Data];
}

-(void)initPBOCPicker:(UIView*)a_View Font:(UIFont*)a_Font TextAlign:(NSTextAlignment)a_Align Data:(NSArray*)a_Data
{
    arrData = a_Data;
//    NSLog(@"initPBOCPicker:%@", arrData);
    
    vRoot = a_View;
    fontText = a_Font;
    alignText = a_Align;
    
//    [self showPicker];
    [self initPicker];
    
    [pvOption selectRow:0 inComponent:0 animated:NO];
}

-(void)initPickerLayout
{
    id views = @{@"optionpicker": pvOption, @"toolbar": pickerToolbar, @"vbase": vBase};
    id metrics = @{@"vspace": @0, @"hspace": @0, @"toolbarh": @44, @"pickerh": @200, @"baseh": @244};

    NSArray *consToolbarV = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[toolbar(toolbarh)]"
                                                                               options:0
                                                                               metrics:metrics
                                                                                 views:views];
    [pickerToolbar addConstraints:consToolbarV];

    NSArray *consPickerV = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[optionpicker(pickerh)]"
                                                                    options:0
                                                                    metrics:metrics
                                                                      views:views];
    [pvOption addConstraints:consPickerV];
    
    
    NSArray *consBaseV = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[vbase(baseh)]"
                                                                   options:0
                                                                   metrics:metrics
                                                                     views:views];
    [vBase addConstraints:consBaseV];
    
    NSArray *consPickerX = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-hspace-[optionpicker]-hspace-|"
                                                                 options:0
                                                                 metrics:metrics
                                                                   views:views];
    NSArray *consToolbarX = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-hspace-[toolbar]-hspace-|"
                                                                   options:0
                                                                   metrics:metrics
                                                                     views:views];
    NSArray *consY = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-vspace-[toolbar]-[optionpicker]-vspace-|"
                                                                   options:0
                                                                   metrics:metrics
                                                                     views:views];
    
    [vBase addConstraints:consPickerX];
    [vBase addConstraints:consToolbarX];
    [vBase addConstraints:consY];
    
    
    NSArray *consBaseX = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-hspace-[vbase]-hspace-|"
                                                                   options:0
                                                                   metrics:metrics
                                                                     views:views];
    NSArray *consBaseY = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[vbase]-vspace-|"
                                                                    options:0
                                                                    metrics:metrics
                                                                      views:views];
    
    [vRoot addConstraints:consBaseX];
    [vRoot addConstraints:consBaseY];
}

-(void)showPicker
{
    [PBLibOC performTransition:YES andView:vBase andType:kCATransitionMoveIn subType:kCATransitionFromTop];
}

-(void)initPicker
{
    CGSize sizeView = vRoot.frame.size;
    NSLog(@"initPicker:%.0f %.0f", sizeView.width, sizeView.height);
    
    if (vBase == nil) {
        vBase = [[UIView alloc] initWithFrame:CGRectMake(0, sizeView.height-244, sizeView.width, 244)];
        vBase.backgroundColor = [UIColor whiteColor];
    }
    vBase.translatesAutoresizingMaskIntoConstraints = NO;
    
    //Add Picker
    //UIPickerView *pickerObj;
    if (pvOption == nil) {
        pvOption = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 44, sizeView.width, 244-44)];
    }
    pvOption.backgroundColor = [UIColor whiteColor];
    pvOption.dataSource = self;
    pvOption.delegate = self;
    pvOption.showsSelectionIndicator = YES;
    pvOption.translatesAutoresizingMaskIntoConstraints = NO;
    
    pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, sizeView.width, 44)];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
        pickerToolbar.backgroundColor = [UIColor whiteColor];
    } else {
        pickerToolbar.barStyle = UIBarStyleBlackOpaque;
    }
//    [pickerToolbar sizeToFit];
    pickerToolbar.translatesAutoresizingMaskIntoConstraints = NO;
    
    
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                               target:self
                                                                               action:nil];
    UIBarButtonItem *mrgSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                                                                    target:nil
                                                                                    action:nil];
    mrgSpace.width = 10;
    
    UIBarButtonItem *closeBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                                              target:self
                                                                              action:@selector(cancelPicker)];
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                             target:self
                                                                             action:@selector(closePicker)];
    
    [barItems addObject:mrgSpace];
    [barItems addObject:closeBtn];
    [barItems addObject:flexSpace];
    [barItems addObject:doneBtn];
    [barItems addObject:mrgSpace];
    
    [pickerToolbar setItems:barItems animated:YES];
    
    [vBase addSubview:pickerToolbar];
    [vBase addSubview:pvOption];
    [vRoot addSubview:vBase];
    
    [self initPickerLayout];
    
}

-(void)cancelPicker
{
    [delegate onPBOCPickerClose:nil Tag:nTag];
    [PBLibOC performTransition:NO andView:vBase];
//    [vBase removeFromSuperview];
}

-(void)closePicker
{
    long nCol1 = 0;
    
    nCol1 = [pvOption selectedRowInComponent:0];
    if (nCol1 < [arrData count]) {
        NSDictionary *dicItem = [arrData objectAtIndex:nCol1];
        [delegate onPBOCPickerClose:dicItem Tag:nTag];
    }
    
    [PBLibOC performTransition:NO andView:vBase];
}

#pragma mark -
#pragma mark UIPickerView DataSource
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [arrData count];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component;
{
    return kNavBarH;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *pickerLabel = (UILabel *)view;
    int nWL = 260;
    
    if (pickerLabel == nil) {
        pickerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, nWL, kNavBarH/2)];
        [pickerLabel setTextAlignment:NSTextAlignmentCenter];
        [pickerLabel setBackgroundColor:[UIColor clearColor]];
        [pickerLabel setFont:AppFont(16)];
    }
    
    long aRow = [pickerView selectedRowInComponent:component];
    if (row == aRow) {
        pickerLabel.textColor = kDefThemeColor;
    }

    [pickerLabel setText:[[arrData objectAtIndex:row] objectForKey:@"codename"]];
    
//    NSLog(@"viewForRow:%@", pickerLabel.text);
    
    return pickerLabel;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component
{
    [pvOption reloadAllComponents];
}

@end
