//
//  PBPicker.h
//  okedongmu
//
//  Created by lamb on 2015. 3. 5..
//  Copyright (c) 2015년 pointbank. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PBOCPickerDelegate <NSObject>

-(void)onPBOCPickerClose:(NSDictionary*)a_Param Tag:(int)a_Tag;

@end

@interface PBOCPicker : NSObject <UIPickerViewDataSource, UIPickerViewDelegate>
{
    id<PBOCPickerDelegate> delegate;
    
    UIPickerView *pvOption;
    UIToolbar *pickerToolbar;
    UIView *vBase;
    UIView *vRoot;
    
    NSTextAlignment alignText;
    UIFont *fontText;
    
    NSArray *arrData;
    long nTag;
}

@property (strong, nonatomic) id<PBOCPickerDelegate> delegate;
@property (readwrite, nonatomic) long nTag;

-(void)initPBOCPicker:(UIView*)a_View Data:(NSArray*)a_Data;
-(void)initPBOCPicker:(UIView*)a_View Font:(UIFont*)a_Font TextAlign:(NSTextAlignment)a_Align Data:(NSArray*)a_Data;
-(void)cancelPicker;
-(void)showPicker;

@end
