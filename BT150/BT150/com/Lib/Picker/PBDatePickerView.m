//
//  PBDatePickerView.m
//  mcarman5
//
//  Created by lamb on 2014. 12. 22..
//  Copyright (c) 2014년 pointbank. All rights reserved.
//

#import "PBDatePickerView.h"
#import "PBLibOC.h"

@implementation PBDatePickerView

@synthesize delegate;
@synthesize nDateTag;

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, kNavBarHeight)];
        pickerToolbar.backgroundColor = [UIColor whiteColor];
//        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
//            pickerToolbar.backgroundColor = [UIColor whiteColor];
//        } else {
//            pickerToolbar.barStyle = UIBarStyleBlackOpaque;
//        }
        [pickerToolbar sizeToFit];

        NSMutableArray *barItems = [[NSMutableArray alloc] init];
        UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                   target:self
                                                                                   action:nil];
        UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                                 target:self
                                                                                 action:@selector(closePicker)];
        UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                                                 target:self
                                                                                 action:@selector(canclePicker)];

        [barItems addObject:cancelBtn];
        [barItems addObject:flexSpace];
        [barItems addObject:doneBtn];
        [pickerToolbar setItems:barItems animated:YES];
        [self addSubview:pickerToolbar];
        
        dpDate = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, kNavBarHeight, frame.size.width, frame.size.height-kNavBarHeight)];
        dpDate.datePickerMode = UIDatePickerModeDateAndTime;
        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"ko"];
        dpDate.locale = locale;
        dpDate.calendar = [locale objectForKey:NSLocaleCalendar];
        [dpDate setDate:[NSDate date]];
        [dpDate addTarget:self action:@selector(pickerChanged:) forControlEvents:UIControlEventValueChanged];
        [self addSubview:dpDate];


    }
    return self;
}

-(void)setDateValue:(NSDate*)a_Date Tag:(int)a_Tag
{
    nDateTag = a_Tag;
    dpDate.date = a_Date;
}

-(void)canclePicker
{
    [delegate onCloseDatePicker:dpDate.date Tag:-100];
    [PBLibOC performTransition:NO andView:self];
}

-(void)closePicker
{
    [delegate onCloseDatePicker:dpDate.date Tag:nDateTag];
    [PBLibOC performTransition:NO andView:self];
}

- (void)pickerChanged:(id)sender
{
//    NSLog(@"value: %@",[sender date]);    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
