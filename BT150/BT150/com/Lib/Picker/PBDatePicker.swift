//
//  PBDatePicker.swift
//  cmc
//
//  Created by lamb on 2015. 5. 4..
//  Copyright (c) 2015년 CMC. All rights reserved.
//

import UIKit

protocol PBDatePickerDelegate {
    func onCloseDatePicker(_ date : Date, tag : Int)
}

class PBDatePicker: NSObject {
    
    var pickerToolbar = UIToolbar()
    var dpDate = UIDatePicker()
    var nDateTag = 0
    var fMaxDate = false
    var delegate : PBDatePickerDelegate?
    var rootview : UIView!
    var vBase = UIView()
    
    override init() {
        
    }

    func initPBDatePicker (_ view : UIView) {
        initPBDatePicker(view, pickerType: UIDatePicker.Mode.date, a_Date: Date())
    }
    
    func initPBDatePicker (_ view : UIView, pickerType : UIDatePicker.Mode, a_Date : Date) {
//        print("initPBDatePicker:\(a_Date)")
        
        rootview = view
        
        let mrgSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.fixedSpace, target: self, action: nil)
        mrgSpace.width = 10
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: self, action: nil)
        let doneBtn  = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.done, target: self, action: #selector(PBDatePicker.closePicker))
        let cancelBtn  = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.cancel, target: self, action: #selector(PBDatePicker.cancelPicker))
        pickerToolbar.setItems([mrgSpace, cancelBtn, flexSpace, doneBtn, mrgSpace], animated: true)
        pickerToolbar.translatesAutoresizingMaskIntoConstraints = false
        
//        pickerToolbar.frame = CGRect(x: 0, y: 0, width: vBase.frame.width, height: 44)
//        pickerToolbar.translatesAutoresizingMaskIntoConstraints = false
//        pickerToolbar.setItems([flexSpace, doneBtn], animated: true)
        
        vBase.backgroundColor = UIColor.white
        vBase.translatesAutoresizingMaskIntoConstraints = false

        var gregorian = Calendar(identifier: Calendar.Identifier.gregorian)
//        gregorian.locale = Locale(identifier: "ko_KR")
//        gregorian.timeZone = TimeZone(identifier: "GMT")!
        
//        let locale = Locale(identifier: "ko_KR")
        
        var components : DateComponents = DateComponents()
        (components as NSDateComponents).calendar = gregorian
        
        components.year = -90
        let minDate : Date = ((gregorian as NSCalendar?)?.date(byAdding: components, to: a_Date, options: NSCalendar.Options(rawValue:0)))!
        
        components.year = 50
        let maxDate : Date = ((gregorian as NSCalendar?)?.date(byAdding: components, to: a_Date, options: NSCalendar.Options(rawValue:0)))!
        
        dpDate.translatesAutoresizingMaskIntoConstraints = false
        dpDate.datePickerMode = pickerType
        if #available(iOS 14, *) {
            dpDate.preferredDatePickerStyle = .compact
        }
//        dpDate.locale = locale
//        dpDate.timeZone = TimeZone(identifier: "GMT")
//        dpDate.calendar = locale.objectForKey(NSLocaleCalendar) as! NSCalendar
        dpDate.calendar = gregorian
        dpDate.minimumDate = minDate
        if fMaxDate {
            dpDate.maximumDate = maxDate
        } else {
            dpDate.maximumDate = a_Date
        }
        dpDate.addTarget(self, action: #selector(PBDatePicker.pickerChanged(_:)), for: UIControl.Event.touchUpInside)
    
        vBase.addSubview(pickerToolbar)
        vBase.addSubview(dpDate)
        rootview.addSubview(vBase)
        
        initPickerLayout()
        
        showPicker()
    }
    
    func initPickerLayout() {
        
        let metrics = ["vspace": 0, "hspace" : 0, "toolbarh" : 44, "pickerh" : 200, "baseh" : 244]
        let views = ["optionpicker" : dpDate, "toolbar": pickerToolbar, "vbase" : vBase]
        
        let consToolbarV = NSLayoutConstraint.constraints(withVisualFormat: "V:[toolbar(toolbarh)]", options: [], metrics: metrics, views: views)
        pickerToolbar.addConstraints(consToolbarV)
        
        let consPickerV = NSLayoutConstraint.constraints(withVisualFormat: "V:[optionpicker(pickerh)]", options: [], metrics: metrics, views: views)
        dpDate.addConstraints(consPickerV)
        
        let consBaseV = NSLayoutConstraint.constraints(withVisualFormat: "V:[vbase(baseh)]", options: [], metrics: metrics, views: views)
        vBase.addConstraints(consBaseV)
        
        let consPickerX = NSLayoutConstraint.constraints(withVisualFormat: "H:|-hspace-[optionpicker]-hspace-|",
            options: [],
            metrics: metrics,
            views: views)
        let consToolbarX = NSLayoutConstraint.constraints(withVisualFormat: "H:|-hspace-[toolbar]-hspace-|",
            options: [],
            metrics: metrics,
            views: views)
        let consY = NSLayoutConstraint.constraints(withVisualFormat: "V:|-vspace-[toolbar]-[optionpicker]-vspace-|",
            options: [],
            metrics: metrics,
            views: views)
        
        vBase.addConstraints(consPickerX)
        vBase.addConstraints(consToolbarX)
        vBase.addConstraints(consY)
        
        let consBaseX = NSLayoutConstraint.constraints(withVisualFormat: "H:|-hspace-[vbase]-hspace-|",
            options: [],
            metrics: metrics,
            views: views)
        let consBaseY = NSLayoutConstraint.constraints(withVisualFormat: "V:[vbase]-vspace-|",
            options: [],
            metrics: metrics,
            views: views)
        
        rootview.addConstraints(consBaseX)
        rootview.addConstraints(consBaseY)
    }
    
//    func initPickerLayout() {
//        let metrics = ["vspace": 0, "hspace" : 0, "toolbarh" : 44, "pickerh" : 200]
//        let views = ["datepicker" : dpDate, "toolbar": pickerToolbar]
//        
//        let consToolbarV = NSLayoutConstraint.constraintsWithVisualFormat("V:[toolbar(toolbarh)]", options: [], metrics: metrics, views: views)
//        pickerToolbar.addConstraints(consToolbarV)
//        
//        let consPickerV = NSLayoutConstraint.constraintsWithVisualFormat("V:[datepicker(pickerh)]", options: [], metrics: metrics, views: views)
//        dpDate.addConstraints(consPickerV)
//        
//        let consPickerX = NSLayoutConstraint.constraintsWithVisualFormat("H:|-hspace-[datepicker]-hspace-|",
//            options: [],
//            metrics: metrics,
//            views: views)
//        let consToolbarX = NSLayoutConstraint.constraintsWithVisualFormat("H:|-hspace-[toolbar]-hspace-|",
//            options: [],
//            metrics: metrics,
//            views: views)
//        let consY = NSLayoutConstraint.constraintsWithVisualFormat("V:|-vspace-[toolbar]-[datepicker]-vspace-|",
//            options: [],
//            metrics: metrics,
//            views: views)
//        
//        self.addConstraints(consPickerX)
//        self.addConstraints(consToolbarX)
//        self.addConstraints(consY)
//    }
    
    func setDateValue (_ a_Date : Date, a_Tag : Int) {
        print("setDateValue:\(a_Date)")
        nDateTag = a_Tag
        dpDate.date = a_Date
    }
    
    func showPicker () {
        
        PBLib.performTransition(true, view: vBase, type: CATransitionType.moveIn.rawValue, subtype: CATransitionSubtype.fromTop.rawValue)
        
//        option.selectRow(0, inComponent: 0, animated: false)
    }
    
    @objc func cancelPicker() {
        delegate?.onCloseDatePicker(dpDate.date, tag: -100)
        PBLib.performTransition(false, view: vBase)
    }
    
    @objc func closePicker () {
        delegate?.onCloseDatePicker(dpDate.date, tag: nDateTag)
        PBLib.performTransition(false, view: vBase)
    }
    
    @objc func pickerChanged(_ datepicker : UIDatePicker) {
        
    }
}


