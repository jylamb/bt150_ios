//
//  PBDatePickerView.h
//  mcarman5
//
//  Created by lamb on 2014. 12. 22..
//  Copyright (c) 2014년 pointbank. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PBDatePickerViewDelegate <NSObject>

-(void)onCloseDatePicker:(NSDate*)a_Date Tag:(int)a_Tag;

@end

@interface PBDatePickerView : UIView
{
    id<PBDatePickerViewDelegate> delegate;
    
    UIDatePicker *dpDate;
	UIToolbar *pickerToolbar;
    int nDateTag;
}

@property (strong, nonatomic) id<PBDatePickerViewDelegate> delegate;
@property (readwrite, nonatomic) int nDateTag;

-(void)setDateValue:(NSDate*)a_Date Tag:(int)a_Tag;
-(void)canclePicker;

@end
