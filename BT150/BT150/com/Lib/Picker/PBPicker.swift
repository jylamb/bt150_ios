//
//  PBPicker.swift
//  cmc
//
//  Created by lamb on 2015. 5. 13..
//  Copyright (c) 2015년 CMC. All rights reserved.
//

import UIKit

struct listdata {
    var code = ""
    var codename = ""
}

protocol PBPickerDelegate {
    func onPBPickerClose(_ param : listdata, tag : Int)
}

class PBPicker: NSObject, UIPickerViewDataSource, UIPickerViewDelegate {
    var delegate : PBPickerDelegate?
    
    var option = UIPickerView()
    var toolbar = UIToolbar()
    var nTag = 0
    var rootview : UIView!
    var vBase = UIView()
    var fontText = PBLib.AppFont(18)
    var alignText = NSTextAlignment.center
    
    var arrData = [listdata]()

    override init() {
        
    }
    
    func appDelegate() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func initPBPicker(_ view : UIView, data : [listdata]) {
        initPBPicker(view, font: fontText, align: alignText, data: data)
    }
    
    func initPBPicker(_ view : UIView, font : UIFont, align : NSTextAlignment, data : [listdata]) {
        rootview = view
        fontText = font
        alignText = align
        arrData = data
        
        
        let mrgSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.fixedSpace, target: self, action: nil)
        mrgSpace.width = 10
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: self, action: nil)
        let doneBtn  = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.done, target: self, action: #selector(PBPicker.closePicker))
        let cancelBtn  = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.cancel, target: self, action: #selector(PBPicker.cancelPicker))
        toolbar.setItems([mrgSpace, cancelBtn, flexSpace, doneBtn, mrgSpace], animated: true)
        toolbar.translatesAutoresizingMaskIntoConstraints = false
        
        vBase.backgroundColor = UIColor.white
        vBase.translatesAutoresizingMaskIntoConstraints = false
        
//        var sizeView = rootview.frame.size
//        option.frame = CGRect(x: 0, y: 44, width: sizeView.width, height: 200)
        option.backgroundColor = UIColor.clear
        option.dataSource = self
        option.delegate = self
        option.showsSelectionIndicator = true
        option.translatesAutoresizingMaskIntoConstraints = false
        
        vBase.addSubview(toolbar)
        vBase.addSubview(option)
        rootview.addSubview(vBase)
        
        initPickerLayout()

        showPicker()
    }
    
    func initPickerLayout() {
        
        let metrics = ["vspace": 0, "hspace" : 0, "toolbarh" : 44, "pickerh" : 200, "baseh" : 244]
        let views = ["optionpicker" : option, "toolbar": toolbar, "vbase" : vBase]
        
        let consToolbarV = NSLayoutConstraint.constraints(withVisualFormat: "V:[toolbar(toolbarh)]", options: [], metrics: metrics, views: views)
        toolbar.addConstraints(consToolbarV)
        
        let consPickerV = NSLayoutConstraint.constraints(withVisualFormat: "V:[optionpicker(pickerh)]", options: [], metrics: metrics, views: views)
        option.addConstraints(consPickerV)

        let consBaseV = NSLayoutConstraint.constraints(withVisualFormat: "V:[vbase(baseh)]", options: [], metrics: metrics, views: views)
        vBase.addConstraints(consBaseV)
        
        let consPickerX = NSLayoutConstraint.constraints(withVisualFormat: "H:|-hspace-[optionpicker]-hspace-|",
            options: [],
            metrics: metrics,
            views: views)
        let consToolbarX = NSLayoutConstraint.constraints(withVisualFormat: "H:|-hspace-[toolbar]-hspace-|",
            options: [],
            metrics: metrics,
            views: views)
        let consY = NSLayoutConstraint.constraints(withVisualFormat: "V:|-vspace-[toolbar]-[optionpicker]-vspace-|",
            options: [],
            metrics: metrics,
            views: views)
        
        vBase.addConstraints(consPickerX)
        vBase.addConstraints(consToolbarX)
        vBase.addConstraints(consY)
        
        let consBaseX = NSLayoutConstraint.constraints(withVisualFormat: "H:|-hspace-[vbase]-hspace-|",
            options: [],
            metrics: metrics,
            views: views)
        let consBaseY = NSLayoutConstraint.constraints(withVisualFormat: "V:[vbase]-vspace-|",
            options: [],
            metrics: metrics,
            views: views)
        
        rootview.addConstraints(consBaseX)
        rootview.addConstraints(consBaseY)
    }
    
    func showPicker () {
        
        PBLib.performTransition(true, view: vBase, type: CATransitionType.moveIn.rawValue, subtype: CATransitionSubtype.fromTop.rawValue)

        option.selectRow(0, inComponent: 0, animated: false)
    }
    
    @objc func cancelPicker() {
        PBLib.performTransition(false, view: vBase)
    }
    
    @objc func closePicker() {
        let nCol1 = option.selectedRow(inComponent: 0)
        if nCol1 < arrData.count {
            let item = arrData[nCol1]
            delegate?.onPBPickerClose(item, tag: nTag)
        }
        PBLib.performTransition(false, view: vBase)
    }
    
    //MARK: - UIPickerViewDelegate
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrData.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 44
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        var lblPickerTitle : UILabel
        if view == nil {
            lblPickerTitle = UILabel()
        } else {
            lblPickerTitle = view as! UILabel
        }
        lblPickerTitle.backgroundColor = UIColor.clear
        lblPickerTitle.textAlignment = alignText
        lblPickerTitle.font = fontText
        lblPickerTitle.text = arrData[row].codename
        return lblPickerTitle
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
    }
    
}
