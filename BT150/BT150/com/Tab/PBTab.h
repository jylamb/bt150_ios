//
//  PBTab.h
//  mcarman4
//
//  Created by lamb on 2014. 11. 19..
//  Copyright (c) 2014년 pointbank. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PBTabDelegate <NSObject>

-(void)onTabClick:(long)a_Idx;

@end

@interface PBTab : UIView
{
    id<PBTabDelegate> delegate;

    UILabel *lblTitle;
    UIButton *btnTouch;
    UIView *vSelect;
    
    NSString *szTitle;
    UIFont *fontTitle;
    UIColor *colorTitle;
    UIColor *colorTitleSelect;
    UIColor *colorBack;
    UIColor *colorSelect;
    
    int nSelectH;
}

@property (strong, nonatomic) id<PBTabDelegate> delegate;

-(void)initPBTabTitle:(NSString*)a_Title andFont:(UIFont*)a_Font andColor:(UIColor*)a_Color;
-(void)initPBTabColor:(UIColor*)a_Back andSelect:(UIColor*)a_Select;
-(void)selectPBTab:(BOOL)a_Select;

@end
