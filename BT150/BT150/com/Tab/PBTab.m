//
//  PBTab.m
//  mcarman4
//
//  Created by lamb on 2014. 11. 19..
//  Copyright (c) 2014년 pointbank. All rights reserved.
//

#import "PBTab.h"
#import "PBLibOC.h"

@implementation PBTab


@synthesize delegate;

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initPBTab];
    }
    return self;
}

-(void)initPBTab
{
    nSelectH = 5;
    colorTitleSelect = [UIColor whiteColor];
    colorTitle = RGB(0x77, 0x77, 0x77);
    colorBack = [UIColor darkGrayColor];
    colorSelect = [UIColor whiteColor];
    fontTitle = [UIFont systemFontOfSize:12];
    szTitle = @"";
    
    lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    lblTitle.backgroundColor = [UIColor clearColor];
    lblTitle.font = fontTitle;
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.textColor = colorTitle;
    [self addSubview:lblTitle];
    
    self.backgroundColor = colorBack;
    vSelect = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height-nSelectH, self.frame.size.width, nSelectH)];
    vSelect.backgroundColor = colorBack;
    [self addSubview:vSelect];
    
    btnTouch = [UIButton buttonWithType:UIButtonTypeCustom];
    btnTouch.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    btnTouch.backgroundColor = [UIColor clearColor];
    [btnTouch addTarget:self action:@selector(onTouch) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btnTouch];
}

-(void)initPBTabTitle:(NSString*)a_Title andFont:(UIFont*)a_Font andColor:(UIColor*)a_Color
{
    szTitle = a_Title;
    fontTitle = a_Font;
    colorTitleSelect = a_Color;
    
    lblTitle.text = a_Title;
    lblTitle.font = fontTitle;
    lblTitle.textColor = colorTitle;
    
//    [self setNeedsDisplay];
}

-(void)initPBTabColor:(UIColor*)a_Back andSelect:(UIColor*)a_Select
{
    colorBack = a_Back;
    colorSelect = a_Select;
    
    self.backgroundColor = colorBack;
    vSelect.backgroundColor = colorBack;
    
//    [self setNeedsDisplay];
}

-(void)selectPBTab:(BOOL)a_Select
{
    if (a_Select) {
        vSelect.backgroundColor = colorSelect;
        lblTitle.textColor = colorTitleSelect;
    } else {
        vSelect.backgroundColor = colorBack;
        lblTitle.textColor = colorTitle;        
    }
//    [self setNeedsDisplay];
}

-(void)onTouch
{
    [self selectPBTab:YES];
    
    [delegate onTabClick:self.tag];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
