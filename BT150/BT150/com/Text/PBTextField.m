//
//  PBTextField.m
//  mievpad
//
//  Created by Yang JungYun on 13. 2. 28..
//  Copyright (c) 2013년 miev. All rights reserved.
//

#import "PBTextField.h"

@implementation PBTextField

@synthesize placeholderColor;

@synthesize nLeft, nTop, nRight, nBottom;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setPlaceholder:@""];
        [self setPlaceholderColor:[UIColor lightGrayColor]];
        nLeft = 5;
        nRight = 5;
        nTop = 0;
        nBottom = 0;
    }
    return self;
}

-(void)setPaddingTop:(int)a_Top Left:(int)a_Left Bottom:(int)a_Botttom Right:(int)a_Right
{
    nTop = a_Top;
    nLeft = a_Left;
    nBottom = a_Botttom;
    nRight = a_Right;
}

// Placeholder position
- (CGRect)textRectForBounds:(CGRect)bounds
{
    CGRect rect = [super textRectForBounds:bounds];
    UIEdgeInsets insets = UIEdgeInsetsMake(nTop, nLeft, nBottom, nRight);
    return UIEdgeInsetsInsetRect(rect, insets);
}

// Text position
- (CGRect)editingRectForBounds:(CGRect)bounds
{
    CGRect rect = [super editingRectForBounds:bounds];
    UIEdgeInsets insets = UIEdgeInsetsMake(nTop, nLeft, nBottom, nRight);
    return UIEdgeInsetsInsetRect(rect, insets);
}

// Clear button
//- (CGRect)clearButtonRectForBounds:(CGRect)bounds {
//    CGRect rect = [super clearButtonRectForBounds:bounds];
//
//    return CGRectOffset(rect, -5, 0);
//}

//-(void)drawPlaceholderInRect:(CGRect)rect
//{
//    if (!placeholderColor) {
//        [super drawPlaceholderInRect:rect];
//        return;
//    }
//
//    [placeholderColor setFill];
//
//    //check iOS version is >= 7.0
//    if (([[[UIDevice currentDevice] systemVersion] compare:@"7.0" options:NSNumericSearch] != NSOrderedAscending)) {
//        rect =  CGRectInset(rect, 0, (rect.size.height - self.font.lineHeight) / 2.0);
//        [self.placeholder drawInRect:rect withAttributes:@{NSFontAttributeName:self.font, NSForegroundColorAttributeName:placeholderColor}];
//    } else {
//#pragma clang diagnostic push
//#pragma clang diagnostic ignored "-Wenum-conversion"
//#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_6_0
//        [self.placeholder drawInRect:rect withFont:self.font lineBreakMode:NSLineBreakByTruncatingTail alignment:self.textAlignment];
//#else
//        [self.placeholder drawInRect:rect withFont:self.font lineBreakMode:UILineBreakModeTailTruncation alignment:self.textAlignment];
//#endif
//#pragma clang diagnostic pop
//    }
//}
//
//- (CGRect)textRectForBounds:(CGRect)bounds {
//    return CGRectInset(bounds, 5, 0);
//}
//- (CGRect)editingRectForBounds:(CGRect)bounds {
//    return CGRectInset(bounds, 5, 0);
//}


@end
