//
//  PBTextField.h
//  mievpad
//
//  Created by Yang JungYun on 13. 2. 28..
//  Copyright (c) 2013년 miev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PBTextField : UITextField
{
    UIColor *placeholderColor;
}

@property (nonatomic, strong) UIColor *placeholderColor;
@property (readwrite, nonatomic) int nLeft;
@property (readwrite, nonatomic) int nTop;
@property (readwrite, nonatomic) int nRight;
@property (readwrite, nonatomic) int nBottom;

-(void)setPaddingTop:(int)a_Top Left:(int)a_Left Bottom:(int)a_Botttom Right:(int)a_Right;

@end
