//
//  VoiceCommand.swift
//  BT150
//
//  Created by KOBONGHWAN on 01/01/2019.
//  Copyright © 2019 bistos. All rights reserved.
//

import UIKit

class VoiceCommand: NSObject {

    public static let CMD_TYPE_NORMAL     = 100
    public static let CMD_TYPE_STEP       = 200
    
    public static let CMD_NONE            = 0
    
    public static let CMD_MODE_CHANGE     = 1001
    
    public static let CMD_PRESSURE_UP     = 1006
    public static let CMD_PRESSURE_DOWN   = 1007
    
    public static let CMD_CYCLE_UP        = 1009
    public static let CMD_CYCLE_DOWN      = 1010
    public static let CMD_PROGRAMID_N     = 1016
    
    public static let CMD_PLAY            = 1019
    public static let CMD_STOP            = 1021
    public static let CMD_SKIP            = 1023
    

    
    public static let CMD_POWER           = 1038
    public static let CMD_LAMP            = 1039
    
    public static let CMD_NUMBER_1        = 1040
    public static let CMD_NUMBER_2        = 1041
    public static let CMD_NUMBER_3        = 1042
    public static let CMD_NUMBER_4        = 1043
    public static let CMD_NUMBER_5        = 1044
    public static let CMD_NUMBER_6        = 1045
    public static let CMD_NUMBER_7        = 1046
    public static let CMD_NUMBER_8        = 1047
    
    public var nCmdCode = 0
    public var nCmdType = CMD_TYPE_NORMAL
    public var nStepVal = 0
    public var arrCmdPhrase = [String]()

    init(_ a_CmdCode : Int, Phrase a_Phrase : String) {
        super.init()
        nCmdCode = a_CmdCode
        addPhrase(a_Phrase)
    }
    
    init(_ a_CmdCode : Int, Phrase a_Phrase : String, Type a_Type : Int) {
        super.init()
        nCmdCode = a_CmdCode
        nCmdType = a_Type
        addPhrase(a_Phrase)
    }
    
    func addPhrase(_ a_Phrase : String) {
        for aPhrase in arrCmdPhrase {
            if aPhrase == a_Phrase {
                return
            }
        }
        arrCmdPhrase.append(a_Phrase)
    }

    func printPhrase() {
        for aPhrase in arrCmdPhrase {
            print("\(nCmdCode) \(aPhrase)");
        }
    }
    
    func isMatch(_ a_Phrase : String) -> Bool {
        for aCmd in arrCmdPhrase {
            if aCmd.lowercased() == a_Phrase.lowercased() {
                return true
            }
        }
        return false
    }
    
    func isContains(_ a_Phrase : String) -> Bool {
        for aCmd in arrCmdPhrase {
            let aChunk = aCmd.replacingOccurrences(of: " ", with: "")
            if aChunk.lowercased().contains(find: a_Phrase.lowercased()) {
                return true
            }
        }
        return false
    }
    
    func checkCommand(_ a_Phrase : String) -> Bool {
        for aCmd in arrCmdPhrase {
            let aChunk = aCmd.replacingOccurrences(of: " ", with: "")                        
            if a_Phrase.lowercased().contains(find: aChunk.lowercased()) {
                if a_Phrase.lowercased() == "slow" && aChunk.lowercased() == "low" {
                    return false
                }
//                print("checkCommand: a_Phrase:" + a_Phrase)
                return true
            }
        }
        return false
    }
    
    
    func initCmdPhrase() {
        arrCmdPhrase.removeAll()
    }
    
    func getCmdPhraseCount() -> Int {
        return arrCmdPhrase.count
    }

}
