//
//  StatisticsEditViewController.h
//  BT150
//
//  Created by KOBONGHWAN on 25/12/2018.
//  Copyright © 2018 bistos. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PBOCPicker.h"
#import "PBDatePickerView.h"

NS_ASSUME_NONNULL_BEGIN

@protocol StatisticsEditDelegate <NSObject>

-(void)onCloseStatisticsEdit:(NSDictionary*)a_Param;

@end

@interface StatisticsEditViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate, PBOCPickerDelegate, PBDatePickerViewDelegate>
{
    id <StatisticsEditDelegate> delegate;
    long lDate;
    int nAmtL;
    int nAmtR;
    long nTime;
    int nIdx;
}

@property(strong, nonatomic) id <StatisticsEditDelegate> delegate;
@property(readwrite, nonatomic) long lDate;
@property(readwrite, nonatomic) int nAmtL;
@property(readwrite, nonatomic) int nAmtR;
@property(readwrite, nonatomic) long nTime;
@property(readwrite, nonatomic) int nIdx;

-(void)initStatisticsEditPop:(NSDictionary*)a_Param;

@end

NS_ASSUME_NONNULL_END
