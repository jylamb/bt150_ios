//
//  StatisticsEditViewController.m
//  BT150
//
//  Created by KOBONGHWAN on 25/12/2018.
//  Copyright © 2018 bistos. All rights reserved.
//

#import "StatisticsEditViewController.h"
#import "PBLibOC.h"


#define kTableCellPicker    120

#define kPickerMinTag       501
#define kPickerSecTag       502

#define kModeExpression     1
#define kModeMassage        0

#define toOZ                0.033814
#define toML                29.57353


@interface StatisticsEditViewController ()
{
    UITableView *tvList;
    UIPickerView *pvOptionMin;
    UIPickerView *pvOptionSec;
    UIPickerView *pvOptionAmt;
    PBOCPicker *pickerSelect;
    PBDatePickerView *pickerDate;
    
    CGSize sizeView;
    CGFloat fCellH;
    NSMutableArray *arrMin;
    NSMutableArray *arrSec;
    
    NSMutableArray *arrMilkAmt;
    
    NSString *sUnit;
}

@end

@implementation StatisticsEditViewController

@synthesize lDate, nAmtL, nAmtR, nTime, nIdx;
@synthesize delegate;

-(void)initStatisticsEditPop:(NSDictionary*)a_Param
{
    NSLog(@"initStatisticsEditPop:%@", a_Param);
        
    lDate = [[a_Param objectForKey:@"date"] longValue];
    nAmtL = [[a_Param objectForKey:@"left"] intValue];
    nAmtR = [[a_Param objectForKey:@"right"] intValue];
    nTime = [[a_Param objectForKey:@"time"] longValue];
    
    [self initControls];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = RGBA(0, 0, 0, 0.6);
    sizeView = self.view.frame.size;
}

-(void)initControls
{
    fCellH = kNavBarH;
    
    CGFloat fTableH = (fCellH + 10) * 2 + fCellH * 3 + fCellH / 2 + kTableCellPicker;
    CGFloat fTableY = (sizeView.height - fTableH) / 2;
    
    if (tvList == nil) {
        
        tvList = [[UITableView alloc] initWithFrame:CGRectMake(10, fTableY, sizeView.width-20, fTableH) style:UITableViewStylePlain];
        tvList.bounces = NO;
        tvList.separatorStyle = UITableViewCellSeparatorStyleNone;
        tvList.scrollEnabled = YES;
        tvList.allowsSelection = NO;
        tvList.delegate = self;
        tvList.dataSource = self;
        tvList.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:tvList];
        
        arrMilkAmt = [[NSMutableArray alloc] init];
        for (int i = 0; i < 40; i++) {
            [arrMilkAmt addObject:@{@"code" : [NSString stringWithFormat:@"%d", i*5], @"codename" : [NSString stringWithFormat:@"%02d", i*5]}];
        }
        
        arrMin = [[NSMutableArray alloc] init];
        arrSec = [[NSMutableArray alloc] init];
        
        for (int i = 0; i < 100; i++) {
            [arrMin addObject:@{@"code" : [NSString stringWithFormat:@"%d", i], @"codename" : [NSString stringWithFormat:@"%02d", i]}];
        }
        
        for (int i = 0; i < 60; i++) {
            [arrSec addObject:@{@"code" : [NSString stringWithFormat:@"%d", i], @"codename" : [NSString stringWithFormat:@"%02d", i]}];
        }
    }
    
    sUnit = @"ml";
    NSString *aUnit = [[NSUserDefaults standardUserDefaults] objectForKey:@"unit"];
    if(aUnit != nil && aUnit.length > 0) {
        sUnit = aUnit;
    }
    
    [tvList reloadData];
    
}

#pragma mark -
#pragma mark UITableViewDelegate, UITableViewDataSource
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0 || indexPath.row == 6) {
        return fCellH + 10;
    } else if (indexPath.row == 4) {
        return fCellH / 2;
    } else if (indexPath.row == 5) {
        return kTableCellPicker;
    }
    return fCellH;
}

-(NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section
{
    return 7;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = kDefWhite;
    
    for(UIView *subView in cell.contentView.subviews) {
        if (subView.tag >= 100) {
            [subView removeFromSuperview];
        }
    }
    
    CGSize sizeCell = tvList.frame.size;
    
//    NSString *aUnit = [[NSUserDefaults standardUserDefaults] objectForKey:@"unit"];
    
    UILabel *lblTitle = [PBLibOC buildFlatColorLabel:[UIColor clearColor] TitleColor:kTextGray];
    lblTitle.frame = CGRectMake(0, 0, sizeCell.width/3-10, fCellH);
    lblTitle.font = AppFont(16);
    lblTitle.textAlignment = NSTextAlignmentRight;
    lblTitle.tag = 999;
    [cell.contentView addSubview:lblTitle];
    
    if (indexPath.row == 0) {
        cell.backgroundColor = kDefThemeColor;
        lblTitle.hidden = YES;
        
        UILabel *lblPopTitle = [PBLibOC buildFlatColorLabel:[UIColor clearColor] TitleColor:kTextBlack];
        lblPopTitle.frame = CGRectMake(10, 0, sizeCell.width-20, fCellH+10);
        lblPopTitle.font = AppFont(18);
        lblPopTitle.textColor = kDefWhite;
        lblPopTitle.tag = 999;
        lblPopTitle.text = NSLocalizedString(@"PumpingData", @"Pumping Data");
        [cell.contentView addSubview:lblPopTitle];
        
        UIButton *btnClose = [UIButton buttonWithType:UIButtonTypeCustom];
        btnClose.frame = CGRectMake(sizeCell.width-60, (fCellH+10-40)/2, 40, 40);
        [btnClose setImage:[UIImage imageNamed:@"ic_close_white_24dp.png"] forState:UIControlStateNormal];
        [btnClose addTarget:self action:@selector(onClose) forControlEvents:UIControlEventTouchUpInside];
        btnClose.tag = 999;
        [cell.contentView addSubview:btnClose];
        
    } else if (indexPath.row == 1) {
        lblTitle.text = NSLocalizedString(@"DateTimeNewLine", @"DateTime NewLine");
        
        NSDate *dtDay = [[NSDate alloc] initWithTimeIntervalSince1970:lDate/1000];
        NSDateFormatter *dfDate = [[NSDateFormatter alloc] init];
        dfDate.dateFormat = @"yyyy - MM - dd  HH : mm";
        
        UIButton *btnDate = [PBLibOC buildFlatColorButton:[UIColor clearColor]];
        btnDate.frame = CGRectMake(sizeCell.width/3, 5, sizeCell.width*2/3-40, fCellH-10);
        btnDate.titleLabel.font = AppFont(18);
        [btnDate setTitleColor:kDefBlack forState:UIControlStateNormal];
        [btnDate setTitle:[dfDate stringFromDate:dtDay] forState:UIControlStateNormal];
        [btnDate addTarget:self action:@selector(onDate) forControlEvents:UIControlEventTouchUpInside];
        btnDate.tag = 999;
        [cell.contentView addSubview:btnDate];

        UIView *vLine01 = [[UIView alloc] initWithFrame:CGRectMake(0, fCellH-1, sizeCell.width, 1)];
        vLine01.backgroundColor = kMenuLineGray;
        vLine01.tag = 999;
        [cell.contentView addSubview:vLine01];

    } else if (indexPath.row == 2) {
        lblTitle.text = NSLocalizedString(@"Left", @"Left");
        
        UIButton *btnLeft = [PBLibOC buildFlatColorButton:[UIColor clearColor] BorderColor:kMenuLineGray Radius:1 Thick:1];
        btnLeft.frame = CGRectMake(sizeCell.width/3, 5, sizeCell.width*2/3-40, fCellH-10);
        [btnLeft setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 5.0, 0.0, 5.0)];
        btnLeft.titleLabel.font = AppFont(15);
        btnLeft.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [btnLeft setTitleColor:kDefBlack forState:UIControlStateNormal];
        if ([sUnit isEqualToString:@"ml"]) {
            [btnLeft setTitle:[NSString stringWithFormat:@"%d", nAmtL] forState:UIControlStateNormal];
        } else {
            [btnLeft setTitle:[NSString stringWithFormat:@"%.2f", nAmtL*toOZ] forState:UIControlStateNormal];
        }
        [btnLeft addTarget:self action:@selector(onLeft) forControlEvents:UIControlEventTouchUpInside];
        btnLeft.tag = 999;
        [cell.contentView addSubview:btnLeft];

        UILabel *lblUnit = [PBLibOC buildFlatColorLabel:[UIColor clearColor] TitleColor:kTextGray];
        lblUnit.frame = CGRectMake(sizeCell.width-40, 0, 40, fCellH);
        lblUnit.font = AppFont(14);
        lblUnit.textAlignment = NSTextAlignmentCenter;
        lblUnit.tag = 999;
        lblUnit.text = sUnit;
        [cell.contentView addSubview:lblUnit];

    } else if (indexPath.row == 3) {
        lblTitle.text = NSLocalizedString(@"Right", @"Right");

        UIButton *btnRight = [PBLibOC buildFlatColorButton:[UIColor clearColor] BorderColor:kMenuLineGray Radius:1 Thick:1];
        btnRight.frame = CGRectMake(sizeCell.width/3, 5, sizeCell.width*2/3-40, fCellH-10);
        [btnRight setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 5.0, 0.0, 5.0)];
        btnRight.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        btnRight.titleLabel.font = AppFont(15);
        [btnRight setTitleColor:kDefBlack forState:UIControlStateNormal];
        if ([sUnit isEqualToString:@"ml"]) {
            [btnRight setTitle:[NSString stringWithFormat:@"%d", nAmtR] forState:UIControlStateNormal];
        } else {
            [btnRight setTitle:[NSString stringWithFormat:@"%.2f", nAmtR*toOZ] forState:UIControlStateNormal];
        }
        [btnRight addTarget:self action:@selector(onRight) forControlEvents:UIControlEventTouchUpInside];
        btnRight.tag = 999;
        [cell.contentView addSubview:btnRight];

        UILabel *lblUnit = [PBLibOC buildFlatColorLabel:[UIColor clearColor] TitleColor:kTextGray];
        lblUnit.frame = CGRectMake(sizeCell.width-40, 0, 40, fCellH);
        lblUnit.font = AppFont(14);
        lblUnit.textAlignment = NSTextAlignmentCenter;
        lblUnit.tag = 999;
        lblUnit.text = sUnit;
        [cell.contentView addSubview:lblUnit];

        UIView *vLine01 = [[UIView alloc] initWithFrame:CGRectMake(0, fCellH-1, sizeCell.width, 1)];
        vLine01.backgroundColor = kMenuLineGray;
        vLine01.tag = 999;
        [cell.contentView addSubview:vLine01];
        
    } else if (indexPath.row == 4) {
        
        UILabel *lblMin = [PBLibOC buildFlatColorLabel:[UIColor clearColor] TitleColor:kTextGray];
        lblMin.frame = CGRectMake(sizeCell.width/3, 0, sizeCell.width/3, fCellH/2);
        lblMin.font = AppFont(16);
        lblMin.textAlignment = NSTextAlignmentCenter;
        lblMin.tag = 999;
        lblMin.text = @"min";
        [cell.contentView addSubview:lblMin];
        
        UILabel *lblSec = [PBLibOC buildFlatColorLabel:[UIColor clearColor] TitleColor:kTextGray];
        lblSec.frame = CGRectMake(sizeCell.width*2/3, 0, sizeCell.width/3, fCellH/2);
        lblSec.font = AppFont(16);
        lblSec.textAlignment = NSTextAlignmentCenter;
        lblSec.tag = 999;
        lblSec.text = @"sec";
        [cell.contentView addSubview:lblSec];
        
        
    } else if (indexPath.row == 5) {
        lblTitle.text = NSLocalizedString(@"PumpOperatingTimeStatistics", @"Pump Operating Time");
        lblTitle.numberOfLines = 3;
        lblTitle.frame = CGRectMake(0, 0, sizeCell.width/3-10, kTableCellPicker);
        
        pvOptionMin = [[UIPickerView alloc] initWithFrame:CGRectMake(sizeCell.width/3, 0, sizeCell.width/3, kTableCellPicker)];
        pvOptionMin.backgroundColor = kDefWhite;
        pvOptionMin.dataSource = self;
        pvOptionMin.delegate = self;
        pvOptionMin.showsSelectionIndicator = YES;
//        pvOptionMin.translatesAutoresizingMaskIntoConstraints = NO;
        pvOptionMin.tag = kPickerMinTag;
        [cell.contentView addSubview:pvOptionMin];
        
        pvOptionSec = [[UIPickerView alloc] initWithFrame:CGRectMake(sizeCell.width*2/3, 0, sizeCell.width/3, kTableCellPicker)];
        pvOptionSec.backgroundColor = kDefWhite;
        pvOptionSec.dataSource = self;
        pvOptionSec.delegate = self;
        pvOptionSec.showsSelectionIndicator = YES;
//        pvOptionSec.translatesAutoresizingMaskIntoConstraints = NO;
        pvOptionSec.tag = kPickerSecTag;
        [cell.contentView addSubview:pvOptionSec];
        
        long aMin = nTime / 60;
        long aSec = nTime % 60;
        
        [pvOptionMin selectRow:aMin inComponent:0 animated:NO];
        [pvOptionSec selectRow:aSec inComponent:0 animated:NO];
        
    } else if (indexPath.row == 6) {
        
        UIButton *btnCancel = [PBLibOC buildFlatColorButton:kCancelGray BorderColor:kCancelGray Radius:0];
        btnCancel.frame = CGRectMake(0, 0, sizeCell.width/2, fCellH+10);
        btnCancel.titleLabel.font = AppFont(18);
        [btnCancel setTitleColor:kDefThemeColor forState:UIControlStateNormal];
        [btnCancel setTitle:NSLocalizedString(@"NO", @"NO") forState:UIControlStateNormal];
        [btnCancel addTarget:self action:@selector(onClose) forControlEvents:UIControlEventTouchUpInside];
        btnCancel.tag = 999;
        [cell.contentView addSubview:btnCancel];
        
        UIButton *btnConfirm = [PBLibOC buildFlatColorButton:kDefThemeColor BorderColor:kDefThemeColor Radius:0];
        btnConfirm.frame = CGRectMake(sizeCell.width/2, 0, sizeCell.width/2, fCellH+10);
        btnConfirm.titleLabel.font = AppFont(18);
        [btnConfirm setTitleColor:kDefWhite forState:UIControlStateNormal];
        [btnConfirm setTitle:NSLocalizedString(@"YES", @"YES") forState:UIControlStateNormal];
        [btnConfirm addTarget:self action:@selector(onConfirm) forControlEvents:UIControlEventTouchUpInside];
        btnConfirm.tag = 999;
        [cell.contentView addSubview:btnConfirm];
        
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [PBLibOC performTransition:NO andView:self.view];
}

-(void)onDate
{
    [pickerDate canclePicker];
    if (pickerDate == nil) {
        pickerDate = [[PBDatePickerView alloc] initWithFrame:CGRectMake(0, sizeView.height-230, sizeView.width, 230)];
        pickerDate.delegate = self;
        [self.view addSubview:pickerDate];
    }
    NSDate *dtDay = [[NSDate alloc] initWithTimeIntervalSince1970:lDate/1000];
    [pickerDate setDateValue:dtDay Tag:100];
    [PBLibOC performTransition:YES andView:pickerDate];
}

-(void)onLeft
{
    [pickerSelect cancelPicker];
    if(arrMilkAmt != nil && [arrMilkAmt count] > 0) {
        if (pickerSelect == nil) {
            pickerSelect = [[PBOCPicker alloc] init];
            pickerSelect.delegate = self;
        }
        [pickerSelect initPBOCPicker:self.view Data:arrMilkAmt];
        pickerSelect.nTag = 100;
        [pickerSelect showPicker];
    }
    
//    [self initPicker];
}

-(void)onRight
{
    [pickerSelect cancelPicker];
    if(arrMilkAmt != nil && [arrMilkAmt count] > 0) {
        if (pickerSelect == nil) {
            pickerSelect = [[PBOCPicker alloc] init];
            pickerSelect.delegate = self;
        }
        [pickerSelect initPBOCPicker:self.view Data:arrMilkAmt];
        pickerSelect.nTag = 200;
        [pickerSelect showPicker];
    }
}

-(void)onConfirm
{
    [PBLibOC performTransition:NO andView:self.view];
    
    NSLog(@"nAmtL:%d", nAmtL);
    NSLog(@"nAmtR:%d", nAmtR);

    NSLog(@"nAmtL toOZ:%@", [NSNumber numberWithDouble:nAmtL*toOZ]);
    NSLog(@"nAmtR toOZ:%@", [NSNumber numberWithDouble:nAmtR*toOZ]);

//    NSString *aUnit = [[NSUserDefaults standardUserDefaults] objectForKey:@"unit"];
    if ([sUnit isEqualToString:@"ml"]) {
        [delegate onCloseStatisticsEdit:@{@"date" : [NSNumber numberWithLong:lDate],
                                       @"left" : [NSNumber numberWithInt:nAmtL],
                                       @"right" : [NSNumber numberWithInt:nAmtR],
                                       @"time" : [NSNumber numberWithLong:nTime]}];
    } else {
        [delegate onCloseStatisticsEdit:@{@"date" : [NSNumber numberWithLong:lDate],
                                       @"left" : [NSNumber numberWithDouble:nAmtL*toOZ],
                                       @"right" : [NSNumber numberWithDouble:nAmtR*toOZ],
                                       @"time" : [NSNumber numberWithLong:nTime]}];
    }
}

-(void)onClose
{
    [PBLibOC performTransition:NO andView:self.view];
}

#pragma mark -
#pragma mark UIPickerView DataSource
// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (pickerView.tag == kPickerMinTag) {
        return [arrMin count];
    }
    return [arrSec count];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component;
{
    return kNavBarH/2;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *pickerLabel = (UILabel *)view;
    int nWL = 260;
    
    if (pickerLabel == nil) {
        pickerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, nWL, kNavBarH/2)];
        [pickerLabel setTextAlignment:NSTextAlignmentCenter];
        [pickerLabel setBackgroundColor:[UIColor clearColor]];
        [pickerLabel setFont:AppFont(16)];
    }
    
    long aRow = [pickerView selectedRowInComponent:component];
    if (row == aRow) {
        pickerLabel.textColor = kDefThemeColor;
    }
    
    NSString *aTitleName = @"";
    if (pickerView.tag == kPickerMinTag) {
        aTitleName = [PBLibOC filterNSNull:[[arrMin objectAtIndex:row] objectForKey:@"codename"]];
    } else {
        aTitleName = [PBLibOC filterNSNull:[[arrSec objectAtIndex:row] objectForKey:@"codename"]];
    }
    
    pickerLabel.text = aTitleName;
    return pickerLabel;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    [pvOptionMin reloadAllComponents];
    [pvOptionSec reloadAllComponents];
    
    long aMin = [pvOptionMin selectedRowInComponent:0];
    long aSec = [pvOptionSec selectedRowInComponent:0];
    nTime = aMin*60 + aSec;
}

#pragma mark - PBOCPickerDelegate
- (void)onPBOCPickerClose:(NSDictionary *)a_Param Tag:(int)a_Tag
{
//    NSString *aUnit = [[NSUserDefaults standardUserDefaults] objectForKey:@"unit"];
    if (a_Param != nil) {
        if (a_Tag == 100) {
            if ([sUnit isEqualToString:@"ml"]) {
                nAmtL = [[a_Param objectForKey:@"code"] intValue];
            } else {
                nAmtL = ceil([[a_Param objectForKey:@"code"] doubleValue] * toML);
            }
        } else if (a_Tag == 200) {
            if ([sUnit isEqualToString:@"ml"]) {
                nAmtR = [[a_Param objectForKey:@"code"] intValue];
            } else {
                nAmtR = ceil([[a_Param objectForKey:@"code"] doubleValue] * toML);
            }
        }
    }
    [tvList reloadData];
}

#pragma mark - PBDatePickerViewDelegate
- (void)onCloseDatePicker:(NSDate *)a_Date Tag:(int)a_Tag
{
    if (a_Tag > 0) {
        lDate = [a_Date timeIntervalSince1970]*1000;
//        [tvList reloadData];
    }
    [tvList reloadData];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
