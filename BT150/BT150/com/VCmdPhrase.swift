//
//  VCmdPhrase.swift
//  BT150
//
//  Created by KOBONGHWAN on 08/01/2019.
//  Copyright © 2019 bistos. All rights reserved.
//

import UIKit

class VCmdPhrase: NSObject {

    static var CMD_MODE_CHANGE  = ""
    static var CMD_MODE_CHANGE_DESC = ""
    static var CMD_PRESSURE_UP  = ""
    static var CMD_PRESSURE_UP_DESC  = ""
    static var CMD_PRESSURE_DOWN  = ""
    static var CMD_PRESSURE_DOWN_DESC  = ""

    static var CMD_CYCLE_UP  = ""
    static var CMD_CYCLE_UP_DESC  = ""
    static var CMD_CYCLE_DOWN  = ""
    static var CMD_CYCLE_DOWN_DESC  = ""
    
    static var CMD_PROGRAMID_N  = ""
    static var CMD_PROGRAMID_N_DESC  = ""
    
    static var CMD_PLAY  = ""
    static var CMD_PLAY_DESC  = ""
    static var CMD_STOP  = ""
    static var CMD_STOP_DESC  = ""
    static var CMD_SKIP  = ""
    static var CMD_SKIP_DESC  = ""
    
    static var CMD_POWER  = ""
    static var CMD_POWER_DESC  = ""
    static var CMD_LAMP  = ""
    static var CMD_LAMP_DESC  = ""
    
    static var CMD_NUMBER_1  = ""
    static var CMD_NUMBER_2  = ""
    static var CMD_NUMBER_3  = ""
    static var CMD_NUMBER_4  = ""
    static var CMD_NUMBER_5  = ""
    static var CMD_NUMBER_6  = ""
    static var CMD_NUMBER_7  = ""
    static var CMD_NUMBER_8  = ""

    static var SPEECH_NUMBER_ONE  = ""
    static var SPEECH_NUMBER_TWO  = ""
    static var SPEECH_NUMBER_TWO1  = ""
    static var SPEECH_NUMBER_TWO2  = ""
    static var SPEECH_NUMBER_TWO3  = ""
    static var SPEECH_NUMBER_TWO4  = ""
    static var SPEECH_NUMBER_TWO5  = ""

    static var SPEECH_NUMBER_THREE  = ""
    //pole, pool, poor, poll, for, fore, paw
    static var SPEECH_NUMBER_Four  = ""
    static var SPEECH_NUMBER_Four1  = ""
    static var SPEECH_NUMBER_Four2  = ""
    static var SPEECH_NUMBER_Four3  = ""
    static var SPEECH_NUMBER_Four4  = ""
    static var SPEECH_NUMBER_Four5  = ""
    static var SPEECH_NUMBER_Four6  = ""
    static var SPEECH_NUMBER_Four7  = ""
    static var SPEECH_NUMBER_Four8  = ""
    static var SPEECH_NUMBER_Four9  = ""
    static var SPEECH_NUMBER_Five  = ""
    static var SPEECH_NUMBER_Five1  = ""
    static var SPEECH_NUMBER_Five2  = ""
    static var SPEECH_NUMBER_SIX  = ""
    static var SPEECH_NUMBER_SEVEN  = ""
    static var SPEECH_NUMBER_EIGHT  = ""
    static var SPEECH_NUMBER_NINE  = ""
    static var SPEECH_NUMBER_TEN  = ""

    
    class func initVCmdPhrase(_ a_Language : String) {
        
        print("initVCmdPhrase: a_Language:\(a_Language)")
        
        if a_Language == VOICE_REC_LANG_KOREAN {
            initVCmdPhraseKR()
        
        } else if a_Language == VOICE_REC_LANG_FRENCH {
            initVCmdPhraseFR()
            
        } else {
            initVCmdPhraseEN()
        }
    }
    
    class func initVCmdPhraseKR() {
        CMD_MODE_CHANGE  = "변경"
        CMD_MODE_CHANGE_DESC  = "동작 모드를 변경한다."
        
        CMD_PRESSURE_UP  = "높게"
        CMD_PRESSURE_UP_DESC  = "압력 레벨을 1 단계 증가 시킨다."
        CMD_PRESSURE_DOWN  = "낮게"
        CMD_PRESSURE_DOWN_DESC  = "압력 레벨을 1 단계 감소 시킨다."
        
        CMD_CYCLE_UP  = "빠르게"
        CMD_CYCLE_UP_DESC  = "압력 주기를 1 단계 증가 시킨다."
        CMD_CYCLE_DOWN  = "느리게"
        CMD_CYCLE_DOWN_DESC  = "압력 주기를 1 단계 감소 시킨다."
        
        CMD_PROGRAMID_N  = "번호"
        CMD_PROGRAMID_N_DESC  = "프로그램 아이디 선택 n=1~8."
        
        CMD_PLAY  = "재생"
        CMD_PLAY_DESC  = "프로그램 아이디의 시퀀스를 재생한다."
        CMD_STOP  = "정지"
        CMD_STOP_DESC  = "재생 시퀀스를 정지한다."
        CMD_SKIP  = "통과"
        CMD_SKIP_DESC  = "재생 시퀀스를 건너 뛴다."
        
        CMD_POWER  = "전원"
        CMD_POWER_DESC  = "전원을 켠다."
        CMD_LAMP  = "전등"
        CMD_LAMP_DESC  = "수유등을 켜거나 밝기를 변화시킨다."
        
        CMD_NUMBER_1  = "1번"
        CMD_NUMBER_2  = "2번"
        CMD_NUMBER_3  = "3번"
        CMD_NUMBER_4  = "4번"
        CMD_NUMBER_5  = "5번"
        CMD_NUMBER_6  = "6번"
        CMD_NUMBER_7  = "7번"
        CMD_NUMBER_8  = "8번"
        
        SPEECH_NUMBER_ONE  = "일"
        SPEECH_NUMBER_TWO  = "이"
        SPEECH_NUMBER_TWO1  = "이"
        SPEECH_NUMBER_TWO2  = "이"
        SPEECH_NUMBER_TWO3  = "이"
        SPEECH_NUMBER_TWO4  = "이"
        SPEECH_NUMBER_TWO5  = "이"
        SPEECH_NUMBER_THREE  = "삼"
        SPEECH_NUMBER_Four  = "사"
        SPEECH_NUMBER_Four1  = "사"
        SPEECH_NUMBER_Four2  = "사"
        SPEECH_NUMBER_Four3  = "사"
        SPEECH_NUMBER_Four4  = "사"
        SPEECH_NUMBER_Four5  = "사"
        SPEECH_NUMBER_Four6  = "사"
        SPEECH_NUMBER_Four7  = "사"
        SPEECH_NUMBER_Four8  = "사"
        SPEECH_NUMBER_Four9  = "사"
        SPEECH_NUMBER_Five  = "오"
        SPEECH_NUMBER_Five1  = "오"
        SPEECH_NUMBER_Five2  = "오"
        SPEECH_NUMBER_SIX  = "육"
        SPEECH_NUMBER_SEVEN  = "칠"
        SPEECH_NUMBER_EIGHT  = "팔"
        SPEECH_NUMBER_NINE  = "구"
        SPEECH_NUMBER_TEN  = "십"

    }

    class func initVCmdPhraseEN() {
        CMD_MODE_CHANGE  = "Change"
        CMD_MODE_CHANGE_DESC  = "Change the operation mode."
        
        CMD_PRESSURE_UP  = "HIGH"
        CMD_PRESSURE_UP_DESC  = "Increase the pressure level by one step."
        CMD_PRESSURE_DOWN  = "LOW"
        CMD_PRESSURE_DOWN_DESC  = "Decrease the pressure level by one step."
        
        CMD_CYCLE_UP  = "FAST"
        CMD_CYCLE_UP_DESC  = "Increase the pressure cycle by one step."
        CMD_CYCLE_DOWN  = "SLOW"
        CMD_CYCLE_DOWN_DESC  = "Decrease the pressure cycle by one step."
        
        CMD_PROGRAMID_N  = "Number"
        CMD_PROGRAMID_N_DESC  = "Select the coreesponding program ID n = 1 to 8."
        
        CMD_PLAY  = "Play"
        CMD_PLAY_DESC  = "Play the sequence in the corresponding program ID."
        CMD_STOP  = "Stop"
        CMD_STOP_DESC  = "Stop the playback sequence."
        CMD_SKIP  = "Skip"
        CMD_SKIP_DESC  = "Skip the playback sequence."
        
        CMD_POWER  = "POWER"
        CMD_POWER_DESC  = "Power on"
        CMD_LAMP  = "LAMP"
        CMD_LAMP_DESC  = "Turn on the feeding light or change the brightness."
        
        CMD_NUMBER_1  = "number one"
        CMD_NUMBER_2  = "number two"
        CMD_NUMBER_3  = "number three"
        CMD_NUMBER_4  = "number four"
        CMD_NUMBER_5  = "number five"
        CMD_NUMBER_6  = "number six"
        CMD_NUMBER_7  = "number seven"
        CMD_NUMBER_8  = "number eight"
        
        SPEECH_NUMBER_ONE  = "one"
        //to, too, toe, top, tow
        SPEECH_NUMBER_TWO  = "two"
        SPEECH_NUMBER_TWO1  = "to"
        SPEECH_NUMBER_TWO2  = "too"
        SPEECH_NUMBER_TWO3  = "toe"
        SPEECH_NUMBER_TWO4  = "top"
        SPEECH_NUMBER_TWO5  = "tow"
        SPEECH_NUMBER_THREE  = "three"
        SPEECH_NUMBER_Four  = "four"
        //pole, pool, poor, poll, for, fore, paw
        SPEECH_NUMBER_Four1  = "pole"
        SPEECH_NUMBER_Four2  = "pool"
        SPEECH_NUMBER_Four3  = "poor"
        SPEECH_NUMBER_Four4  = "poll"
        SPEECH_NUMBER_Four5  = "for"
        SPEECH_NUMBER_Four6  = "fore"
        SPEECH_NUMBER_Four7  = "paw"
        SPEECH_NUMBER_Four8  = "paul"
        SPEECH_NUMBER_Four9  = "phone"
        
        SPEECH_NUMBER_Five  = "five"
        SPEECH_NUMBER_Five1  = "bible"
        SPEECH_NUMBER_Five2  = "fiber"
        
        SPEECH_NUMBER_SIX  = "six"
        SPEECH_NUMBER_SEVEN  = "seven"
        SPEECH_NUMBER_EIGHT  = "eight"
        SPEECH_NUMBER_NINE  = "nine"
        SPEECH_NUMBER_TEN  = "ten"

    }

    class func initVCmdPhraseFR() {
        CMD_MODE_CHANGE  = "CHANGER"
        CMD_MODE_CHANGE_DESC  = "Changer de mode de fonctionnement."
        
        CMD_PRESSURE_UP  = "AUGMENTER"
        CMD_PRESSURE_UP_DESC  = "Augmenter la puissance d'une unité"
        CMD_PRESSURE_DOWN  = "BAISSER"
        CMD_PRESSURE_DOWN_DESC  = "Diminuer la puissance d'une unité."
        
        CMD_CYCLE_UP  = "ACCELERER"
        CMD_CYCLE_UP_DESC  = "Augmenter la fréquence d'une unité."
        CMD_CYCLE_DOWN  = "RALENTIR"
        CMD_CYCLE_DOWN_DESC  = "Diminuer la fréquence d'une unité."
        
        CMD_PROGRAMID_N  = "Numéro"
        CMD_PROGRAMID_N_DESC  = "Select the coreesponding program ID n = 1 to 8."
        
        CMD_PLAY  = "LECTURE"
        CMD_PLAY_DESC  = "Lancer la séquence du programme correspondant"
        CMD_STOP  = "STOP"
        CMD_STOP_DESC  = "Stopper la séquence en cours."
        CMD_SKIP  = "PASSER"
        CMD_SKIP_DESC  = "Passer à la séquence suivante."
        
        CMD_POWER  = "ACTIVER"
        CMD_POWER_DESC  = "Allumer le tire-lait."
        CMD_LAMP  = "LAMPE"
        CMD_LAMP_DESC  = "Allumer la lampe et changer sa luminosité."
        
        CMD_NUMBER_1  = "number one"
        CMD_NUMBER_2  = "number two"
        CMD_NUMBER_3  = "number three"
        CMD_NUMBER_4  = "number four"
        CMD_NUMBER_5  = "number five"
        CMD_NUMBER_6  = "number six"
        CMD_NUMBER_7  = "number seven"
        CMD_NUMBER_8  = "number eight"
        
        SPEECH_NUMBER_ONE  = "one"
        //to, too, toe, top, tow
        SPEECH_NUMBER_TWO  = "two"
        SPEECH_NUMBER_TWO1  = "to"
        SPEECH_NUMBER_TWO2  = "too"
        SPEECH_NUMBER_TWO3  = "toe"
        SPEECH_NUMBER_TWO4  = "top"
        SPEECH_NUMBER_TWO5  = "tow"
        SPEECH_NUMBER_THREE  = "three"
        SPEECH_NUMBER_Four  = "four"
        //pole, pool, poor, poll, for, fore, paw
        SPEECH_NUMBER_Four1  = "pole"
        SPEECH_NUMBER_Four2  = "pool"
        SPEECH_NUMBER_Four3  = "poor"
        SPEECH_NUMBER_Four4  = "poll"
        SPEECH_NUMBER_Four5  = "for"
        SPEECH_NUMBER_Four6  = "fore"
        SPEECH_NUMBER_Four7  = "paw"
        SPEECH_NUMBER_Four8  = "paul"
        SPEECH_NUMBER_Four9  = "phone"
        
        SPEECH_NUMBER_Five  = "five"
        SPEECH_NUMBER_Five1  = "bible"
        SPEECH_NUMBER_Five2  = "fiber"
        
        SPEECH_NUMBER_SIX  = "six"
        SPEECH_NUMBER_SEVEN  = "seven"
        SPEECH_NUMBER_EIGHT  = "eight"
        SPEECH_NUMBER_NINE  = "nine"
        SPEECH_NUMBER_TEN  = "ten"

    }
}
