//
//  HelpViewController.swift
//  BT150
//
//  Created by KOBONGHWAN on 29/08/2019.
//  Copyright © 2019 bistos. All rights reserved.
//

import UIKit

class HelpViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {

    struct tablelist {
        static let kTableViewCellHTop : CGFloat = 40
        static let kTableViewCellH : CGFloat = 50
    }

    struct vcmddesc {
        static var CMD_MODE_CHANGE  = ""
        static var CMD_MODE_CHANGE_DESC = ""
        static var CMD_PRESSURE_UP  = ""
        static var CMD_PRESSURE_UP_DESC  = ""
        static var CMD_PRESSURE_DOWN  = ""
        static var CMD_PRESSURE_DOWN_DESC  = ""

        static var CMD_CYCLE_UP  = ""
        static var CMD_CYCLE_UP_DESC  = ""
        static var CMD_CYCLE_DOWN  = ""
        static var CMD_CYCLE_DOWN_DESC  = ""
        
        static var CMD_PROGRAMID_N  = ""
        static var CMD_PROGRAMID_N_DESC  = ""
        
        static var CMD_PLAY  = ""
        static var CMD_PLAY_DESC  = ""
        static var CMD_STOP  = ""
        static var CMD_STOP_DESC  = ""
        static var CMD_SKIP  = ""
        static var CMD_SKIP_DESC  = ""
        
        static var CMD_POWER  = ""
        static var CMD_POWER_DESC  = ""
        static var CMD_LAMP  = ""
        static var CMD_LAMP_DESC  = ""
    }
    
    @IBOutlet weak var tvList: UITableView!
    @IBOutlet weak var btnClose: UIButton!
    
    public var arrData = [VCmdHelpData]()
    
    
    func initVCmdPhraseKR() {
        vcmddesc.CMD_MODE_CHANGE  = "변경"
        vcmddesc.CMD_MODE_CHANGE_DESC  = "동작 모드를 변경한다."
        
        vcmddesc.CMD_PRESSURE_UP  = "높게"
        vcmddesc.CMD_PRESSURE_UP_DESC  = "압력 레벨을 1 단계 증가 시킨다."
        vcmddesc.CMD_PRESSURE_DOWN  = "낮게"
        vcmddesc.CMD_PRESSURE_DOWN_DESC  = "압력 레벨을 1 단계 감소 시킨다."
        
        vcmddesc.CMD_CYCLE_UP  = "빠르게"
        vcmddesc.CMD_CYCLE_UP_DESC  = "압력 주기를 1 단계 증가 시킨다."
        vcmddesc.CMD_CYCLE_DOWN  = "느리게"
        vcmddesc.CMD_CYCLE_DOWN_DESC  = "압력 주기를 1 단계 감소 시킨다."
        
        vcmddesc.CMD_PROGRAMID_N  = "번호"
        vcmddesc.CMD_PROGRAMID_N_DESC  = "프로그램 아이디 선택 n=1~8."
        
        vcmddesc.CMD_PLAY  = "재생"
        vcmddesc.CMD_PLAY_DESC  = "프로그램 아이디의 시퀀스를 재생한다."
        vcmddesc.CMD_STOP  = "정지"
        vcmddesc.CMD_STOP_DESC  = "재생 시퀀스를 정지한다."
        vcmddesc.CMD_SKIP  = "통과"
        vcmddesc.CMD_SKIP_DESC  = "재생 시퀀스를 건너 뛴다."
        
        vcmddesc.CMD_POWER  = "전원"
        vcmddesc.CMD_POWER_DESC  = "전원을 켠다."
        vcmddesc.CMD_LAMP  = "전등"
        vcmddesc.CMD_LAMP_DESC  = "수유등을 켜거나 밝기를 변화시킨다."
    }

    func initVCmdPhraseEN() {
        vcmddesc.CMD_MODE_CHANGE  = "Change"
        vcmddesc.CMD_MODE_CHANGE_DESC  = "Change the operation mode."
        
        vcmddesc.CMD_PRESSURE_UP  = "HIGH"
        vcmddesc.CMD_PRESSURE_UP_DESC  = "Increase the pressure level by one step."
        vcmddesc.CMD_PRESSURE_DOWN  = "LOW"
        vcmddesc.CMD_PRESSURE_DOWN_DESC  = "Decrease the pressure level by one step."
        
        vcmddesc.CMD_CYCLE_UP  = "FAST"
        vcmddesc.CMD_CYCLE_UP_DESC  = "Increase the pressure cycle by one step."
        vcmddesc.CMD_CYCLE_DOWN  = "SLOW"
        vcmddesc.CMD_CYCLE_DOWN_DESC  = "Decrease the pressure cycle by one step."
        
        vcmddesc.CMD_PROGRAMID_N  = "Number"
        vcmddesc.CMD_PROGRAMID_N_DESC  = "Select the coreesponding program ID n = 1 to 8."
        
        vcmddesc.CMD_PLAY  = "Play"
        vcmddesc.CMD_PLAY_DESC  = "Play the sequence in the corresponding program ID."
        vcmddesc.CMD_STOP  = "Stop"
        vcmddesc.CMD_STOP_DESC  = "Stop the playback sequence."
        vcmddesc.CMD_SKIP  = "Skip"
        vcmddesc.CMD_SKIP_DESC  = "Skip the playback sequence."
        
        vcmddesc.CMD_POWER  = "POWER"
        vcmddesc.CMD_POWER_DESC  = "Power on"
        vcmddesc.CMD_LAMP  = "LAMP"
        vcmddesc.CMD_LAMP_DESC  = "Turn on the feeding light or change the brightness."
    }

    func initVCmdPhraseFR() {
        vcmddesc.CMD_MODE_CHANGE  = "CHANGER"
        vcmddesc.CMD_MODE_CHANGE_DESC  = "Change the operation mode."
        
        vcmddesc.CMD_PRESSURE_UP  = "AUGMENTER"
        vcmddesc.CMD_PRESSURE_UP_DESC  = "Increase the pressure level by one step."
        vcmddesc.CMD_PRESSURE_DOWN  = "BAISSER"
        vcmddesc.CMD_PRESSURE_DOWN_DESC  = "Decrease the pressure level by one step."
        
        vcmddesc.CMD_CYCLE_UP  = "ACCELERER"
        vcmddesc.CMD_CYCLE_UP_DESC  = "Increase the pressure cycle by one step."
        vcmddesc.CMD_CYCLE_DOWN  = "RALENTIR"
        vcmddesc.CMD_CYCLE_DOWN_DESC  = "Decrease the pressure cycle by one step."
        
        vcmddesc.CMD_PROGRAMID_N  = "Numéro"
        vcmddesc.CMD_PROGRAMID_N_DESC  = "Select the coreesponding program ID n = 1 to 8."
        
        vcmddesc.CMD_PLAY  = "LECTURE"
        vcmddesc.CMD_PLAY_DESC  = "Play the sequence in the corresponding program ID."
        vcmddesc.CMD_STOP  = "STOP"
        vcmddesc.CMD_STOP_DESC  = "Stop the playback sequence."
        vcmddesc.CMD_SKIP  = "PASSER"
        vcmddesc.CMD_SKIP_DESC  = "Skip the playback sequence."
        
        vcmddesc.CMD_POWER  = "ACTIVER"
        vcmddesc.CMD_POWER_DESC  = "Power on"
        vcmddesc.CMD_LAMP  = "LAMPE"
        vcmddesc.CMD_LAMP_DESC  = "Turn on the feeding light or change the brightness."
    }
    
    public class VCmdHelpData {
        public var sVCmd : String = ""
        public var sVCmdDesc : String = ""

        init() {
            initVCmdHelpData()
        }
        
        public func initVCmdHelpData() {
            sVCmd = ""
            sVCmdDesc = ""
        }
    }

    
    func initVCmdDataList() {
        
        if let languageCode = (Locale.current as NSLocale).object(forKey: .languageCode) as? String {
            print("languageCode:\(languageCode)")
            if languageCode == "ko" {
                initVCmdPhraseKR()
            } else if languageCode == "fr" {
                initVCmdPhraseFR()
            } else {
                initVCmdPhraseEN()
            }
        }
        
        arrData.removeAll()
        
        addVCmdData(VCmdPhrase.CMD_MODE_CHANGE, VcmdDesc: vcmddesc.CMD_MODE_CHANGE_DESC)

        addVCmdData(VCmdPhrase.CMD_PRESSURE_UP, VcmdDesc: vcmddesc.CMD_PRESSURE_UP_DESC)
        addVCmdData(VCmdPhrase.CMD_PRESSURE_DOWN, VcmdDesc: vcmddesc.CMD_PRESSURE_DOWN_DESC)

        addVCmdData(VCmdPhrase.CMD_CYCLE_UP, VcmdDesc: vcmddesc.CMD_CYCLE_UP_DESC)
        addVCmdData(VCmdPhrase.CMD_CYCLE_DOWN, VcmdDesc: vcmddesc.CMD_CYCLE_DOWN_DESC)

        addVCmdData(VCmdPhrase.CMD_PLAY, VcmdDesc: vcmddesc.CMD_PLAY_DESC)
        addVCmdData(VCmdPhrase.CMD_STOP, VcmdDesc: vcmddesc.CMD_STOP_DESC)
        addVCmdData(VCmdPhrase.CMD_SKIP, VcmdDesc: vcmddesc.CMD_SKIP_DESC)
        addVCmdData(VCmdPhrase.CMD_PROGRAMID_N, VcmdDesc: vcmddesc.CMD_PROGRAMID_N_DESC)

        addVCmdData(VCmdPhrase.CMD_POWER, VcmdDesc: vcmddesc.CMD_POWER_DESC)
        addVCmdData(VCmdPhrase.CMD_LAMP, VcmdDesc: vcmddesc.CMD_LAMP_DESC)
        
        tvList.reloadData()
    }

    func addVCmdData(_ a_VCmd : String, VcmdDesc a_VCmdDesc : String) {
        let vCmdHelpData = VCmdHelpData()
        vCmdHelpData.sVCmd = a_VCmd
        vCmdHelpData.sVCmdDesc = a_VCmdDesc
        arrData.append(vCmdHelpData)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tvList.bounces = false
        tvList.separatorStyle = UITableViewCell.SeparatorStyle.none
        tvList.separatorColor = UIColor.orange
        tvList.allowsSelection = true
        tvList.isScrollEnabled = true
        tvList.backgroundColor = UIColor.clear
        tvList.delegate = self
        tvList.dataSource = self
        tvList.register(UITableViewCell.self, forCellReuseIdentifier: "mycell")

        initVCmdDataList()
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
//        VCmdPhrase.initVCmdPhrase(PBCommon.curSpeechLanguage)
    }
    
    @IBAction func onCloseViewController(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tablelist.kTableViewCellH
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "mycell", for: indexPath)
        
        cell.backgroundColor = UIColor.white
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        for vSub in cell.contentView.subviews {
            if vSub.tag >= 100 {
                vSub.removeFromSuperview()
            }
        }

        let aVcmdData = arrData[indexPath.row]

        let sizeView = self.view.frame.size
        
        let vBase = UIView(frame: CGRect(x: 0, y: 0, width: sizeView.width, height: tablelist.kTableViewCellH))
        vBase.backgroundColor = UIColor.white
        vBase.tag = 999
        cell.contentView.addSubview(vBase)
        
        let sizeBase = vBase.frame.size
        let fTextW : CGFloat = sizeBase.width / 4

        let lblCmd = UILabel(frame: CGRect(x: 0, y: 0, width: fTextW, height: tablelist.kTableViewCellH))
        lblCmd.backgroundColor = UIColor.clear
        lblCmd.font = PBLib.BoldFont(16)
        lblCmd.textColor = PBLib.RGB([0xe6, 0x62, 0x72])
        lblCmd.text = aVcmdData.sVCmd
        lblCmd.textAlignment = NSTextAlignment.center
        lblCmd.tag = 999
        vBase.addSubview(lblCmd)

        let lblCmdDesc = UILabel(frame: CGRect(x: fTextW, y: 0, width: sizeBase.width-fTextW, height: tablelist.kTableViewCellH))
        lblCmdDesc.backgroundColor = UIColor.clear
        lblCmdDesc.font = PBLib.AppFont(16)
        lblCmdDesc.textColor = PBLib.RGB([51,51,51])
        lblCmdDesc.text = aVcmdData.sVCmdDesc
        lblCmdDesc.numberOfLines = 2
//        lblCmdDesc.textAlignment = NSTextAlignment.center
        lblCmdDesc.tag = 999
        vBase.addSubview(lblCmdDesc)

        let vLine01 = UIView(frame: CGRect(x: 0, y: sizeBase.height - 1, width: sizeBase.width, height: 1))
        vLine01.backgroundColor = GLColor.kThemeLine
        vLine01.tag = 999
        cell.contentView.addSubview(vLine01)

        return cell
    }

}
