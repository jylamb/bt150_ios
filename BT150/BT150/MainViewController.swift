//
//  MainViewController.swift
//  BT150
//
//  Created by KOBONGHWAN on 14/11/2018.
//  Copyright © 2018 bistos. All rights reserved.
//

import UIKit
import CoreBluetooth

extension String {
    func index2(from: Int) -> Index {
        return self.index(startIndex, offsetBy: from)
    }

    func substring2(from: Int) -> String {
        let fromIndex = index(from: from)
        return String(self[fromIndex...])
    }

    func substring2(to: Int) -> String {
        let toIndex = index(from: to)
        return String(self[..<toIndex])
    }

    func substring2(with r: Range<Int>) -> String {
        let startIndex = index(from: r.lowerBound)
        let endIndex = index(from: r.upperBound)
        return String(self[startIndex..<endIndex])
    }
}

class MainViewController: UIViewController, TIOManagerDelegate, TIOPeripheralDelegate, PBDatePickerDelegate, BLEScanViewDelegate, SettingsViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIAlertViewDelegate {
    
    var hud : MBProgressHUD?
    
    var vPopPhoto = UIView()
    var dpDate = PBDatePicker()
    var dpBirth = UIDatePicker()
    var dfShow = DateFormatter()
    var vcBLEScan = BLEScanViewController()
    var vcSettings = SettingsViewController()
    var mTimerStatusCheck : Timer?
    var mTimerProgramData : Timer?
    var mTimerFWUp : Timer?
    var mTimerFWDataBlock : Timer?
    var alertFW : UIAlertController?
    var progressView : UIProgressView?
    var nProgressStatus : UInt8 = 0

    var nCntElapsedTime : Int = 0
    var nCntElapsedBattery : Int = 0
    var nCntElapsedProgramProcess : Int = 0
    
    var centralManager: CBCentralManager?
    
    var bActivityShow : Bool = false
    var bAlreayConnected = false
    var bFirst : Bool = true
    var bFWUpMode : Bool = false

    @IBOutlet weak var imgvPhoto: UIImageView!
    @IBOutlet weak var btnFemale: UIButton!
    @IBOutlet weak var btnMale: UIButton!
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblBirthday: UILabel!
    @IBOutlet weak var lblDaysFromBirth: UILabel!
    
    @IBOutlet weak var btnBLEStatus: UIButton!
    @IBOutlet weak var btnVCmdInfo: UIButton!
    
    var txtLog = UITextView()
    var imgvSTT = UIImageView()
    
    @IBAction func unwindToMainViewController(segue: UIStoryboardSegue) {
        print("### unwindToMainViewController ###")
        bActivityShow = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let segid = segue.identifier {
            if segid == "SEG_MAIN_MENU01" {
                bActivityShow = false
            } else if segid == "SEG_MAIN_MENU02" {
                bActivityShow = false
            } else if segid == "SEG_MAIN_MENU03" {
                bActivityShow = false
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dfShow.dateFormat = "yyyy-MM-dd"
//        dfShow.locale = Locale(identifier: "ko_KR")
//        dfShow.timeZone = TimeZone(identifier: "GMT")!

        TIOManager.sharedInstance()?.delegate = self
                
        initEditPhoto()
        
        let sizeView = self.view.frame.size
        
        vcBLEScan.view.frame = CGRect(x: 0, y: 0, width: sizeView.width, height: sizeView.height)
        vcBLEScan.delegate = self
        self.view.addSubview(vcBLEScan.view)
        vcBLEScan.view.isHidden = true
        
        vcSettings.view.frame = CGRect(x: 0, y: 0, width: sizeView.width, height: sizeView.height)
        vcSettings.delegate = self
        self.view.addSubview(vcSettings.view)
        vcSettings.view.isHidden = true
        
        lblUserName.adjustsFontSizeToFitWidth = true
        lblUserName.numberOfLines = 3
        lblUserName.contentMode = .bottomRight
        lblUserName.lineBreakMode = .byCharWrapping
        
        lblDaysFromBirth.numberOfLines = 2
                
        PBCommon.loadDefaultProgramData_20210325()

        loadSavedInfo()
        
        if PBCommon.getLocaleLanguage() == "fr" {
            btnVCmdInfo.isHidden = true
        } else {
            btnVCmdInfo.isHidden = false
        }
        
        PBCommon.doRecvParsingThread()
        PBCommon.doSendiingDataThread()
        
        hud = MBProgressHUD(view: self.view)
        hud?.opacity = 0.6
        self.view.addSubview(hud!)
        
        txtLog.frame = CGRect(x: 0, y: 500, width: self.view.frame.width, height: self.view.frame.height-550)
//        self.view.addSubview(txtLog)
        
        initSTTStatusView()
    }
    
    func initSTTStatusView() {
        
        let sizeView = self.view.frame
        let aSTTW : CGFloat = 48
        let aSTTH : CGFloat = 48
        
        imgvSTT.image = UIImage(named: "ic_mic_off_48pt.png")
        imgvSTT.frame = CGRect(x: sizeView.width-aSTTW*1.5, y: sizeView.height-aSTTH*2, width: aSTTW, height: aSTTH)
        self.view.addSubview(imgvSTT)
        imgvSTT.isHidden = true
    }
    
    func scrollTextViewToBottom(textView: UITextView) {
        if textView.text.count > 0 {
            let location = textView.text.count - 1
            let bottom = NSMakeRange(location, 1)
            textView.scrollRangeToVisible(bottom)
        }
    }
            
    @objc func onCmdDisconnect(_ a_Noti : NSNotification) {
        doDisconnectPeripheral()
    }

    @objc func onVoiceLog(_ a_Noti : NSNotification) {
        if let aData = a_Noti.userInfo?["String"] as? String {
            txtLog.text = "\(txtLog.text ?? "")\n\(aData)"
            scrollTextViewToBottom(textView: txtLog)
        }
    }
    
    @objc func onSTTTask(_ a_Noti : NSNotification) {
        if let aAction = a_Noti.userInfo?["Action"] as? String {
            if aAction == "Stop" {
                imgvSTT.isHidden = false
            } else {
                imgvSTT.isHidden = true
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        
        PBCommon.nActivityNo = PBCommon.ACT_MAIN
        
        if PBCommon.sGender == "male" {
            self.btnMale.isSelected = true
            self.btnFemale.isSelected = false
        
        } else if PBCommon.sGender == "female" {
            self.btnMale.isSelected = false
            self.btnFemale.isSelected = true
        
        } else {
            self.btnMale.isSelected = false
            self.btnFemale.isSelected = false
        }
        
//        if !PBCommon.bMainVCmdReady {
//            PBCommon.bMainVCmdReady = true
            NotificationCenter.default.addObserver(self, selector: #selector(onRecvData(_:)), name: Notification.Name("MSG_DATA_RECV"), object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(onSRRsult(_:)), name: Notification.Name("MSG_SR_RESULT"), object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(onVoiceLog(_:)), name: Notification.Name("MSG_LOGTO_MAIN"), object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(onCmdDisconnect(_:)), name: Notification.Name("MSG_CMD_DISCONNECT"), object: nil)
    //        NotificationCenter.default.addObserver(self, selector: #selector(onSTTTask(_:)), name: Notification.Name("MSG_STT_TASK"), object: nil)

//        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        bActivityShow = true
        if bFirst {
            bFirst = false
            onPopBLEScan()
        }
    }
    
    @objc func onSRRsult(_ a_Noti : NSNotification) {
//        print("Main onSRRsult-1")
        if let aData = a_Noti.userInfo?["String"] as? String {
            print("Main onSRRsult aData:\(aData)")
            if let aResult = a_Noti.userInfo?["Result"] as? Bool {
                if aResult {
                    Toast.show(message: aData, baseView: self.view)
                } else {
                    Toast.show(message: "Recognizing Fail:" + aData, baseView: self.view)
                }
            }
        }        
    }
    
    @objc func onRecvData(_ a_Noti : NSNotification) {
        print("onRecvData MainViewController: \(a_Noti)")
        if let _ = a_Noti.userInfo?["req_prgdata"] as? String {
            inQueryProgramData()
        }
    }
    
    func onPopBLEScan() {
        PBLib.performTransition(true, view: vcBLEScan.view)
        vcBLEScan.initControls()
    }
    
    func onPopSettings() {
        PBLib.performTransition(true, view: vcSettings.view)
        vcSettings.initControls()
    }
    
    func showDialogProgress(Msg a_msg : String, Title a_title : String) {
        
        if a_msg.count > 0 {
            alertFW?.title = a_msg
        }
        
        if a_title.count > 0 {
            alertFW?.message = a_title
        }
        
        progressView?.progress = Float(nProgressStatus)/Float(100)
        progressView?.tintColor = UIColor.blue
    }

    
    func initDialogProgress(Msg a_msg : String, Title a_title : String) {
        //  Just create your alert as usual:
        alertFW = UIAlertController(title: a_msg, message: a_title, preferredStyle: .alert)
        alertFW?.addAction(UIAlertAction(title: NSLocalizedString("CANCEL", comment: "CANCEL"), style: .cancel, handler: nil))

        //  Show it to your users
        present(alertFW!, animated: true, completion: {
            //  Add your progressbar after alert is shown (and measured)
            let margin:CGFloat = 8.0
            let rect = CGRect(x: margin, y: 110.0, width: self.alertFW!.view.frame.width - margin * 2.0 , height: 2.0)
            self.progressView = UIProgressView(frame: rect)
            self.progressView?.progress = Float(self.nProgressStatus)
            self.progressView?.tintColor = UIColor.blue
            self.alertFW?.view.addSubview(self.progressView!)
        })
    }
    
    func loadProgramData(_ a_TabIdx : Int) {
        
        for idx in 0 ..< 8 {
            if let aData = UserDefaults.standard.object(forKey: "programdata_\(a_TabIdx)_\(idx)") as? String {
                print("loadProgramData a_TabIdx:\(a_TabIdx) idx:\(idx) Data: \(aData)")
                if aData.count == 17 {
                    PBCommon.parseProgramData(aData)
                }
            }
        }
        
    }
    
    func loadProgramCntOpType() {
        
        if let aData = UserDefaults.standard.object(forKey: "program_cnt_optype") as? String {
            if aData.count == 9 {
                nPrgReqCnt5 = Int(aData.substring(with: 1..<2)) ?? 0
                nPrgReqCnt6 = Int(aData.substring(with: 2..<3)) ?? 0
                nPrgReqCnt7 = Int(aData.substring(with: 3..<4)) ?? 0
                nPrgReqCnt8 = Int(aData.substring(with: 4..<5)) ?? 0
                
                nPrgOpType5 = Int(aData.substring(with: 5..<6)) ?? 0
                nPrgOpType6 = Int(aData.substring(with: 6..<7)) ?? 0
                nPrgOpType7 = Int(aData.substring(with: 7..<8)) ?? 0
                nPrgOpType8 = Int(aData.substring(with: 8..<9)) ?? 0
                
                print("loadProgramCntOpType: nPrgOpType5:\(nPrgOpType5) nPrgOpType6:\(nPrgOpType6) nPrgOpType7:\(nPrgOpType7) nPrgOpType8:\(nPrgOpType8)")
                print("loadProgramCntOpType: nPrgReqCnt5:\(nPrgReqCnt5) nPrgReqCnt6:\(nPrgReqCnt6) nPrgReqCnt7:\(nPrgReqCnt7) nPrgReqCnt8:\(nPrgReqCnt8)")
            }
        }
    }
    
    func loadSavedInfo() {
                
//        if PBCommon.getLocaleLanguage() != "fr" {
            loadProgramData(4)
            loadProgramData(5)
//        }
        loadProgramData(6)
        loadProgramData(7)
        
        loadProgramCntOpType()
                
        if let aRet = UserDefaults.standard.object(forKey: "userphoto") as? Data {
            PBCommon.sUserPhoto = UIImage(data: aRet)!
            imgvPhoto.layer.cornerRadius = imgvPhoto.frame.size.width/2
            imgvPhoto.layer.masksToBounds = true
            imgvPhoto.image = PBCommon.sUserPhoto
        }

        if let aRet = UserDefaults.standard.object(forKey: "username") as? String {
            if aRet.count > 0 {
                PBCommon.sUserName = aRet
                lblUserName.text = aRet
            }
        }

        if let aRet = UserDefaults.standard.object(forKey: "birthday") as? Date {
            PBCommon.dtBirthday = aRet
            lblBirthday.text = dfShow.string(from: PBCommon.dtBirthday)
            lblDaysFromBirth.text = "\(getNumberOfDaysFromBirth()) \(NSLocalizedString("DaysFromBirth", comment: ""))"
        }

        if let aRet = UserDefaults.standard.object(forKey: "gender") as? String {
            if aRet.count > 0 {
                PBCommon.sGender = aRet
                if PBCommon.sGender == "male" {
                    self.btnMale.isSelected = true
                    self.btnFemale.isSelected = false
                
                } else if PBCommon.sGender == "female" {
                    self.btnMale.isSelected = false
                    self.btnFemale.isSelected = true
                
                } else {
                    self.btnMale.isSelected = false
                    self.btnFemale.isSelected = false
                }
                
            }
        }
        
        PBCommon.nProgramLastTime = 30*60
        if let aRet = UserDefaults.standard.object(forKey: "program_last_time") as? String {
            if aRet.count > 0 {
                PBCommon.nProgramLastTime = Int(aRet)!
            }
        }
        
        if let aRet = UserDefaults.standard.object(forKey: "vcmd_language") as? String {
            PBCommon.curSpeechLanguage = aRet
        }
        
        if let aRet = UserDefaults.standard.object(forKey: "vcmd_enable") as? String {
            PBCommon.sUseVCmd = aRet
        }
        
        VCmdPhrase.initVCmdPhrase(PBCommon.curSpeechLanguage)
        
        VCmdControl.initVoiceCommand()
        VCmdControl.initAddingPhrase(PBCommon.curSpeechLanguage)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func onVCmdInfo(_ sender: Any) {
        performSegue(withIdentifier: "SEG_VCMD_HELP", sender: self)
    }

    @IBAction func onSettings(_ sender: Any) {
        onPopSettings()
    }
    
    @IBAction func onDelPhoto(_ sender: Any) {
        PBCommon.sUserPhoto = UIImage(named: "main_ic_photo.png")!
        imgvPhoto.image = PBCommon.sUserPhoto
        UserDefaults.standard.removeObject(forKey: "userphoto")
    }
    
    @IBAction func onRegPhoto(_ sender: Any) {
        onEditPhoto()
    }
    
    
    @IBAction func onPopUserName(_ sender: Any) {
        handleInputAlert()
    }
    
    @IBAction func onPopBirthday(_ sender: Any) {

        if #available(iOS 14, *) {
            let alertController = UIAlertController(title: NSLocalizedString("", comment: "Birthday"), message: "\n\n\n\n\n\n\n\n\n", preferredStyle: UIAlertController.Style.actionSheet)
//            let dpBirth = UIDatePicker(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 100))
            dpBirth.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 100)
            let gregorian = Calendar(identifier: Calendar.Identifier.gregorian)
            var components : DateComponents = DateComponents()
            (components as NSDateComponents).calendar = gregorian
            components.year = -90
            components.year = 50
            dpBirth.translatesAutoresizingMaskIntoConstraints = false
            dpBirth.preferredDatePickerStyle = .wheels
            dpBirth.datePickerMode = .date
            dpBirth.calendar = gregorian
            dpBirth.date = PBCommon.dtBirthday
            alertController.view.addSubview(dpBirth)
            
            let saveAction = UIAlertAction(title: NSLocalizedString("YES", comment: "YES"), style: UIAlertAction.Style.default, handler: { alert -> Void in
                self.setDateValue(self.dpBirth.date, a_Tag: 0)
            })
            let cancelAction = UIAlertAction(title: NSLocalizedString("NO", comment: "NO"), style: UIAlertAction.Style.default, handler: {
                (action : UIAlertAction!) -> Void in })
            
            alertController.addAction(saveAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        
        } else {
            self.view.endEditing(true)
            dpDate.cancelPicker()
            dpDate.delegate = self
            dpDate.setDateValue(PBCommon.dtBirthday, a_Tag: 0)
            dpDate.initPBDatePicker(self.view)
        }
    }
    
    
    @IBAction func onFemale(_ sender: Any) {
        self.btnMale.isSelected = false
        self.btnFemale.isSelected = true
        PBCommon.sGender = "female"
        UserDefaults.standard.set(PBCommon.sGender, forKey: "gender")
    }
    
    @IBAction func onMale(_ sender: Any) {
        self.btnMale.isSelected = true
        self.btnFemale.isSelected = false
        PBCommon.sGender = "male"
        UserDefaults.standard.set(PBCommon.sGender, forKey: "gender")
    }
    
    @IBAction func onGoOperation(_ sender: Any) {
        if PBCommon.bRecvProgramCnt && !PBCommon.bProcessingProgramData {
            bActivityShow = false
            performSegue(withIdentifier: "SEG_MAIN_MENU01", sender: self)
        }
    }

    @IBAction func onGoProgram(_ sender: Any) {
//        if PBCommon.bRecvProgramCnt && !PBCommon.bProcessingProgramData {
            bActivityShow = false
            performSegue(withIdentifier: "SEG_MAIN_MENU02", sender: self)
//        }
    }

    @IBAction func onGoStatistics(_ sender: Any) {
        bActivityShow = false
        performSegue(withIdentifier: "SEG_MAIN_MENU03", sender: self)
    }

    @IBAction func onPopBLE(_ sender: Any) {
        print("onPopBLE: PBCommon.gPeripheral?.isConnected:\(PBCommon.gPeripheral?.isConnected)")
        if (PBCommon.gPeripheral?.isConnected ?? false) {
            let alert = UIAlertView(title: "", message: NSLocalizedString("AlreadyConnected", comment: "Already Connected"), delegate: self, cancelButtonTitle: NSLocalizedString("NO", comment: "NO"), otherButtonTitles: NSLocalizedString("YES", comment: "YES"))
            alert.tag = 100
            alert.show()

        } else {
            onPopBLEScan()
        }
    }
    
    func handleInputAlert() {
        
        let alertController = UIAlertController(title: NSLocalizedString("Name", comment: "Name"), message: "", preferredStyle: UIAlertController.Style.alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = NSLocalizedString("InsertUserName", comment: "Insert User Name")
        }
        let saveAction = UIAlertAction(title: NSLocalizedString("YES", comment: "YES"), style: UIAlertAction.Style.default, handler: { alert -> Void in
            let aUserName = alertController.textFields![0] as UITextField
            PBCommon.sUserName = aUserName.text ?? ""
            self.lblUserName.text = aUserName.text
            UserDefaults.standard.set(PBCommon.sUserName, forKey: "username")

        })
        let cancelAction = UIAlertAction(title: NSLocalizedString("NO", comment: "NO"), style: UIAlertAction.Style.default, handler: {
            (action : UIAlertAction!) -> Void in })
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func onEditPhoto() {
        PBLib.performTransition(true, view: vPopPhoto, type: CATransitionType.moveIn.rawValue, subtype: CATransitionSubtype.fromTop.rawValue)
    }

    func initEditPhoto() {
        let sizeView = self.view.frame.size
        let fW : CGFloat = 530/2
        let fH : CGFloat = 426/2
        
        vPopPhoto.frame = CGRect(x: 0, y: 0, width: sizeView.width, height: sizeView.height)
        vPopPhoto.backgroundColor = PBLib.RGBA([0,0,0,0.7])
        self.view.addSubview(vPopPhoto)
        vPopPhoto.isHidden = true
        
        let imgvPop = UIImageView(image: UIImage(named: "pop_upload_bg.png"))
        imgvPop.frame = CGRect(x: (sizeView.width-fW)/2, y: (sizeView.height-fH)/2-20, width: fW, height: fH)
        imgvPop.isUserInteractionEnabled = true
        vPopPhoto.addSubview(imgvPop)
        
        let lblTitle = UILabel(frame: CGRect(x: 0, y: 0, width: fW, height: fH/4))
        lblTitle.backgroundColor = UIColor.clear
        lblTitle.textColor = PBLib.RGB([200, 38, 78])
        lblTitle.font = PBLib.AppFont(18)
        lblTitle.textAlignment = NSTextAlignment.center
        lblTitle.text = NSLocalizedString("select_photo", comment: "select photo")
        imgvPop.addSubview(lblTitle)
        
        let btnAlbum = UIButton(frame: CGRect(x: 0, y: fH/4, width: fW, height: fH/4))
        btnAlbum.backgroundColor = UIColor.clear
        btnAlbum.titleLabel?.font = PBLib.AppFont(18)
        btnAlbum.setTitle(NSLocalizedString("select_photo_album", comment: "select photo album"), for: UIControl.State())
        btnAlbum.setTitleColor(PBLib.RGB([51, 51, 51]), for: UIControl.State())
        btnAlbum.addTarget(self, action: #selector(self.onAlbum), for: UIControl.Event.touchUpInside)
        imgvPop.addSubview(btnAlbum)
        
        let btnCamera = UIButton(frame: CGRect(x: 0, y: (fH/4)*2, width: fW, height: fH/4))
        btnCamera.backgroundColor = UIColor.clear
        btnCamera.titleLabel?.font = PBLib.AppFont(18)
        btnCamera.setTitle(NSLocalizedString("select_photo_camera", comment: "select photo camera"), for: UIControl.State())
        btnCamera.setTitleColor(PBLib.RGB([51, 51, 51]), for: UIControl.State())
        btnCamera.addTarget(self, action: #selector(self.onCamera), for: UIControl.Event.touchUpInside)
        imgvPop.addSubview(btnCamera)
        
        let btnCancel = UIButton(frame: CGRect(x: 0, y: (fH/4)*3, width: fW, height: fH/4))
        btnCancel.backgroundColor = UIColor.clear
        btnCancel.titleLabel?.font = PBLib.AppFont(18)
        btnCancel.setTitle(NSLocalizedString("select_photo_cancel", comment: "select photo cancel"), for: UIControl.State())
        btnCancel.setTitleColor(PBLib.RGB([51, 51, 51]), for: UIControl.State())
        btnCancel.addTarget(self, action: #selector(self.onCancel), for: UIControl.Event.touchUpInside)
        imgvPop.addSubview(btnCancel)
    }
    
    @objc func onCancel() {
        PBLib.performTransition(false, view: vPopPhoto)
    }

    @objc func onCamera() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerController.SourceType.camera
        imagePicker.videoQuality = UIImagePickerController.QualityType.typeMedium
        imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: imagePicker.sourceType)!
        imagePicker.videoMaximumDuration = 20
//        imagePicker.allowsEditing = true
        present(imagePicker, animated: true, completion: nil)
    }
    
    @objc func onAlbum() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }

    //MARK:- UIImagePickerControllerDelegate
//    private func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    
        PBLib.performTransition(false, view: vPopPhoto)
        
        if let imgSel = info[.originalImage] as? UIImage {
            let imgResize = PBLib.resizeImageWithMax(imgSel, a_Max: 800)
            imgvPhoto.image = imgResize
            imgvPhoto.layer.cornerRadius = imgvPhoto.frame.size.width/2
            imgvPhoto.layer.masksToBounds = true
            PBCommon.sUserPhoto = imgResize
            
            UserDefaults.standard.set(imgResize.pngData(), forKey: "userphoto")
        }
       
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        PBLib.performTransition(false, view: vPopPhoto)
        
        picker.dismiss(animated: true, completion: nil)
    }

    //MARK: - PBDatePickerDelegate
    func onCloseDatePicker(_ date: Date, tag: Int) {
        if tag >= 0 {
            setDateValue(date, a_Tag: tag)
        }        
    }
    
    func setDateValue(_ a_Date : Date, a_Tag : Int) {
        PBCommon.dtBirthday = a_Date
        
        lblBirthday.text = dfShow.string(from: PBCommon.dtBirthday)
        lblDaysFromBirth.text = "\(getNumberOfDaysFromBirth()) \(NSLocalizedString("DaysFromBirth", comment: ""))"
        
        UserDefaults.standard.set(PBCommon.dtBirthday, forKey: "birthday")
    }
    
    func getNumberOfDaysFromBirth() -> Int {
        var aDays = 0
        let calendar = Calendar.current
        let dateNow = calendar.startOfDay(for: Date())
        let dateBirth = calendar.startOfDay(for: PBCommon.dtBirthday)
        let components = calendar.dateComponents([.day], from: dateBirth, to: dateNow)
        if let aVal = components.day {
            if aVal > 0 {
                aDays = aVal
            }
        }
        return aDays
    }

    //MARK: - TIOManagerDelegate
    func tioManager(_ central: CBCentralManager!, willRestoreState peripheral: TIOPeripheral!) {
        print("################ willRestoreState ################")
        bAlreayConnected = true
//        setPeripheral(peripheral)
//        peripheral.connect()
//
//        PBLib.performTransition(false, view: vcBLEScan.view)
    }
    
//    func tioManager(_ central: CBCentralManager!, willRestoreState dict: [AnyHashable : Any]!) {
//
//        if let aPerpherals = dict["kCBRestoredPeripherals"] as? [CBPeripheral] {
//            for aPeripheral in aPerpherals {
//                if aPeripheral.state == CBPeripheralState.connected {
//                    if let aDevName = aPeripheral.name {
//                        if aDevName.contains(find: "BM+S") {
//                            PBLib.performTransition(false, view: vcBLEScan.view)
//                        }
//                    }
////                    print("################### willRestoreState cancelPeripheralConnection: \(String(describing: aPeripheral.name))")
////                    central.cancelPeripheralConnection(aPeripheral)
//                }
//            }
//        }
//    }
    
    func tioManagerBluetoothAvailable(_ manager: TIOManager!) {
        print("tioManagerBluetooth Available")
        PBCommon.bBLEAvailable = true
    }
    
    func tioManagerBluetoothUnavailable(_ manager: TIOManager!) {
        print("tioManagerBluetooth Unavailable")
        PBCommon.bBLEAvailable = false
        PBLib.showAlert(NSLocalizedString("BlutoothIsOff", comment: "Blutooth Is Off"))
        updateConnectionState(NSLocalizedString("DeviceIsNotConnected", comment: "Device Is Not Connected"))
        PBCommon.gPeripheral = nil
    }
    
    func tioManager(_ manager: TIOManager!, didDiscover peripheral: TIOPeripheral!) {
//        print("tioManager didDiscover !!!!!!")
        vcBLEScan.refreshList()
        
        printBLEPeripheral()
    }
    
    func tioManager(_ manager: TIOManager!, didRetrievePeripheral peripheral: TIOPeripheral!) {
//        print("tioManager didRetrievePeripheral !!!!!!")
        vcBLEScan.refreshList()
        
        printBLEPeripheral()
    }
    
func updateUIState() {
        var aDevName = "Device"
        if let aName = PBCommon.gPeripheral?.name {
            aDevName = aName
        }
//        if (PBCommon.gPeripheral?.isConnecting ?? false) {
//            updateConnectionState("\(aDevName) Connecting!")
        
        if (PBCommon.gPeripheral?.isConnected ?? false) {
            updateConnectionState("\(aDevName) \(NSLocalizedString("isConnected", comment: "is Connected"))")
        
        } else {
            updateConnectionState(NSLocalizedString("DeviceIsNotConnected", comment: "Device Is Not Connected"))
        }
    }
    
    func updateConnectionState(_ a_Text : String) {
        btnBLEStatus.setTitle(a_Text, for: UIControl.State.normal)
        btnBLEStatus.setTitle(a_Text, for: UIControl.State.selected)
        btnBLEStatus.setTitle(a_Text, for: UIControl.State.highlighted)
    }
    
    //MARK : - BLEScanViewDelegate
    func onPeripheralCellPressed(_ a_Peripheral: TIOPeripheral) {
        if let aName = a_Peripheral.name {
            print("onPeripheralCellPressed name:\(aName)")
            var sDeviceName = ""
            if aName.count > 10 {
                var aS = aName.index(aName.startIndex, offsetBy: 1)
                var aE = aName.index(aName.startIndex, offsetBy: 3)
                let aPrefix = "\(aName[aS..<aE])"
                var aSerial = ""
                if aPrefix.lowercased() == "hb" {
                    aS = aName.index(aName.startIndex, offsetBy: 7)
                    aE = aName.index(aName.startIndex, offsetBy: 11)
                    aSerial = "\(aName[aS..<aE])"
                } else if aPrefix.lowercased() == "hi" {
                    aS = aName.index(aName.startIndex, offsetBy: 9)
                    aE = aName.index(aName.startIndex, offsetBy: 11)
                    let aHex = "\(aName[aS..<aE])"
                    if let aIntVal = Int(aHex, radix: 16) {
                        aSerial = String(format: "%04d", aIntVal)
                    }
                }
                print("onPeripheralCellPressed aSerial:\(aSerial)")
                sDeviceName = "Hi Bebe Breast Pump – \(aSerial)"
            } else {
                sDeviceName = aName
            }
            UserDefaults.standard.set(sDeviceName, forKey: "device_name")
        }
        if let aUUID = a_Peripheral.identifier {
            print("onPeripheralCellPressed uuid:\(a_Peripheral.identifier!)")
            UserDefaults.standard.set("\(aUUID)", forKey: "device_addr")
        }
        
        setPeripheral(a_Peripheral)
        
        print("onPeripheralCellPressed: a_Peripheral.isConnected:\(a_Peripheral.isConnected)")
        if bAlreayConnected {
            bAlreayConnected = false
            PBLib.showAlert("BLE Connection is not cleared. Please reconnect!")
        } else {
            a_Peripheral.connect()
        }
    }
    
    func setPeripheral(_ a_Peripheral: TIOPeripheral) {
        a_Peripheral.delegate = self
        PBCommon.gPeripheral = a_Peripheral
    }
    
    func printBLEPeripheral() {
//        print("##################### printBLEPeripheral #####################")
        for aVal in (TIOManager.sharedInstance()?.peripherals)! {
            let aPeripheral = aVal as! TIOPeripheral
            print("\(String(describing: aPeripheral.name))")
        }
    }

    //MARK : - TIOPeripheralDelegate
    func tioPeripheralDidConnect(_ peripheral: TIOPeripheral!) {
        print("tioPeripheralDidConnect:-!!!!!!!!!!!!!!!!!!")
        
        setPeripheral(peripheral)
        
//        if((mConnectingDialog != null) && mConnectingDialog.isShowing()) {
//            mConnectingDialog.dismiss();
//        };

        PBCommon.nOperationModeCur = PBCommon.OP_MODE_NONE
        PBCommon.nExpressionTimeS = 0
        PBCommon.nElapsedTime = 0
        PBCommon.bProgramDataLoaded = false
        PBCommon.gRecvData = ""
        PBCommon.gRecvBattery = ""
        PBCommon.bDeviceWakeUp = true
        
        updateUIState()

        if bFWUpMode {
            initDialogProgress(Msg: NSLocalizedString("FWUpWarning", comment: "FWUpWarning"), Title: NSLocalizedString("FWUpgrade", comment: "FWUpgrade"))
            doFWUpgrade()
            
        } else {
            startCheckDeviceStatusTimer()
        }
        
        

        if !peripheral.shallBeSaved {
            peripheral.shallBeSaved = true
            TIOManager.sharedInstance()?.savePeripherals()
        }
    }
    
    func stopStatusTimer() {
        mTimerStatusCheck?.invalidate()
        mTimerStatusCheck = nil
    }
    
    func startCheckDeviceStatusTimer() {
        
//        PBCommon.sendData(PBCommon.sCmdElapsedTime)        
//        PBCommon.sendData(PBCommon.sCmdInqueryStatus)
        
        stopStatusTimer()
        
        mTimerStatusCheck = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(taskStatusCheck), userInfo: nil, repeats: true)
    }
    
    @objc func taskStatusCheck() {
        
        if (PBCommon.gPeripheral?.isConnected ?? false) {
            
            if (!PBCommon.bProgramDataLoaded) {
                PBCommon.bProgramDataLoaded = true
                
                PBCommon.sendProgramDataAll()
                
                DispatchQueue.main.async {
                    if PBCommon.getLocaleLanguage() != "fr" {
                        gPBSR.startTransribing()
                    }
                    
                    self.nCntElapsedProgramProcess = 0
                    PBCommon.bProcessingProgramData = false
                }
                
//                mTimerProgramData?.invalidate()
//                mTimerProgramData = nil
//                mTimerProgramData = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(taskProgramData), userInfo: nil, repeats: false)

                
//                hud?.labelText = ""
//                hud?.show(true)
//
//                nCntElapsedProgramProcess += 1
//
//                if nCntElapsedProgramProcess == 1 {
//                    PBCommon.sendData("A;")
//                    PBCommon.bRecvProgramCnt = false
//
//                } else if nCntElapsedProgramProcess == 2 {
//                    PBCommon.sendData("A;")
//                    PBCommon.bProcessingProgramData = true
//                    nCntElapsedProgramProcess = 0
//                }
                
                PBCommon.bRecvProgramCnt = true
                
            } else {
                if !PBCommon.bRecvProgramCnt {
                    doDisconnectPeripheral()
                }
                
                if !PBCommon.bProcessingProgramData && nCntElapsedProgramProcess > 60*5 {
                    //상태체크
                    PBCommon.bProcessingProgramData = false
                    nCntElapsedProgramProcess = 0
                    
                    PBCommon.sendData(PBCommon.sCmdInqueryStatus)
                }
                nCntElapsedProgramProcess += 1

                if (nCntElapsedTime > 5 || PBCommon.nElapsedTime == 0) {
                    nCntElapsedTime = 0;
                    PBCommon.sendData(PBCommon.sCmdElapsedTime)
                }
                nCntElapsedTime += 1

                if (nCntElapsedBattery > 7 || PBCommon.nElapsedTime == 0) {
                    nCntElapsedBattery = 0
                    PBCommon.sendData(PBCommon.sCmdInqueryBattery)
                }
                nCntElapsedBattery += 1
            }
            
        } else {
            nCntElapsedProgramProcess = 0
            nCntElapsedBattery = 0
            nCntElapsedTime = 0
        }
    }
    
    func tioPeripheral(_ peripheral: TIOPeripheral!, didReceiveUARTData data: Data!) {
        let aRecv = String(decoding: data, as: UTF8.self)
//        print("tioPeripheral <<<<<<<<<<<< didReceiveUARTData: \(aRecv)")
        
        if bFWUpMode {
            
            let arrData = [UInt8](data)

            var aPrint = ""
            for aByte in arrData {
                aPrint += " \( String(format: "%02x", aByte) )"
            }
            print("onDataReceived: \(aPrint)")
            
            sCmdFWRecv = arrData[0]
            
            print("sCmdFWRecv: \(String(format: "%02x", sCmdFWRecv))")
            return ;
        }
        
        _ = PBCommon.recvDataQue.Put(aRecv)
    }
    
    
    func inQueryProgramData() {
        
        showProgramDataLoading()
        
        for  i in 4 ..< arrProgramData.count {
            
            print("inQueryProgramData: nPrgReqCnt5:\(nPrgReqCnt5) nPrgReqCnt6:\(nPrgReqCnt6) nPrgReqCnt7:\(nPrgReqCnt7) nPrgReqCnt8:\(nPrgReqCnt8)")
            
            if i == 4 && nPrgReqCnt5 == 0
                || i == 5 && nPrgReqCnt6 == 0
                || i == 6 && nPrgReqCnt7 == 0
                || i == 7 && nPrgReqCnt8 == 0
                 {
                continue
            }
            
            let aData = arrProgramData[i]
            for j in 0 ..< aData.programDataList.count {
                if i == 4 && j >= nPrgReqCnt5
                    || i == 5 && j >= nPrgReqCnt6
                    || i == 6 && j >= nPrgReqCnt7
                    || i == 7 && j >= nPrgReqCnt8
                     {
                    break
                }
                PBCommon.sendData(String(format: "O%dS%d;", i, j))
            }
        }
        
        mTimerProgramData?.invalidate()
        mTimerProgramData = nil
        mTimerProgramData = Timer.scheduledTimer(timeInterval: 6, target: self, selector: #selector(taskProgramData), userInfo: nil, repeats: false)
    }
    
    @objc func taskProgramData() {
        
        DispatchQueue.main.async {
//            gPBSR.startTransribing()
            
            self.hud?.hide(true)
            self.hud?.labelText = ""
            
            self.nCntElapsedProgramProcess = 0
            PBCommon.bProcessingProgramData = false
        }
    }
    
    func showProgramDataLoading() {
//        hud?.labelText = "\(NSLocalizedString("ProgramDataLoading", comment: "Program Data Loading"))"
        hud?.detailsLabelText = "\(NSLocalizedString("ProgramDataLoading", comment: "Program Data Loading"))"
        hud?.show(true)
    }
    
    func tioPeripheral(_ peripheral: TIOPeripheral!, didFailToConnectWithError error: Error?) {
        
        var aErrMsg = ""
        if let aError = error {
            aErrMsg = aError.localizedDescription
            if aError.localizedDescription.count > 0 {
                print("Failed to connect : " + aError.localizedDescription)
            }
        }
        print("tioPeripheral didFailToConnectWithError: \(aErrMsg)")
        
        gPBSR.pauseTranscribing()
        
        PBLib.performTransition(false, view: vcBLEScan.view)
        
        PBLib.showAlert("Failed to connect device.")
     }
    
    func tioPeripheral(_ peripheral: TIOPeripheral!, didDisconnectWithError error: Error?) {
        var aErrMsg = ""
        if let aError = error {
            aErrMsg = aError.localizedDescription
        }
        print("tioPeripheral didDisconnectWithError: \(aErrMsg)")
        
        gPBSR.pauseTranscribing()
        
        PBLib.performTransition(false, view: vcBLEScan.view)
        
        if PBCommon.nOperationModeCur == PBCommon.OP_MODE_EXP {
            PBCommon.saveExpressionTime()
        }
        
        PBCommon.nOperationModeCur = PBCommon.OP_MODE_NONE
        PBCommon.bDeviceWakeUp = false
        
        updateUIState()
        
//        PBLib.showAlert("Device disconnected.")
        
        PBCommon.bProgramDataLoaded = false
        
        print("tioPeripheral didDisconnectWithError: bActivityShow:\(bActivityShow)")
        
        if bActivityShow {
            DispatchQueue.main.async {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    self.onPopBLEScan()
                }
            }
        } else {
            bFirst = true
//            performSegue(withIdentifier: "UnwindToMain", sender: self)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UNWIND_TO_MAIN"), object: nil, userInfo: nil)
        }

    }
    
    func tioPeripheral(_ peripheral: TIOPeripheral!, didWriteNumberOfUARTBytes bytesWritten: Int) {
//        print("tioPeripheral didWriteNumberOfUARTBytes: bytesWritten: \(bytesWritten)")
    }
    
    func tioPeripheralUARTWriteBufferEmpty(_ peripheral: TIOPeripheral!) {
//        print("tioPeripheral tioPeripheralUARTWriteBufferEmpty")
    }
    
    func tioPeripheralDidUpdateAdvertisement(_ peripheral: TIOPeripheral!) {
//        print("tioPeripheral tioPeripheralDidUpdateAdvertisement:\(String(describing: peripheral.name))")
    }

    
    //MARK: - UIAlertViewDelegate
    func alertView(_ alertView: UIAlertView, didDismissWithButtonIndex buttonIndex: Int) {
        if alertView.tag == 100 {
            if buttonIndex == 1 {
                doDisconnectPeripheral()
            }
            
        }
    }
    
    func doDisconnectPeripheral() {
        print("doDisconnectPeripheral")

        PBCommon.gPeripheral?.cancelConnection()
        
        updateUIState()
    }

    //MARK: - SettingsViewDelegate
    func onSettingsClosed(_ a_Param: [AnyHashable : Any]) {
        
        if let aRet = UserDefaults.standard.object(forKey: "bFWUpMode") as? String {
            if aRet == "1" {
                bFWUpMode = true
                
                stopStatusTimer()
                gPBSR.pauseTranscribing()
                
                if (PBCommon.gPeripheral?.isConnected ?? false) {
                    doDisconnectPeripheral()
                } else {
                    onPopBLEScan()
                }
                
                //TEST ->
//                initDialogProgress(Msg: NSLocalizedString("FWUpgrade", comment: "FWUpgrade"), Title: NSLocalizedString("FWUpWarning", comment: "FWUpWarning"))
//                doFWUpgrade()
                //TEST
                
                return ;
            }
        }
        
        if let aRet = UserDefaults.standard.object(forKey: "program_last_time") as? String {
            if aRet.count > 0 {
                PBCommon.nProgramLastTime = Int(aRet)!
            }
        }
        
        if let aLanguage = UserDefaults.standard.object(forKey: "vcmd_language") as? String {
            if aLanguage != PBCommon.curSpeechLanguage {
                PBLib.showAlert("Changing voice command language is in effect after restarting app.")
            }
            
            if aLanguage == VOICE_REC_LANG_KOREAN {
                PBCommon.curSpeechLanguage = VOICE_REC_LANG_KOREAN
            
            } else if aLanguage == VOICE_REC_LANG_FRENCH {
                PBCommon.curSpeechLanguage = VOICE_REC_LANG_FRENCH
                
            } else {
                PBCommon.curSpeechLanguage = VOICE_REC_LANG_ENGLISH
            }
        }
        
        if let aVCmdEnable = UserDefaults.standard.object(forKey: "vcmd_enable") as? String {
            if aVCmdEnable != PBCommon.sUseVCmd {
                PBCommon.sUseVCmd = aVCmdEnable
                PBLib.showAlert(NSLocalizedString("VoiceCmdEnableRestart", comment: "Voice Command Enable Needs Restart"))
            }
        }
        
//        gPBSR.initSpeechRecognition(PBCommon.curSpeechLanguage)
    }
    
    var nFWUpMsec : Float = 0
    let FW_INTV_MSEC : Float = 0.7
    
    var sCmdFWLast : UInt8 = 0x00
    var sCmdFWRecv : UInt8 = PBCommon.sCmdFWReadySend_Idle
    var nSendDataIdx : UInt8 = 0
    
    var nFWDataSize : UInt16 = 0
    var nFWSendCnt : UInt8 = 0
    var byteFWSend = [UInt8]()

    var nFWBlockSendIdx : Int = 0

    func showFWUpMessage(_ a_Msg : String) {
        print("showFWUpMessage:\(a_Msg)")
        
        let aMin : Int32 = Int32(nFWUpMsec) / 60
        let aSec : Int32 = Int32(nFWUpMsec) % 60

        showDialogProgress(Msg: NSLocalizedString("FWUpgrade", comment: "FW Upgrade"),
            Title: NSLocalizedString("Elapsed", comment: "Elapsed") + ":" + String(format: "%02d:%02d", aMin, aSec)
                + "\n" + NSLocalizedString("FWUpWarning", comment: "FW Up Warning"))
    }
    
    func stringToBytes(_ string: String) -> [UInt8]? {
        let length = string.characters.count
        if length & 1 != 0 {
            return nil
        }
        var bytes = [UInt8]()
        bytes.reserveCapacity(length/2)
        var index = string.startIndex
        for _ in 0..<length/2 {
            let nextIndex = string.index(index, offsetBy: 2)
            if let b = UInt8(string[index..<nextIndex], radix: 16) {
                bytes.append(b)
            } else {
                return nil
            }
            index = nextIndex
        }
        return bytes
    }
    
    func LoadingFWData() {
        showFWUpMessage("Loading FW Data.")
        nFWDataSize = 0
        
//        if let path = Bundle.main.path(forResource: "BT-150-R02", ofType: "hex") {
        if let path = Bundle.main.path(forResource: "FW-150-Hospital-R2.00_220302", ofType: "hex") {
            do {
                let data = try String(contentsOfFile: path, encoding: .utf8)
                let myStrings = data.components(separatedBy: .newlines)
//                let aString = myStrings.joined(separator: ", ")
                for aString in myStrings {
                    let aLine = aString.substring(with: 1..<aString.count-2)
                    let bytes = stringToBytes(aLine)
                    nFWDataSize += UInt16(bytes![0])
//                    var aPrint = ""
//                    for aByte in bytes! {
//                        aPrint += " \( String(format: "%02x", aByte) )"
//                    }
//                    print("line:\(aLine)")
//                    print("print:\(aPrint)")
                }

                nFWSendCnt = UInt8(nFWDataSize/UInt16(PBCommon.FW_BLOCK_SIZE) + 1)
                print("nFWDataSize:\(nFWDataSize) nFWSendCnt:\(nFWSendCnt)")
                
                byteFWSend.removeAll()
                var aCopyIdx = 0
                for aString in myStrings {
                    let aLine = aString.substring(with: 9..<aString.count-2)
                    let bytes = stringToBytes(aLine)
                    for aByte in bytes! {
                        byteFWSend.append(aByte)
                        aCopyIdx += 1
                    }
                }
                print("byteFWSend size: \(byteFWSend.count)")
//                var aPrint = ""
//                for aByte in byteFWSend {
//                    aPrint += " \( String(format: "%02x", aByte) )"
//                }
//                print(aPrint)
                
            } catch {
                print(error)
            }
        }
        
    }
    
    func doFWUpgrade() {

        sCmdFWRecv = PBCommon.sCmdFWReadySend_Ready
        nFWUpMsec = 0
        nProgressStatus = 0
        nSendDataIdx = 0
        
        LoadingFWData()
        
        mTimerFWUp?.invalidate()
        mTimerFWUp = nil

        mTimerFWUp = Timer.scheduledTimer(timeInterval: TimeInterval(FW_INTV_MSEC), target: self, selector: #selector(taskFWUp), userInfo: nil, repeats: true)

    }
    
    @objc func taskFWUp() {
        nFWUpMsec += FW_INTV_MSEC
        
        let aMin : Int32 = Int32(nFWUpMsec) / 60
        let aSec : Int32 = Int32(nFWUpMsec) % 60
        
        if (aMin >= 1 && aSec > 0)
            && (sCmdFWRecv == PBCommon.sCmdFWReadySend_Ready
                    || sCmdFWRecv == PBCommon.sCmdFWReadyRecv_R
                    || sCmdFWRecv == PBCommon.sCmdFWReadyRecv_E
                    || sCmdFWRecv == PBCommon.sCmdFWReadyRecv_D
                    || sCmdFWRecv == PBCommon.sCmdFWReadyRecv_Y) {
            mTimerFWUp?.invalidate()
            mTimerFWUp = nil
            PBLib.showAlert(NSLocalizedString("FWTimeOut", comment: "FW TimeOut"))
            return ;
        }

        if aMin >= 5 && aSec > 0 {
            mTimerFWUp?.invalidate()
            mTimerFWUp = nil
            PBLib.showAlert(NSLocalizedString("FWTimeOut", comment: "FW TimeOut"))
            return
        }
        
        if sCmdFWRecv == PBCommon.sCmdFWReadySend_Ready {
            showFWUpMessage("FW Up Check Authentifiction: Z");
            sendByteBLE(PBCommon.sCmdFWReadySend_Z)
            sCmdFWLast = PBCommon.sCmdFWReadySend_Z
//            nProgressStatus = 1
            showDialogProgress(Msg: "", Title: "")
            
        } else if sCmdFWRecv == PBCommon.sCmdFWReadyRecv_R {
            showFWUpMessage("FW Up Check Authentifiction: X")
            sendByteBLE(PBCommon.sCmdFWReadySend_X)
            sCmdFWLast = PBCommon.sCmdFWReadySend_X
//            nProgressStatus = 2
            showDialogProgress(Msg: "", Title: "")
            
        } else if sCmdFWRecv == PBCommon.sCmdFWReadyRecv_E {
            showFWUpMessage("FW Up Check Authentifiction: V")
            sendByteBLE(PBCommon.sCmdFWReadySend_V)
            sCmdFWLast = PBCommon.sCmdFWReadySend_V
//            nProgressStatus = 3
            showDialogProgress(Msg: "", Title: "")

        } else if sCmdFWRecv == PBCommon.sCmdFWReadyRecv_D {
            showFWUpMessage("FW Up Check Authentifiction: U")
            sendByteBLE(PBCommon.sCmdFWReadySend_U)
            sCmdFWLast = PBCommon.sCmdFWReadySend_U
//            nProgressStatus = 4
            showDialogProgress(Msg: "", Title: "")

        } else if sCmdFWRecv == PBCommon.sCmdFWReadyRecv_Y {
            if sCmdFWLast == PBCommon.sCmdFWReadySend_U {
                Thread.sleep(forTimeInterval: 1)
                sCmdFWLast = PBCommon.sCmdFWEraseDelay

            } else if sCmdFWLast == PBCommon.sCmdFWEraseDelay {
                sendByteBLE(PBCommon.sCmdFWErase)
                sendByteBLE(PBCommon.FW_BLOCK_SIZE)
                sendByteBLE(nFWSendCnt)
                sCmdFWLast = PBCommon.sCmdFWErase
                sCmdFWRecv = PBCommon.sCmdFWReadySend_Idle
                showFWUpMessage("Erasing FW flash : Block Cnt: \(nFWSendCnt)")
            }
//            nProgressStatus = 5
            showDialogProgress(Msg: "", Title: "")

        } else if sCmdFWRecv == PBCommon.sCmdFWReadySend_Idle {

        } else if sCmdFWRecv == PBCommon.sCmdFWReadyRecv_ERR {
            showFWUpMessage("FW Up Unknown Error.");

        } else if sCmdFWRecv == PBCommon.sCmdFWReadyRecv_CR {
            
            if sCmdFWLast == PBCommon.sCmdFWErase || sCmdFWLast == PBCommon.sCmdFWFlash {
                
                if sCmdFWLast == PBCommon.sCmdFWErase {
                    showFWUpMessage("Erasing finished!!")

//                    nProgressStatus = 6
                    showDialogProgress(Msg: "", Title: "")
                }
                sendByteBLE(PBCommon.sCmdFWAddress)

//                let aBSize = Float(PBCommon.FW_BLOCK_SIZE)
//                let aSndIdx = Float(nSendDataIdx)
//                let aSendAddr = (aBSize * aSndIdx) / 2
                
                let aSendAddr = Float(PBCommon.FW_BLOCK_SIZE) * Float(nSendDataIdx) / 2
                sendShortBLE(UInt16(aSendAddr))

                sCmdFWLast = PBCommon.sCmdFWAddress
                sCmdFWRecv = PBCommon.sCmdFWReadySend_Idle
                showFWUpMessage("Writing Data Address:\(aSendAddr)")
             
            } else if sCmdFWLast == PBCommon.sCmdFWAddress {
                sendByteBLE(PBCommon.sCmdFWBinary)

                let aSendIdx = UInt16( Float(PBCommon.FW_BLOCK_SIZE) * Float(nSendDataIdx))
                var aLen = UInt16(PBCommon.FW_BLOCK_SIZE)
                if aSendIdx + aLen > nFWDataSize {
                    aLen = nFWDataSize - aSendIdx
                }
                sendShortBLE(aLen)

                sendByteBLE(PBCommon.sCmdFWFlash)

                sCmdFWLast = PBCommon.sCmdFWBinary

                showFWUpMessage("Writing Data block No: \(nSendDataIdx)")
                
            } else if sCmdFWLast == PBCommon.sCmdFWBinary {

                let aSendIdx = UInt16( Float(PBCommon.FW_BLOCK_SIZE) * Float(nSendDataIdx))
                var aLen = UInt16(PBCommon.FW_BLOCK_SIZE)
                if aSendIdx + aLen > nFWDataSize {
                    aLen = nFWDataSize - aSendIdx
                }
                
                var aSendBlock = [UInt8]()
                for i in aSendIdx ... aSendIdx+aLen {
                    if i >= byteFWSend.count {
                        break
                    }
                    aSendBlock.append(byteFWSend[Int(i)])
                }
                sendFWDataBlock(aSendBlock, Len: Int(aLen))

                sCmdFWLast = PBCommon.sCmdFWFlash
                sCmdFWRecv = PBCommon.sCmdFWReadySend_Idle
//                        LOGe(TAG, "Write FW Data:" + nSendDataIdx);
                nSendDataIdx += 1

                let aPer = UInt8( (Float(nSendDataIdx) * Float(100)) / Float(nFWSendCnt) )
                if (aPer > nProgressStatus ) {
                    nProgressStatus = aPer
                }
                showDialogProgress(Msg: "", Title: "")
                print("#################################### aPer: \(aPer) nProgressStatus: \(nProgressStatus)")


                if nSendDataIdx >= nFWSendCnt {
                    nProgressStatus = 100
                    showDialogProgress(Msg: "", Title: "")

                    sCmdFWRecv = PBCommon.sCmdFWReadyRecv_CR
                    sCmdFWLast = PBCommon.sCmdFWEnd
                }
                
            } else if sCmdFWLast == PBCommon.sCmdFWEnd {

                if  nSendDataIdx >= (nFWSendCnt+10)  {
                    print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! nSendDataIdx: \(nSendDataIdx) nFWSendCnt: \(nFWSendCnt)")

                    bFWUpMode = false

                    mTimerFWUp?.invalidate()
                    mTimerFWUp = nil
                    
                    alertFW?.dismiss(animated: true, completion: {
                        PBLib.showAlert(NSLocalizedString("FWRestart", comment: "FW Restart"))
                    });

                }
                nSendDataIdx += 1

            }
            
        }

    }
        
    func sendFWDataBlock(_ a_Data : [UInt8], Len a_Len : Int) {
        
        print("sendFWDataBlock: nSendDataIdx: \(nSendDataIdx)")

        sCmdFWRecv = PBCommon.sCmdFWReadySend_Idle
        nFWBlockSendIdx = 0

        mTimerFWDataBlock?.invalidate()
        mTimerFWDataBlock = nil

        mTimerFWDataBlock = Timer.scheduledTimer(timeInterval: TimeInterval(0.01), target: self, selector: #selector(taskFWDataBlock), userInfo: ["len" : a_Len, "data" : a_Data], repeats: true)

    }
    
    @objc func taskFWDataBlock(timer: Timer) {
        
        let dict = timer.userInfo as! NSDictionary

        let aLen = dict["len"] as! Int
        let aData = dict["data"] as! [UInt8]

        if nFWBlockSendIdx+1 < aLen {
            send2BytesBLE(aData[nFWBlockSendIdx], Two: aData[nFWBlockSendIdx+1])
            nFWBlockSendIdx += 2
        } else {
            mTimerFWDataBlock?.invalidate()
            mTimerFWDataBlock = nil
        }
    }
    
    func sendByteBLE(_ a_One : UInt8) {
        print("sendByteBLE: \(String(format: "%02x", a_One))")
        let arrValue: [UInt8] = [a_One]
        PBCommon.gPeripheral?.writeUARTData(Data(arrValue))
    }

    func sendShortBLE(_ a_Data : UInt16) {
        let aOne : UInt8 = UInt8((a_Data >> 8 ) & 0xFF)
        let aTwo : UInt8 = UInt8(a_Data & 0xFF)
        
        print("sendShortBLE: \(String(format: "%04x -> %02x %02x", a_Data, aOne, aTwo))")
        let arrValue: [UInt8] = [aOne, aTwo]
        PBCommon.gPeripheral?.writeUARTData(Data(arrValue))
    }

    func send2BytesBLE(_ a_One : UInt8, Two a_Two : UInt8) {
//        print("send2BytesBLE: \(String(format: "%02x %02x", a_One, a_Two))")
        let arrValue: [UInt8] = [a_One, a_Two]
        PBCommon.gPeripheral?.writeUARTData(Data(arrValue))
    }


}
